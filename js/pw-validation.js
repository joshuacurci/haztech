$(".role-req-client").hide();
$("#role_type_letter").on('change', function() {
    if($(this).val() == "E") {
        $(".role-req-client").show();
        $(".role-req-client").attr("required",true);
    } else if ($(this).val() == "F") {
      $(".role-req-client").show();
      $(".role-req-client").attr("required",true);
    } else {
      $(".role-req-client").hide();
    }
});

$(".pw-change-show").hide();
$(".change_pass").click(function() {
    if($(this).is(":checked")) {
        $(".pw-change-show").show();
    } else {
        $(".pw-change-show").hide();
    }
});

$(document).ready(function() {
  $("#password_change1").onblur(validate);
});

  function validate() {
    var password1 = $("#password1").val();
    var password2 = $("#password2").val();
    if(password1 == password2) {
     $("#validate-status").text("Password Matched!");
     $("#validate-status").addClass("pw-valid");
     $("#validate-status").removeClass("pw-invalid");
   }
   else {
    $("#validate-status").text("Password Mismatch");  
    $("#validate-status").addClass("pw-invalid");
    $("#validate-status").removeClass("pw-valid");
  }
}

$(document).ready(function() {
  $("#password_change1").onblur(validate_change);
});

  function validate_change() {
    var password_change = $("#password_change").val();
    var password_change1 = $("#password_change1").val();


    if(password_change == password_change1) {
     $("#validate-status-change").text("Password Matched!");
     $("#validate-status-change").addClass("pw-valid");
     $("#validate-status-change").removeClass("pw-invalid");
   }
   else {
    $("#validate-status-change").text("Password Mismatch");  
    $("#validate-status-change").addClass("pw-invalid");
    $("#validate-status-change").removeClass("pw-valid");
  }
}
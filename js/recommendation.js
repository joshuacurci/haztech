
$("#control_priority_option, #hazard_type_option, #material_option, #item_condition, #friability, #current_label").on('change',function() {
  var material_option_value = $("#material_option").val();
  var hazard_type_value = $("#hazard_type_option").val();
  var control_priority_value = $("#control_priority_option").val();
  var friability_value = $("#friability").val();
  var current_label_value = $("#current_label").val();
  var item_condition_value = $("#item_condition").val();

  if (hazard_type_value == '1' ) {
    // Asbestos
    if (control_priority_value == 'P1') {
        // Asbestos and P1
        document.getElementById("control_recommendationID").value = "Restrict access and isolate area. Engage a Class A asbestos contractor ASAP to undertake remedial works immediately.";
    } else if (control_priority_value == 'P2') {
        // Control Priority = P3
        if (friability_value == 'Friable') {
              // Asbestos, p2 and friable
              document.getElementById("control_recommendationID").value = "Engage a Class A asbestos contractor to clean up or remove the material as soon as practicable [less than 3 months].";
        } else if (friability_value == 'Non-Friable') {
            // Asbestos, P2 and Non Friable
            document.getElementById("control_recommendationID").value = "Engage a Class A/B asbestos contractor to clean up or remove the material as soon as practicable [less than 3 months].";
        } else if (friability_value == '') {
            // 
            document.getElementById("control_recommendationID").value = "";
            document.getElementById("control_recommendationID").placeholder = "This field will be auto populated based the selected fields above.";
        }
    } else if (control_priority_value == 'P3') {
            // Control Priority = P3
            if (friability_value == 'Friable') {
                if (current_label_value != '') {
                //  Friable and labelled 
                document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions by a Class A asbestos contractor prior to renovations.";
                }
                else if (current_label_value == '') {
                    // p3, Friable, Not labelled 
                    document.getElementById("control_recommendationID").value = "Label & maintain in good condition. Remove under controlled conditions by a Class A asbestos contractor prior to renovations.";
                }
            } else if (friability_value == 'Non-friable') {
                if (current_label_value != '') {
                    // P3, Non-Friable, Labelled
                    document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.";
                } else if (current_label_value == '') {
                    // P3, Non-Friable, Not Labelled
                    document.getElementById("control_recommendationID").value = "Label & maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.";
                }
            }
        } else if (control_priority_value == 'P4') {
            if (current_label_value != '') {
                // Labelled
                document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.";
            } else if (current_label_value == '') {
                // Not Labelled
                document.getElementById("control_recommendationID").value = "Label & maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.";
            }
        } 
    } else if (hazard_type_value == '2' ) {
        // SMF Products
        if (item_condition_value == 'Good' || item_condition_value == 'Fair') {
         document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions prior to renovations.";
         } else if (item_condition_value == 'Bad') {
            document.getElementById("control_recommendationID").value = "Engage contractor to clean up or remove under controlled conditions as soon as practicable [less than 6 months].";
        } else {
            document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions prior to renovations.";
        }
    } else if (hazard_type_value == '3' ) {
      // Lead Products 
      // SMF Products
        if (item_condition_value == 'Good' || item_condition_value == 'Fair') {
         document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.";
         } else if (item_condition_value == 'Bad') {
            document.getElementById("control_recommendationID").value = "Engage appropriate contractor to remove flaking paint and over paint with a lead-free paint.";
        } else {
            document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.";
        }
    } else if (hazard_type_value == '4' ) {
      // Lead Products 
      // SMF Products
        if (item_condition_value == 'Good' || item_condition_value == 'Fair') {
         document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions prior to renovations.";
         } else if (item_condition_value == 'Bad') {
            document.getElementById("control_recommendationID").value = "Engage contractor to clean up or remove under controlled conditions as soon as practicable [less than 6 months].";
        } else {
            document.getElementById("control_recommendationID").value = "Maintain in good condition. Remove under controlled conditions prior to renovations.";
        }
    } else {
      document.getElementById("control_recommendationID").value = "";
      document.getElementById("control_recommendationID").placeholder = "This field will be auto populated based the selected fields above.";
    }

    if (material_option_value == '' ) {
        document.getElementById("submititem").disabled = true; 
    } else {
        document.getElementById("submititem").disabled = false;
    }
});



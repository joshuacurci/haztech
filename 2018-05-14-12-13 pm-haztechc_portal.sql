# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 122.129.218.34 (MySQL 5.6.38)
# Database: haztechc_portal
# Generation Time: 2018-05-14 02:13:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_au_states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_au_states`;

CREATE TABLE `tbl_au_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(3) NOT NULL DEFAULT '',
  `state_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_au_states` WRITE;
/*!40000 ALTER TABLE `tbl_au_states` DISABLE KEYS */;

INSERT INTO `tbl_au_states` (`id`, `state_code`, `state_name`)
VALUES
	(1,'NSW','New South Wales'),
	(2,'QLD','Queensland'),
	(3,'SA','South Australia'),
	(4,'TAS','Tasmania'),
	(5,'VIC','Victoria'),
	(6,'WA','Western Australia'),
	(7,'ACT','Australian Capital Territory'),
	(8,'NT','Northern Territory');

/*!40000 ALTER TABLE `tbl_au_states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_basevalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_basevalues`;

CREATE TABLE `tbl_basevalues` (
  `materialID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `material` varchar(255) DEFAULT NULL,
  `costper` varchar(255) DEFAULT NULL,
  `calloutfee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`materialID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_basevalues` WRITE;
/*!40000 ALTER TABLE `tbl_basevalues` DISABLE KEYS */;

INSERT INTO `tbl_basevalues` (`materialID`, `material`, `costper`, `calloutfee`)
VALUES
	(1,'Arc shields','30','1500'),
	(2,'Bituminous membrane','100','1500'),
	(3,'Corrugated cement sheet','50','1500'),
	(4,'Electrical backing board','120','1500'),
	(5,'Fibre cement sheet','50','1500'),
	(6,'Fire door core','300','1500'),
	(7,'Friction material','60','1500'),
	(8,'Gasket material','60','1500'),
	(9,'Insulation','400','1500'),
	(10,'Lagging','400','1500'),
	(11,'Mastic sealant','200','1500'),
	(12,'Millboard insulation','400','1500'),
	(13,'Screed','300','1500'),
	(14,'Sprayed limpet','500','1500'),
	(15,'Vermiculite','500','1500'),
	(16,'Vinyl sheet','250','1500'),
	(17,'Vinyl floor tiles','150','1500'),
	(18,'Window caulking','200','1500'),
	(19,'Woven material','600','1500'),
	(20,'SMF','30','1500'),
	(21,'Lead Paint/flashing','400','1500'),
	(22,'Ballast/Capacitors','125','1500');

/*!40000 ALTER TABLE `tbl_basevalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(2,'We previously instructed this to be removed as it is irrelevant ','2017-06-28 00:00:00','21','<p>Previoulsy advised not to have a \"news section\" Generally a waste time typing text for a blog &nbsp;when our client decision makers are all in their 40\"s. &nbsp;and the probability of this adding ANY value or creating ANY revenue is miniscule.</p>',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_access`;

CREATE TABLE `tbl_blog_access` (
  `blogaccessID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(5) DEFAULT NULL,
  `type_letter` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`blogaccessID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_access` WRITE;
/*!40000 ALTER TABLE `tbl_blog_access` DISABLE KEYS */;

INSERT INTO `tbl_blog_access` (`blogaccessID`, `blogID`, `type_letter`)
VALUES
	(5,2,'0'),
	(8,1,'B'),
	(9,1,'D');

/*!40000 ALTER TABLE `tbl_blog_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_images`;

CREATE TABLE `tbl_blog_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_blog_images` WRITE;
/*!40000 ALTER TABLE `tbl_blog_images` DISABLE KEYS */;

INSERT INTO `tbl_blog_images` (`imageID`, `blogID`, `full_path`)
VALUES
	(3,2,'risktech-logo_(1).png'),
	(4,13,'lod-search.png'),
	(5,14,'reporting.png'),
	(6,1,'images.png'),
	(7,3,'Image-Coming-Soon-Placeholder.png');

/*!40000 ALTER TABLE `tbl_blog_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_OFF
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_OFF`;

CREATE TABLE `tbl_blog_OFF` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_OFF` WRITE;
/*!40000 ALTER TABLE `tbl_blog_OFF` DISABLE KEYS */;

INSERT INTO `tbl_blog_OFF` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(1,'Sample Blog Post 1','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','Array',NULL),
	(2,'Sample Blog Post 3','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(3,'Sample Blog Post','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(7,'Sample Blog Post 2','2016-08-16 00:00:00','3','<p>This is test 2</p>','',NULL,NULL),
	(8,'Test','2016-10-24 00:00:00','3','<p>Test</p>','',NULL,NULL),
	(14,'Test','2016-10-26 00:00:00','3','<p>test content</p>',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog_OFF` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `site_address`, `contact_name`, `contact_number`, `hide`)
VALUES
	(1,'Cushman & Wakefield','','','','N');

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_images`;

CREATE TABLE `tbl_client_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_client_images` WRITE;
/*!40000 ALTER TABLE `tbl_client_images` DISABLE KEYS */;

INSERT INTO `tbl_client_images` (`imageID`, `clientID`, `full_path`)
VALUES
	(1,1,'image026.png');

/*!40000 ALTER TABLE `tbl_client_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_global
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_global`;

CREATE TABLE `tbl_global` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_global` WRITE;
/*!40000 ALTER TABLE `tbl_global` DISABLE KEYS */;

INSERT INTO `tbl_global` (`id`, `description`, `value`)
VALUES
	(1,'photo_number','');

/*!40000 ALTER TABLE `tbl_global` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_hazard_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_hazard_types`;

CREATE TABLE `tbl_hazard_types` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_hazard_types` WRITE;
/*!40000 ALTER TABLE `tbl_hazard_types` DISABLE KEYS */;

INSERT INTO `tbl_hazard_types` (`typeID`, `type_name`)
VALUES
	(1,'Asbestos'),
	(2,'SMF Products'),
	(3,'Lead Products'),
	(4,'PCBs');

/*!40000 ALTER TABLE `tbl_hazard_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_document`;

CREATE TABLE `tbl_item_document` (
  `docID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`docID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_document` WRITE;
/*!40000 ALTER TABLE `tbl_item_document` DISABLE KEYS */;

INSERT INTO `tbl_item_document` (`docID`, `itemID`, `full_path`)
VALUES
	(46,87,''),
	(47,88,''),
	(48,87,''),
	(49,87,''),
	(50,87,''),
	(51,87,''),
	(52,87,''),
	(53,89,''),
	(54,89,''),
	(55,90,''),
	(56,91,''),
	(57,92,''),
	(58,93,''),
	(59,94,''),
	(60,91,''),
	(61,88,''),
	(62,94,''),
	(63,93,''),
	(64,92,''),
	(65,90,''),
	(66,93,''),
	(67,94,''),
	(68,93,''),
	(69,93,''),
	(70,93,''),
	(71,93,''),
	(72,93,''),
	(73,88,''),
	(74,88,''),
	(75,94,''),
	(76,90,''),
	(77,91,''),
	(78,95,''),
	(79,96,''),
	(80,97,''),
	(81,98,''),
	(82,92,''),
	(83,98,''),
	(84,97,''),
	(85,96,''),
	(86,91,''),
	(87,90,''),
	(88,94,''),
	(89,94,''),
	(90,88,''),
	(91,93,''),
	(92,93,''),
	(93,94,''),
	(94,88,''),
	(95,90,''),
	(96,91,''),
	(97,92,''),
	(98,98,''),
	(99,99,''),
	(100,100,''),
	(101,101,''),
	(102,100,''),
	(103,100,''),
	(104,101,''),
	(105,99,''),
	(106,98,''),
	(107,92,''),
	(108,91,''),
	(109,88,''),
	(110,100,''),
	(111,97,''),
	(112,93,''),
	(113,90,''),
	(114,94,''),
	(115,96,''),
	(116,99,''),
	(117,99,''),
	(118,101,''),
	(119,100,''),
	(120,94,'P1100157.JPG'),
	(121,102,''),
	(122,103,''),
	(123,104,''),
	(124,105,''),
	(125,105,''),
	(126,104,''),
	(127,103,''),
	(128,102,''),
	(129,106,''),
	(130,107,''),
	(131,100,''),
	(132,101,''),
	(133,99,''),
	(134,96,''),
	(135,97,''),
	(136,108,''),
	(137,108,''),
	(138,97,''),
	(139,96,''),
	(140,99,''),
	(141,97,''),
	(142,101,''),
	(143,102,''),
	(144,105,''),
	(145,103,''),
	(146,104,''),
	(147,94,''),
	(148,90,''),
	(149,93,''),
	(150,88,''),
	(151,91,''),
	(152,92,''),
	(153,100,''),
	(154,107,''),
	(155,109,''),
	(156,109,''),
	(157,106,''),
	(158,107,''),
	(159,98,''),
	(160,93,''),
	(161,107,''),
	(162,106,''),
	(163,107,''),
	(164,109,''),
	(165,110,''),
	(166,110,''),
	(167,111,''),
	(168,111,''),
	(169,111,''),
	(170,111,''),
	(171,112,''),
	(172,113,''),
	(173,114,''),
	(174,114,''),
	(175,115,''),
	(176,115,''),
	(177,109,''),
	(178,107,''),
	(179,106,''),
	(180,108,''),
	(181,117,''),
	(182,118,''),
	(183,107,''),
	(184,114,''),
	(185,106,''),
	(186,119,''),
	(187,120,''),
	(188,119,''),
	(189,120,''),
	(190,121,''),
	(191,122,''),
	(192,123,''),
	(193,124,''),
	(194,124,''),
	(195,125,''),
	(196,125,''),
	(197,126,''),
	(198,127,''),
	(199,128,''),
	(200,129,''),
	(201,130,''),
	(202,131,''),
	(203,132,''),
	(204,133,''),
	(205,134,''),
	(206,134,''),
	(207,135,''),
	(208,136,''),
	(209,137,''),
	(210,138,''),
	(211,139,''),
	(212,140,''),
	(213,141,''),
	(214,142,''),
	(215,143,''),
	(216,144,''),
	(217,145,''),
	(218,124,''),
	(219,124,''),
	(220,146,''),
	(221,147,''),
	(222,148,''),
	(223,149,''),
	(224,150,''),
	(225,151,''),
	(226,152,''),
	(227,153,''),
	(228,154,''),
	(229,147,''),
	(230,155,''),
	(231,156,''),
	(232,157,''),
	(233,158,''),
	(234,159,''),
	(235,160,''),
	(236,161,''),
	(237,161,''),
	(238,162,''),
	(239,162,''),
	(240,163,''),
	(241,164,''),
	(242,165,''),
	(243,166,''),
	(244,167,''),
	(245,168,''),
	(246,145,''),
	(247,144,''),
	(248,143,''),
	(249,169,''),
	(250,170,''),
	(251,171,''),
	(252,171,''),
	(253,172,''),
	(254,171,''),
	(255,173,''),
	(256,174,''),
	(257,175,''),
	(258,176,''),
	(259,177,''),
	(260,176,''),
	(261,178,''),
	(262,179,''),
	(263,114,''),
	(264,110,''),
	(265,106,''),
	(266,107,''),
	(267,147,''),
	(268,180,''),
	(269,181,''),
	(270,182,''),
	(271,181,''),
	(272,181,''),
	(273,183,''),
	(274,181,''),
	(275,182,''),
	(276,181,''),
	(277,184,''),
	(278,184,''),
	(279,184,''),
	(280,184,''),
	(281,185,''),
	(282,184,''),
	(283,186,''),
	(284,186,''),
	(285,186,''),
	(286,187,''),
	(287,188,''),
	(288,189,''),
	(289,190,''),
	(290,190,''),
	(291,191,''),
	(292,191,''),
	(293,192,''),
	(294,193,''),
	(295,194,''),
	(296,194,''),
	(297,195,''),
	(298,196,''),
	(299,180,''),
	(300,197,''),
	(301,198,''),
	(302,199,''),
	(303,200,''),
	(304,201,''),
	(305,202,''),
	(306,203,''),
	(307,204,''),
	(308,205,''),
	(309,206,''),
	(310,207,''),
	(311,208,''),
	(312,209,''),
	(313,210,''),
	(314,211,''),
	(315,212,''),
	(316,213,''),
	(317,214,''),
	(318,211,''),
	(319,215,''),
	(320,216,''),
	(321,217,''),
	(322,218,''),
	(323,219,''),
	(324,220,''),
	(325,221,''),
	(326,222,''),
	(327,223,''),
	(328,224,''),
	(329,225,''),
	(330,226,''),
	(331,227,''),
	(332,228,''),
	(333,229,''),
	(334,230,''),
	(335,229,''),
	(336,199,''),
	(337,218,''),
	(338,200,''),
	(339,231,''),
	(340,183,''),
	(341,217,''),
	(342,232,''),
	(343,217,''),
	(344,228,''),
	(345,216,''),
	(346,233,''),
	(347,214,''),
	(348,214,''),
	(349,215,''),
	(350,227,''),
	(351,233,''),
	(352,234,''),
	(353,235,''),
	(354,236,''),
	(355,237,''),
	(356,238,''),
	(357,239,''),
	(358,240,''),
	(359,241,''),
	(360,242,''),
	(361,243,''),
	(362,244,''),
	(363,245,''),
	(364,246,''),
	(365,247,''),
	(366,248,''),
	(367,249,''),
	(368,250,''),
	(369,251,''),
	(370,252,''),
	(371,253,''),
	(372,254,''),
	(373,255,''),
	(374,256,''),
	(375,257,''),
	(376,258,''),
	(377,258,''),
	(378,257,''),
	(379,259,''),
	(380,257,''),
	(381,258,''),
	(382,260,''),
	(383,261,''),
	(384,262,''),
	(385,263,''),
	(386,264,''),
	(387,265,''),
	(388,266,''),
	(389,267,''),
	(390,268,''),
	(391,269,''),
	(392,270,''),
	(393,271,''),
	(394,272,''),
	(395,273,''),
	(396,274,''),
	(397,275,''),
	(398,275,''),
	(399,276,''),
	(400,276,''),
	(401,277,''),
	(402,278,''),
	(403,279,''),
	(404,280,''),
	(405,272,''),
	(406,281,''),
	(407,282,''),
	(408,283,''),
	(409,283,''),
	(410,283,''),
	(411,283,''),
	(412,284,''),
	(413,285,''),
	(414,286,''),
	(415,287,''),
	(416,287,''),
	(417,287,''),
	(418,287,''),
	(419,288,''),
	(420,289,''),
	(421,289,''),
	(422,288,''),
	(423,290,''),
	(424,290,''),
	(425,291,''),
	(426,292,''),
	(427,293,''),
	(428,294,''),
	(429,295,''),
	(430,296,''),
	(431,297,''),
	(432,298,''),
	(433,299,''),
	(434,300,''),
	(435,301,''),
	(436,302,''),
	(437,303,''),
	(438,303,''),
	(439,303,''),
	(440,298,''),
	(441,298,''),
	(442,304,''),
	(443,305,''),
	(444,306,''),
	(445,307,''),
	(446,308,''),
	(447,304,''),
	(448,305,''),
	(449,306,''),
	(450,308,''),
	(451,307,''),
	(452,307,''),
	(453,308,''),
	(454,307,''),
	(455,306,''),
	(456,305,''),
	(457,304,''),
	(458,309,''),
	(459,310,''),
	(460,311,''),
	(461,311,''),
	(462,312,''),
	(463,313,''),
	(464,312,''),
	(465,314,''),
	(466,315,''),
	(467,316,''),
	(468,317,''),
	(469,318,''),
	(470,319,''),
	(471,319,''),
	(472,320,''),
	(473,320,''),
	(474,321,''),
	(475,321,''),
	(476,322,''),
	(477,323,''),
	(478,324,''),
	(479,325,''),
	(480,326,''),
	(481,327,''),
	(482,328,''),
	(483,329,''),
	(484,330,''),
	(485,331,''),
	(486,326,''),
	(487,332,''),
	(488,333,''),
	(489,334,''),
	(490,335,''),
	(491,336,''),
	(492,337,''),
	(493,338,''),
	(494,339,''),
	(495,340,''),
	(496,341,''),
	(497,342,''),
	(498,343,''),
	(499,344,''),
	(500,345,''),
	(501,346,''),
	(502,347,''),
	(503,348,''),
	(504,349,''),
	(505,350,''),
	(506,350,''),
	(507,351,''),
	(508,352,''),
	(509,352,''),
	(510,353,''),
	(511,354,''),
	(512,355,''),
	(513,356,''),
	(514,357,''),
	(515,358,''),
	(516,359,''),
	(517,360,''),
	(518,360,''),
	(519,361,''),
	(520,361,''),
	(521,358,''),
	(522,362,''),
	(523,363,''),
	(524,363,''),
	(525,364,''),
	(526,365,''),
	(527,366,''),
	(528,366,''),
	(529,367,''),
	(530,368,''),
	(531,369,''),
	(532,369,''),
	(533,370,''),
	(534,371,''),
	(535,372,''),
	(536,373,''),
	(537,374,''),
	(538,375,''),
	(539,376,''),
	(540,377,''),
	(541,378,''),
	(542,379,''),
	(543,380,''),
	(544,381,''),
	(545,379,''),
	(546,382,''),
	(547,383,''),
	(548,377,''),
	(549,374,''),
	(550,384,''),
	(551,385,''),
	(552,386,''),
	(553,387,''),
	(554,387,''),
	(555,388,''),
	(556,389,''),
	(557,390,''),
	(558,391,''),
	(559,392,''),
	(560,393,''),
	(561,394,''),
	(562,394,''),
	(563,395,''),
	(564,396,''),
	(565,397,''),
	(566,391,''),
	(567,397,''),
	(568,396,''),
	(569,395,''),
	(570,394,''),
	(571,393,''),
	(572,392,''),
	(573,398,''),
	(574,399,''),
	(575,400,''),
	(576,400,''),
	(577,401,''),
	(578,401,''),
	(579,402,''),
	(580,402,''),
	(581,403,''),
	(582,404,''),
	(583,404,''),
	(584,405,''),
	(585,406,''),
	(586,406,''),
	(587,407,''),
	(588,408,''),
	(589,409,''),
	(590,410,''),
	(591,411,''),
	(592,411,''),
	(593,412,''),
	(594,413,''),
	(595,414,''),
	(596,415,''),
	(597,416,''),
	(598,416,''),
	(599,417,''),
	(600,418,''),
	(601,419,''),
	(602,419,''),
	(603,420,''),
	(604,421,''),
	(605,422,''),
	(606,423,''),
	(607,424,''),
	(608,424,''),
	(609,425,''),
	(610,426,''),
	(611,427,''),
	(612,428,''),
	(613,429,''),
	(614,430,''),
	(615,430,''),
	(616,431,''),
	(617,431,''),
	(618,432,''),
	(619,426,''),
	(620,430,''),
	(621,433,''),
	(622,433,''),
	(623,426,''),
	(624,434,''),
	(625,435,''),
	(626,436,''),
	(627,437,''),
	(628,438,''),
	(629,439,''),
	(630,440,''),
	(631,440,''),
	(632,440,''),
	(633,441,''),
	(634,442,''),
	(635,442,''),
	(636,443,''),
	(637,444,''),
	(638,445,''),
	(639,446,''),
	(640,447,''),
	(641,448,''),
	(642,449,''),
	(643,450,''),
	(644,451,'');

/*!40000 ALTER TABLE `tbl_item_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_images`;

CREATE TABLE `tbl_item_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_images` WRITE;
/*!40000 ALTER TABLE `tbl_item_images` DISABLE KEYS */;

INSERT INTO `tbl_item_images` (`imageID`, `itemID`, `full_path`)
VALUES
	(19,106,'P1030306.JPG'),
	(20,107,'P1030296.JPG'),
	(21,114,'P1030307.JPG'),
	(22,115,'P1030298.JPG'),
	(24,118,'image0261.png'),
	(25,119,'P1030315.JPG'),
	(26,120,'P1030312.JPG'),
	(27,121,'P1030313.JPG'),
	(28,122,'P1030309.JPG'),
	(29,124,'P1030318.JPG'),
	(30,125,'P1030297.JPG'),
	(31,126,'P1030157.JPG'),
	(32,127,'P1030159.JPG'),
	(33,128,'P10301591.JPG'),
	(34,131,'P1030166.JPG'),
	(35,132,'P1030174.JPG'),
	(36,133,'P1030170.JPG'),
	(37,135,'P1030175.JPG'),
	(38,137,'P1030178.JPG'),
	(39,138,'P1030181.JPG'),
	(40,140,'P1030185.JPG'),
	(41,141,'P1030188.JPG'),
	(42,142,'P1030189.JPG'),
	(43,143,'P1030194.JPG'),
	(44,144,'P1030198.JPG'),
	(45,145,'P1030199.JPG'),
	(46,146,'P1030206.JPG'),
	(47,147,'P1030208.JPG'),
	(48,148,'P1030213.JPG'),
	(49,149,'P1030218.JPG'),
	(50,151,'P1030223.JPG'),
	(51,153,'P1030226.JPG'),
	(52,154,'P1030228.JPG'),
	(53,155,'P1030234.JPG'),
	(54,156,'P1030235.JPG'),
	(55,157,'P1030236.JPG'),
	(56,158,'P1030279.JPG'),
	(57,159,'P10303151.JPG'),
	(58,161,'P1030249.JPG'),
	(59,162,'P1030257.JPG'),
	(60,163,'P1030253.JPG'),
	(61,164,'P1030259.JPG'),
	(62,167,'P1030265.JPG'),
	(63,168,'P1030266.JPG'),
	(64,169,'P10302651.JPG'),
	(65,170,'P1030281.JPG'),
	(66,171,'P1030272.JPG'),
	(67,172,'P1030273.JPG'),
	(68,174,'P10302061.JPG'),
	(69,175,'P1030280.JPG'),
	(70,176,'P10302811.JPG'),
	(71,177,'P10302841.JPG'),
	(72,178,'P1030285.JPG'),
	(73,179,'P1030320.JPG'),
	(74,181,'P1090094.JPG'),
	(75,182,'P1090098.JPG'),
	(76,184,'IMG_0264.JPG'),
	(77,185,'IMG_0261.JPG'),
	(78,186,'IMG_0262.JPG'),
	(79,189,'IMG_0265.JPG'),
	(80,199,'P1090132.JPG'),
	(81,218,'P1090138.JPG'),
	(82,200,'P1090136.JPG'),
	(83,231,'P10900981.JPG'),
	(84,217,'P1090140.JPG'),
	(85,228,'P1090109.JPG'),
	(86,214,'P1090107.JPG'),
	(87,233,'P1090119.JPG'),
	(88,234,'P1090113.JPG'),
	(89,240,'P1090151.JPG'),
	(90,244,'P1090161.JPG'),
	(91,245,'P1090159.JPG'),
	(92,246,'P1090147.JPG'),
	(93,247,'P1090156.JPG'),
	(94,251,'P1090152.JPG'),
	(95,252,'P1090154.JPG'),
	(96,256,'IMG_0287.JPG'),
	(97,258,'IMG_0302.JPG'),
	(98,257,'IMG_0301.JPG'),
	(99,259,'IMG_0304.JPG'),
	(100,260,'IMG_0300.JPG'),
	(101,261,'IMG_0303.JPG'),
	(102,262,'IMG_0299.JPG'),
	(103,263,'IMG_0307.JPG'),
	(104,264,'IMG_0305.JPG'),
	(105,265,'IMG_0308.JPG'),
	(106,266,'IMG_0195.JPG'),
	(107,267,'IMG_0196.JPG'),
	(108,268,'P1090172.JPG'),
	(109,270,'IMG_01951.JPG'),
	(110,273,'IMG_01961.JPG'),
	(111,275,'P1090180.JPG'),
	(112,276,'P1090182.JPG'),
	(113,277,'P1090179.JPG'),
	(114,278,'P1090178.JPG'),
	(115,283,'IMG_0175.JPG'),
	(116,285,'IMG_0178.JPG'),
	(117,287,'IMG_0181.JPG'),
	(118,288,'P1090352.JPG'),
	(119,289,'P1090362.JPG'),
	(120,290,'P1090361.JPG'),
	(121,295,'IMG_0185.JPG'),
	(122,298,'IMG_0180.JPG'),
	(123,303,'IMG_0194.JPG'),
	(124,307,'IMG_0173.JPG'),
	(125,311,'P1090505.JPG'),
	(126,315,'P1090513.JPG'),
	(127,317,'P1090521.JPG'),
	(128,319,'P1090525.JPG'),
	(129,320,'P1090523.JPG'),
	(130,321,'P1090526.JPG'),
	(131,322,'P1090528.JPG'),
	(132,323,'P1090530.JPG'),
	(133,327,'IMG_0140.JPG'),
	(134,328,'P1090534.JPG'),
	(135,332,'IMG_0141.JPG'),
	(136,336,'P1090519.JPG'),
	(137,337,'IMG_0148.JPG'),
	(138,340,'IMG_0152.JPG'),
	(139,342,'IMG_0153.JPG'),
	(140,348,'IMG_0165.JPG'),
	(141,349,'IMG_0166.JPG'),
	(142,350,'P1090673.JPG'),
	(143,352,'P10906731.JPG'),
	(144,354,'P1090674.JPG'),
	(145,356,'P10906741.JPG'),
	(146,360,'P1090676.JPG'),
	(147,361,'P1090675.JPG'),
	(148,358,'P1090672.JPG'),
	(149,362,'P1090678.JPG'),
	(150,363,'P1090679.JPG'),
	(151,364,'IMG_0199.JPG'),
	(152,366,'P1090683.JPG'),
	(153,367,'IMG_0200.JPG'),
	(154,368,'P1090682.JPG'),
	(155,369,'P1090680.JPG'),
	(156,373,'P1090681.JPG'),
	(157,375,'P1090677.JPG'),
	(158,377,'IMG_0216.JPG'),
	(159,378,'IMG_0220.JPG'),
	(160,379,'IMG_0201.JPG'),
	(161,384,'IMG_0215.JPG'),
	(162,385,'IMG_0217.JPG'),
	(163,386,'IMG_0218.JPG'),
	(164,387,'IMG_02161.JPG'),
	(165,388,'IMG_02201.JPG'),
	(166,394,'P1090697.JPG'),
	(167,397,'P1090700.JPG'),
	(168,398,'P1090703.JPG'),
	(169,399,'P1090709.JPG'),
	(170,400,'P1090701.JPG'),
	(171,401,'P10907031.JPG'),
	(172,402,'P1090704.JPG'),
	(173,405,'P1090705.JPG'),
	(174,406,'P10907051.JPG'),
	(175,416,'P1090733.JPG'),
	(176,419,'P1090720.JPG'),
	(177,422,'P1090719.JPG'),
	(178,423,'P1090724.JPG'),
	(179,427,'P1090770.JPG'),
	(180,428,'P1090764.JPG'),
	(181,429,'P1090759.JPG'),
	(182,430,'P1090773.JPG'),
	(183,431,'P1090756.JPG'),
	(184,432,'P1090755.JPG'),
	(185,433,'P1090776.JPG'),
	(186,440,'P1090908.JPG'),
	(187,442,'P1090911.JPG'),
	(188,447,'P1090937_(Small).JPG'),
	(189,448,'P1090936_(Small).JPG'),
	(190,450,'P1090925_(Small).JPG'),
	(191,451,'P1090927_(Small).JPG');

/*!40000 ALTER TABLE `tbl_item_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_materials`;

CREATE TABLE `tbl_item_materials` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_material` varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_materials` WRITE;
/*!40000 ALTER TABLE `tbl_item_materials` DISABLE KEYS */;

INSERT INTO `tbl_item_materials` (`itemID`, `item_material`, `group`)
VALUES
	(1,'Adhesive','Asbestos'),
	(2,'Arc shields','Asbestos'),
	(3,'Bituminous material','Asbestos'),
	(4,'Bituminous membrane','Asbestos'),
	(5,'Compressed cement sheet','Asbestos'),
	(6,'Construction joint mastic','Asbestos'),
	(7,'Corrugated cement sheet','Asbestos'),
	(8,'Dust','Asbestos'),
	(9,'Electrical backing board','Asbestos'),
	(10,'Fibre cement sheet','Asbestos'),
	(11,'Fire door core','Asbestos'),
	(12,'Friction material','Asbestos'),
	(13,'Friction pads','Asbestos'),
	(14,'Galbestos','Asbestos'),
	(15,'Gasket material','Asbestos'),
	(16,'Insulation','Asbestos'),
	(17,'Lagging','Asbestos'),
	(18,'Low density fibre board','Asbestos'),
	(19,'Mastic sealant','Asbestos'),
	(20,'Millboard insulation','Asbestos'),
	(21,'Moulded fibre cement','Asbestos'),
	(22,'Packing','Asbestos'),
	(23,'Pointing','Asbestos'),
	(24,'Screed','Asbestos'),
	(25,'Sheet vinyl','Asbestos'),
	(26,'Sheet vinyl & adhesive','Asbestos'),
	(27,'Sheet vinyl - fibrous backed','Asbestos'),
	(28,'Sheet vinyl - Hessian backed','Asbestos'),
	(29,'Sprayed vermiculite','Asbestos'),
	(30,'Sprayed limpet','Asbestos'),
	(31,'Textured coatings','Asbestos'),
	(32,'Vermiculite','Asbestos'),
	(33,'Vinyl floor tiles','Asbestos'),
	(34,'Vinyl floor tiles & adhesives','Asbestos'),
	(35,'Window caulking','Asbestos'),
	(36,'Woven material','Asbestos');

/*!40000 ALTER TABLE `tbl_item_materials` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `buildingName` varchar(255) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `item_no` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `room_specific` varchar(255) DEFAULT NULL,
  `materialID` int(11) DEFAULT NULL,
  `materialOther` varchar(255) DEFAULT NULL,
  `hazard_type` int(11) DEFAULT NULL,
  `samples_taken` varchar(225) DEFAULT NULL,
  `sample_no` varchar(255) DEFAULT NULL,
  `sample_status` varchar(255) DEFAULT NULL,
  `extent` varchar(255) DEFAULT NULL,
  `extent_mesurement` varchar(10) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_number` mediumint(25) DEFAULT NULL,
  `recommendations` longtext,
  `item_condition` varchar(255) DEFAULT NULL,
  `disturb_potential` varchar(255) DEFAULT NULL,
  `risk_rating` varchar(255) DEFAULT NULL,
  `current_label` varchar(11) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `control_priority` varchar(255) DEFAULT NULL,
  `work_records` varchar(255) DEFAULT NULL,
  `description` longtext,
  `photo_no` varchar(450) NOT NULL,
  `location_level` varchar(255) DEFAULT NULL,
  `friability` varchar(255) DEFAULT NULL,
  `control_recommendation` longtext,
  `hide` char(11) DEFAULT NULL,
  `work_action` longtext,
  `item_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_items` WRITE;
/*!40000 ALTER TABLE `tbl_items` DISABLE KEYS */;

INSERT INTO `tbl_items` (`itemID`, `siteID`, `buildingName`, `clientID`, `item_no`, `item_name`, `room_specific`, `materialID`, `materialOther`, `hazard_type`, `samples_taken`, `sample_no`, `sample_status`, `extent`, `extent_mesurement`, `contact`, `contact_number`, `recommendations`, `item_condition`, `disturb_potential`, `risk_rating`, `current_label`, `date_updated`, `control_priority`, `work_records`, `description`, `photo_no`, `location_level`, `friability`, `control_recommendation`, `hide`, `work_action`, `item_status`)
VALUES
	(87,293,'Inspection Station',1,NULL,NULL,'Kitchen, Under sink ',20,'',1,NULL,'','Suspected Positive','1','cm',NULL,NULL,NULL,'Good','Low','Low','','2018-02-16 11:29:16','P4',NULL,'Hot Water Unit','','Ground Level ','Non-Friable','Label & maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','Y','',''),
	(88,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Kitchen, Under Sink ',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-02-22 17:21:32','P4',NULL,'Hot Water Unit','','Level 1 Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(89,293,'Inspection Station',1,NULL,NULL,'Kitchen, Under Sink',9,'',2,NULL,'','Suspected Positive','1','cm',NULL,NULL,NULL,'Good','','Low','','2018-02-16 11:30:50','P4',NULL,'Hot Water Unit','','Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','Y','',''),
	(90,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Kitchen, Under Sink ',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-02-22 17:19:59','P4',NULL,'Hot Water Unit','','Ground Level Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(91,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Cleaners Store',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-02-22 17:22:01','P4',NULL,'Hot Water Unit','','Ground Level Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(92,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Office Roof',0,'Insulation Material',2,NULL,'','Suspected Positive','40','m',NULL,NULL,NULL,'Good','Low','Low','','2018-02-22 17:22:23','P4',NULL,'Rigid Ductwork','','Ground Level Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(93,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Throughout',0,'Sarking Insulation',2,NULL,'','Suspected Positive','1200','m²',NULL,NULL,NULL,'Good','Low','Low','','2018-02-23 11:43:24','P4',NULL,'Roof Lining','','Ground Level Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(94,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Eastern Side ',0,'Moulded Fibre Cement',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-02-22 17:19:38','P4',NULL,'Pit','','Ground Level Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(95,293,'Inspection Station',1,NULL,NULL,'Perimeter',0,'Expansion Joints',0,NULL,'50300-270-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-20 09:41:17','',NULL,'Mastic Sealant ','','Ground Level Exterior','','','Y','',''),
	(96,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Perimeter ',0,'Mastic Sealant',1,NULL,'Prensa 50300-270-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:16:16','',NULL,'Expansion Joints','','Ground Level Exterior','','','N','',''),
	(97,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Eastern Side',0,'Moulded Fibre Cement',1,NULL,'Prensa 50300-270-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:16:53','',NULL,'Pit','','Ground Level Exterior','','','N','',''),
	(98,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Northern Store, East',0,'Wall Lining',1,NULL,'Prensa 50300-270-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-23 11:22:36','',NULL,'Fibre Cement Sheeting','','Ground Level Interior','','','N','',''),
	(99,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Northern Store, Framework',0,'Lead Dust',3,NULL,'Prensa 50300-270-LD001','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:16:36','',NULL,'Settled Dust','','Ground Level Interior','','','N','',''),
	(100,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Vehicle Inspection Area, Beams',0,'Lead Dust',3,NULL,'Prensa 50300-270-LD002','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:25:05','',NULL,'Settled Dust','','Ground Level Interior','','','N','',''),
	(101,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Vehicle Inspection Area, Framework',0,'Lead Dust',3,NULL,'Prensa 50300-270-LD0003','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:17:25','',NULL,'Settled Dust','','Ground Level Interior','','','N','',''),
	(102,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Throughout',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:18:03','',NULL,'Fluorescent Light Fitting','','Ground Level Interior','','','N','',''),
	(103,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Throughout',0,'Capacitor ',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:18:55','',NULL,'Fluorescent Light Fittings','','Level 1 Interior','','','N','',''),
	(104,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Throughout ',0,'Paint Systems ',3,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:19:20','',NULL,'Walls and Ceilings','','Ground Level Interior','','','N','',''),
	(105,293,'Heavy Vehicle Inspection Station',1,NULL,NULL,'Throughout',0,'Paint System',3,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-02-22 17:18:33','',NULL,'Walls and Ceilings','','Level 1 Interior','','','N','',''),
	(106,296,'Administration Building',1,NULL,NULL,'North side',5,'',1,NULL,'A4','Positive','30','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-03-08 11:14:36','P4',NULL,'Eaves','19','East Wing - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(107,296,'Administration Building',1,NULL,NULL,'East and South sides',5,'',1,NULL,'Same as A4','Assumed Positive','50','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-03-08 11:14:53','P4',NULL,'Eaves','20','East Wing - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(108,296,'Administration Building',1,NULL,NULL,'East Side',0,'Moulded Fibre Cement',1,NULL,'Prensa-50300-287-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-27 10:21:02','',NULL,'PMG Pit','','Ground Level - Exterior','','','N','',''),
	(109,296,'Administration Building',1,NULL,NULL,'East - South East corner',0,'Moulded Fibre Cement',1,NULL,'A3','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-27 10:20:14','',NULL,'Electrical Pit','','East Wing - Exterior','','','N','',''),
	(110,296,'Administration Building',1,NULL,NULL,'Footpath',0,'Bituminous material',1,NULL,'Prensa-50300-287-002','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','No','2018-03-08 11:13:33','P4',NULL,'Expansion Gap material','','East Wing - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(111,296,'Administration Building',1,NULL,NULL,'East side',5,'',1,NULL,'Prensa 50300-287-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-27 10:08:34','',NULL,'Fascia','','South & North Wing - Exterior','','','N','',''),
	(112,296,'Administration Building',1,NULL,NULL,'Kitchen',17,'',1,NULL,'Prensa 50300-287-004','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-27 10:09:37','',NULL,'Grey tiles','','East Wing - Demountable','','','N','',''),
	(113,296,'Administration Building',1,NULL,NULL,'South side, entrance door to roofspace',5,'',1,NULL,'Prensa 50300-287-006','Negative','','m',NULL,NULL,NULL,'','','','','2018-02-27 10:11:15','',NULL,'Infill panels','','East Wing - Interior','','','N','',''),
	(114,296,'Administration Building',1,NULL,NULL,'Server Room beneath server',17,'',1,NULL,'A5','Positive','1','m',NULL,NULL,NULL,'Good','Low','Low','No','2018-03-08 11:12:31','P4',NULL,'Olive green colour','21','Ground Floor - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(115,296,'Administration Building',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Fair','','Low','','2018-02-27 15:19:52','',NULL,'Ductwork/flexi ductwork','22','East Wing - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(118,296,'Administration Building',1,NULL,NULL,'Sample',3,'',3,NULL,'ethe46','Suspected Positive','5356','m',NULL,NULL,NULL,'Poor','Low','Medium','fdgjdfj','2018-03-07 16:03:10','P1',NULL,'sdhweth','24','Sample Test','Non-Friable','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','Y','<p>Sample</p>','Rectified'),
	(119,296,'Administration Building',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Assumed Positive','2','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-05 14:03:46','',NULL,'Zip boil above sink and hot water heater below sink','25','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(120,296,'Administration Building',1,NULL,NULL,'Cleaners Store',20,'',2,NULL,'','Assumed Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-05 14:04:18','',NULL,'Hot water heater','26','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(121,296,'Administration Building',1,NULL,NULL,'Cleaners Store',17,'',1,NULL,'A6','Negative','5','m²',NULL,NULL,NULL,'Fair','Low','Low','No','2018-03-07 16:03:20','P4',NULL,'Speckled grey colour','27','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(122,296,'Administration Building',1,NULL,NULL,'Throughout',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Good','','','','2018-03-05 14:07:06','',NULL,'Compressed ceiling tiles','28','South Wing - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(123,296,'Administration Building',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-05 14:08:27','',NULL,'Zip hydratap below sink','','South Wing - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(124,296,'Administration Building',1,NULL,NULL,'South Wing, south wall',21,'',3,NULL,'Pb7','Positive','5','m²',NULL,NULL,NULL,'Fair','','','','2018-03-06 08:53:33','',NULL,'Flaking cream paint','29','Ground Level - Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(125,296,'Administration Building',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Fair','','','','2018-03-05 15:42:03','',NULL,'Roof insulation batts','30','East Wing - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(126,296,'Fleet Workshop',1,NULL,NULL,'All sides',5,'',1,NULL,'ESP E30215','Positive','50','m²',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-03-07 16:03:25','P4',NULL,'Eaves','31','Ground Level - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(127,296,'Fleet Workshop',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Good','','','','2018-03-05 16:03:49','',NULL,'Air conditioning ducting','32','Roof Space - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(128,296,'Fleet Workshop',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Good','','','','2018-03-05 16:05:06','',NULL,'Batts on top of ceiling','33','Roofspace - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(129,296,'Fleet Warehouse',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:23:48','',NULL,'','','Throughout','','','N','',''),
	(130,296,'Welders Workshop',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:24:22','',NULL,'','','Throughout','','','N','',''),
	(131,296,'Fleet Storage Sheds',1,NULL,NULL,'Balcony',5,'',1,NULL,'ESP E30216','Positive','20','m²',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-03-07 16:03:29','P4',NULL,'Eaves','34','North Side - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(132,296,'Fleet Storage Sheds',1,NULL,NULL,'Adjacent fenceline',0,'Fibre cement',1,NULL,'','Suspected Positive','10','m²',NULL,NULL,NULL,'Poor','Low','Medium','','2018-03-07 16:03:33','P2',NULL,'Debris','35','South Side - Exterior','Non-Friable','Engage a Class A/B asbestos contractor to clean up or remove the material as soon as practicable [less than 3-6 months].','N','',''),
	(133,296,'Fleet Storage Sheds',1,NULL,NULL,'Each storage shed',22,'',4,NULL,'','Suspected Positive','8','unit(s)',NULL,NULL,NULL,'','','','','2018-03-05 16:36:56','',NULL,'Single tubed fluorescent lights','36','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(134,296,'ITSM Storage Shed',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:38:00','',NULL,'','','Throughout - Interior','','','N','',''),
	(135,296,'ITSM Storage Shed',1,NULL,NULL,'Adjacent South boundary fence',0,'Moulded Fibre Cement',1,NULL,'A1','Positive','1','unit(s)',NULL,NULL,NULL,'Fair','Low','Low','No','2018-03-07 16:03:36','P4',NULL,'Electrical Pit','37','Exterior, South East','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(136,296,'Covered Shed opposite ITSM Design',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:41:01','',NULL,'','','Throughout','','','N','',''),
	(137,296,'Covered Shed opposite ITSM Design',1,NULL,NULL,'',21,'',3,NULL,'Pb2','Negative','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:42:16','',NULL,'Green paint on shipping containers','38','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(138,296,'Washing Bay Shed',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:43:51','',NULL,'','39','Throughout','','','N','',''),
	(139,296,'ITSM Warehouse',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-05 16:45:28','',NULL,'','','Throughout','','','N','',''),
	(140,296,'ITSM Amenities',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-05 16:47:06','',NULL,'Zip boil below sink','40','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(141,296,'ITSM Amenities',1,NULL,NULL,'Mens Toilets',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-05 16:48:04','',NULL,'Hot water heater','41','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(142,296,'ITSM Factory',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','450','m²',NULL,NULL,NULL,'Good','','','','2018-03-05 16:51:04','',NULL,'Sarking insulation beneath roof','42','Throughout - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(143,296,'ITSM Design',1,NULL,NULL,'South West corner, under signs',0,'Moulded Fibre Cement',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Fair','','Low','No','2018-03-06 13:02:47','P4',NULL,'Electrical Pit','43','Exterior','','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(144,296,'ITSM Design',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:02:26','',NULL,'Billi hot water heater under sink','44','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(145,296,'ITSM Design',1,NULL,NULL,'South entrance area',20,'',2,NULL,'','Suspected Positive','84','m',NULL,NULL,NULL,'Good','','','','2018-03-06 13:02:05','',NULL,'Compressed ceiling tiles','45','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(146,296,'DAS Sheds',1,NULL,NULL,'North West, Adjacent entrance awning',0,'Moulded Fibre Cement',1,NULL,'A2','Positive','1','unit(s)',NULL,NULL,NULL,'Fair','Medium','Low','No','2018-03-07 16:03:54','P3',NULL,'Remants of large diameter pipe in ground','46','Ground Level - Exterior','Non-Friable','','Y','',''),
	(147,296,'Amenities Building',1,NULL,NULL,'All sides',5,'',1,NULL,'Same as ESP E30198','Positive','70','m²',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-03-08 11:15:33','P4',NULL,'Eaves','47','Ground Level - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(148,296,'Amenities Building',1,NULL,NULL,'North side',21,'',3,NULL,'Pb3','Negative','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:19:55','',NULL,'Flaking white paint','48','Ground Level - Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(149,296,'Amenities Building',1,NULL,NULL,'South, South West',0,'Moulded Fibre Cement',1,NULL,'Same as Prensa 50300-287-008','Positive','3','unit(s)',NULL,NULL,NULL,'Fair','Low','Low','No','2018-03-07 16:03:40','P4',NULL,'Electrical pits in ground (1x adjacent water tanks, 2x on path)','49','Ground Level - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(150,296,'Electrical Unit',1,NULL,NULL,'',0,'No asbestos materials identified/suspected',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:25:02','',NULL,'','','Throughout - Interior','','','N','',''),
	(151,296,'Electrical Unit',1,NULL,NULL,'Throughout',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Good','','','','2018-03-06 11:26:16','',NULL,'Sarking insulation beneath roof','50','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(152,296,'Electrical Unit',1,NULL,NULL,'Above offices',20,'',2,NULL,'','Suspected Positive','30','m²',NULL,NULL,NULL,'Good','','','','2018-03-06 11:27:18','',NULL,'Batt insulation on top of ceiling','','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(153,296,'Electrical Unit',1,NULL,NULL,'Adjacent storage - white storage container with lights on east side',21,'',3,NULL,'Pb4','Negative','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:30:17','',NULL,'Flaking white paint','51','Ground Level - Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(154,296,'Amenities & TES Storage Sheds',1,NULL,NULL,'All sides',5,'',1,NULL,'Same as ESP E30182','Positive','100','m²',NULL,NULL,NULL,'Fair','Low','Low','Yes','2018-03-07 16:03:43','P4',NULL,'Eaves (thin strips)','52','Ground Level - Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(155,296,'Amenities & TES Storage Sheds',1,NULL,NULL,'North side',21,'',3,NULL,'Pb5','Negative','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:35:36','',NULL,'Flaking brown paint on gutters, downpipes','53','Ground Level - Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(156,296,'Amenities & TES Storage Sheds',1,NULL,NULL,'Eastern rooms',22,'',4,NULL,'','Suspected Positive','8','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 11:38:02','',NULL,'Double-tubed fluorescent lights hanging from roof','54','Ground Level - Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(157,296,'Amenities & TES Storage Sheds',1,NULL,NULL,'Central area around traffic lights',20,'',2,NULL,'','Suspected Positive','60','m²',NULL,NULL,NULL,'Good','','','','2018-03-06 11:39:20','',NULL,'Compressed ceiling tiles','55','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(158,296,'Amenities & TES Storage Sheds',1,NULL,NULL,'Western rooms (IT Store)',17,'',1,NULL,'ESP E30185','Positive','60','m²',NULL,NULL,NULL,'Fair','Low','Low','Yes','2018-03-07 16:03:47','P4',NULL,'Brown 9\" floor tiles','56','Ground Level - Interior','','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(159,296,'SRS Conference Room',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 11:44:32','',NULL,'Zip boil above sink','57','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(160,296,'Traffic Equipment Standards Demountable',1,NULL,NULL,'Throughout',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:47:37','',NULL,'','','Interior & Exterior','','','N','',''),
	(161,296,'Electronic Unit Demountables',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 11:49:07','',NULL,'Zip boil above sink and hot water heater below sink','58','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(162,296,'Electronic Unit Demountables',1,NULL,NULL,'South Boundary, adjacent light pole',0,'Moulded Fibre Cement',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','No','2018-03-06 11:53:04','P4',NULL,'Electrical pit','59','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(163,296,'Electronic Unit Demountables',1,NULL,NULL,'South Boundary, adjacent light pole',0,'Moulded Fibre Cement',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Poor','Medium','Medium','No','2018-03-07 16:03:58','P2',NULL,'Electrical Pit - damaged','60','Exterior','Non-Friable','Engage a Class A/B asbestos contractor to clean up or remove the material as soon as practicable [less than 3 months]. This includes associated debris on adjacent slope.','N','',''),
	(164,296,'Electronic Unit Demountables',1,NULL,NULL,'South Boundary',21,'',3,NULL,'Pb6','Negative','2','m²',NULL,NULL,NULL,'','','','','2018-03-06 11:56:34','',NULL,'Flaking red paint on small half container','61','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(165,296,'Gas Yard',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:57:23','',NULL,'','','Throughout','','','N','',''),
	(166,296,'ITSM Project Delivery',1,NULL,NULL,'',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-06 11:58:23','',NULL,'','','Throughout','','','N','',''),
	(167,296,'ITSM Project Delivery',1,NULL,NULL,'',20,'',2,NULL,'','Suspected Positive','500','m²',NULL,NULL,NULL,'Good','','','','2018-03-06 11:59:42','',NULL,'Compressed ceiling tiles','62','Throughout - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(168,296,'ITSM Project Delivery',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 12:01:12','',NULL,'Zip unit below sink','63','Ground Level - Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(169,296,'Fleet Building',1,NULL,NULL,'Throughout',20,'',2,NULL,'','Suspected Positive','500','m²',NULL,NULL,NULL,'Good','','','','2018-03-06 13:05:26','',NULL,'Compressed ceiling tiles','64','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(170,296,'Fleet Building',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:06:25','',NULL,'Zip unit below sink','65','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(171,296,'GIS Building',1,NULL,NULL,'Throughout',22,'',4,NULL,'','Suspected Positive','12','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:11:26','',NULL,'Single, double & triple tubed fluorescent light fittings','66','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(172,296,'GIS Building',1,NULL,NULL,'South West',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:10:59','',NULL,'Hot water heater','67','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(173,296,'GIS Building',1,NULL,NULL,'Small shed to South East',0,'No hazardous materials identified',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-03-06 13:12:17','',NULL,'','','Exterior','','','N','',''),
	(174,296,'DAS Building',1,NULL,NULL,'North West, Adjacent entrance',0,'Moulded Fibre Cement',1,NULL,'A2','Positive','1','unit(s)',NULL,NULL,NULL,'Fair','Medium','Low','No','2018-03-07 16:04:01','P3',NULL,'Remants of large diameter pipe in ground','68','Exterior','Non-Friable','','N','',''),
	(175,296,'DAS Building',1,NULL,NULL,'Throughout',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Good','','','','2018-03-06 13:18:19','',NULL,'Compressed ceiling tiles','69','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(176,296,'DAS Building',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:21:47','',NULL,'Hot water unit under sink','70','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(177,296,'DAS Demountable',1,NULL,NULL,'Kitchen',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','','','2018-03-06 13:21:14','',NULL,'Zip hydratap below sink','71','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(178,296,'DAS Storage Shed',1,NULL,NULL,'Throughout',20,'',2,NULL,'','Suspected Positive','throughout','m',NULL,NULL,NULL,'Fair','','','','2018-03-06 13:22:51','',NULL,'Sarking insulation beneath roof','72','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(179,296,'Carpark area',1,NULL,NULL,'East of entry gate',0,'Moulded Fibre Cement',1,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low','Low','No','2018-03-07 16:03:50','P4',NULL,'Electrical pits in ground at base of light poles','73','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(180,297,'Amenities Block ',1,NULL,NULL,'Locker Room and Lunch Room',17,'',1,NULL,'Singleton-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 09:50:16','',NULL,'Floor Covering ','','Interior','','','N','',''),
	(181,297,'Amenities Block ',1,NULL,NULL,'Surrounding ',5,'',1,NULL,'Singleton-02','Positive','20','m²',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-19 15:47:07','P4',NULL,'Eaves ','74','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(182,297,'Amenities Block ',1,NULL,NULL,'Male Toilets',0,'Internal Insulation',2,NULL,'','Suspected Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 13:06:42','',NULL,'Hot Water Unit','75','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','Y','',''),
	(183,297,'Amenities Block ',1,NULL,NULL,'Male Toilet',21,'',3,NULL,'Singleton-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 13:09:25','',NULL,'Doors - Blue Paint System','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(184,301,'Office/Checking Station',1,NULL,NULL,'Office Area-Throughout',20,'',2,NULL,'','Suspected Positive','120','m',NULL,NULL,NULL,'Good','Low','Low','','2018-04-20 11:00:01','',NULL,'Sarking Insulation','76','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(185,301,'Office/Checking Station',1,NULL,NULL,'Kitchen - Below Sink',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-20 10:58:47','',NULL,'Hot Water Unit','77','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(186,301,'Office/Checking Station',1,NULL,NULL,'Exterior - South eastern Side',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-20 11:05:35','',NULL,'Hot Water Unit','78','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(187,301,'Office/Checking Station',1,NULL,NULL,'Checking Station Various Expansion Joints',0,'Bituminous Material',1,NULL,'50300-156-002','Negative','30','m',NULL,NULL,NULL,'','','','','2018-04-20 11:13:59','',NULL,'Expansion Joints','','Ground Level','','','N','',''),
	(188,301,'Office/Checking Station',1,NULL,NULL,'Office',4,'',1,NULL,'','Suspected Negative','3','unit(s)',NULL,NULL,NULL,'','','','','2018-04-20 11:17:30','',NULL,'New Appearance boards','','Ground Level','','','N','',''),
	(189,301,'Office/Checking Station',1,NULL,NULL,'Ceiling Space',20,'',2,NULL,'','Suspected Positive','20','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-20 11:21:01','',NULL,'Flexible A/C Ductwork','79','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(190,301,'Office/Checking Station',1,NULL,NULL,'Ceiling Space',22,'',4,NULL,'','Suspected Negative','15','unit(s)',NULL,NULL,NULL,'','','','','2018-04-20 11:25:55','',NULL,'New Appearance','','Ground Level','','','N','',''),
	(191,301,'Office/Checking Station',1,NULL,NULL,'Checking Station Building Framework',21,'',3,NULL,'','Negative','500','m',NULL,NULL,NULL,'','','','','2018-04-20 11:29:27','',NULL,'Light Tan Colour Systems','','Ground Level','','','N','',''),
	(192,302,'Administration/Ammentities',1,NULL,NULL,'Northern Side',4,'',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-23 09:19:09','P4',NULL,'Bituminous Electrical Backing Board','','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(193,302,'Administration/Ammentities',1,NULL,NULL,'Office, Ammenities & Lobby Areas',16,'',1,NULL,'','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-23 09:26:36','',NULL,'Dark Grey speckled Vinyl','','Interior - Ground Level','','','N','<p>New Vinyl sheeting installed 2016</p>',''),
	(194,302,'Administration/Ammentities',1,NULL,NULL,'Throughout',5,'',1,NULL,'Previously 50300-021-05','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-23 09:31:13','',NULL,'Eaves','','Exterior - Ground Level','','','N','',''),
	(195,302,'Administration/Ammentities',1,NULL,NULL,'Various Throughout',22,'',4,NULL,'','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-23 09:35:25','',NULL,'Capacitors','','Interior - Ground Level','','','N','<p>New Syle Light fittings installed 2016</p>',''),
	(196,302,'Administration/Ammentities',1,NULL,NULL,'Eastern Side',9,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-23 09:40:28','',NULL,'Hot Water Unit','','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','<p>Internal insulation unit to be removed prior to any major renovations or demolition works</p>',''),
	(197,297,'Amenities Block ',1,NULL,NULL,'Throughout ',18,'',1,NULL,'Prensa Report 50300-220-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 09:51:34','',NULL,'Window Frames','','Exterior ','','','N','',''),
	(198,297,'Amenities Block ',1,NULL,NULL,'Throughout ',0,'Light Green Paint ',3,NULL,'Singleton-LP02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 09:53:30','',NULL,'Gutters','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(199,297,'Works Office ',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Singleton-04','Positive','40','m',NULL,NULL,NULL,'Good','','Low','Yes','2018-04-24 13:02:56','',NULL,'Eaves ','80','Exterior ','Non-Friable','','N','',''),
	(200,297,'Works Office ',1,NULL,NULL,'Throughout ',18,'',1,NULL,'Prensa Report 50300-220-001','Positive','20','m',NULL,NULL,NULL,'Good','','Low','','2018-04-24 13:05:20','',NULL,'Window Frames','82','Exterior ','Non-Friable','','N','',''),
	(201,297,'Works Office ',1,NULL,NULL,'Toilet Lobbies ',0,'Blue Vinyl Floor Tiles',0,NULL,'Prensa Report 50300-220-002','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:08:48','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','Y','',''),
	(202,297,'Works Office ',1,NULL,NULL,'Toilet Lobbies ',0,'Blue Vinyl Floor Tiles',1,NULL,'Prensa Report 50300-220-002','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:09:26','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(203,297,'Works Office ',1,NULL,NULL,'Offices',0,'Brown Vinyl Floor Tiles',1,NULL,'ESP Report E38749','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:10:54','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(204,297,'Works Office ',1,NULL,NULL,'Entrance Lobby',0,'Blue Vinyl Floor Tiles',1,NULL,'ESP Report E32293','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:14:07','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(205,297,'Works Office ',1,NULL,NULL,'Meeting Room',0,'Brown Vinyl Floor Tiles',1,NULL,'ESP Report E32293','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:16:41','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(206,297,'Works Office ',1,NULL,NULL,'Fire Place',0,'Brown Vinyl Floor Tiles',1,NULL,'ESP Report E32293','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:17:32','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(207,297,'Works Office ',1,NULL,NULL,'Kitchenette',0,'Brown Vinyl Floor Tiles',1,NULL,'ESP Report E32293','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:18:28','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(208,297,'Works Office ',1,NULL,NULL,'Stores',0,'Brown Vinyl Floor Tiles',1,NULL,'Floor Covering','Assumed Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:19:14','',NULL,'Floor Covering','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(209,297,'Works Office ',1,NULL,NULL,'Female Toilets',5,'',1,NULL,'Singleton-03','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:21:41','',NULL,'Localised Wall Linings','','Interior','','','N','',''),
	(210,297,'Works Office ',1,NULL,NULL,'Meeting Room',0,'Compressed Ceiling Tiles',2,NULL,'','Suspected Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:22:31','',NULL,'Ceiling ','','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(211,297,'Works Office ',1,NULL,NULL,'Rear Section of Office ',0,'Sarking Insulation',2,NULL,'','Suspected Positive','100','m',NULL,NULL,NULL,'Good','','Low','','2018-04-24 13:12:00','P4',NULL,'Roof Lining ','','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','Y','',''),
	(212,297,'Demountable 34',1,NULL,NULL,'Throughout ',0,'Green Vinyl Floor Tiles ',1,NULL,'Prensa Report 50400-220-04','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:31:43','',NULL,'Floor Covering','','Interior','','','N','',''),
	(213,297,'Laboratory',1,NULL,NULL,'Throughout ',17,'',1,NULL,'ESP Report E38751','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 10:36:05','',NULL,'Floor Covering ','','Interior','','This item had been removed prior to 2018 RiskTech inspection','N','',''),
	(214,297,'Workshop',1,NULL,NULL,'Hoist Area',4,'',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-24 13:44:11','P4',NULL,'Electrical Distribution Board','86','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(215,297,'Workshop',1,NULL,NULL,'Office/Kitchenette',0,'Brown Vinyl Floor Tiles',1,NULL,'ESP Report E38749','Assumed Positive','16','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-24 13:46:39','P4',NULL,'Floor Covering','','Interior','Non-Friable','Not sighted during 2018 inspection. May be present beneath black sheet vinyl. Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(216,297,'Workshop',1,NULL,NULL,'Compressor Shed',8,'',1,NULL,'Not Sampled','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-24 13:18:02','P3',NULL,'Compressor ','','Exterior ','Friable','Not sighted during 2018 RiskTech inspection. Maintain in good condition. Remove under controlled conditions by a Class A asbestos contractor prior to renovations.','N','',''),
	(217,297,'Works Office ',1,NULL,NULL,'Ceiling Space',0,'Flexible Ductwork Insulation ',2,NULL,'','Suspected Positive','100','m',NULL,NULL,NULL,'Good','Low','Low','','2018-04-24 13:14:14','',NULL,'Insulation Material ','84','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(218,297,'Works Office ',1,NULL,NULL,'Car Park Garden',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','Low','','2018-04-24 13:04:41','',NULL,'Hot Water Heater ','81','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(219,297,'Works Office ',1,NULL,NULL,'Throughout ',0,'Grey Paint ',3,NULL,'Prensa Sample','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:06:32','',NULL,'Pillars','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(220,297,'Works Office ',1,NULL,NULL,'Throughout ',0,'White Paint System ',3,NULL,'Prensa Sample ','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:12:25','',NULL,'Eaves ','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(221,297,'Works Office ',1,NULL,NULL,'Throughout ',0,'Beige Paint System',3,NULL,'Prensa Sample ','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:13:33','',NULL,'Walls ','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(222,297,'Laboratory',1,NULL,NULL,'Throughout ',0,'White Paint System ',3,NULL,'Prensa Sample ','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:15:47','',NULL,'Walls ','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(223,297,'Demountable 33',1,NULL,NULL,'Throughout ',0,'Yellow Paint System ',3,NULL,'Prensa Sample ','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:26:34','',NULL,'Walls','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(224,297,'Demountable 34',1,NULL,NULL,'Throughout ',0,'Yellow Paint System ',3,NULL,'Prensa Sample ','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:27:45','',NULL,'Walls ','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(225,297,'Amenities Block ',1,NULL,NULL,'Throughout ',0,'Light Blue Paint System',3,NULL,'Prensa Sample','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:28:45','',NULL,'Walls','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(226,297,'Amenities Block ',1,NULL,NULL,'Throughout ',0,'Light Blue Paint System',3,NULL,'Prensa Sample','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:29:28','',NULL,'Door Frames','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(227,297,'Workshop',1,NULL,NULL,'Office Area ',0,'Blue Paint System',3,NULL,'Prensa Sample','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 13:49:41','',NULL,'Walls','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(228,297,'Workshop',1,NULL,NULL,'Throughout ',0,'Capacitor ',4,NULL,'','Suspected Positive','14','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-24 13:15:40','',NULL,'Fluorescent Light Fittings','85','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(229,297,'Amenities Block ',1,NULL,NULL,'Throughout ',0,'Capacitor ',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:45:07','',NULL,'Fluorescent Light Fittings','','Interior','','','N','',''),
	(230,297,'Amenities Block ',1,NULL,NULL,'Throughout ',0,'White Paint System ',3,NULL,'Singleton-LP03','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 12:38:12','',NULL,'Ceilings','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(231,297,'Amenities Block ',1,NULL,NULL,'Male Toilets',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 13:07:43','',NULL,'Hot Water Unit','83','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(232,297,'Works Office ',1,NULL,NULL,'Ceilng Space, Rear Section of Office',0,'Sarking Insulation ',2,NULL,'','Suspected Positive','100','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 13:13:34','',NULL,'Roof Lining ','','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(233,297,'Carpenters Shed',1,NULL,NULL,'Throughout ',0,'Capacitor ',4,NULL,'','Suspected Positive','5','unit(s)',NULL,NULL,NULL,'Good','','Low','','2018-04-24 14:10:39','',NULL,'Fluorescent Light Fittings','87','Interior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(234,297,'Northern Metal Shed',1,NULL,NULL,'Throughout ',0,'Capacitor ',4,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 14:13:06','',NULL,'Fluorescent Light Fittings','88','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(235,303,'Motor Registry ',1,NULL,NULL,'Main Reception Area',0,'Insulation Material',2,NULL,'','Suspected Positive','150','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 14:29:49','',NULL,'Compressed Ceiling Tiles','','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(236,303,'Motor Registry ',1,NULL,NULL,'Safe Room',9,'',1,NULL,'Prensa Report 50300-165-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:31:24','',NULL,'Safe','','Interior','','','N','',''),
	(237,303,'Motor Registry ',1,NULL,NULL,'Safe Room',0,'Green Vinyl Floor Tiles ',0,NULL,'Prensa Report 50300-165-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:33:11','',NULL,'Floor Covering ','','Interior','','','N','',''),
	(238,303,'Motor Registry ',1,NULL,NULL,'Corridor adjacent Toilets',0,'White Vinyl Floor Tiles',1,NULL,'Prensa Report 50300-023-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:34:31','',NULL,'Floor Covering (Beneath Carpet)','','Interior','','','N','',''),
	(239,303,'Motor Registry ',1,NULL,NULL,'Teller Area/Office ',0,'Green Vinyl Floor Tiles ',1,NULL,'Same as Prensa Report 50300-023-002','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:38:49','',NULL,'Floor Covering (Beneath Carpet)','','Interior','','','N','',''),
	(240,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space ',5,'',1,NULL,'Prensa Report 50300-165-004','Positive','20','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-24 14:41:18','P4',NULL,'SMF Fastening Panels','89','Interior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(241,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space',5,'',1,NULL,'Prensa Report 50300-165-005','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:45:06','',NULL,'Debris','','Interior','','Not sighted during 2018 RiskTech inspection. ','N','',''),
	(242,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space Air Conditioning Room',5,'',1,NULL,'ESP E32300','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:46:47','',NULL,'Insulation Panels','','Interior','','Not sighted during 2018 RiskTech inspection. ','N','',''),
	(243,303,'Motor Registry ',1,NULL,NULL,'Staff Kitchen',4,'',1,NULL,'Not Sampled - Newer Appearance','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:47:34','',NULL,'Electrical Distribution Board','','Interior','','','N','',''),
	(244,303,'Motor Registry ',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Prensa Report 50300-165-006','Positive','20','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','No','2018-04-24 14:50:15','P4',NULL,'Roof Tile Undercloaking','90','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(245,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','200','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 14:51:40','',NULL,'Roof Lining ','91','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(246,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 14:53:32','',NULL,'Hot Water Unit','92','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(247,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space',0,'Capacitor ',4,NULL,'','Suspected Positive','10','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 14:55:20','',NULL,'Fluorescent Light Fittings','93','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(248,303,'Motor Registry ',1,NULL,NULL,'Ladies Toilet ',0,'White Paint System ',3,NULL,'Muswellbrook-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:57:43','',NULL,'Door Frames','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(249,303,'Motor Registry ',1,NULL,NULL,'Ladies Toilet ',0,'White Paint System ',3,NULL,'Muswellbrook-LP02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:58:31','',NULL,'Walls','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(250,303,'Motor Registry ',1,NULL,NULL,'Kitchen',0,'Capacitor ',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 14:59:16','',NULL,'Fluorescent Light Fittings','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(251,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','80','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 15:16:48','',NULL,'Flexible Ductwork ','94','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(252,303,'Motor Registry ',1,NULL,NULL,'Ceiling Space Air Conditioning Room',9,'',2,NULL,'','Suspected Positive','20','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 15:17:58','',NULL,'Rigid Ductwork','95','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(253,305,'',1,NULL,NULL,'Interior - ',17,'',1,NULL,'50300-104-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 15:28:30','',NULL,'Floor Covering','','Ground Level','','','N','<p>Black colour vinyl tiles</p>',''),
	(254,305,'',1,NULL,NULL,'Exterior - Sub Floor',5,'',1,NULL,'50300-104-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 15:32:49','',NULL,'Pier Packing','','Ground Level','','','N','',''),
	(255,305,'',1,NULL,NULL,'Various Throughout',22,'',4,NULL,'','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-24 15:37:13','',NULL,'Fluorescent Light Fittings','','Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','<p>New style light fittings.</p>',''),
	(256,305,'',1,NULL,NULL,'Kitchen',2,'',1,NULL,'S - 01','','1','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-24 15:41:55','P4',NULL,'Under Sink Pad','96','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(257,307,'',1,NULL,NULL,'Roof Plant Room',5,'',1,NULL,'S - 01','','30','m',NULL,NULL,NULL,'Fair','','Low','Suspected','2018-04-24 16:56:42','P3',NULL,'Flooring','98','Level 3','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','<p>Uncoated requires encapsulation&nbsp;</p>','<p>Uncoated requires encapsulation&nbsp;</p>'),
	(258,307,'',1,NULL,NULL,'Roof Plant Room',5,'',1,NULL,'S - 02','','2000','m',NULL,NULL,NULL,'Good','','Low','Suspected','2018-04-24 16:57:03','P4',NULL,'Wall Cladding','97','Level 3','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(259,307,'',1,NULL,NULL,'Office areas',20,'',2,NULL,'','Positive','1000','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 16:56:12','',NULL,'Compressed Ceiling Tiles','99','Levels 2 & 3','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(260,307,'',1,NULL,NULL,'Roof Plant Room',20,'',2,NULL,'','Suspected Positive','700','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 16:59:01','',NULL,'Sarking Insulation','100','Level 3','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(261,307,'',1,NULL,NULL,'Kitchens',20,'',2,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 17:01:26','',NULL,'Hot Water Units','101','Levels 2 & 3','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(262,307,'',1,NULL,NULL,'Roof Plant Room',20,'',2,NULL,'','Suspected Positive','150','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 17:05:56','',NULL,'Flexible A/C Ductwork','102','Level 3','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(263,307,'',1,NULL,NULL,'Generator Room',20,'',2,NULL,'','Positive','3','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 17:08:15','',NULL,'Stored Ceiling Tiles','103','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(264,307,'',1,NULL,NULL,'Garage',20,'',2,NULL,'','Suspected Positive','20','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 17:10:31','',NULL,'Flexible A/C Ductwork','104','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(265,307,'',1,NULL,NULL,'Generator Room',20,'',2,NULL,'','Suspected Positive','10','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-24 17:12:39','',NULL,'Exhaust Pipe Insulation','105','Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(266,310,'',1,NULL,NULL,'North East Corner',4,'',1,NULL,'A - 02','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-26 08:33:55','P4',NULL,'Bituminous Electrical Backing Board','106','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(267,310,'',1,NULL,NULL,'Under Sink',9,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 08:37:07','',NULL,'Hot Water Unit','107','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(268,304,'Weigh Station',1,NULL,NULL,'South',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 09:51:01','',NULL,'Hot Water Unit','108','Exterior ','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(269,304,'Weigh Station',1,NULL,NULL,'Throughout ',0,'Cream Paint System',3,NULL,'Kankool-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 09:52:53','',NULL,'Walls ','','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(270,311,'Office A',1,NULL,NULL,'North East Corner',4,'',1,NULL,'A - 02','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-26 09:53:36','P4',NULL,'Bituminous Electrical Backing Board','109','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(271,304,'Weigh Station',1,NULL,NULL,'Throughout',0,'Light Grey Paint System',3,NULL,'Kankool-LP02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 09:53:58','',NULL,'Door Frames ','','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(272,304,'Weigh Station',1,NULL,NULL,'North Section, Main Entrance ',5,'',1,NULL,'Kankool-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:05:55','',NULL,'Eave Linings','','Exterior','','','N','',''),
	(273,311,'Office A',1,NULL,NULL,'Under Sink',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 09:55:58','',NULL,'Hot Water Unit','110','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(274,304,'Weigh Station',1,NULL,NULL,'Main Entrance',5,'',1,NULL,'Same as Kankool-01','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 09:56:42','',NULL,'Fascia','','Exterior','','','N','',''),
	(275,304,'Weigh Station',1,NULL,NULL,'Above Suspended Ceiling Throughout',5,'',1,NULL,'Kankool-02','Positive','45','m²',NULL,NULL,NULL,'Poor','','Medium','Yes','2018-04-26 09:59:59','P3',NULL,'Ceiling ','111','Interior','Non-Friable','Undertake repair/remediation works on damaged sections of fibre cement sheet ceiling. ','N','',''),
	(276,304,'Weigh Station',1,NULL,NULL,'Ceiling Space, On Top of Gyprock Ceiling ',5,'',1,NULL,'Same as Kankool-02','Assumed Positive','10','unit(s)',NULL,NULL,NULL,'Poor','','Medium','Yes','2018-04-26 10:01:31','P2',NULL,'Debris','112','Interior ','Non-Friable','Engage a Class A/B asbestos contractor to clean up or remove the material as soon as practicable [less than 3 months].','N','',''),
	(277,304,'Weigh Station',1,NULL,NULL,'Beneath Roof',9,'',2,NULL,'','Suspected Positive','60','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 10:03:13','',NULL,'Roof Lining','113','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(278,304,'Weigh Station',1,NULL,NULL,'Ceiling Space',0,'Capacitor',4,NULL,'','Suspected Positive','4','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 10:04:10','',NULL,'Fluorescent Light Fitting','114','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(279,311,'Office D',1,NULL,NULL,'Throughout',5,'',1,NULL,'50300-225-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:04:48','',NULL,'Eaves Lining','','Exterior','','','N','',''),
	(280,304,'Weigh Station',1,NULL,NULL,'South Section',5,'',1,NULL,'Kankool-03','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:05:25','',NULL,'Eave Linings','','Exterior ','','','N','',''),
	(281,304,'Weigh Station',1,NULL,NULL,'Surrounding ',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:06:51','',NULL,'Fluorescent Light Fittings','','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(282,304,'Storage Shed',1,NULL,NULL,'Throughout ',0,'No asbestos materials identified ',1,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:08:42','',NULL,'','','Interior & Exterior','','','N','',''),
	(283,311,'Office D',1,NULL,NULL,'South West Corner',0,'Moulded Fibre Cement',1,NULL,'50300-225-001','Positive','2','unit(s)',NULL,NULL,NULL,'Fair','Low','Low','Not Labelle','2018-04-26 10:15:33','P3',NULL,'Telcomunications Pit','115','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(284,311,'Office D',1,NULL,NULL,'Western Side',5,'',0,NULL,'Same As 50300-225-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:18:16','',NULL,'Awning Lining','','Exterior','','','N','',''),
	(285,311,'Office D',1,NULL,NULL,'Kitchen-Above Sink',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 10:22:57','',NULL,'Hot Water Unit','116','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(286,311,'Office D',1,NULL,NULL,'Toilets, Showers & Change Rooms',5,'',1,NULL,'A - 01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:30:17','',NULL,'Wall Lining','','Interior','','','N','',''),
	(287,311,'Office D',1,NULL,NULL,'All Rooms',22,'',4,NULL,'','Suspected Positive','40','unit(s)',NULL,NULL,NULL,'Good','','Low','','2018-04-26 10:38:35','',NULL,'Fluorescent Light Fittings','117','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','<p>No access to light fittings was available during this inspection, further invetigation required before major renovations when light fittings are de-energised.</p>','<p>No access to light fittings was available durin'),
	(288,312,'General Grounds ',1,NULL,NULL,'Southern Side of Site',4,'',1,NULL,'Not Sampled - Live','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 10:43:27','P4',NULL,'Distribution Board ','118','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(289,312,'General Grounds ',1,NULL,NULL,'North Side of Site',4,'',1,NULL,'Not Sampled - Live','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 10:43:09','P4',NULL,'Distribution Board ','119','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(290,312,'Amenities Block',1,NULL,NULL,'Sink Area',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 10:44:51','',NULL,'Hot Water Unit','120','Interior ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(291,312,'Amenities Block',1,NULL,NULL,'Ceiling',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:45:55','',NULL,'Fluorescent Light Fitting','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(292,311,'Office D',1,NULL,NULL,'All Rooms',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:46:19','',NULL,'Door Frames-Blue, Cream & White Paint','','Interior','','','N','',''),
	(293,312,'Amenities Block',1,NULL,NULL,'Entrance ',0,'Green Paint System ',0,NULL,'Narribri-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:46:42','',NULL,'Railing','','Exterior ','','','N','',''),
	(294,312,'Site Office',1,NULL,NULL,'Ceiling',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:49:54','',NULL,'Fluorescent Light Fitting','','Interior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(295,311,'Office B',1,NULL,NULL,'Under Sink',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 10:50:16','',NULL,'Hot Water Unit','121','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(296,312,'Site Office',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:50:17','',NULL,'','','Interior & Exterior','','','N','',''),
	(297,312,'Various Storage Containers ',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:51:12','',NULL,'','','Interior & Exterior','','','N','',''),
	(298,311,'Office D',1,NULL,NULL,'Shower Room',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:04:32','',NULL,'Hot Water Unit','122','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(299,312,'Lunch Room',1,NULL,NULL,'Ceiling',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:54:11','',NULL,'Fluorescent Light Fitting','','Interior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(300,312,'Lunch Room',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:54:37','',NULL,'','','Interior & Exterior','','','N','',''),
	(301,311,'Office E',1,NULL,NULL,'Throughout',0,'None',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:55:22','',NULL,'New Appearance Demountable','','Interior & Exterior','','','N','',''),
	(302,311,'Office F',1,NULL,NULL,'All Areas',0,'None',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 10:56:40','',NULL,'New Appearance Demountable','','Interior & Exterior','','','N','',''),
	(303,311,'Team Leaders Storage',1,NULL,NULL,'Throughout',5,'',1,NULL,'50300-225-006','Positive','12','m²',NULL,NULL,NULL,'Good','','Low','Confirmed','2018-04-26 11:02:03','P4',NULL,'Eaves Lining Too 9 Storage Units','123','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(304,313,'Shed 1',1,NULL,NULL,'South',19,'',2,NULL,'50300-274-001','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:45:49','',NULL,'Roller Shutter - Guides','','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(305,313,'Shed 2',1,NULL,NULL,'South',19,'',2,NULL,'50300-274-001','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:45:18','',NULL,'Roller Shutter - Guides','','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(306,313,'Shed 3',1,NULL,NULL,'South',19,'',2,NULL,'50300-274-001','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:44:40','',NULL,'Roller Shutter - Guides','','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(307,313,'Shed 4',1,NULL,NULL,'South',19,'',2,NULL,'50300-274-001','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:44:10','',NULL,'Roller Shutter - Guides','124','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(308,313,'Sheds 5 & 6',1,NULL,NULL,'South',19,'',2,NULL,'50300-274-001','Negative','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 11:43:54','',NULL,'Roller Shutter - Guides','','Exterior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(309,313,'Sheds 1 - 6',1,NULL,NULL,'Various Throughout',0,'Bituminous Expansion Joint',1,NULL,'50300-274-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 11:53:09','',NULL,'Raised Decks','','Exterior','','','N','',''),
	(310,312,'Various Storage Containers ',1,NULL,NULL,'Throughout',0,'Red Paint System',3,NULL,'Narrabri-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 11:57:23','',NULL,'Walls','','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(311,314,'Motor Registry ',1,NULL,NULL,'Throughout',0,'Capacitor',4,NULL,'','Assumed Positive','14','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 12:05:11','',NULL,'Fluorescent Light Fitting','125','Exterior - Ground Level ','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(312,314,'Motor Registry ',1,NULL,NULL,'Underside of Concrete Slab',0,'White Paint System',3,NULL,'Moree-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:07:29','',NULL,'Soffit ','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(313,314,'Motor Registry ',1,NULL,NULL,'Throughout ',0,'Mastic Sealant',1,NULL,'Moree-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:07:12','',NULL,'Precast Pebble Wall Panels','','Exterior - Ground Level ','','','N','',''),
	(314,314,'Motor Registry ',1,NULL,NULL,'Lift Motor Room',4,'',1,NULL,'Not Sampled - Newer Appearance','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:08:11','',NULL,'Distribution Board ','','Interior - Level 1','','','N','',''),
	(315,314,'Motor Registry ',1,NULL,NULL,'Lift Motor Room',0,'Core Insulation',1,NULL,'Not Sampled - No Access','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 12:09:32','P4',NULL,'Fire Door (Not Tagged)','126','Interior - Level 1','Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(316,314,'Motor Registry ',1,NULL,NULL,'Throughout',5,'',1,NULL,'Moree-02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:10:24','',NULL,'Eave Linings','','Exterior - Level 1','','','N','',''),
	(317,314,'Motor Registry ',1,NULL,NULL,'Motor Registry and Lift Motor Room',9,'',2,NULL,'','Suspected Positive','300','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 12:11:55','',NULL,'Compressed Ceiling Tiles','127','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(318,314,'Motor Registry ',1,NULL,NULL,'Throughout',0,'Green Sheet Vinyl ',1,NULL,'Not Sampled ','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:13:21','',NULL,'Floor Covering','','Interior - Level 1','','','N','',''),
	(319,314,'Motor Registry ',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','225','m',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 12:15:17','',NULL,'Roof Lining ','128','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(320,314,'Motor Registry ',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','60','m',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 12:16:32','',NULL,'Flexible Ductwork ','129','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(321,314,'Motor Registry ',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','40','m',NULL,NULL,NULL,'Good','Low','Low','','2018-04-26 12:17:23','',NULL,'Rigid Ductwork ','130','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(322,314,'Motor Registry ',1,NULL,NULL,'Plant Room',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 12:18:47','',NULL,'Hot Water Unit','131','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(323,314,'Motor Registry ',1,NULL,NULL,'Plant Room',4,'Intern',1,NULL,'Not Sampled - Live','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 12:22:02','P4',NULL,'AHU Distribution Board ','132','Interior - Level 1','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(324,313,'Carpenters Workshop',1,NULL,NULL,'Throughout',5,'',1,NULL,'A - 01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:33:15','',NULL,'Ceiling Lining','','Interior - Ground Level','','','N','',''),
	(325,313,'Carpenters Workshop',1,NULL,NULL,'Throughout',5,'',1,NULL,'Same As - A-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:35:55','',NULL,'Wall Lining','','Interior - Ground Level','','','N','',''),
	(326,313,'Carpenters Workshop',1,NULL,NULL,'Throughout',5,'',1,NULL,'A - 02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:45:30','',NULL,'Eaves Lining','','Exterior - Ground Level','','','N','',''),
	(327,313,'Carpenters Workshop',1,NULL,NULL,'North West Corner',4,'',1,NULL,'','Assumed Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-26 12:40:47','P4',NULL,'Bituminous Electrical Backing Board','133','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(328,314,'Motor Registry ',1,NULL,NULL,'Plant Room',0,'Capacitor',4,NULL,'','Suspected Positive','6','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 12:42:54','',NULL,'Fluorescent Light Fittings','134','Interior - Level 1','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(329,313,'Carpenters Workshop',1,NULL,NULL,'Various Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:43:48','',NULL,'Cream Colour Paint Systems','','Interior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(330,314,'Motor Registry ',1,NULL,NULL,'Storage Room',0,'Capacitor',4,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 12:44:15','',NULL,'Fluorescent Light Fittings','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(331,314,'Motor Registry ',1,NULL,NULL,'Storage Room',0,'White Paint System',3,NULL,'Moree-LP02','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:45:25','',NULL,'Walls ','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(332,313,'Carpenters Workshop',1,NULL,NULL,'South West Corner',0,'Moulded Fibre Cement',1,NULL,'A - 03','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Not Labelle','2018-04-26 12:51:17','P4',NULL,'Telcomunications Pit','135','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(333,313,'Shed 7',1,NULL,NULL,'All Rooms',0,'None',0,NULL,'','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:54:01','',NULL,'No Asbestos detected','','Interior & Exterior','','','N','',''),
	(334,313,'Shed 7',1,NULL,NULL,'Exterior - Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:56:19','',NULL,'Yellow Colour Paint Systems','','Exterior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(335,313,'Shed 8',1,NULL,NULL,'Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 12:58:33','',NULL,'Yellow Colour Paint Systems','','Exterior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(336,314,'Motor Registry ',1,NULL,NULL,'Records Store ',9,'',1,NULL,'Not Sampled - No Access','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 13:01:46','P4',NULL,'Safe','136','Interior - Level 1','Friable','Maintain in good condition. Remove under controlled conditions by a Class A asbestos contractor prior to renovations.','N','',''),
	(337,313,'Sheds 5 & 6',1,NULL,NULL,'Computer Room',20,'',2,NULL,'','Positive','12','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 13:02:17','',NULL,'Wall Insulation','137','Interior - Mezzanine Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(338,314,'Motor Registry ',1,NULL,NULL,'Teller Area',5,'',1,NULL,'Prensa Report 50300-004-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:02:44','',NULL,'Wall Panels Below Windows','','Interior - Level 1','','','N','',''),
	(339,313,'Sheds 5 & 6',1,NULL,NULL,'Building Frame-Work',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:05:04','',NULL,'Grey Colour Paint Systems','','Interior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(340,313,'Sheds 5 & 6',1,NULL,NULL,'North Wall',4,'',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspected','2018-04-26 13:09:03','P4',NULL,'Bituminous Electrical Backing Board','138','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(341,313,'Sheds 5 & 6',1,NULL,NULL,'Workshop - Sub Board',5,'',1,NULL,'A - 04','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:12:59','',NULL,'Door Lining','','Interior - Ground Level','','','N','',''),
	(342,313,'Ammenities Block',1,NULL,NULL,'Staff Room - Above Sink',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 13:15:44','',NULL,'Hot Water Unit','139','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(343,313,'Ammenities Block',1,NULL,NULL,'Staff Room - Under Sink',0,'Acoustic Pad',1,NULL,'50300-274-11','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:18:57','',NULL,'Bituminous Membrane','','Interior - Ground Level','','','N','',''),
	(344,313,'Shed 1',1,NULL,NULL,'All Rooms',0,'None',0,NULL,'','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:21:17','',NULL,'No Asbestos detected','','Exterior - Ground Level','','','N','',''),
	(345,313,'Shed 1',1,NULL,NULL,'Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:22:57','',NULL,'Yellow Colour Paint Systems','','Exterior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(346,313,'Shed 2',1,NULL,NULL,'Throughout',5,'',1,NULL,'A - 05','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:25:20','',NULL,'Eaves Lining','','Exterior - Ground Level','','','N','',''),
	(347,313,'Shed 2',1,NULL,NULL,'Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:26:40','',NULL,'Yellow Colour Paint Systems','','Exterior - Ground Level','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(348,313,'Ammenities Block',1,NULL,NULL,'Throughout',5,'',1,NULL,'50300-274-010','Positive','10','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 13:33:21','P4',NULL,'Awning Lining','140','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(349,313,'Ammenities Block',1,NULL,NULL,'All Rooms',5,'',1,NULL,'50300-274-007','Positive','600','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 13:36:24','P4',NULL,'Ceiling Lining','141','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(350,315,'Residence',1,NULL,NULL,'Kitchen Pantry ',5,'',1,NULL,'Prensa 50299-07-001','Positive','6','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 13:38:24','P4',NULL,'Walls','142','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(351,313,'Bridge Section',1,NULL,NULL,'All Rooms',0,'No Access - No Keys',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:39:37','',NULL,'','','Interior - Ground Level','','','N','',''),
	(352,315,'Residence',1,NULL,NULL,'Kitchen Pantry ',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','1','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 13:40:01','P4',NULL,'Ceiling ','143','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(353,313,'Bridge Section',1,NULL,NULL,'Throughout',5,'',1,NULL,'A - 06','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:40:45','',NULL,'Eaves Lining','','Exterior - Ground Level','','','N','',''),
	(354,315,'Residence',1,NULL,NULL,'Kitchen ',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','11','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 13:45:49','P4',NULL,'Ceiling ','144','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(355,315,'Residence',1,NULL,NULL,'Kitchen',16,'',1,NULL,'Prensa 50299-07-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:46:45','',NULL,'Floor Covering','','Interior - Ground Level ','','','N','',''),
	(356,315,'Residence',1,NULL,NULL,'Kitchen',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','10','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 13:50:44','P4',NULL,'Walls - Upper Section','145','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(357,315,'Residence',1,NULL,NULL,'Kitchen',5,'Sheet Vinyl Underlay',1,NULL,'Prensa 50299-07-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 13:52:15','',NULL,'Underlay Beneath Sheet Vinyl ','','Interior - Ground Level ','','','N','',''),
	(358,315,'Residence',1,NULL,NULL,'Toilet/Bathroom',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','5','m',NULL,NULL,NULL,'Good','','Low','Yes','2018-04-26 14:09:48','P4',NULL,'Ceiling','148','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(359,315,'Residence',1,NULL,NULL,'Toilet/Bathroom',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','18','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 13:54:54','P4',NULL,'Walls','','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(360,315,'Residence',1,NULL,NULL,'Toilet/Bathroom',5,'',1,NULL,'Not Sampled ','Suspected Positive','3','m',NULL,NULL,NULL,'Good','Low','Low','No','2018-04-26 14:07:44','P4',NULL,'Flooring Beneath Tiles','146','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(361,315,'Residence',1,NULL,NULL,'Toilet/Bathroom',5,'',1,NULL,'Not Sampled - No Access','Suspected Positive','1','m',NULL,NULL,NULL,'Good','Low','Low','No','2018-04-26 14:09:22','P4',NULL,'Behind Ceramic Tiles to Bathtub','147','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(362,315,'Residence',1,NULL,NULL,'Laundry ',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','12','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 14:14:17','P4',NULL,'Walls','149','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(363,315,'Residence',1,NULL,NULL,'Laundry ',5,'',1,NULL,'Same as Prensa 50299-07-001','Assumed Positive','3','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 14:15:22','P4',NULL,'Ceiling','150','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(364,316,'Office & Staff Ammenities',1,NULL,NULL,'Throughout',5,'',1,NULL,'ESP - E29761','Positive','20','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 14:15:46','P4',NULL,'Eaves Lining','151','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(365,315,'Residence',1,NULL,NULL,'Ceiling Space',9,'',1,NULL,'Prensa 50299-07-004','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:16:32','',NULL,'Insulation Material','','Interior - Ground Level ','','','N','',''),
	(366,315,'Residence',1,NULL,NULL,'Ground Surfaces Below Laundry ',5,'',1,NULL,'Same as Prensa 50299-07-005','Assumed Positive','1','m',NULL,NULL,NULL,'Poor','Medium','Medium','No','2018-04-26 14:18:21','P3',NULL,'Debris ','152','Exterior - Ground Level ','Non-Friable','Restrict access and isolate area. Engage a Class A asbestos contractor ASAP to undertake remedial works immediately.','N','',''),
	(367,316,'Office & Staff Ammenities',1,NULL,NULL,'South West Corner',0,'Moulded Fibre Cement',1,NULL,'50300-153-005','Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Not Labelle','2018-04-26 14:19:26','P4',NULL,'Telcomunications Pit','153','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','<p>Not Labelled due to traffic area</p>',''),
	(368,315,'Residence',1,NULL,NULL,'Surrounding ',5,'',1,NULL,'Prensa 50299-07-005','Positive','150','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 14:19:32','P4',NULL,'Wall Cladding','154','Exterior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(369,315,'Residence',1,NULL,NULL,'South-East Side of Residence',4,'',1,NULL,'Not Sampled - Live','Suspected Positive','2 ','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-26 14:21:28','P4',NULL,'Distribution Board ','155','Exterior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(370,316,'Office & Staff Ammenities',1,NULL,NULL,'Southern area',5,'',1,NULL,'50300-153-001','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:21:55','',NULL,'Window Infill Panels','','Exterior - Ground Level','','','N','',''),
	(371,315,'Residence',1,NULL,NULL,'Outhouse Behind Carport ',0,'Moulded Fibre Cement',1,NULL,'Prensa 50299-07-007','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:22:57','',NULL,'Sewer Breather Pipe','','Exterior - Ground Level ','','Not sighted during 2018 RiskTech inspection. ','N','',''),
	(372,316,'Office & Staff Ammenities',1,NULL,NULL,'Southern Side',0,'Window Caulking',1,NULL,'50300-153-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:25:13','',NULL,'Window Beading','','Exterior - Ground Level','','','N','',''),
	(373,315,'Residence',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Same as Prensa 50299-07-005','Assumed Positive','50','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-26 14:26:55','P4',NULL,'Eave Linings','156','Exterior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(374,316,'Office & Staff Ammenities',1,NULL,NULL,'Northern Area',2,'Expansion Joint',1,NULL,'50300-153-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:55:51','',NULL,'Bituminous Expansion Joint','','Exterior - Basement Level','','','N','',''),
	(375,315,'Residence',1,NULL,NULL,'Laundry ',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 14:27:45','',NULL,'Hot Water Unit','157','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(376,315,'Residence',1,NULL,NULL,'Ceiling Space',9,'',2,NULL,'','Suspected Positive','100','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 14:34:27','',NULL,'Loose Insulation ','','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(377,316,'Stores & Workshops',1,NULL,NULL,'North East Corner',4,'',1,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','Low','Suspected','2018-04-26 14:53:32','P4',NULL,'Bituminous Electrical Backing Board','158','Exterior - Basement Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(378,316,'Office & Staff Ammenities',1,NULL,NULL,'Staff Lunch Room',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 14:39:44','',NULL,'Hot Water Unit','159','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(379,316,'Office & Staff Ammenities',1,NULL,NULL,'Staff Showers',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','Low','','2018-04-26 14:46:05','',NULL,'Hot Water Unit','160','Interior - Basement Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(380,315,'Residence',1,NULL,NULL,'Laundry ',0,'Cream Paint System',3,NULL,'Southgate-LP01','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:44:34','',NULL,'Ceiling ','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(381,316,'Office & Staff Ammenities',1,NULL,NULL,'Various Throughout',4,'',1,NULL,'50300-153-004','Negative','4','unit(s)',NULL,NULL,NULL,'','','','','2018-04-26 14:44:38','',NULL,'Bituminous Electrical Backing Board','','Interior & Exterior','','','N','',''),
	(382,316,'Office & Staff Ammenities',1,NULL,NULL,'Various Throughout',22,'',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:48:34','',NULL,'Fluorescent Light Fittings','','Interior - All Levels','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','<p>New Style Light Fittings</p>',''),
	(383,316,'Office & Staff Ammenities',1,NULL,NULL,'Various Throughout',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 14:50:57','',NULL,'Doors & Window Frames','','Interior - All Levels','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(384,317,'Ferry Terminus',1,NULL,NULL,'Throughout',5,'',1,NULL,'50300-153-006','Positive','6','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 15:16:09','P4',NULL,'Eaves Lining','161','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(385,317,'Ferry Terminus',1,NULL,NULL,'Waiting Area - Throughout',5,'',1,NULL,'Same As 50300-153-006','Positive','16','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 15:19:05','P4',NULL,'Ceiling Lining','162','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(386,317,'Ferry Terminus',1,NULL,NULL,'Waiting Area',5,'',1,NULL,'Same As 50300-153-006','Positive','5','m',NULL,NULL,NULL,'Good','Low Accessibility','Low','Confirmed','2018-04-26 15:21:18','P4',NULL,'Facia ','163','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(387,317,'Ferry Terminus',1,NULL,NULL,'Southern Side',4,'',1,NULL,'50300-153-007','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Confirmed','2018-04-26 15:23:51','P4',NULL,'Bituminous Electrical Backing Board','164','Exterior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(388,317,'Ferry Terminus',1,NULL,NULL,'Staff Room',20,'',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-26 15:26:06','',NULL,'Hot Water Unit','165','Interior - Ground Level','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(389,317,'Ferry Terminus',1,NULL,NULL,'Staff Room, Toilet & Shower',22,'',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 15:29:20','',NULL,'Fluorescent Light Fittings','','Interior - Ground Level','','','N','<p>New Style Light Fittings</p>',''),
	(390,317,'Ferry Terminus',1,NULL,NULL,'Waiting Area',21,'',3,NULL,'Lead Check','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-26 15:31:41','',NULL,'White & Blue Paint Systems','','Exterior - Ground Level','','','N','',''),
	(391,318,'Site Office/Workshop',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Prensa 50299-01-002','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 13:31:00','',NULL,'Ceiling','','Interior - Ground Level ','','','N','',''),
	(392,318,'Site Office/Workshop',1,NULL,NULL,'Throughout',5,'',1,NULL,'Same as Prensa 50299-01-002','Assumed Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 13:32:21','',NULL,'Wall Lining','','Interior - Ground Level ','','','N','',''),
	(393,318,'Site Office/Workshop',1,NULL,NULL,'Northern Storeroom, adjacent Office',16,'',1,NULL,'Prensa 50299-01-003','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 13:32:10','',NULL,'Covering to Timber Shelves','','Interior - Ground Level ','','','N','',''),
	(394,318,'Site Office/Workshop',1,NULL,NULL,'North-East Corner above Washing Machine',5,'',1,NULL,'Prensa 50299-01-004','Positive','0.5','m',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-04-30 13:31:58','P4',NULL,'Infill Panel to Wall Penetration','166','Interior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(395,318,'Site Office/Workshop',1,NULL,NULL,'South Side Bench and West End Carport',8,'',1,NULL,'Prensa 50299-01-005','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 13:31:44','',NULL,'Stored Material','','Interior - Ground Level ','','','N','',''),
	(396,318,'Site Office/Workshop',1,NULL,NULL,'North Wall ',4,'',1,NULL,'Not Sampled - Live','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 13:31:33','',NULL,'Electrical Switch Board','','Exterior - Ground Level ','','','N','',''),
	(397,318,'Site Office/Workshop',1,NULL,NULL,'Near Door to Site Office ',9,'',1,NULL,'Not Sampled - No Access','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','','Low','Yes','2018-04-30 13:31:22','P3',NULL,'Steel Safe ','167','Interior - Ground Level ','Friable','Maintain in good condition. Remove under controlled conditions by a Class A asbestos contractor prior to renovations.','N','',''),
	(398,318,'Site Office/Workshop',1,NULL,NULL,'Throughout',5,'',1,NULL,'Prensa 50299-01-001','Positive','10','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-04-30 13:34:26','P4',NULL,'Eaves Linings ','168','Exterior - Ground Level ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(399,318,'Site Office/Workshop',1,NULL,NULL,'Throughout ',0,'Capacitor',4,NULL,'','Suspected Positive','3','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-30 13:35:53','',NULL,'Fluorescent Light Fittings','169','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(400,318,'Site Office/Workshop',1,NULL,NULL,'Throughout ',0,'Blue Paint System ',3,NULL,'Prensa Swab','Positive','10','m',NULL,NULL,NULL,'Fair','Low','Low','','2018-04-30 13:47:12','',NULL,'Walls','170','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(401,318,'Site Office/Workshop',1,NULL,NULL,'Throughout',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Positive','20','m',NULL,NULL,NULL,'Fair','Low','Low','','2018-04-30 13:54:02','',NULL,'Walls','171','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(402,318,'Site Office/Workshop',1,NULL,NULL,'West ',0,'White Paint System',3,NULL,'Ashby-LP01','Positive','20','m',NULL,NULL,NULL,'Fair','Low','Low','','2018-04-30 14:01:44','',NULL,'Wall','172','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(403,318,'Site Office/Workshop',1,NULL,NULL,'Ceiling ',0,'Capacitor',4,NULL,'','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 14:02:58','',NULL,'Fluorescent Light Fitting','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(404,318,'Site Office/Workshop',1,NULL,NULL,'Ceiling Space, On Top of Site Office ',0,'Settled Dust',3,NULL,'Prensa 50299-01-LD01','Positive','10','m',NULL,NULL,NULL,'Poor','Medium','Medium','','2018-04-30 14:24:45','',NULL,'Dust','','Interior - Ground Level ','','Remove under controlled conditions prior to renovations or refurbishment. ','N','',''),
	(405,318,'Site Office/Workshop',1,NULL,NULL,'Adacent Sliding Entrance Door',0,'Green Lower Paint System',3,NULL,'Prensa Swab','Positive','2','m',NULL,NULL,NULL,'Poor','Low Accessibility','Low','','2018-04-30 14:33:02','',NULL,'Timber Corner Post ','173','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(406,318,'Site Office/Workshop',1,NULL,NULL,'Adjacent Sliding Door Entrance',0,'White Upper Paint System',3,NULL,'Prensa Swab','Positive','2','m',NULL,NULL,NULL,'Poor','Low','Low','','2018-04-30 14:35:00','',NULL,'Timber Corner Post ','174','Interior - Ground Level ','','Consider removal of lead paint systems in poor condition or overpaint painted surfaces under controlled conditions. ','N','',''),
	(407,318,'Staff Amenities Block ',1,NULL,NULL,'Ceiling ',0,'Capacitor',4,NULL,'','Suspected Positive','2','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-04-30 14:40:48','',NULL,'Fluorescent Light Fitting','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(408,318,'Staff Amenities Block ',1,NULL,NULL,'Throughout ',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 14:42:06','',NULL,'Walls ','','Interior & Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(409,318,'Staff Amenities Block ',1,NULL,NULL,'North ',0,'White Lower Paint System',3,NULL,'Prensa Swab','Positive','','m',NULL,NULL,NULL,'','','','','2018-04-30 14:43:29','',NULL,'Wall','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(410,318,'Staff Amenities Block ',1,NULL,NULL,'Throughout',0,'Green Paint System ',3,NULL,'Prensa Swab','Negative','','m',NULL,NULL,NULL,'','','','','2018-04-30 14:57:14','',NULL,'Window Frames ','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(411,318,'Staff Amenities Block ',1,NULL,NULL,'On Top of Timber Framework above Shower Cubicles',0,'Settled Dust',3,NULL,'Prensa 50299-01-LD02','Positive','20','m',NULL,NULL,NULL,'Poor','Medium','Medium','','2018-04-30 14:59:17','',NULL,'Dust ','','Interior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(412,318,'Small Rope Store Shed',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-04-30 15:32:18','',NULL,'','','Interior & Exterior ','','','N','',''),
	(413,318,'Small Rope Store Shed',1,NULL,NULL,'Throughout ',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Positive','10','m²',NULL,NULL,NULL,'Fair','Low Accessibility','Low','','2018-04-30 15:39:11','',NULL,'Walls ','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(414,318,'Storage Garage with Roller Door (East of Site Office/Workshop)',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:23:18','',NULL,'','','Interior & Exterior ','','','N','',''),
	(415,318,'Storage Garage with Roller Door (East of Site Office/Workshop)',1,NULL,NULL,'Throughout',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:24:49','',NULL,'Corrugated Iron Wall Cladding ','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(416,318,'Storage Garage with Roller Door (East of Site Office/Workshop)',1,NULL,NULL,'Ceiling',0,'Capacitor',4,NULL,'','Suspected Positive','4','unit(s)',NULL,NULL,NULL,'Good','Low','Low','','2018-05-01 14:27:25','',NULL,'Fluorescent Light Fittings','175','Interior - Ground Level ','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(417,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Prensa 50299-01-006','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:31:53','',NULL,'Ceiling Lining ','','Interior - Ground Level ','','','N','',''),
	(418,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout',17,'',1,NULL,'Prensa 50299-01-007','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:32:45','',NULL,'Floor Covering ','','Interior - Ground Level ','','','N','',''),
	(419,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Ground Surfaces, North of Shed ',5,'',1,NULL,'Prensa 50299-01-008','Positive','1','m',NULL,NULL,NULL,'Poor','Medium','Medium','No','2018-05-01 14:36:36','P3',NULL,'Debris ','176','Exterior','Non-Friable','Restrict access and isolate area. Engage a Class A/B asbestos contractor ASAP to undertake remedial works immediately.','N','',''),
	(420,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:37:37','',NULL,'Timber Weatherboard Walls','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(421,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout, Beneath Upper Painted Surfaces ',0,'White Paint System',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-01 14:39:38','',NULL,'Timber ','','Exterior - Ground Level ','','','Y','',''),
	(422,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout, Beneath Yellow Upper Surfaces ',0,'White Paint System',3,NULL,'Prensa Swab','Positive','20','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-01 14:40:52','',NULL,'Timber Weatherboard Walls','177','Exterior - Ground Level ','','Maintain in good condition. Remove painted surfaces under controlled conditions prior to renovations.','N','',''),
	(423,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout',0,'Green Paint System ',3,NULL,'Ashby-LP02','Positive','3','m',NULL,NULL,NULL,'Fair','Medium Accessibility','Medium','','2018-05-01 14:47:33','',NULL,'Window Frames ','178','Exterior - Ground Level ','','Consider removal of lead paint systems in fair-poor condition.','N','',''),
	(424,318,'Lunch Shed (East of Site Office/Workshop)',1,NULL,NULL,'Throughout, Beneath Green Upper Paint System',0,'White Lower Paint System',3,NULL,'Prensa Swab','Positive','3','m',NULL,NULL,NULL,'Good','Low','Low','','2018-05-01 14:49:43','',NULL,'Window Frames ','','Exterior - Ground Level ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(425,318,'Steel Framed Shed adjacent Site Entrance',1,NULL,NULL,'Throughout ',5,'',1,NULL,'Prensa 50299-01-011','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-01 15:07:09','',NULL,'Wall Cladding','','Exterior - Ground Level ','','','N','',''),
	(426,319,'Site Office Demountable',1,NULL,NULL,'Throughout',16,'No asbestos materials identified ',1,NULL,'Woodburn-03','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 14:56:43','',NULL,'Floor Covering','','Interior','','','N','',''),
	(427,319,'The Vault',1,NULL,NULL,'Adjacent Entrance ',9,'In',1,NULL,'Woodburn-01','Positive','1','m²',NULL,NULL,NULL,'Fair','Medium Accessibility','Medium','Yes','2018-05-01 15:56:19','P2',NULL,'Door Lining ','179','Interior','Friable','Engage a Class A asbestos contractor to clean up or remove the material as soon as practicable [less than 3 months].','N','',''),
	(428,319,'Flammable Liquids Store ',1,NULL,NULL,'Throughout',0,'Yellow Paint System ',3,NULL,'Prensa Swab','Positive','10','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-01 16:01:49','',NULL,'Wall Cladding ','180','Exterior','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(429,319,'Amenities Block',1,NULL,NULL,'',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-01 16:04:49','',NULL,'Hot Water Unit','181','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(430,319,'Asphalt Storage Tank',1,NULL,NULL,'Pipework ',8,'',1,NULL,'Woodburn-02','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low','Low','Yes','2018-05-02 14:40:59','P4',NULL,'Pipework Flange Joint ','182','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(431,319,'Shipping Container 1',1,NULL,NULL,'Throughout',0,'Beige Paint System',3,NULL,'Prensa Swab','Positive','20','m',NULL,NULL,NULL,'Poor','Medium','Medium','','2018-05-01 16:11:48','',NULL,'Wall Cladding ','183','Exterior ','','consider removal or encapsulation of lead paint systems identified in poor condition.','N','',''),
	(432,319,'Shipping Container 2',1,NULL,NULL,'Throughout',0,'Beige Paint System',3,NULL,'Prensa Swab','Positive','20','m²',NULL,NULL,NULL,'Good','Medium Accessibility','Medium','','2018-05-01 16:19:51','',NULL,'Wall Cladding','184','Exterior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','',''),
	(433,319,'Asphalt Storage Tank',1,NULL,NULL,'Throughout ',9,'',2,NULL,'','Suspected Positive','6','m',NULL,NULL,NULL,'Good','Low','Low','','2018-05-02 14:50:06','',NULL,'Wall Lining ','185','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(434,319,'Site Office ',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:07:00','',NULL,'','','Interior & Exterior','','','N','',''),
	(435,319,'Site Office ',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:08:07','',NULL,'','','Interior & Exterior','','','Y','',''),
	(436,319,'Bridge Shed',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:08:44','',NULL,'','','Interior & Exterior','','','N','',''),
	(437,319,'Large Metal Shed',1,NULL,NULL,'Throughout',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:10:55','',NULL,'','','Interior & Exterior','','','N','',''),
	(438,319,'Bridge Services Shed',1,NULL,NULL,'Throughout ',9,'',2,NULL,'','Suspected Positive','400','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-02 15:27:18','',NULL,'Roof Lining ','','Interior','','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(439,320,'Office',1,NULL,NULL,'Pump Room',8,'',1,NULL,'Ewingdale-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:40:55','',NULL,'Pipework Joint','','Interior','','','N','',''),
	(440,320,'Office',1,NULL,NULL,'Throughout',9,'',2,NULL,'','Suspected Positive','600','m²',NULL,NULL,NULL,'Good','Low','Low','','2018-05-02 15:42:44','',NULL,'Roof Lining','186','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(441,320,'Office',1,NULL,NULL,'Surrounding',5,'',1,NULL,'Not Sampled - Newer Appearance','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:43:52','',NULL,'Eave Linings','','Exterior ','','','N','',''),
	(442,320,'Office',1,NULL,NULL,'Throughout ',9,'',2,NULL,'','Suspected Positive','150','m',NULL,NULL,NULL,'Good','Low','Low','','2018-05-02 15:46:44','',NULL,'Wall Lining ','187','Interior ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(443,320,'Office',1,NULL,NULL,'Electrical Room',4,'',1,NULL,'Not Sampled - Newer Appearance','Suspected Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 15:47:23','',NULL,'Electrical Switch Board','','Interior ','','','N','',''),
	(444,321,'ATCO Demountable',1,NULL,NULL,'Shower ',5,'',1,NULL,'Billinudgel-01','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 16:00:41','',NULL,'Shower Partition Wall ','','Interior ','','','N','',''),
	(445,321,'ATCO Demountable',1,NULL,NULL,'Throughout',16,'',1,NULL,'Billinudgel-02','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 16:01:16','',NULL,'Floor Covering ','','Interior ','','','N','',''),
	(446,321,'Main Shed/Workshop',1,NULL,NULL,'Office Area',5,'',1,NULL,'Billinudgel-03','Negative','','m',NULL,NULL,NULL,'','','','','2018-05-02 16:02:08','',NULL,'Walls ','','Interior ','','','N','',''),
	(447,321,'Bitumen Tank Bund Area ',1,NULL,NULL,'Pipework',8,'',1,NULL,'Billinudgel-04','Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','Yes','2018-05-02 16:04:31','P4',NULL,'Pipework Joint','188','Exterior ','Non-Friable','Maintain in good condition. Remove under controlled conditions by a Class A/B asbestos contractor prior to renovations.','N','',''),
	(448,321,'Bitumen Tank Bund Area ',1,NULL,NULL,'Throughout ',9,'',2,NULL,'','Suspected Positive','6','m²',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-02 16:05:40','',NULL,'Wall Lining','189','Interior ','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(449,321,'Shed 1',1,NULL,NULL,'Throughout ',0,'No asbestos materials identified ',0,NULL,'','','','m',NULL,NULL,NULL,'','','','','2018-05-02 16:13:13','',NULL,'','','Interior & Exterior ','','','N','',''),
	(450,321,'Main Shed/Workshop',1,NULL,NULL,'',0,'Internal Insulation',2,NULL,'','Suspected Positive','1','unit(s)',NULL,NULL,NULL,'Good','Low Accessibility','Low','','2018-05-02 16:17:28','',NULL,'Hot Water Unit','190','Interior','Non-Friable','Maintain in good condition. Remove under controlled conditions prior to renovations.','N','',''),
	(451,321,'Main Shed/Workshop',1,NULL,NULL,'South-West Compressor Store ',0,'Grey Paint System',3,NULL,'Prensa Swab','Positive','10','m²',NULL,NULL,NULL,'Fair','Low Accessibility','Low','','2018-05-02 16:20:29','',NULL,'Door, Door Frame & Wall','191','Interior ','','Maintain in good condition. Remove or overpaint painted surfaces under controlled conditions prior to renovations.','N','','');

/*!40000 ALTER TABLE `tbl_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report`;

CREATE TABLE `tbl_report` (
  `reportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT '',
  `scope_of_works` varchar(255) DEFAULT '',
  `recommendations` varchar(255) DEFAULT NULL,
  `methologies` varchar(255) DEFAULT NULL,
  `risk_factors` varchar(255) DEFAULT NULL,
  `priority_rating_system` varchar(255) DEFAULT NULL,
  `asbestos_mng_req` varchar(255) DEFAULT NULL,
  `haz_material_mr` varchar(255) DEFAULT NULL,
  `statement_of_limitations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report` WRITE;
/*!40000 ALTER TABLE `tbl_report` DISABLE KEYS */;

INSERT INTO `tbl_report` (`reportID`, `siteID`, `introduction`, `scope_of_works`, `recommendations`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,17,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at Capital building. This risk assessment was generated on 31/01/2018</p>','Y','Y','Y','Y','Y','Y','Y','Y'),
	(2,1,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at Abbotsford. This risk assessment was generated on 31/01/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(3,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(4,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(5,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(6,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(7,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(8,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(9,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(10,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(11,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(12,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(13,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(14,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(15,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(16,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(17,118,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(18,119,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(19,200,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(20,120,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(21,121,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(22,122,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(23,206,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(24,123,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(25,124,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(26,125,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(27,126,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(28,127,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(29,178,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(30,179,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(31,220,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(32,221,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(33,222,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at 12 High Street. The risk assessment was performed by Bernard Day on .</p>','Y','Y','Y','Y','Y','Y','Y','Y'),
	(34,223,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(35,224,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(36,225,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(37,226,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(38,227,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(39,228,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(40,229,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(41,230,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(42,231,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(43,232,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(44,233,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(45,234,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(46,235,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(47,236,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(48,237,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(49,238,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(50,239,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(51,240,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(52,241,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(53,242,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(54,243,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(55,244,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(56,245,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(57,246,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(58,258,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(59,261,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(60,262,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 25-10-2016.</p>','N','N','N','N','N','N','Y','N'),
	(61,263,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(62,264,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at Building name. The risk assessment was performed by 0 on 15-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(63,265,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 01-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(64,266,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 01-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(65,267,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(66,268,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(67,269,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(68,270,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(69,271,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(70,272,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(71,273,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(72,274,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(73,275,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(74,276,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(75,277,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(76,278,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(77,279,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(78,280,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(79,282,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','N','Y'),
	(80,283,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for RiskTech of the site located at Test site GH. This risk assessment was generated on 01/02/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(81,284,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 85 Test Street. The risk assessment was performed by Inspected By on 28-03-2017. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(82,176,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 12. The risk assessment was performed by John Smith on .</p>','Y','Y','N','N','Y','N','N','N'),
	(83,281,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(84,177,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 123. The risk assessment was performed by John Smith on 23-09-2016.</p>','N','N','N','N','N','N','N','N'),
	(85,285,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at zzz. The risk assessment was performed by Fred Bloggs on 27-06-2012.</p>','Y','N','Y','N','N','Y','N','N'),
	(86,286,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(87,287,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at \" or \"\"=\". The risk assessment was performed by \" or \"\"=\" on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(88,288,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at tjhfhtfymjkst. The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(89,289,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at tjhfhtfymjkst. The risk assessment was performed by Greg Harradine on 01-12-2017. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(90,290,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at ewlhyfgo9[ed. This risk assessment was generated on 14/02/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(91,294,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 248 Victoria Street. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(92,291,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at Wetherill Park HVIS. This risk assessment was generated on 15/02/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(93,292,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at 248 Victoria Street. The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(94,293,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Wetherill Park HVIS. This risk assessment was generated on 27/03/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(95,295,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 23 Young Street . The risk assessment was performed by Simone Walsh.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(96,296,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Yennora Works Centre. This risk assessment was generated on 24/04/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(97,297,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 51 Maison Dieu Road. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(98,298,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Great Western Highway. The risk assessment was performed by .','Y','Y','Y','Y','Y','Y','Y','Y'),
	(99,299,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Great Western Highway. The risk assessment was performed by .','Y','Y','Y','Y','Y','Y','Y','Y'),
	(100,300,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 12 High Street. The risk assessment was performed by Bernard Day.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(101,301,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Mount Boyce Weigh Station HVSS. This risk assessment was generated on 24/04/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(102,302,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Bell HVSS. This risk assessment was generated on 24/04/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(103,303,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Corner Hill and Bridge Streets. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(104,304,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at New england Highway . The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(105,305,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 30 Wentworth Street. The risk assessment was performed by Paul Brown. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(106,306,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 123 Adderley Street. The risk assessment was performed by Paul Brown.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(107,307,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 123 Adderley Street. The risk assessment was performed by Paul Brown. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(108,308,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Ainsbury Road. The risk assessment was performed by Paul Brown.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(109,309,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Ainsbury Road. The risk assessment was performed by Paul Brown.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(110,310,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at St Marys Depot. This risk assessment was generated on 26/04/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(111,311,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 1 Ainsbury Road. The risk assessment was performed by Paul Brown. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(112,312,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Cnr Newell & Kamilaroi Highway. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(113,313,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 66 - 68 Mileham Street. The risk assessment was performed by Paul Brown. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(114,314,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Moree Motor Registry . This risk assessment was generated on 26/04/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(115,315,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 139 Southgate Ferry Road. The risk assessment was performed by Simone Walsh.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(116,316,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Mortlake Ferry - Leased Land. This risk assessment was generated on 09/05/2018','Y','Y','Y','Y','Y','Y','Y','Y'),
	(117,317,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 33A Hilly Street. The risk assessment was performed by Paul Brown.','Y','Y','Y','Y','Y','Y','Y','Y'),
	(118,318,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Clarence Street. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(119,319,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at 17 Trustums Hill Road . The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(120,320,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at McClouds Shoot Tunnel Office, St. Helena Road . The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(121,321,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Cushman & Wakefield of the site located at Brunswick Street. The risk assessment was performed by Simone Walsh. ','Y','Y','Y','Y','Y','Y','Y','Y');

/*!40000 ALTER TABLE `tbl_report` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report_common
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report_common`;

CREATE TABLE `tbl_report_common` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recommendations` longtext,
  `scope_of_works` longtext,
  `methologies` longtext,
  `risk_factors` longtext,
  `priority_rating_system` longtext,
  `asbestos_mng_req` longtext,
  `haz_material_mr` longtext,
  `statement_of_limitations` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report_common` WRITE;
/*!40000 ALTER TABLE `tbl_report_common` DISABLE KEYS */;

INSERT INTO `tbl_report_common` (`id`, `recommendations`, `scope_of_works`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,'<h3>Recommendations</h3>\r\n<ul>\r\n<li>Schedule periodic re-assessments of the asbestos-containing materials remaining in-situ to monitor their condition in accordance with the Code of Practice.</li>\r\n<li>Areas highlighted in the Areas Not Accessed section as areas of \'no access\' should be presumed to contain hazardous materials. Appropriate management planning should be implemented in order to control access to and maintenance activities in these areas, until such a time as they can be inspected and the presence or absence of hazardous materials can be confirmed.</li>\r\n<li>Should any personnel come across any suspected asbestos or hazardous materials, work should cease immediately in the affected areas until further sampling and investigation is performed.</li>\r\n<li>Prior to demolition/refurbishment works undertake a destructive hazardous materials survey of the premises as per the requirements of AS 2601: 2001 The Demolition of Structures, Part 1.6.1.&nbsp;</li>\r\n<li>Synthetic Mineral Fibre (SMF) materials should be removed under controlled conditions prior to demolition/refurbishment works, in accordance with the requirements of the Code of Practice for the Safe Use of Synthetic Mineral Fibres [NOHSC:2006(1990)].</li>\r\n</ul>','<h3>Scope of Works</h3>\n<p>The scope of works for this project was as follows:\n<ul>\n<li>Undertake a survey of the site in accordance with recognised standards and guidelines.</li>\n<li>Inspect representative and accessible areas of the site to assess hazardous materials.</li>\n<li>Assess the current condition of hazardous materials and assess the risks posed by the materials at the site.</li>\n<li>Recommend control measures to manage any hazardous material related risks.</li>\n<li>Identify the likelihood of hazardous materials in inaccessible areas.</li>\n<li>Compile an updated hazardous materials register for the site.</li>\n<li>Photograph hazardous materials.</li>\n<li>Label asbestos containing materials.</li>\n</ul>\nRefer to &lsquo;Methodology&rsquo; for full details.</p>','<p>This assessment was undertaken in accordance with the following documents and within the constraints of the scope of works:</p>\n<ul>\n<li>NSW Work Health &amp; Safety Regulation 2017</li>\n<li>How to Manage and Control Asbestos in the Workplace: Code of Practice (Safe Work Australia, 2016)</li>\n</ul>\n\n<h3>Asbestos</h3>\n<p>Detailed sampling and identification of suspected asbestos materials was undertaken. Small representative samples of suspected asbestos-containing material are collected in plastic bags with clip-lock seals.</p>\n<p>6 samples were collected and subsequently analysed in an external NATA-accredited laboratory (Safe Work &amp; Environments) for the presence of asbestos by Polarised Light Microscopy. Refer to Appendix 3 for laboratory results.</p>\n<p>Where it was determined that asbestos was present, a risk and priority assessment was conducted in accordance with RiskTech\'s standard Risk Assessment and Priority Ranking System. Refer to section on &lsquo;Priority Rating System&rsquo; for detailed information on this system.</p>\n<p>Inaccessible areas that are likely to contain asbestos have been assumed to contain asbestos until further inspection and analysis of samples has been undertaken by an approved analyst.</p>\n<p>Due to the nature of the survey methodology, it is possible that not every area of the site have been accessed. Reference should be made to the \'Areas Not Accessed\' section of this report for further details.</p>\n<p>Asbestos materials on site have been labelled. The labels have either been placed on the asbestos containing material during previous assessments and their condition assessed or they have been placed during the current assessment.</p>\n\n<center><img src=\"http://haztech.swim.net.au/images/warning-label.png\" alt=\"\" style=\"text-align:center\" /></center>\n\n<h3>SMF</h3>\n<p class=\"MsoNormal\" style=\"text-align: justify; mso-layout-grid-align: none; text-autospace: none; margin: 6.0pt 0cm 3.0pt 0cm;\">Synthetic Mineral Fibre (SMF) - accessible areas where Synthetic Mineral Fibre (SMF) insulation was visually confirmed as being present were noted to give a general indication to the presence of materials throughout the building.</p>\n\n<h3>PCBs</h3>\n<p>Polychlorinated Biphenyls (PCBs) - manufacturer&rsquo;s details on metallised capacitors in light fittings are assessed against published information in I<em>dentification of PCB Containing Capacitors Information Booklet, ANZECC 1997</em> to determine if the capacitors contain PCBs. Due to the inherent safety issue in accessing light fittings, safe access to light fitting capacitors within insitu light fittings was not available at the time of the audit. An assessment on the likelihood of light fittings containing PCB capacitors has been made, based on the apparent age and style of the light fittings.</p>\n\n<h3>Lead Paint</h3>\n<p>Representative painted surfaces were tested unobtrusively for the presence of lead using the LeadCheck paint swab method. This method can give an instantaneous qualitative result and reproducibly detect lead in paints at concentrations of 0.5% (5,000ppm) and above, and may indicate lead in some paint films as low as 0.2% (2,000ppm).</p>\n<ul>\n<li>7 lead swabs were undertaken as part of this assessment</li>\n</ul>\n<p>The sampling program was representative of the various types of paints found within the site, concentrating on areas where lead-based paints may have been used (Eg. Gloss paints on doors, railings, guttering and downpipes, columns, window and door architraves, skirting boards etc). The objective of lead paint identification in this survey is to highlight the presence of lead-based paints within the building, not to specifically quantify every source of lead-based paint.</p>\n\n<h2>Previous Reports</h2>\n<p>The following previous reports were reviewed, and may be referred to, as part of this assessment:</p>\n<ul>\n<li>Hazardous Materials Assessment, Yennora Works Centre, 129a Orchardleigh Street, Yennora NSW 2161 &ndash; Report Date December 2011, Prepared by Prensa.</li>\n</ul>\n<p>It is noted that this report references a previous report dated May 2004, presumably undertaken by consultants ESP (as samples are referenced in this report).</p>\n<p>It is also noted that many buildings have changed use since the last report in 2011. Results of previous reports have been included in this report where applicable.</p>\n','<h3>Risk Assessment Factors - Asbestos</h3>\n\n<p>The presence of asbestos-containing materials (ACMs) does not necessarily constitute an exposure risk. However, if the ACM is sufficiently disturbed to cause the release of airborne respirable fibres, then an exposure risk may be posed to individuals. The assessment of the exposure risk posed by ACMs assesses (a) the material condition and friability, and (b) the disturbance potential.</p>\n\n<h3>Material Condition</h3>\n<p>The assessment factors for material condition include:</p>\n<ul>\n<li>Evidence of physical deterioration and/or water damage.</li>\n<li>Degree of friability of the ACM.</li>\n<li>Surface treatment, lining or coating (if present).</li>\n<li>Likelihood to sustain damage or deterioration in its current location and state.</li>\n</ul>\n\n<h3>Physical Condition and Damage</h3>\n\n<p>The condition of the ACM is rated as either being good, fair or poor.</p>\n<div><em>Good</em>&nbsp; &nbsp; refers to an ACM that has not been damaged or has not deteriorated</div>\n<div><em>Fair</em>&nbsp; &nbsp; &nbsp; refers to an ACM having suffered minor cracking or de-surfacing.</div>\n<div><em>Poor</em>&nbsp; &nbsp; &nbsp;describes an ACM which has been damaged or its condition has deteriorated over time.</div>\n\n<h3>Friability and Surface Treatment</h3>\n<p>The degree of friability of ACMs describes the ease of which the material can be crumbled, and hence to release fibres, and takes into account surface treatment.</p>\n\n<p><em>Friable asbestos</em><br/>\n(e.g. sprayed asbestos beam insulation (limpet), pipe lagging) can be easily crumbled and is more hazardous than non-friable asbestos products.</p>\n\n<p><em>Non-friable asbestos</em><br/>\nalso referred to as bonded asbestos, typically comprises asbestos fibres tightly bound in a stable non-asbestos matrix or impregnated with a coating. Examples of non-friable asbestos products include asbestos cement materials (sheeting, pipes etc), asbestos containing vinyl floor tiles, compressed gaskets and electrical backing boards.</p>\n\n<h3>Disturbance Potential</h3>\n<p>In order to assess the disturbance potential, the following factors are considered:</p>\n<ul>\n<li>Requirement for access for either building work or maintenance operations.</li>\n<li>Likelihood and frequency of disturbance of the ACM.</li>\n<li>Accessibility of the ACM.</li>\n<li>Proximity of the ACM to air plenums and direct air stream.</li>\n<li>Quantity and exposed surface areas of ACM.</li>\n<li>Normal use and activity in area, and numbers of persons in vicinity of ACM.</li>\n</ul>\n<p>These factors are used to determine (i) the potential for fibre generation, and (ii) the potential for exposure to person/s, as a rating of low, medium or high disturbance potential</p>\n\n<h3>Risk Status</h3>\n<p>The risk factors described previously are used to rank the asbestos exposure risk posed by the presence of the ACM.</p>\n<ul>\n<li>A low risk rating describes ACMs that pose a low exposure risk to personnel, employees and the general public providing they stay in a stable condition, for example asbestos materials that are in good condition and have low accessibility.</li>\n<li>A medium risk rating applies to ACMs that pose an increased exposure risk to people in the area.</li>\n<li>A high risk rating applies to ACMs that pose a higher exposure risk to personnel or the public in the vicinity of the material due to their condition or disturbance potential.</li>\n</ul>','<h3>Asbestos Priority Rating System for Control Recommendations</h3>\n\n<p>The following priority rating system is adopted to assist in the programming and budgeting of the control of asbestos risk identified at the site.</p>\n','','<p class=\"p1\">&nbsp;The Occupational Health and Safety Regulations of most Australian states &amp; territories have requirements for the identification and control of risks within workplaces. These broad requirements extends to the hazardous materials that may be present within buildings at the workplace. The requirements for management of hazardous materials is summarised below.</p>\r\n<p class=\"p2\"><strong>Synthetic Mineral Fibre (SMF)</strong></p>\r\n<p class=\"p1\">&nbsp;Synthetic Mineral Fibre (SMF) is a man-made insulation material used extensively in industrial, commercial and residential sites as fire rating, reinforcement in construction materials and as acoustic and thermal insulators. Types of SMF materials include fibreglass, rockwool, ceramic fibres and continuous glass filaments.</p>\r\n<p class=\"p1\">There are two basic forms of Synthetic Mineral Fibre (SMF) insulation, bonded and un-bonded.</p>\r\n<ul>\r\n<li>Bonded SMF is where adhesives, binders or cements have been applied to the SMF before delivery and the SMF product has a specific shape.</li>\r\n<li>Un-bonded SMF has no adhesives, binders or cements and the SMF is loose material packed into a package.</li>\r\n</ul>\r\n<p class=\"p1\">Exposure to SMF can result in short-term skin, eye and respiratory irritation. SMF is also classified as a possible human carcinogen with a possible increase in risk in lung cancer from long-term exposure.</p>\r\n<p class=\"p1\">The use of and the safe removal of SMF materials should be conducted in accordance with the National Code of Practice for the safe use of Synthetic Mineral Fibres [NOHSC: 2006 (1990)].</p>\r\n<p class=\"p2\"><strong>Polychlorinated Biphenyls (PCBs)</strong></p>\r\n<p class=\"p1\">&nbsp;Polychlorinated Biphenyls (PCBs) are a toxic organochlorine used as insulating fluids in electrical equipment such as transformers, capacitors and fluorescent light ballasts that were largely banned from importation in Australia in the 1970s.</p>\r\n<p class=\"p1\">PCBs are listed as a probable human carcinogen and should be managed in accordance with the ANZECC Polychlorinated Biphenyls Management Plan, 2003. The handling and disposal of PCBs must be performed in accordance with applicable state and commonwealth environmental protection laws as scheduled PCB waste.</p>\r\n<p class=\"p1\">The following Personal Protective Equipment (PPE) should be worn when handling items containing or suspected to contain PCBs - nitrile gloves, eye protection, and disposable overalls. The PPE should be worn when removing capacitors from light fittings in case PCBs leak from the capacitor housing.</p>\r\n<p class=\"p2\"><strong>Lead Paint</strong></p>\r\n<p class=\"p1\">Lead paint, as defined by the Australian Standard \"AS4361.2: 1998 Guide to Lead Paint Management; Part 2:</p>\r\n<p class=\"p1\">Residential and Commercial Buildings\", is that which contains in excess of 1% Lead by weight.</p>\r\n<p class=\"p1\">Lead carbonate (white lead) was once the main white pigment in paints for houses and public buildings. Paint with lead pigment was manufactured up until the late 1960\'s, and in 1969 the National Health and Medical Research Council\'s Uniform Paint Standard was amended to restrict lead content in domestic paint.</p>\r\n<p class=\"p1\">Lead in any form is toxic to humans when ingested or inhaled, with repeated transmission of particles cumulating in lead poisoning. Lead paint is assessed based on two potential routes of exposure. Firstly by the likelihood of inhalation or ingestion by people working in the vicinity of the paint and secondly by the condition of the paint. Paint that is flaking or in poor condition is more likely to be ingested than paint that is in a good, stable condition.</p>\r\n<p class=\"p1\">Any work relating to lead paint should be conducted in accordance with the \'National Code of Practice for the Control and Safe Use of Inorganic Lead at Work [NOHSC: 2015 (1994)]\'.</p>','<p>This report has been prepared in accordance with the agreement between Cushman &amp; Wakefield and RiskTech.</p>\r\n<p>Within the limitations of the agreed upon scope of services, this work has been undertaken and performed in a professional manner, in accordance with generally accepted practices, using a degree of skill and care ordinarily exercised by members of its profession and consulting practice. No other warranty, expressed or implied, is made.</p>\r\n<p>This report is solely for the use of Cushman &amp; Wakefield and its client Roads and Maritime and any reliance on this report by third parties shall be at such party\'s sole risk and may not contain sufficient information for purposes of other parties or for other uses.</p>\r\n<p>This report shall only be presented in full and may not be used to support any other objective than those set out in the report, except where written approval with comments are provided by RiskTech.</p>\r\n<p>This report relates only to the identification of asbestos/hazardous materials used in the construction of the building and does not include the identification of dangerous goods or hazardous substances in the form of chemicals used, stored or manufactured within the building or plant.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; mso-layout-grid-align: none; text-autospace: none; margin: 6.0pt 0cm 3.0pt 0cm;\">The following should also be noted:</p>\r\n<p>While the survey has attempted to locate the asbestos/hazardous materials within the site it should be noted that the review was a visual inspection and a limited sampling program was conducted and/or the analysis results of the previous report were used.</p>\r\n<p>Representative samples of suspect asbestos materials are collected for analysis where possible. Other asbestos materials of similar appearance are assumed to have a similar content.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; mso-layout-grid-align: none; text-autospace: none; margin: 6.0pt 0cm 3.0pt 0cm;\"><span lang=\"EN-US\">This is a non-destructive assessment for occupational purposes. It is not to be used for any major refurbishment or demolition, where a more invasive destructive survey would be undertaken in line with plans for re-development.</span></p>\r\n<h1 style=\"mso-list: l1 level1 lfo1;\"><a name=\"_Toc508289759\"></a><!-- [if !supportLists]--><span style=\"mso-fareast-font-family: \'Arial Bold\'; mso-bidi-font-family: \'Arial Bold\';\"><span style=\"mso-list: Ignore;\">1<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Areas not Accessed</h1>\r\n<p><span lang=\"EN-US\">In accordance with the <em>NSW Work Health and Safety Regulation, 2017</em> inaccessible areas that are likely to contain asbestos must be presumed as containing asbestos material until further inspection and analysis of samples has been undertaken by an approved analyst.</span></p>\r\n<p><span lang=\"EN-US\">Typical areas likely to be deemed inaccessible under this regulation are:</span></p>\r\n<ul type=\"disc\">\r\n<li>Height restricted areas e.g. Inaccessible ceiling/roof spaces;</li>\r\n<li>Inaccessible sub-floor spaces/tunnels;</li>\r\n<li>Under carpet/vinyl floor coverings;</li>\r\n<li>Wall cavities/partitions;</li>\r\n<li>Behind ceramic wall tiles;</li>\r\n<li>Inside mechanical equipment e.g. within air conditioning re-heat boxes;</li>\r\n<li>Gaskets &amp; sealants to pipework, ductwork, mechanical equipment &amp; construction joints;</li>\r\n<li>Waterproof membranes;</li>\r\n<li>Sealed fire doors;</li>\r\n<li>Within live electrical switchboards.</li>\r\n</ul>\r\n<p><span lang=\"EN-US\">Other specific areas not accessed are outlined are listed below</span>:</p>\r\n<ul style=\"margin-top: 0cm;\" type=\"disc\">\r\n<li>Storage Containers on site</li>\r\n<li>2x metal garages west of Fleet Building</li>\r\n</ul>');

/*!40000 ALTER TABLE `tbl_report_common` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_document`;

CREATE TABLE `tbl_site_document` (
  `docID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`docID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_document` WRITE;
/*!40000 ALTER TABLE `tbl_site_document` DISABLE KEYS */;

INSERT INTO `tbl_site_document` (`docID`, `siteID`, `full_path`)
VALUES
	(87,293,''),
	(88,294,''),
	(89,293,''),
	(90,293,''),
	(91,293,''),
	(92,293,''),
	(93,295,''),
	(94,293,''),
	(95,293,''),
	(96,296,''),
	(97,296,''),
	(98,293,''),
	(99,293,''),
	(100,293,''),
	(101,296,''),
	(102,296,''),
	(103,296,''),
	(104,296,''),
	(105,296,''),
	(106,296,''),
	(107,296,''),
	(108,296,''),
	(109,296,''),
	(110,296,''),
	(111,296,'S106975.89-FID Report.pdf.pdf'),
	(112,297,''),
	(113,297,''),
	(114,298,''),
	(115,299,''),
	(116,300,''),
	(117,301,''),
	(118,302,''),
	(119,302,''),
	(120,302,''),
	(121,302,''),
	(122,302,''),
	(123,302,''),
	(124,302,''),
	(125,297,''),
	(126,297,''),
	(127,297,''),
	(128,297,''),
	(129,297,''),
	(130,297,''),
	(131,297,''),
	(132,297,''),
	(133,303,''),
	(134,304,''),
	(135,305,''),
	(136,306,''),
	(137,307,''),
	(138,307,''),
	(139,307,''),
	(140,305,''),
	(141,308,''),
	(142,309,''),
	(143,310,''),
	(144,311,''),
	(145,311,''),
	(146,304,''),
	(147,304,''),
	(148,304,''),
	(149,303,''),
	(150,297,''),
	(151,312,''),
	(152,312,''),
	(153,312,''),
	(154,312,''),
	(155,312,''),
	(156,312,''),
	(157,312,''),
	(158,313,''),
	(159,313,''),
	(160,313,''),
	(161,313,''),
	(162,314,''),
	(163,314,''),
	(164,315,''),
	(165,313,''),
	(166,313,''),
	(167,316,''),
	(168,316,''),
	(169,317,''),
	(170,318,''),
	(171,318,''),
	(172,318,''),
	(173,318,''),
	(174,318,''),
	(175,318,''),
	(176,318,''),
	(177,318,''),
	(178,318,''),
	(179,319,''),
	(180,319,''),
	(181,319,''),
	(182,319,''),
	(183,319,''),
	(184,319,''),
	(185,319,''),
	(186,319,''),
	(187,319,''),
	(188,319,''),
	(189,319,''),
	(190,319,''),
	(191,319,''),
	(192,319,''),
	(193,320,''),
	(194,320,''),
	(195,321,''),
	(196,321,'');

/*!40000 ALTER TABLE `tbl_site_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_images`;

CREATE TABLE `tbl_site_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_images` WRITE;
/*!40000 ALTER TABLE `tbl_site_images` DISABLE KEYS */;

INSERT INTO `tbl_site_images` (`imageID`, `siteID`, `full_path`)
VALUES
	(51,296,'site.jpg'),
	(52,297,'P1090092.JPG'),
	(53,300,'P1030156.JPG'),
	(54,301,'Mount_Boyce.PNG'),
	(55,302,'IMG_02741.JPG'),
	(56,303,'P1090142.JPG'),
	(57,305,'IMG_0284.JPG'),
	(58,307,'IMG_0310.JPG'),
	(59,308,'IMG_0174.JPG'),
	(60,309,'IMG_01741.JPG'),
	(61,310,'IMG_01742.JPG'),
	(62,311,'IMG_01743.JPG'),
	(63,304,'P1090171.JPG'),
	(64,312,'P1090364.JPG'),
	(65,313,'IMG_0135.JPG'),
	(66,314,'P1090503.JPG'),
	(67,315,'P1090671.JPG'),
	(68,316,'IMG_0198.JPG'),
	(69,317,'Mortlake_Ferry.PNG'),
	(70,318,'P1090752.JPG'),
	(71,319,'P1090753.JPG'),
	(72,320,'P1090903.JPG'),
	(73,321,'P1090915_(Small).JPG');

/*!40000 ALTER TABLE `tbl_site_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_plan`;

CREATE TABLE `tbl_site_plan` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_name` text,
  `site_url` text,
  `site_type` varchar(255) DEFAULT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `tenant` varchar(255) DEFAULT NULL,
  `site_contact` varchar(255) DEFAULT NULL,
  `description` longtext,
  `site` varchar(255) DEFAULT NULL,
  `property_number` int(11) DEFAULT NULL,
  `full_address` varchar(255) DEFAULT NULL,
  `survey_date` varchar(255) DEFAULT NULL,
  `levels` int(11) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `building_age` int(11) DEFAULT NULL,
  `construction_type` varchar(255) DEFAULT NULL,
  `previous_report` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roof_type` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  `clientID` int(11) DEFAULT NULL,
  `report_name` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `state` int(2) DEFAULT '0',
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `site_description` mediumtext,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `building_name`, `site_url`, `site_type`, `no_floors`, `tenant`, `site_contact`, `description`, `site`, `property_number`, `full_address`, `survey_date`, `levels`, `inspected_by`, `contact_title`, `building_age`, `construction_type`, `previous_report`, `company`, `date_inspected`, `area`, `roof_type`, `site_image`, `hide`, `clientID`, `report_name`, `city`, `post_code`, `state`, `date_updated`, `site_description`)
VALUES
	(293,'Heavy Vehicle Inspection Station',NULL,NULL,NULL,NULL,'Danny Wunsch ',NULL,'Wetherill Park HVIS',49000654,'248 Victoria Street','30-01-2018',2,'Simone Walsh','',NULL,'Brick and Metal','No','RiskTech',NULL,'1200 m2','Metal',NULL,'N',1,'Hazardous Materials Survey','Wetherill Park','2164',1,'2018-02-23 11:45:07',''),
	(294,'',NULL,NULL,NULL,NULL,'Danny Wunsch',NULL,'Wetherill Park HVIS',0,'248 Victoria Street','30-01-2018',1,'Simone Walsh','',NULL,'Metal','','Metal',NULL,'','',NULL,'Y',1,'','Wetherill Park ','2164',1,'2018-02-16 12:01:36',''),
	(295,'Main Building ',NULL,NULL,NULL,NULL,'Peter Jones',NULL,'Yennora Station ',30566,'23 Young Street ','27-02-2018',2,'Simone Walsh','',NULL,'Metal','No','RiskTech',NULL,'1200 sqm','Metal',NULL,'Y',1,'Hazardous Materials Survey','Bondi','2026',1,'2018-02-16 12:01:32',''),
	(296,'Administration Building///SRS Conference Room///Fleet Workshop///Fleet Warehouse///Welders Workshop///Fleet Storage Sheds///ITSM Storage Shed///Covered Shed opposite ITSM Design///Washing Bay Shed///ITSM Warehouse///ITSM Amenities///ITSM Factory///ITSM Design///DAS Sheds///Amenities Building///Electrical Unit///Amenities & TES Storage Sheds///Traffic Equipment Standards Demountable///Electronic Unit Demountables///Gas Yard///ITSM Project Delivery///Fleet Building///GIS Building///DAS Building///DAS Demountable///DAS Storage Shed///Carpark area///',NULL,NULL,NULL,NULL,'Dylan Munro',NULL,'Yennora Works Centre',49000072,'129a Orchardleigh Street','25-01-2018',2,'Bernard Day','',NULL,'Brick','Yes','Prensa',NULL,'','Metal',NULL,'N',1,'','Yennora','2161',1,'2018-04-11 15:44:08','<p>The site consists of a main administration building and many sheds and storage containers on site. The age of construction is approimately 1970s.</p>'),
	(297,'Amenities Block ///Works Office ///Demountable 34///Laboratory///Workshop///Demountable 33///Carpenters Shed///Northern Metal Shed',NULL,NULL,NULL,NULL,'',NULL,'Singleton Works Depot ',49000077,'51 Maison Dieu Road','15-01-2018',0,'Simone Walsh','',NULL,'Brick and Metal','','RiskTech',NULL,'','Metal',NULL,'N',1,'','Singleton','2164',1,'2018-04-26 10:25:57',''),
	(298,'',NULL,NULL,NULL,NULL,'Peter Donnelley',NULL,'Mount Boyce Weigh Station HVSS',49000713,'Great Western Highway','',0,'','Facilities Manager',NULL,'','','',NULL,'','',NULL,'Y',1,'','Mount Boyce','2785',1,'2018-04-20 10:01:17',''),
	(299,'',NULL,NULL,NULL,NULL,'Peter Donnelley',NULL,'Mount Boyce Weigh Station HVSS',49000713,'Great Western Highway','',0,'','Facilities Manager',NULL,'','','',NULL,'','',NULL,'Y',1,'','Mount Boyce','2785',1,'2018-04-20 10:39:54',''),
	(300,'Main Building',NULL,NULL,NULL,NULL,'Fred Blogs',NULL,'Test no.3',1234,'12 High Street','',5,'Bernard Day','Dr',NULL,'Straw','Yes','RiskTech',NULL,'1m2','Straw',NULL,'N',1,'','Big Town','2000',1,'2018-04-20 10:39:09',''),
	(301,'Office/Checking Station',NULL,NULL,NULL,NULL,'Peter Donnelley',NULL,'Mount Boyce Weigh Station HVSS',49000713,'Great Western Highway','16-04-2018',1,'Paul Brown','Facilities Manager',NULL,'Concrete/Metal & Cement Block','Yes','RiskTech',NULL,'600','Metal',NULL,'N',1,'','Mount Boyce','2785',1,'2018-04-20 10:42:13',''),
	(302,'Administration/Ammentities',NULL,NULL,NULL,NULL,'Peter Donnelley',NULL,'Bell HVSS',49001842,'Bells Line of Road','16-04-2018',1,'Paul Brown','Facilities Manager',NULL,'Concrete/Metal/Fibre Cement sheeting & Brick','Yes','RiskTech',NULL,'200m','Metal',NULL,'N',1,'','Bell','2786',1,'2018-04-23 09:15:43','<p>The site consists of an office/ammenities block,&nbsp;Two demountable WC facilities and a shipping container for storage.</p>'),
	(303,'Motor Registry ',NULL,NULL,NULL,NULL,'',NULL,'Muswellbrook Motor Registry ',49000638,'Corner Hill and Bridge Streets','15-01-2018',1,'Simone Walsh','',NULL,'Brick','Yes','RiskTech',NULL,'225','Metal',NULL,'N',1,'','Muswellbrook','2333',1,'2018-04-26 10:25:29',''),
	(304,'Weigh Station///Storage Shed',NULL,NULL,NULL,NULL,'',NULL,'Kankool Weigh Station ',49001223,'New england Highway ','15-01-2018',1,'Simone Walsh','',NULL,'Brick','','RiskTech',NULL,'100','Metal',NULL,'N',1,'','Kankool','2338',1,'2018-04-26 10:23:08',''),
	(305,'Rider Training Facilities',NULL,NULL,NULL,NULL,'Paul Willows',NULL,'Granville Rider Training',49001498,'30 Wentworth Street','24-04-2018',1,'Paul Brown','Manager',NULL,'Metal Clad Demountables','Yes','RiskTech',NULL,'500','Metal',NULL,'N',1,'','Granville','2142',1,'2018-04-26 08:01:55',''),
	(306,'',NULL,NULL,NULL,NULL,'Jannine Sharp',NULL,'Adderley Street M4 Offices',49001717,'123 Adderley Street','24-04-2018',4,'Paul Brown','Manager',NULL,'Metal/Fibre Cement/Block work','Yes','RiskTech',NULL,'700','Metal',NULL,'Y',1,'','Auburn','2144',1,'2018-04-26 07:59:00',''),
	(307,'M4 Offices',NULL,NULL,NULL,NULL,'Jannine Sharp',NULL,'Adderley Street M4 Offices',49001717,'123 Adderley Street','24-04-2018',4,'Paul Brown','Manager',NULL,'Metal/Fibre Cement/Block work','Yes','RiskTech',NULL,'700','Metal',NULL,'N',1,'','Auburn','2144',1,'2018-04-26 08:00:22',''),
	(308,'',NULL,NULL,NULL,NULL,'Jason Eaton',NULL,'St Marys Depot',49000125,'Ainsbury Road','10-04-2018',1,'Paul Brown','Manager',NULL,'','Yes','RiskTech',NULL,'80','Metal',NULL,'Y',1,'','St Marys','2760',1,'2018-04-26 08:44:29',''),
	(309,'',NULL,NULL,NULL,NULL,'Jason Eaton',NULL,'St Marys Depot',49000125,'Ainsbury Road','10-04-2018',1,'Paul Brown','Manager',NULL,'','Yes','RiskTech',NULL,'80','Metal',NULL,'Y',1,'','St Marys','2760',1,'2018-04-26 08:44:05',''),
	(310,'',NULL,NULL,NULL,NULL,'Jason Eaton',NULL,'St Marys Depot',49000125,'Ainsbury Road','10-04-2018',1,'Paul Brown','Manager',NULL,'Demountable','Yes','RiskTech',NULL,'80','Metal',NULL,'Y',1,'','St Marys','2760',1,'2018-04-26 08:57:54',''),
	(311,'Office A///Team Leaders Storage///Office B///Office C///Office D///Office E///Office F',NULL,NULL,NULL,NULL,'Jason Eaton',NULL,'St Marys Depot',49000125,'1 Ainsbury Road','10-04-2018',1,'Paul Brown','Manager',NULL,'Metal Clad Demountables','Yes','RiskTech',NULL,'100','Metal',NULL,'N',1,'','St Marys','2760',1,'2018-04-26 10:02:09','<p>The site consist of 7 demountable buildings used for office, ammenities &amp; conferance rooms. 3 shed structures are used for storage &amp; workshops.</p>'),
	(312,'General Grounds ///Amenities Block///Site Office///Various Storage Containers ///Lunch Room',NULL,NULL,NULL,NULL,'',NULL,'Narrabri West Depot ',49000091,'Cnr Newell & Kamilaroi Highway','16-01-2018',1,'Simone Walsh','',NULL,'Metal','','RiskTech ',NULL,'1000','Metal',NULL,'N',1,'','Narrabri West ','2390',1,'2018-04-26 10:52:06',''),
	(313,'Carpenters Workshop///Ammenities Block///Main Office & Team Leaders Office///Shed 1///Shed 2///Shed 3///Shed 4///Sheds 5 & 6///Shed 7///Shed 8///Sheds 1 - 6///Bridge Section',NULL,NULL,NULL,NULL,'Gavin ',NULL,'Windsor Depot',49000121,'66 - 68 Mileham Street','10-04-2018',1,'Paul Brown','Manager',NULL,'Metal/Fibre Cement/Block work & Concrete','Yes','RiskTech',NULL,'3000','Metal',NULL,'N',1,'','Windsor','2756',1,'2018-04-26 13:37:59','<p>The site consist of&nbsp;4 permanent buildings &amp; 8 demountable or semi-permanent structures.</p>'),
	(314,'Motor Registry ',NULL,NULL,NULL,NULL,'',NULL,'Moree Motor Registry ',49000614,'57 Balo Street','18-01-2018',2,'Simone Walsh','',NULL,'Brick and Concrete ','Yes','RiskTech',NULL,'225','Metal',NULL,'N',1,'','Moree','2400',1,'2018-04-26 12:01:33',''),
	(315,'Residence',NULL,NULL,NULL,NULL,'',NULL,'Ulmarra Ferry House ',49000628,'139 Southgate Ferry Road','19-01-2018',1,'Simone Walsh','',NULL,'Brick, Timber & Fibre Cement ','Yes','RiskTech ',NULL,'100 sqm','Metal',NULL,'N',1,'','Southgate ','2460',1,'2018-04-26 13:35:09',''),
	(316,'Office & Staff Ammenities///Sand Blasting///Stores & Workshops',NULL,NULL,NULL,NULL,'Gavin ',NULL,'Mortlake Ferry - Leased Land',49000773,'35 Hilly Street','11-04-2018',2,'Paul Brown','Manager',NULL,'Brick/Concrete','Yes','RiskTech',NULL,'300','Metal',NULL,'N',1,'','Mortlake','2137',1,'2018-04-26 14:57:29',''),
	(317,'Ferry Terminus',NULL,NULL,NULL,NULL,'Gavin ',NULL,'Mortlake Ferry',49000013,'33A Hilly Street','11-04-2018',1,'Paul Brown','Manager',NULL,'Brick & Concrete','Yes','RiskTech',NULL,'120','Metal',NULL,'N',1,'','Mortlake','2137',1,'2018-04-26 15:11:17',''),
	(318,'Site Office/Workshop///Staff Amenities Block ///Small Rope Store Shed///Storage Garage with Roller Door (East of Site Office/Workshop)///Lunch Shed (East of Site Office/Workshop)///Steel Framed Shed adjacent Site Entrance',NULL,NULL,NULL,NULL,'',NULL,'Ashby Dry Dock',49000720,'Clarence Street','19-01-2018',1,'Simone Walsh','',NULL,'','Yes','RiskTech',NULL,'','',NULL,'N',1,'','Ashby','2463',1,'2018-05-01 15:06:11',''),
	(319,'Site Office Demountable///The Vault///Flammable Liquids Store ///Amenities Block///Asphalt Storage Tank///Shipping Container 1///Shipping Container 2///Site Office ///Bridge Shed///Large Metal Shed///Bridge Services Shed',NULL,NULL,NULL,NULL,'',NULL,'Woodburn Depot',49000104,'17 Trustums Hill Road ','22-01-2018',1,'Simone Walsh','',NULL,'Metal','Yes','RiskTech',NULL,'40,000','Metal',NULL,'N',1,'','Woodburn','2472',1,'2018-05-02 15:25:56',''),
	(320,'Office',NULL,NULL,NULL,NULL,'',NULL,'Ewingsdale Office ',49005056,'McClouds Shoot Tunnel Office, St. Helena Road ','23-01-2017',1,'Simone Walsh','',NULL,'Metal','No','',NULL,'600','Metal',NULL,'N',1,'','Ewingsdale','2481',1,'2018-05-02 15:39:54',''),
	(321,'Shed 1///ATCO Demountable///Main Shed/Workshop///Bitumen Tank Bund Area ',NULL,NULL,NULL,NULL,'',NULL,'Billinudgel Depot ',49000565,'Brunswick Street','24-01-2018',1,'Simone Walsh','',NULL,'','Yes','RiskTech',NULL,'','Metal',NULL,'N',1,'','Billinudgel ','2483',1,'2018-05-02 15:58:14','');

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`, `hide`)
VALUES
	(3,'A','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',1,'April 11, 2018, 3:46 pm','N'),
	(39,'B','risktechadmin@swim.com.au','b3d803edd45aff5a9196c0a43811cbef','Risktech Admin','risktechadmin@swim.com.au','A',0,'May 9, 2018, 2:32 pm','N'),
	(40,'C','clienttest@swim.com.au','ff244385fb10642d6238b98782e83177','Client Test','clienttest@swim.com.au','A',1,'September 22, 2017, 12:10 pm','N'),
	(41,'C','Dvandevorstenbosch@wcd.net.au','6adadca9fc8ae5020cb91652d4c69a2a','Test','Dvandevorstenbosch@wcd.net.au','A',24,'November 30, 2017, 8:41 am','N'),
	(42,'C','GREGHARRADINE@GMAIL.com.au','d93591bdf7860e1e4ee2fca799911215','Greg Harradine','gregharradine@gmail.com','A',24,'November 30, 2017, 2:43 pm','N');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Clients','C');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: haztechc_portal
# Generation Time: 2016-08-15 07:25:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`)
VALUES
	(1,'Sample Blog Post','29/04/2016','demouser','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','public'),
	(2,'Sample Blog Post','29/04/2016','demouser','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','public'),
	(3,'Sample Blog Post','29/04/2016','demouser','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','public');

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_site
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site`;

CREATE TABLE `tbl_site` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `site_phone` varchar(255) DEFAULT NULL,
  `site_property_number` varchar(255) DEFAULT NULL,
  `site_details_name` varchar(255) DEFAULT NULL,
  `site_details_levels` varchar(255) DEFAULT NULL,
  `site_details_age` varchar(255) DEFAULT NULL,
  `site_details_type` varchar(255) DEFAULT NULL,
  `site_details_area` varchar(255) DEFAULT NULL,
  `site_details_roof` varchar(255) DEFAULT NULL,
  `site_details_description` text,
  `site_contact_name` varchar(255) DEFAULT NULL,
  `site_contact_jobtitle` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `site_siteplan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_site_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_plan`;

CREATE TABLE `tbl_site_plan` (
  `siteplanID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `filedata` text,
  `filename` text,
  `filetype` varchar(10) DEFAULT NULL,
  `date_uploaded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteplanID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_upload
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_upload`;

CREATE TABLE `tbl_upload` (
  `fileID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filedata` text,
  `filename` text,
  `areaconected` varchar(10) DEFAULT NULL,
  `atachID` varchar(10) DEFAULT NULL,
  `date_uploaded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`)
VALUES
	(3,'A','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',NULL,'August 15, 2016, 5:17 pm');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Risktech General','C'),
	(4,'Risktech Contractor','D'),
	(5,'Client Admin','E'),
	(6,'Client General','F');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

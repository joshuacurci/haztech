# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: haztechc_portal
# Generation Time: 2016-10-25 04:52:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_au_states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_au_states`;

CREATE TABLE `tbl_au_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(3) NOT NULL DEFAULT '',
  `state_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_au_states` WRITE;
/*!40000 ALTER TABLE `tbl_au_states` DISABLE KEYS */;

INSERT INTO `tbl_au_states` (`id`, `state_code`, `state_name`)
VALUES
	(1,'NSW','New South Wales'),
	(2,'QLD','Queensland'),
	(3,'SA','South Australia'),
	(4,'TAS','Tasmania'),
	(5,'VIC','Victoria'),
	(6,'WA','Western Australia'),
	(7,'ACT','Australian Capital Territory'),
	(8,'NT','Northern Territory');

/*!40000 ALTER TABLE `tbl_au_states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(1,'Sample Blog Post 1','0000-00-00 00:00:00','5','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','Array',NULL),
	(2,'Sample Blog Post 3','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(3,'Sample Blog Post','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(7,'Sample Blog Post 2','2016-08-16 00:00:00','3','<p>This is test 2</p>','',NULL,NULL),
	(8,'Test','2016-10-24 00:00:00',NULL,'<p>Test</p>','',NULL,NULL),
	(9,'','2016-10-24 00:00:00',NULL,'',NULL,NULL,NULL),
	(10,'','2016-10-24 00:00:00',NULL,'',NULL,NULL,NULL),
	(11,'','2016-10-24 00:00:00',NULL,'',NULL,NULL,NULL),
	(12,'','2016-10-24 00:00:00',NULL,'',NULL,NULL,NULL),
	(13,'','2016-10-24 00:00:00',NULL,'',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_access`;

CREATE TABLE `tbl_blog_access` (
  `blogaccessID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(5) DEFAULT NULL,
  `type_letter` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`blogaccessID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_access` WRITE;
/*!40000 ALTER TABLE `tbl_blog_access` DISABLE KEYS */;

INSERT INTO `tbl_blog_access` (`blogaccessID`, `blogID`, `type_letter`)
VALUES
	(5,2,'0'),
	(8,1,'B'),
	(9,1,'D');

/*!40000 ALTER TABLE `tbl_blog_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_images`;

CREATE TABLE `tbl_blog_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_blog_images` WRITE;
/*!40000 ALTER TABLE `tbl_blog_images` DISABLE KEYS */;

INSERT INTO `tbl_blog_images` (`imageID`, `blogID`, `full_path`)
VALUES
	(3,2,'greencap-logo.png'),
	(4,13,'lod-search.png');

/*!40000 ALTER TABLE `tbl_blog_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blogID
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blogID`;

CREATE TABLE `tbl_blogID` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_blogID` WRITE;
/*!40000 ALTER TABLE `tbl_blogID` DISABLE KEYS */;

INSERT INTO `tbl_blogID` (`id`, `blogID`)
VALUES
	(1,13);

/*!40000 ALTER TABLE `tbl_blogID` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `site_address`, `contact_name`, `contact_number`, `hide`)
VALUES
	(1,'GreenCap','Hawthorn West 3122','test','0000000','N'),
	(7,'Goodman Property','12 Mars Road, lane Cove NSW 2066','test Goodman ','0000000','Y'),
	(8,'Haztech','123 Lean Road, VIC 2210','Jim Doe','0000000','N'),
	(9,'','','','','Y'),
	(10,'','','','','Y'),
	(11,'testname','building name','inspected','contact number','N'),
	(12,'testname','building name','','','N'),
	(13,'testname1','building name','inspected','contact number','N'),
	(14,'testname3','building name','inspected','contact number','N'),
	(15,'test123','test123','test123','test123','N'),
	(16,'test16','test16','test16','test16','N');

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_images`;

CREATE TABLE `tbl_client_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_client_images` WRITE;
/*!40000 ALTER TABLE `tbl_client_images` DISABLE KEYS */;

INSERT INTO `tbl_client_images` (`imageID`, `clientID`, `full_path`)
VALUES
	(1,1,'greencap-logo2.png'),
	(2,8,'risktech-logo1.png'),
	(3,0,'reporting.png'),
	(4,11,'reporting2.png'),
	(5,13,'checked2.png'),
	(6,14,'checked1.png'),
	(7,15,'reporting4.png'),
	(8,16,'reporting5.png');

/*!40000 ALTER TABLE `tbl_client_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_clientID
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_clientID`;

CREATE TABLE `tbl_clientID` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_clientID` WRITE;
/*!40000 ALTER TABLE `tbl_clientID` DISABLE KEYS */;

INSERT INTO `tbl_clientID` (`id`, `clientID`)
VALUES
	(1,16);

/*!40000 ALTER TABLE `tbl_clientID` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_global
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_global`;

CREATE TABLE `tbl_global` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_global` WRITE;
/*!40000 ALTER TABLE `tbl_global` DISABLE KEYS */;

INSERT INTO `tbl_global` (`id`, `description`, `value`)
VALUES
	(1,'photo_number','15 ');

/*!40000 ALTER TABLE `tbl_global` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_hazard_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_hazard_types`;

CREATE TABLE `tbl_hazard_types` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_hazard_types` WRITE;
/*!40000 ALTER TABLE `tbl_hazard_types` DISABLE KEYS */;

INSERT INTO `tbl_hazard_types` (`typeID`, `type_name`)
VALUES
	(1,'Asbestos'),
	(2,'SMF Products'),
	(3,'Lead Products'),
	(4,'PCBs');

/*!40000 ALTER TABLE `tbl_hazard_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_images`;

CREATE TABLE `tbl_item_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_images` WRITE;
/*!40000 ALTER TABLE `tbl_item_images` DISABLE KEYS */;

INSERT INTO `tbl_item_images` (`imageID`, `itemID`, `full_path`)
VALUES
	(1,20,'default-image.png'),
	(2,24,'default-image1.png'),
	(3,22,'default-image2.png'),
	(4,44,'default-image3.png'),
	(5,45,'default-image4.png'),
	(6,63,'reporting.png');

/*!40000 ALTER TABLE `tbl_item_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_materials`;

CREATE TABLE `tbl_item_materials` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_material` varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_materials` WRITE;
/*!40000 ALTER TABLE `tbl_item_materials` DISABLE KEYS */;

INSERT INTO `tbl_item_materials` (`itemID`, `item_material`, `group`)
VALUES
	(1,'Adhesive','Asbestos'),
	(2,'Arc shields','Asbestos'),
	(3,'Bituminous material','Asbestos'),
	(4,'Bituminous membrane','Asbestos'),
	(5,'Compressed cement sheet','Asbestos'),
	(6,'Construction joint mastic','Asbestos'),
	(7,'Corrugated cement sheet','Asbestos'),
	(8,'Dust','Asbestos'),
	(9,'Electrical backing board','Asbestos'),
	(10,'Fibre cement sheet','Asbestos'),
	(11,'Fire door core','Asbestos'),
	(12,'Friction material','Asbestos'),
	(13,'Friction pads','Asbestos'),
	(14,'Galbestos','Asbestos'),
	(15,'Gasket material','Asbestos'),
	(16,'Insulation','Asbestos'),
	(17,'Lagging','Asbestos'),
	(18,'Low density fibre board','Asbestos'),
	(19,'Mastic sealant','Asbestos'),
	(20,'Millboard insulation','Asbestos'),
	(21,'Moulded fibre cement','Asbestos'),
	(22,'Packing','Asbestos'),
	(23,'Pointing','Asbestos'),
	(24,'Screed','Asbestos'),
	(25,'Sheet vinyl','Asbestos'),
	(26,'Sheet vinyl & adhesive','Asbestos'),
	(27,'Sheet vinyl - fibrous backed','Asbestos'),
	(28,'Sheet vinyl - Hessian backed','Asbestos'),
	(29,'Sprayed vermiculite','Asbestos'),
	(30,'Sprayed limpet','Asbestos'),
	(31,'Textured coatings','Asbestos'),
	(32,'Vermiculite','Asbestos'),
	(33,'Vinyl floor tiles','Asbestos'),
	(34,'Vinyl floor tiles & adhesives','Asbestos'),
	(35,'Window caulking','Asbestos'),
	(36,'Woven material','Asbestos'),
	(37,NULL,NULL),
	(38,NULL,NULL),
	(39,NULL,NULL);

/*!40000 ALTER TABLE `tbl_item_materials` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_itemID
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_itemID`;

CREATE TABLE `tbl_itemID` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_itemID` WRITE;
/*!40000 ALTER TABLE `tbl_itemID` DISABLE KEYS */;

INSERT INTO `tbl_itemID` (`id`, `itemID`)
VALUES
	(1,64);

/*!40000 ALTER TABLE `tbl_itemID` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `item_no` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `room_specific` varchar(255) DEFAULT NULL,
  `hazard_type` int(11) DEFAULT NULL,
  `samples_taken` varchar(225) DEFAULT NULL,
  `sample_no` varchar(255) DEFAULT NULL,
  `sample_status` varchar(11) DEFAULT NULL,
  `extent` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_number` mediumint(25) DEFAULT NULL,
  `recommendations` longtext,
  `item_condition` varchar(255) DEFAULT NULL,
  `disturb_potential` varchar(255) DEFAULT NULL,
  `risk_rating` varchar(255) DEFAULT NULL,
  `current_label` varchar(11) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `control_priority` varchar(255) DEFAULT NULL,
  `work_records` varchar(255) DEFAULT NULL,
  `description` longtext,
  `photo_no` text,
  `location_level` varchar(255) DEFAULT NULL,
  `friability` varchar(255) DEFAULT NULL,
  `control_recommendation` longtext,
  `hide` char(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_items` WRITE;
/*!40000 ALTER TABLE `tbl_items` DISABLE KEYS */;

INSERT INTO `tbl_items` (`itemID`, `siteID`, `item_no`, `item_name`, `room_specific`, `hazard_type`, `samples_taken`, `sample_no`, `sample_status`, `extent`, `contact`, `contact_number`, `recommendations`, `item_condition`, `disturb_potential`, `risk_rating`, `current_label`, `date_updated`, `control_priority`, `work_records`, `description`, `photo_no`, `location_level`, `friability`, `control_recommendation`, `hide`, `clientID`)
VALUES
	(6,1,1,'Material','Meeting Room',4,'yes','1','Negative','300 m2','Test',0,NULL,'Good','Non-selected','Low','Yes','2016-09-15 16:58:23','Non-selected','No Action','Roof - Corrugated cement','4','External - Ground','Friable','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim<span style=\"color: #666666; font-family: Verdana, Geneva, sans-serif; font-size: 10px;\">.</span></p>','N',1),
	(7,5,2,'Test','Meeting Room',0,'Yes','1','Positive','1','Contact Name',1231212,NULL,'Good','test','Test','Test','2016-09-29 15:01:23','Test','Test',NULL,NULL,NULL,NULL,NULL,'N',7),
	(18,17,NULL,NULL,'',NULL,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-03 11:35:54','',NULL,'',NULL,'','','','Y',8),
	(19,1,NULL,NULL,'',NULL,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 15:35:57','',NULL,'',NULL,'','','','Y',1),
	(20,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:33:25','P1',NULL,'Beams - Sprayed Vermiculite','6 ','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(21,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 15:36:01','',NULL,'',NULL,'','','','Y',1),
	(22,1,NULL,NULL,'Ceiling',4,NULL,'J108740-AU019-52','','1 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Medium','Suspect','2016-10-12 09:16:02','P1',NULL,'Compressed cement sheet','3 ','Level 2','Friable','<p>Engage a license asbestos conrtactor to undertake remedial/removal works on this item as soon as practicable (within 3 months)</p>','N',1),
	(23,1,NULL,NULL,'South',4,NULL,'Similar to J108740-AU019-52','','7 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect','2016-09-15 16:58:07','P2',NULL,'Service Riser - Moulded Fibre Cement Flue','2','Wasrehouse - Underside Of  Roof','Friable','<p>Maintain in current condition, label and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',1),
	(24,17,NULL,NULL,'Underside of roof',4,NULL,'','','1000m2',NULL,NULL,NULL,'Bad','High Accessibility','Low','Suspect','2016-10-03 11:36:14','P3',NULL,'Roof Lining - Insulation Material','10','Warehouse','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Removed under controlled conditions prior to demolition or refurbishment</p>','N',8),
	(25,177,NULL,NULL,'North',1,NULL,'','Assumed Neg','500m2',NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect','2016-10-03 11:43:06','P3',NULL,'Beams - Sprayed Vermiculite',NULL,'Warehouse','Friable','<p>Maintain in Good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',1),
	(26,177,NULL,NULL,'Interior - Unit 14',2,NULL,'','Suspected P','3 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect','2016-10-03 11:43:26','P3',NULL,'Hot water sevice insulation - Insulation material',NULL,'Ground Level','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under contolled conditions prior to demolition or refurbishment.</p>','N',1),
	(27,177,NULL,NULL,'Office Area - Throughout',2,NULL,'','Suspected P','4000m2',NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect','2016-10-03 11:43:30','P3',NULL,'Ceiling Tiles - INsulation Material',NULL,'Level One','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment&nbsp;</p>','N',1),
	(28,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:36:36','P1',NULL,'Beams - Sprayed Vermiculite','7','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(29,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 15:36:01','',NULL,'',NULL,'','','','Y',1),
	(30,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:36:44','P1',NULL,'Beams - Sprayed Vermiculite','8','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(31,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-20 17:55:55','',NULL,'','2','','','','Y',1),
	(32,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:36:52','P1',NULL,'Beams - Sprayed Vermiculite','5','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(33,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:36:48','P1',NULL,'Beams - Sprayed Vermiculite','11 ','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(34,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:37:18','P1',NULL,'Beams - Sprayed Vermiculite','3','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(35,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:37:48','P1',NULL,'Beams - Sprayed Vermiculite','12 ','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(36,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:37:57','P1',NULL,'Beams - Sprayed Vermiculite','13','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(37,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-10-03 11:39:09','P1',NULL,'Beams - Sprayed Vermiculite','14 ','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',8),
	(38,17,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-03 11:38:30','',NULL,'',NULL,'','','','N',8),
	(39,17,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-03 11:38:05','',NULL,'',NULL,'','','','N',8),
	(40,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-12 09:19:18','',NULL,'',NULL,'','','','Y',1),
	(41,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:52:15','',NULL,'',NULL,'','','','Y',1),
	(42,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:52:41','',NULL,'',NULL,'','','','Y',1),
	(43,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 12:05:57','',NULL,'',NULL,'','','','Y',1),
	(44,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:56:11','',NULL,'',NULL,'','','','Y',1),
	(45,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:54:12','',NULL,'',NULL,'','','','Y',1),
	(46,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 12:05:54','',NULL,'',NULL,'','','','Y',1),
	(47,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-12 09:20:21','',NULL,'',NULL,'','','','Y',1),
	(48,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:57:03','',NULL,'',NULL,'Backing Board','','','Y',1),
	(49,1,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-24 14:57:36','',NULL,'',NULL,'','','','Y',1),
	(50,0,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'','','','N',11),
	(51,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'','','','N',1),
	(52,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'','','','N',1),
	(53,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test-Asbestos','','','N',1),
	(54,177,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test','','','N',177),
	(55,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test-Asbestos1','','','N',1),
	(56,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test-1','','','N',1),
	(57,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test Item Material','','','N',1),
	(58,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:50:09','',NULL,'',NULL,'Test Timestamp','','','N',1),
	(59,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:55:34','',NULL,'',NULL,'Backing Board','','','Y',1),
	(60,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','0000-00-00 00:00:00','',NULL,'',NULL,'Test Timestamp','','','N',1),
	(61,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','0000-00-00 00:00:00','',NULL,'',NULL,'','','','N',1),
	(62,1,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 11:55:02','',NULL,'',NULL,'Timestamp123','','','N',1),
	(63,252,NULL,NULL,'',0,NULL,'','','',NULL,NULL,NULL,'','','','','2016-10-25 12:24:25','',NULL,'',NULL,'','','','Y',252),
	(64,247,NULL,NULL,'',4,NULL,'','','',NULL,NULL,NULL,'','Non-selected','Non-selected','','2016-10-25 12:27:10','Non-selected',NULL,'','15 ','Test Item Material 2','','','N',247);

/*!40000 ALTER TABLE `tbl_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report`;

CREATE TABLE `tbl_report` (
  `reportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT '',
  `scope_of_works` varchar(255) DEFAULT '',
  `recommendations` varchar(255) DEFAULT NULL,
  `methologies` varchar(255) DEFAULT NULL,
  `risk_factors` varchar(255) DEFAULT NULL,
  `priority_rating_system` varchar(255) DEFAULT NULL,
  `asbestos_mng_req` varchar(255) DEFAULT NULL,
  `haz_material_mr` varchar(255) DEFAULT NULL,
  `statement_of_limitations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report` WRITE;
/*!40000 ALTER TABLE `tbl_report` DISABLE KEYS */;

INSERT INTO `tbl_report` (`reportID`, `siteID`, `introduction`, `scope_of_works`, `recommendations`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,17,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123 Victoria Street. The risk assessment was performed by Nigel johnson on 01-01-2008.</p>','Y','Y','Y','Y','N','N','Y','N'),
	(2,1,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 445. The risk assessment was performed by Andrew Pakeham on 10-08-2016.</p>','N','N','N','N','N','N','Y','N'),
	(3,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(4,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(5,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(6,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(7,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(8,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(9,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(10,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(11,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(12,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(13,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(14,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(15,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for 8 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(16,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(17,118,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(18,119,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(19,200,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(20,120,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(21,121,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(22,122,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(23,206,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(24,123,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(25,124,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(26,125,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(27,126,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(28,127,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(29,178,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(30,179,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(31,220,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(32,221,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(33,222,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at street address. The risk assessment was performed by inspected by on 21-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(34,223,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(35,224,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(36,225,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(37,226,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(38,227,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(39,228,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(40,229,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(41,230,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(42,231,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(43,232,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(44,233,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(45,234,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(46,235,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(47,236,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(48,237,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(49,238,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(50,239,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(51,240,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(52,241,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(53,242,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(54,243,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(55,244,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(56,245,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y');

/*!40000 ALTER TABLE `tbl_report` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report_common
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report_common`;

CREATE TABLE `tbl_report_common` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recommendations` longtext,
  `scope_of_works` longtext,
  `methologies` longtext,
  `risk_factors` longtext,
  `priority_rating_system` longtext,
  `asbestos_mng_req` longtext,
  `haz_material_mr` longtext,
  `statement_of_limitations` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report_common` WRITE;
/*!40000 ALTER TABLE `tbl_report_common` DISABLE KEYS */;

INSERT INTO `tbl_report_common` (`id`, `recommendations`, `scope_of_works`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,'<p>Findings &amp; Recommendations Recommendations</p>\r\n<ul>\r\n<li>Schedule periodic re-assessments of the asbestos-containing materials remaining in-situ to monitor their condition in accordance with the Code of Practice.</li>\r\n<li>Provide Asbestos Awareness training to staff and site personnel in accordance with the requirements of the Code of Practice.</li>\r\n<li>Consult with staff and health and safety representatives on the findings of this risk assessment and this report must be made available upon request, in accordance with the requirements of the Code of Practice.</li>\r\n<li>Areas highlighted in the Areas Not Accessed section as areas of \'no access\' should be presumed to contain hazardous materials. Appropriate management planning should be implemented in order to control access to and maintenance activities in these areas, until such a time as they can be inspected and the presence or absence of hazardous materials can be confirmed.</li>\r\n<li>Should any personnel come across any suspected asbestos or hazardous materials, work should cease immediately in the affected areas until further sampling and investigation is performed.</li>\r\n<li>Prior to demolition/refurbishment works undertake a destructive hazardous materials survey of the premises as per the requirements of AS 2601: 2001 The Demolition of Structures, Part 1.6.1.&nbsp;</li>\r\n<li>Synthetic Mineral Fibre (SMF) materials should be removed under controlled conditions prior to demolition/refurbishment works, in accordance with the requirements of the Code of Practice for the Safe Use of Synthetic Mineral Fibres [NOHSC:2006(1990)].</li>\r\n<li>Noel Arnold &amp; Associates can assist with the implementation of any of the above recommendations.</li>\r\n</ul>','<h3>Scope of Works</h3>\r\n<p>The Scope of Works for this assessment was as follows:</p>\r\n<ul>\r\n<li>Review previus hazardous materials assessments and associated documentation</li>\r\n<li>Inspect representative &nbsp;and accessible internal and external areas of the site to re-assess previously identified hazardous materials. Hazardous materials assessed includ:</li>\r\n<ul>\r\n<li>Asbestos containing meaterial (ACM)</li>\r\n<li>Synthetic Mineral Fibers (SMF)</li>\r\n<li>Polychlorinated biphenyls (PCBs); &amp;</li>\r\n<li>Lead Pain.</li>\r\n</ul>\r\n<li>Assess the current condition of hazardous materials</li>\r\n<li>Identify the likelihood of asbestos/hazardous materials in inaccessible areas</li>\r\n<li>Collect samples of suspected asbestos materials and have them analysed in a NATA-accredited laboratory</li>\r\n<li>undertake representative lead paint testing using indicative lead swab checks</li>\r\n<li>compile an up to date asbestos/hazardous materials register.</li>\r\n</ul>','<p>Asbestos This assessment was undertaken in accordance with the following documents and within the constraints of the scope of works: How to Manage and Control Asbestos in the Workplace: Code of Practice (Safe Work Australia, 2011) NSW Work Health &amp; Safety Regulation 2011 2 representative samples of suspected asbestos-containing material were collected and placed in plastic bags with clip-lock seals.</p>\r\n<p>These samples were analysed in Greencap\'s NATA-accredited laboratory for the presence of asbestos by Polarised Light Microscopy.&nbsp;Where it was determined that asbestos was present, a risk and priority assessment was conducted in accordance with Greencap\'s standard Risk Assessment and Priority Ranking System.</p>\r\n<p>Refer to section on Priority Rating System for detailed information on this system. Inaccessible areas that are likely to contain asbestos have been assumed to contain asbestos until further inspection and analysis of samples has been undertaken by an approved analyst. Limited destructive sampling techniques have been used to gain access into restricted areas for the purpose of determining the likelihood of asbestos or other hazardous materials in these areas.&nbsp;Due to the nature of the survey methodology, it is possible that not every area of the site have been accessed. Reference should be made to the \'Areas Not Accessible\' section of this report for further details. Subject to the limitations associated with the scope of works, this audit was conducted in accordance with the requirements of AS 2601-2001 The Demolition of Structures.</p>\r\n<p>The survey methodology utilised in this assessment has been NATA-Accredited to meet the requirements of ISO 17020 Conformity assessment - General criteria for the operation of various types of bodies performing inspections SMF Synthetic Mineral Fibre (SMF) Accessible areas where Synthetic Mineral Fibre (SMF) insulation was visually confirmed as being present were noted to give a general indication to the presence of materials throughout the building.</p>\r\n<p>PCB Polychlorinated Biphenyls (PCBs) Representative light fittings containing capacitors were inspected where safely practicable and details noted for cross-referencing with the ANZECC Identification of PCB-Containing Capacitors - 1997. Where metal capacitors were not listed on the database, these capacitors are noted as suspected to contain polychlorinated biphenyls.</p>\r\n<p>Lead Paint Representative painted surfaces were tested unobtrusively for the presence of lead using the LeadCheck paint swab method. This method can give an instantaneous qualitative result and reproducibly detect lead in paints at concentrations of 0.5% (5,000ppm) and above, and may indicate lead in some paint films as low as 0.2% (2,000ppm). The sampling program was representative of the various types of paints found within the site, concentrating on areas where lead based paints may have been used (Eg. Gloss paints on doors, railings, guttering and downpipes, columns, window and door architraves, skirting boards etc). The objective of lead paint identification in this survey is to highlight the presence of lead-based paints within the building, not to specifically quantify every source of lead-based paint.</p>','<p>To assess the health risk posed by the presence of asbestos-containing material, all relevant factors must be considered. These factors include:</p>\r\n<ul>\r\n<li>Evidence of physical damage;</li>\r\n<ul>\r\n<li>Evidence of water damage;</li>\r\n<li>Proximity of air prenums and direct air stream;</li>\r\n<li>Friability of asbestos material;</li>\r\n<li>Requirement for access for building operations;</li>\r\n<li>Requirement for access for maintenance operations;</li>\r\n<li>Likelihood of disturbance of the asbestos material;</li>\r\n<li>Accessibility;</li>\r\n<li>Exposed surface area; and</li>\r\n<li>Environmental conditions</li>\r\n</ul>\r\n</ul>\r\n<p>These aspects are in turn judged upon: (i) potential to fibre generation, and, (ii) the potential for exposure. Where these factors have indicated that theree is a possibility of exposure to airborne fibres, appropriate recommendations for repaur, maintenance or abatement of the asbestos-containing materials are made.&nbsp;</p>\r\n<h3>Condition</h3>\r\n<p>The condition of tehe asbestos products identified during the survey is usually reported as either being good or poor.</p>\r\n<p><em><strong>Good :-&nbsp;</strong></em> refers to asbestos materials, which have not been damaged or have not deteriorated.</p>\r\n<p><em><strong>Fair :-</strong>&nbsp;</em>refers to asbestos material having suffered minor cracking or de-surfacing.</p>\r\n<p><em><strong>Poor :-&nbsp;</strong></em>describes asbestos materials which have been damaged or their condition has deteriorated over time.</p>\r\n<h3>Friability</h3>\r\n<p>The friability of asbestos products describes the ease of which the material can be crumbled, and hence to release fibres.</p>\r\n<p><em><strong>Friable asbestos :-&nbsp;</strong></em>(e.g. limpet beam nsulation, pipe lagging) can be easily crumbled and is more hazardous than non-friable asbestos products.</p>\r\n<p><em><strong>Non-Friable asbestos :-&nbsp;</strong></em>common known as bonoded asbestos, is typically comprised of asbestos fibres tightly bound in sable non-asbestos matrix. Example of non-friable asbestos &nbsp;products include asbestos cement materials (sheeting, pipes etc), asbestos containing vinyl floor tiles and electrical backing boards.</p>\r\n<h3>Accessibility/Disturbance Potential</h3>\r\n<p>Asbestos products can be classified as having low, medium or high accessibility/disturbance potential.</p>\r\n<p><em><strong>Low accessibility&nbsp;</strong></em>describes asbestos products that cannot be easily disturbed, such as materials in building voids, set ceilings, etc.</p>\r\n<p><em><strong>High accessibility&nbsp;</strong></em>asbestos products can be easily accessed or damaged due to their close proximity to personnel, e.g. asbestos cement walls or down pipes.</p>\r\n<h3>Risk Status</h3>\r\n<p>The risk factors described above are used to rank the health risk posed by the presence of asbestos-containing materials.</p>\r\n<p>A <em><strong>low risk ranking&nbsp;</strong></em>describes asbestos materials that pose a low health risk to personnel, employees and the general public providing they stay in a stable condition, for example asbestos materials that are in good conditiin and have low accessibility.</p>\r\n<p>A&nbsp;<em><strong>medium risk ranking</strong></em> applies to materials that pose an increased risk to people in the area.</p>\r\n<p>Asbestos materials that possess a&nbsp;<em><strong>high-risk ranking&nbsp;</strong></em>pose a high health risk to personnel or the public in the area of the material. Materials with a high-risk ranking will also possess a Priority 1 recommendation to manage the asbestos and reduce the risk.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"padding-left: 60px;\">&nbsp;</p>','<h3>Asbestos Priority Rating System for Control Recommendations</h3>\r\n<p>The following priority rating system is adopted to assist in the programming and budgeting of the control od asbestos risk identified at the site</p>\r\n<h4>Priority 1 (P1): Hazard with Elevated Risk Potential / Organise Remedial Works Immediately</h4>\r\n<p>Area has hazardous materials, which are either damaged or are being exposed to continual disturbance. Due to these conditions there is an increased potential for exposure and/or transfer of the material to other parts &nbsp;with continued unrestricted use of this area. It is recommended that the area be isolated, air monitoring be conducted (if relevant) and the hazardous material promptly removed.</p>\r\n<h4>Priority 2 (P2): Hazard with Moderate Risk Potential / Organise Remedial Works Within 3 Months</h4>\r\n<p>Area has hazardous materials with a potential for disturbance due to the following conditions:</p>\r\n<ol>\r\n<li>Material has been disturbed or damaged and its current condition, while not posing an immediate hazard, is unstable;</li>\r\n<li>The material is accessible and can, when disturbed, present a short-term exposure risk; or</li>\r\n<li>Demolition, refurbishment or maintenance works including new installations or modification to air-handling systems, ceilings, lighting, fire safety systems, or floor laylouts.</li>\r\n</ol>\r\n<p>Appropriate abatement measures to be taken as soon as is practical (within 3 months). Negligible health risks if&nbsp;materials remain undisturbed under the control of a hazardous materials management plan.</p>\r\n<h4>Priority 3 (P3): Hazard with Low Risk Potential</h4>\r\n<p>Area has hazardous materials where:</p>\r\n<ol>\r\n<li>The condition of any friable hazardous material is stable and has a low potential for distubance; or</li>\r\n<li>The hazardous material is in a non-friable condition and does not present an exposure risk unless cut, drilled, sanded or otherwise abraded.</li>\r\n</ol>\r\n<p>negligible health risks if the materials are left undisturbed under the control of a hazardous material management plan. Monitor condition during subsequent reviews. Defer abatement unless materials are to be disturbed as a result of maintenance, refurbisment or demolition activities.</p>\r\n<h4>Priority 4 (P4): Hazard with&nbsp;Negligible Risk Potential</h4>\r\n<p>The hazardous material is in non-friable form and in a good condition. It is most unlikely that the material can be disturbed under normal circumstances and can be safety subjected to normal traffic. Even id it were subjeted to minor disturbance the material poses a negligible health risk. Monitor condition during subsequent reviews. Defer abatement unless materials are to be disturbed as a reult of maintenance, refurbishment or demolition activities.</p>\r\n<h3>Labelling Requirements</h3>\r\n<p>Materials confirmed or suspected to contain asbestos should be clearly labelled in accordance with the reuirements outlined in current, relevant state health and safety regulations and Safe Work Australia Code of Practice for the Management and Control of Asbestos in Workplaces [NOHSC: 2018(2005)].</p>\r\n<h4>&nbsp;</h4>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','<p>This is a test asbestos management requirement</p>','<p>hazardous material</p>','<p>This is a non-destructive assessment for occupational purposes. It it not to be used for any major refurbishment or demolition, where a more invasive destructive survey would be undertaken in line with plans for re-development.</p>\r\n<p>In accordance wit &nbsp;the NSW Work Health and Safety Regulation, 2011 inaccessible areas that are likely to asbestos must be presumed as containing asbestos material until further inspection and analysisof samples has been undertaken by an approved analyst.</p>\r\n<p>Typical areas likely to be deemed inaccessible under this regulation are:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Height restricted areas e.g. Inaccessible ceiling/roof spaces;</li>\r\n<li>Inaccessible sub-floor space/tunnels;</li>\r\n<li>Under carpet/vinyl floor coverings;</li>\r\n<li>Wall cavities/partitions;</li>\r\n<li>Behind ceramic wall tiles;</li>\r\n<li>Building fa&ccedil;ade fixing brackets;</li>\r\n<li>Inside mechanical equipment e.g within air conditioning re-heat boxes;</li>\r\n<li>Gasket and sealants to pipework, ductworkm mechanical equipment &amp; construction joints;</li>\r\n<li>Waterproof membranes;</li>\r\n<li>Sealed fire door;</li>\r\n<li>Lift shaft and lift cabin fittings; &amp;</li>\r\n<li>Within live electrical switchboards.</li>\r\n</ul>');

/*!40000 ALTER TABLE `tbl_report_common` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_images`;

CREATE TABLE `tbl_site_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_images` WRITE;
/*!40000 ALTER TABLE `tbl_site_images` DISABLE KEYS */;

INSERT INTO `tbl_site_images` (`imageID`, `siteID`, `full_path`)
VALUES
	(2,17,'default-image.png'),
	(3,1,'abbotsford.jpeg'),
	(4,0,'default-image1.png'),
	(5,118,'default-image6.png'),
	(6,119,'default-image7.png'),
	(7,120,'default-image8.png'),
	(8,122,'default-image12.png'),
	(9,123,'default-image13.png'),
	(10,127,'image001.png'),
	(11,178,'default-image15.png'),
	(12,179,'default-image16.png'),
	(13,220,'default-image17.png'),
	(14,222,'checked.png'),
	(15,223,'checked1.png'),
	(16,224,'checked2.png'),
	(17,225,'reporting.png'),
	(18,226,'checked3.png'),
	(19,227,'checked4.png'),
	(20,228,'checked5.png'),
	(21,229,'checked6.png'),
	(22,230,'checked7.png'),
	(23,231,'checked8.png'),
	(24,232,'reporting2.png'),
	(25,236,'checked9.png'),
	(26,237,'checked10.png'),
	(27,238,'checked17.png');

/*!40000 ALTER TABLE `tbl_site_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_plan`;

CREATE TABLE `tbl_site_plan` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_plan` WRITE;
/*!40000 ALTER TABLE `tbl_site_plan` DISABLE KEYS */;

INSERT INTO `tbl_site_plan` (`imageID`, `siteID`, `full_path`)
VALUES
	(1,230,'reporting1.png'),
	(2,233,'checked.png'),
	(3,235,'checked1.png'),
	(4,238,'reporting3.png');

/*!40000 ALTER TABLE `tbl_site_plan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_siteID
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_siteID`;

CREATE TABLE `tbl_siteID` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_siteID` WRITE;
/*!40000 ALTER TABLE `tbl_siteID` DISABLE KEYS */;

INSERT INTO `tbl_siteID` (`id`, `siteID`)
VALUES
	(1,245);

/*!40000 ALTER TABLE `tbl_siteID` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) DEFAULT NULL,
  `site_url` text,
  `site_type` varchar(255) DEFAULT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `tenant` varchar(255) DEFAULT NULL,
  `site_contact` varchar(255) DEFAULT NULL,
  `description` longtext,
  `site` varchar(255) DEFAULT NULL,
  `property_number` int(11) DEFAULT NULL,
  `full_address` varchar(255) DEFAULT NULL,
  `survey_date` varchar(255) DEFAULT NULL,
  `levels` int(11) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `building_age` int(11) DEFAULT NULL,
  `construction_type` varchar(255) DEFAULT NULL,
  `previous_report` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roof_type` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  `clientID` int(11) DEFAULT NULL,
  `report_name` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `state` int(2) DEFAULT '0',
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `building_name`, `site_url`, `site_type`, `no_floors`, `tenant`, `site_contact`, `description`, `site`, `property_number`, `full_address`, `survey_date`, `levels`, `inspected_by`, `contact_title`, `building_age`, `construction_type`, `previous_report`, `company`, `date_inspected`, `area`, `roof_type`, `site_image`, `hide`, `clientID`, `report_name`, `city`, `post_code`, `state`, `date_updated`)
VALUES
	(1,'Test Site 1','test url','Test Type',4,'Cecilla Chapman','Cecilla Chapman','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','Abbotsford',320001,'445','26-08-2016',1,'Andrew Pakeham','Senior Personal Banker',1960,'Brick','Yes','NAA','10-08-2016','100 m2','metal','','N',8,'','City','0000',5,NULL),
	(17,'Ofiice',NULL,NULL,NULL,NULL,'John Smith',NULL,' Capital building',123456,'123 Victoria Street','',1,'Nigel johnson','Manager',1982,'Brick','Yes','Noel Arnold & Associates','01-01-2008','250m2','Metal',NULL,'N',8,'Asbestos Risk Assesment Report',NULL,NULL,0,NULL),
	(176,'Units 1 - 3, 12 Mars Road',NULL,NULL,NULL,NULL,'',NULL,'AU019 Trans tech Business Park',0,'12',NULL,3,'Shane Morris','',1989,'Concrete','','Noel Arnold & Associates','','80000m2','Concrete',NULL,'N',1,NULL,NULL,NULL,0,NULL),
	(177,'Building 1',NULL,NULL,NULL,NULL,'John Smith',NULL,'Haztech Site',0,'123',NULL,5,'John Smith','Manager',1979,'Concrete','No previous report','Risktech','23-09-2016','2000m2','Concrete',NULL,'N',8,NULL,NULL,NULL,0,NULL),
	(178,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
	(218,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'',NULL,NULL,0,NULL),
	(219,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'',NULL,NULL,0,NULL),
	(220,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'',NULL,NULL,0,NULL),
	(221,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(222,'building name',NULL,NULL,NULL,NULL,'site contact',NULL,'site',200,'street address','21-10-2016',0,'inspected by','title',0,'cons type','','company','21-10-2016','area','roof type ',NULL,'N',1,'Report Name','city','0000',5,NULL),
	(223,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(224,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(225,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(226,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(227,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(228,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(229,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(230,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(231,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(232,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(233,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'N',8,'','','',0,NULL),
	(234,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(235,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(236,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(237,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(238,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(239,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(240,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(241,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(242,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(243,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'N',8,'','','',0,NULL),
	(244,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(245,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(246,'',NULL,NULL,NULL,NULL,'',NULL,'',0,'','',0,'','',0,'','','','','','',NULL,'Y',8,'','','',0,NULL),
	(247,'',NULL,NULL,NULL,NULL,'',NULL,'TEST TIMESTAMP',0,'','',0,'','',0,'','','','','','',NULL,'N',13,'','','',0,'2016-10-25 12:00:52'),
	(248,'',NULL,NULL,NULL,NULL,'',NULL,'test timestamp2',0,'','',0,'','',0,'','','','','','',NULL,'N',14,'','','',0,'2016-10-25 12:02:28'),
	(249,'',NULL,NULL,NULL,NULL,'',NULL,'test timestamp2',0,'','',0,'','',0,'','','','','','',NULL,'N',14,'','','',0,'2016-10-25 12:06:27'),
	(250,'',NULL,NULL,NULL,NULL,'',NULL,'test site',0,'','',0,'','',0,'','','','','','',NULL,'N',15,'','','',0,'2016-10-25 12:08:26'),
	(251,'',NULL,NULL,NULL,NULL,'',NULL,'test site',0,'','',0,'','',0,'','','','','','',NULL,'N',15,'','','',0,'2016-10-25 12:11:23'),
	(252,'',NULL,NULL,NULL,NULL,'',NULL,'test site',0,'','',0,'','',0,'','','','','','',NULL,'N',15,'','','',0,'2016-10-25 12:12:58'),
	(253,'',NULL,NULL,NULL,NULL,'',NULL,'test site',0,'','',0,'','',0,'','','','','','',NULL,'Y',15,'','','',0,'2016-10-25 12:15:17'),
	(254,'',NULL,NULL,NULL,NULL,'',NULL,'test site',0,'','',0,'','',0,'','','','','','',NULL,'Y',15,'','','',0,'2016-10-25 12:15:12'),
	(255,'',NULL,NULL,NULL,NULL,'',NULL,'test site 123',0,'','',0,'','',0,'','','','','','',NULL,'N',15,'','','',0,'2016-10-25 12:14:45');

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`, `hide`)
VALUES
	(3,'B','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',1,'October 25, 2016, 3:03 pm','N'),
	(16,'F','clientgeneral@swim.com.au','d28d4ecc1fa0fddc6efcce683e834b59','Client General','clientgeneral@swim.com.au','A',8,'October 3, 2016, 11:57 am','N'),
	(17,'E','clientadmin@swim.com.au','d3b3da8d67d2a7bdbbb88447e8466d93','Client Admin','clientadmin@swim.com.au','A',8,'September 29, 2016, 2:58 pm','N'),
	(18,'B','risktechadmin@swim.com.au','b3d803edd45aff5a9196c0a43811cbef','Risktech Admin','risktechadmin@swim.com.au','A',NULL,NULL,'N'),
	(19,'C','risktechgeneral@swim.com.au','75d71d93550ab3911ee592363a77080c','Risktech General','risktechgeneral@swim.com.au','A',8,NULL,'N'),
	(20,'D','risktechcontractor@swim.com.au','60db2811e337c3eabd1ee88d6dcd8550','Risktech Contractor','risktechcontractor@swim.com.au','A',NULL,NULL,'N'),
	(21,'A','superadmin@swim.net.au','17c4520f6cfd1ab53d8745e84681eb49','Super Admin','superadmin@swim.net.au','A',8,'October 20, 2016, 4:52 pm','N'),
	(22,'D','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(23,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(24,'E','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(25,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(26,'E','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(27,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(28,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(29,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(30,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(31,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(32,'F','','d41d8cd98f00b204e9800998ecf8427e','','','A',NULL,NULL,'Y'),
	(33,'C','','098f6bcd4621d373cade4e832627b4f6','test','','A',NULL,NULL,'Y'),
	(34,'F','testclientgeneral@swim.com.au','0cec358bdaaabfeaf7b4d18c4695b89b','testclientgeneral','testclientgeneral@swim.com.au','A',NULL,'October 25, 2016, 3:04 pm','Y'),
	(35,'F','clientgeneraltest@swim.com.au','7cf1c46862a09971261f1f4866d60620','clientgeneraltest','clientgeneraltest@swim.com.au','A',NULL,'October 25, 2016, 2:56 pm','N'),
	(36,'F','testclientgeneral@swim.com.au','0cec358bdaaabfeaf7b4d18c4695b89b','testclientgeneral@swim.co.au','testclientgeneral@swim.com.au','A',1,'October 25, 2016, 3:04 pm','N');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Risktech General','C'),
	(4,'Risktech Contractor','D'),
	(5,'Client Admin','E'),
	(6,'Client General','F');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

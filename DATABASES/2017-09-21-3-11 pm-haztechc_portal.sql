# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 122.129.218.34 (MySQL 5.5.55-cll)
# Database: haztechc_portal
# Generation Time: 2017-09-21 05:11:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_au_states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_au_states`;

CREATE TABLE `tbl_au_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(3) NOT NULL DEFAULT '',
  `state_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_au_states` WRITE;
/*!40000 ALTER TABLE `tbl_au_states` DISABLE KEYS */;

INSERT INTO `tbl_au_states` (`id`, `state_code`, `state_name`)
VALUES
	(1,'NSW','New South Wales'),
	(2,'QLD','Queensland'),
	(3,'SA','South Australia'),
	(4,'TAS','Tasmania'),
	(5,'VIC','Victoria'),
	(6,'WA','Western Australia'),
	(7,'ACT','Australian Capital Territory'),
	(8,'NT','Northern Territory');

/*!40000 ALTER TABLE `tbl_au_states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_basevalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_basevalues`;

CREATE TABLE `tbl_basevalues` (
  `materialID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `material` varchar(255) DEFAULT NULL,
  `costper` varchar(255) DEFAULT NULL,
  `calloutfee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`materialID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_basevalues` WRITE;
/*!40000 ALTER TABLE `tbl_basevalues` DISABLE KEYS */;

INSERT INTO `tbl_basevalues` (`materialID`, `material`, `costper`, `calloutfee`)
VALUES
	(1,'Arc shields','30','1500'),
	(2,'Bituminous membrane','100','1500'),
	(3,'Corrugated cement sheet','50','1500'),
	(4,'Electrical backing board','120','1500'),
	(5,'Fibre cement sheet','50','1500'),
	(6,'Fire door core','300','1500'),
	(7,'Friction material','60','1500'),
	(8,'Gasket material','60','1500'),
	(9,'Insulation','400','1500'),
	(10,'Lagging','400','1500'),
	(11,'Mastic sealant','200','1500'),
	(12,'Millboard insulation','400','1500'),
	(13,'Screed','300','1500'),
	(14,'Sprayed limpet','500','1500'),
	(15,'Vermiculite','500','1500'),
	(16,'Vinyl sheet','250','1500'),
	(17,'Vinyl floor tiles','150','1500'),
	(18,'Window caulking','200','1500'),
	(19,'Woven material','600','1500'),
	(20,'SMF','30','1500'),
	(21,'Lead Paint/flashing','400','1500'),
	(22,'Ballast/Capacitors','125','1500');

/*!40000 ALTER TABLE `tbl_basevalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(2,'We previously instructed this to be removed as it is irrelevant ','2017-06-28 00:00:00','21','<p>Previoulsy advised not to have a \"news section\" Generally a waste time typing text for a blog &nbsp;when our client decision makers are all in their 40\"s. &nbsp;and the probability of this adding ANY value or creating ANY revenue is miniscule.</p>',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_access`;

CREATE TABLE `tbl_blog_access` (
  `blogaccessID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(5) DEFAULT NULL,
  `type_letter` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`blogaccessID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_access` WRITE;
/*!40000 ALTER TABLE `tbl_blog_access` DISABLE KEYS */;

INSERT INTO `tbl_blog_access` (`blogaccessID`, `blogID`, `type_letter`)
VALUES
	(5,2,'0'),
	(8,1,'B'),
	(9,1,'D');

/*!40000 ALTER TABLE `tbl_blog_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_images`;

CREATE TABLE `tbl_blog_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_blog_images` WRITE;
/*!40000 ALTER TABLE `tbl_blog_images` DISABLE KEYS */;

INSERT INTO `tbl_blog_images` (`imageID`, `blogID`, `full_path`)
VALUES
	(3,2,'risktech-logo_(1).png'),
	(4,13,'lod-search.png'),
	(5,14,'reporting.png'),
	(6,1,'images.png'),
	(7,3,'Image-Coming-Soon-Placeholder.png');

/*!40000 ALTER TABLE `tbl_blog_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_OFF
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_OFF`;

CREATE TABLE `tbl_blog_OFF` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_OFF` WRITE;
/*!40000 ALTER TABLE `tbl_blog_OFF` DISABLE KEYS */;

INSERT INTO `tbl_blog_OFF` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(1,'Sample Blog Post 1','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','Array',NULL),
	(2,'Sample Blog Post 3','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(3,'Sample Blog Post','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(7,'Sample Blog Post 2','2016-08-16 00:00:00','3','<p>This is test 2</p>','',NULL,NULL),
	(8,'Test','2016-10-24 00:00:00','3','<p>Test</p>','',NULL,NULL),
	(14,'Test','2016-10-26 00:00:00','3','<p>test content</p>',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog_OFF` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `site_address`, `contact_name`, `contact_number`, `hide`)
VALUES
	(1,'RiskTech','Test Address','test','00000001','N'),
	(7,'Goodman Property','12 Mars Road, lane Cove NSW 2066','test Goodman ','0000000','Y'),
	(8,'Haztech','123 Lean Road, VIC 2210','Jim Doe','0000000','N'),
	(11,'testname','building name','inspected','contact number','Y'),
	(12,'testname','building name','','','Y'),
	(13,'testname1','building name','inspected','contact number','Y'),
	(14,'testname3','building name','inspected','contact number','Y'),
	(15,'test123','test123','test123','test123','Y'),
	(16,'test16','test16','test16','test16','Y'),
	(17,'testname','test Address','Contact Person','Contact Number','Y'),
	(21,'','','','','Y'),
	(22,'test1','test','test','test','Y'),
	(23,'test','test','test','test','Y'),
	(24,'Test Client','Test Site Address','Test Contact Person','Test Contact Number','N'),
	(25,'','','','','N');

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_images`;

CREATE TABLE `tbl_client_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_client_images` WRITE;
/*!40000 ALTER TABLE `tbl_client_images` DISABLE KEYS */;

INSERT INTO `tbl_client_images` (`imageID`, `clientID`, `full_path`)
VALUES
	(1,1,'risktech-logo3.png'),
	(2,8,'risktech-logo11.png'),
	(3,0,'reporting.png'),
	(4,11,'reporting2.png'),
	(5,13,'checked2.png'),
	(6,14,'checked1.png'),
	(7,15,'reporting4.png'),
	(8,16,'reporting5.png'),
	(9,17,'reporting6.png'),
	(10,20,'reporting7.png'),
	(11,23,'risktech-logo_(1).png'),
	(12,25,'images.png');

/*!40000 ALTER TABLE `tbl_client_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_global
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_global`;

CREATE TABLE `tbl_global` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_global` WRITE;
/*!40000 ALTER TABLE `tbl_global` DISABLE KEYS */;

INSERT INTO `tbl_global` (`id`, `description`, `value`)
VALUES
	(1,'photo_number','15');

/*!40000 ALTER TABLE `tbl_global` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_hazard_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_hazard_types`;

CREATE TABLE `tbl_hazard_types` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_hazard_types` WRITE;
/*!40000 ALTER TABLE `tbl_hazard_types` DISABLE KEYS */;

INSERT INTO `tbl_hazard_types` (`typeID`, `type_name`)
VALUES
	(1,'Asbestos'),
	(2,'SMF Products'),
	(3,'Lead Products'),
	(4,'PCBs');

/*!40000 ALTER TABLE `tbl_hazard_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_document`;

CREATE TABLE `tbl_item_document` (
  `docID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`docID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_document` WRITE;
/*!40000 ALTER TABLE `tbl_item_document` DISABLE KEYS */;

INSERT INTO `tbl_item_document` (`docID`, `itemID`, `full_path`)
VALUES
	(17,27,'AustralianSuper_Master_RGB.jpg'),
	(18,27,'boral.png'),
	(19,69,'AustralianSuper_Master_RGB.jpg'),
	(20,69,'boral.png'),
	(21,0,'images.png'),
	(22,0,'images.png'),
	(23,37,''),
	(24,70,'images.png'),
	(25,71,'images.png'),
	(26,73,'images.png'),
	(27,74,''),
	(28,75,''),
	(29,76,''),
	(30,77,''),
	(31,78,''),
	(32,77,'');

/*!40000 ALTER TABLE `tbl_item_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_images`;

CREATE TABLE `tbl_item_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_images` WRITE;
/*!40000 ALTER TABLE `tbl_item_images` DISABLE KEYS */;

INSERT INTO `tbl_item_images` (`imageID`, `itemID`, `full_path`)
VALUES
	(1,20,'default-image.png'),
	(2,24,'default-image1.png'),
	(3,22,'default-image2.png'),
	(4,44,'default-image3.png'),
	(5,45,'default-image4.png'),
	(6,63,'reporting.png'),
	(7,65,'reporting1.png'),
	(8,66,'IMG_8977.JPG'),
	(9,177,NULL),
	(10,70,'images1.png'),
	(11,71,'images2.png'),
	(12,76,'20170716_203745.jpg'),
	(13,77,'P1020246.JPG'),
	(14,78,'IMG_2445.JPG');

/*!40000 ALTER TABLE `tbl_item_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_materials`;

CREATE TABLE `tbl_item_materials` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_material` varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_materials` WRITE;
/*!40000 ALTER TABLE `tbl_item_materials` DISABLE KEYS */;

INSERT INTO `tbl_item_materials` (`itemID`, `item_material`, `group`)
VALUES
	(1,'Adhesive','Asbestos'),
	(2,'Arc shields','Asbestos'),
	(3,'Bituminous material','Asbestos'),
	(4,'Bituminous membrane','Asbestos'),
	(5,'Compressed cement sheet','Asbestos'),
	(6,'Construction joint mastic','Asbestos'),
	(7,'Corrugated cement sheet','Asbestos'),
	(8,'Dust','Asbestos'),
	(9,'Electrical backing board','Asbestos'),
	(10,'Fibre cement sheet','Asbestos'),
	(11,'Fire door core','Asbestos'),
	(12,'Friction material','Asbestos'),
	(13,'Friction pads','Asbestos'),
	(14,'Galbestos','Asbestos'),
	(15,'Gasket material','Asbestos'),
	(16,'Insulation','Asbestos'),
	(17,'Lagging','Asbestos'),
	(18,'Low density fibre board','Asbestos'),
	(19,'Mastic sealant','Asbestos'),
	(20,'Millboard insulation','Asbestos'),
	(21,'Moulded fibre cement','Asbestos'),
	(22,'Packing','Asbestos'),
	(23,'Pointing','Asbestos'),
	(24,'Screed','Asbestos'),
	(25,'Sheet vinyl','Asbestos'),
	(26,'Sheet vinyl & adhesive','Asbestos'),
	(27,'Sheet vinyl - fibrous backed','Asbestos'),
	(28,'Sheet vinyl - Hessian backed','Asbestos'),
	(29,'Sprayed vermiculite','Asbestos'),
	(30,'Sprayed limpet','Asbestos'),
	(31,'Textured coatings','Asbestos'),
	(32,'Vermiculite','Asbestos'),
	(33,'Vinyl floor tiles','Asbestos'),
	(34,'Vinyl floor tiles & adhesives','Asbestos'),
	(35,'Window caulking','Asbestos'),
	(36,'Woven material','Asbestos');

/*!40000 ALTER TABLE `tbl_item_materials` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `item_no` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `room_specific` varchar(255) DEFAULT NULL,
  `materialID` int(11) DEFAULT NULL,
  `hazard_type` int(11) DEFAULT NULL,
  `samples_taken` varchar(225) DEFAULT NULL,
  `sample_no` varchar(255) DEFAULT NULL,
  `sample_status` varchar(11) DEFAULT NULL,
  `extent` varchar(255) DEFAULT NULL,
  `extent_mesurement` varchar(10) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_number` mediumint(25) DEFAULT NULL,
  `recommendations` longtext,
  `item_condition` varchar(255) DEFAULT NULL,
  `disturb_potential` varchar(255) DEFAULT NULL,
  `risk_rating` varchar(255) DEFAULT NULL,
  `current_label` varchar(11) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `control_priority` varchar(255) DEFAULT NULL,
  `work_records` varchar(255) DEFAULT NULL,
  `description` longtext,
  `photo_no` varchar(450) NOT NULL,
  `location_level` varchar(255) DEFAULT NULL,
  `friability` varchar(255) DEFAULT NULL,
  `control_recommendation` longtext,
  `hide` char(11) DEFAULT NULL,
  `work_action` longtext,
  `item_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_items` WRITE;
/*!40000 ALTER TABLE `tbl_items` DISABLE KEYS */;

INSERT INTO `tbl_items` (`itemID`, `siteID`, `clientID`, `item_no`, `item_name`, `room_specific`, `materialID`, `hazard_type`, `samples_taken`, `sample_no`, `sample_status`, `extent`, `extent_mesurement`, `contact`, `contact_number`, `recommendations`, `item_condition`, `disturb_potential`, `risk_rating`, `current_label`, `date_updated`, `control_priority`, `work_records`, `description`, `photo_no`, `location_level`, `friability`, `control_recommendation`, `hide`, `work_action`, `item_status`)
VALUES
	(6,1,1,1,'Material','Meeting Room',10,4,'yes','1','Negative','300',NULL,'Test',0,NULL,'Good','Non-selected','Low','Yes','2017-05-17 15:23:35','P1','No Action','Roof - Corrugated cement','4','External - Ground','Friable','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim<span style=\"color: #666666; font-family: Verdana, Geneva, sans-serif; font-size: 10px;\">.</span></p>','N',NULL,NULL),
	(7,5,7,2,'Test','Meeting Room',11,0,'Yes','1','Positive','20',NULL,'Contact Name',1231212,NULL,'Good','test','Test','Test','2017-05-17 15:23:38','P4','Test',NULL,'0',NULL,NULL,NULL,'N',NULL,NULL),
	(20,17,8,NULL,NULL,'Below Sink',12,4,NULL,'J108740-AU019-50','Negative','10',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:23:40','P1',NULL,'Beams - Sprayed Vermiculite','6','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(22,1,1,NULL,NULL,'Ceiling',13,4,NULL,'J108740-AU019-52','','12',NULL,NULL,NULL,NULL,'Good','Low Accessibility','Medium','Suspect','2017-05-17 15:23:43','P2',NULL,'Compressed cement sheet','3','Level 2','Friable','<p>Engage a license asbestos conrtactor to undertake remedial/removal works on this item as soon as practicable (within 3 months)</p>','N',NULL,NULL),
	(23,1,1,NULL,NULL,'South',5,4,NULL,'Similar to J108740-AU019-52','','24',NULL,NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect','2017-05-17 15:23:44','P3',NULL,'Service Riser - Moulded Fibre Cement Flue','2','Wasrehouse - Underside Of  Roof','Friable','<p>Maintain in current condition, label and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',NULL,NULL),
	(24,17,8,NULL,NULL,'Underside of roof',6,4,NULL,'','','50',NULL,NULL,NULL,NULL,'Bad','High Accessibility','Low','Suspect','2017-05-17 15:23:46','P2',NULL,'Roof Lining - Insulation Material','10','Warehouse','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Removed under controlled conditions prior to demolition or refurbishment</p>','N',NULL,NULL),
	(25,177,1,NULL,NULL,'North',20,1,NULL,'','Assumed Neg','500',NULL,NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect','2017-05-17 15:23:49','P3',NULL,'Beams - Sprayed Vermiculite','0','Warehouse','Friable','<p>Maintain in Good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',NULL,NULL),
	(26,177,1,NULL,NULL,'Interior - Unit 14',17,2,NULL,'','Suspected P','34',NULL,NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect','2017-05-17 15:23:51','P4',NULL,'Hot water sevice insulation - Insulation material','0','Ground Level','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under contolled conditions prior to demolition or refurbishment.</p>','N',NULL,NULL),
	(27,177,1,NULL,NULL,'Office Area - Throughout',5,4,NULL,'','','3',NULL,NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect','2017-05-17 15:23:54','P1',NULL,'Ceiling Tiles - INsulation Material','0','Level One','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment&nbsp;</p>','N','<p>Test Work Action</p>',NULL),
	(28,17,8,NULL,NULL,'Below Sink',7,4,NULL,'J108740-AU019-50','Negative','72',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:23:57','P3',NULL,'Beams - Sprayed Vermiculite','7','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(30,17,8,NULL,NULL,'Below Sink',8,4,NULL,'J108740-AU019-50','Negative','50',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:23:58','P4',NULL,'Beams - Sprayed Vermiculite','8','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(32,17,8,NULL,NULL,'Below Sink',6,4,NULL,'J108740-AU019-50','Negative','24',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:00','P1',NULL,'Beams - Sprayed Vermiculite','5','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(33,17,8,NULL,NULL,'Below Sink',5,4,NULL,'J108740-AU019-50','Negative','29',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:02','P2',NULL,'Beams - Sprayed Vermiculite','11','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(34,17,8,NULL,NULL,'Below Sink',14,4,NULL,'J108740-AU019-50','Negative','34',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:04','P3',NULL,'Beams - Sprayed Vermiculite','3','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(35,17,8,NULL,NULL,'Below Sink',22,4,NULL,'J108740-AU019-50','Negative','95',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:06','P4',NULL,'Beams - Sprayed Vermiculite','12','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(36,17,8,NULL,NULL,'Below Sink',5,4,NULL,'J108740-AU019-50','Negative','100',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:08','P1',NULL,'Beams - Sprayed Vermiculite','13','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',NULL,NULL),
	(37,17,8,NULL,NULL,'Below Sink',8,4,NULL,'J108740-AU019-50','','245',NULL,NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2017-05-17 15:24:11','P2',NULL,'Beams - Sprayed Vermiculite','14','kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N','',''),
	(65,262,17,NULL,NULL,'Room Specific',9,4,NULL,'Sample Description','Positive','37',NULL,NULL,NULL,NULL,'Good','Low Accessibility','Medium','current lab','2017-05-17 15:24:12','P2',NULL,'Feqature Material','17','Backing Board','Friable','<p>Control Recommendation</p>','N',NULL,NULL),
	(66,283,17,NULL,NULL,'store',6,1,NULL,'wall','Suspected P','82',NULL,NULL,NULL,NULL,'Bad','High Accessibility','Medium','Extreme Dan','2017-05-17 15:24:14','P3',NULL,'sheet','0','1','Friable','<p>Fix this ASAP</p>','N',NULL,NULL),
	(68,1,17,NULL,NULL,'',7,4,NULL,'','','158',NULL,NULL,NULL,NULL,'','Non-selected','Non-selected','','2017-05-17 15:24:16','Non-selected',NULL,'','0','','','','N','',NULL),
	(69,177,17,NULL,NULL,'',3,0,NULL,'','','146',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:18','',NULL,'','0','','','','N','',NULL),
	(70,17,17,NULL,NULL,'',18,0,NULL,'','','14',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:20','',NULL,'','0','','','','N','',''),
	(71,17,17,NULL,'test','',19,0,NULL,'','','12.5',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:22','',NULL,'','0','test location','','','N','',''),
	(72,17,17,NULL,NULL,'',1,0,NULL,'','','15.8',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:25','',NULL,'','0','','','','N','',''),
	(73,17,17,NULL,NULL,'',2,0,NULL,'','','42.5',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:27','',NULL,'','0','','','','N','',''),
	(74,17,8,NULL,NULL,'Main Room',16,2,NULL,'Extra','Positive','44.5',NULL,NULL,NULL,NULL,'Good','Low Accessibility','Medium','Nothing','2017-05-17 15:24:32','P2',NULL,'Sample','','Level 1','Non-Friable','<p>Sample</p>','N','<p>No Action</p>','Outstanding'),
	(75,17,8,NULL,NULL,'Extra',5,0,NULL,'','','10.5',NULL,NULL,NULL,NULL,'','','','','2017-05-17 15:24:34','',NULL,'','','Sample','','','N','',''),
	(76,283,1,NULL,NULL,'Board room',21,3,NULL,'strip of lead','Positive','10','sqm',NULL,NULL,NULL,'Bad','High Accessibility','Medium','none','2017-08-02 11:55:46','P1',NULL,'old','','33rd Floor','','','N','<p>what is this field for.??? &nbsp;Why is there a \"P\" at eh bottom of the dialogue box</p>','Outstanding'),
	(77,222,1,NULL,NULL,'Plant Room',6,1,NULL,'','','3','',NULL,NULL,NULL,'Good','Low Accessibility','Low','No labelled','2017-08-24 10:08:52','P3',NULL,'Fire door core','','Ground Floor','Friable','','N','',''),
	(78,222,1,NULL,NULL,'Gas Meter Room',11,1,NULL,'','Positive','2','m',NULL,NULL,NULL,'Fair','Low Accessibility','Low','Not labelle','2017-08-24 10:08:24','P4',NULL,'in upper wall pipe penetration','','Ground Floor','Non-Friable','','N','','');

/*!40000 ALTER TABLE `tbl_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report`;

CREATE TABLE `tbl_report` (
  `reportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT '',
  `scope_of_works` varchar(255) DEFAULT '',
  `recommendations` varchar(255) DEFAULT NULL,
  `methologies` varchar(255) DEFAULT NULL,
  `risk_factors` varchar(255) DEFAULT NULL,
  `priority_rating_system` varchar(255) DEFAULT NULL,
  `asbestos_mng_req` varchar(255) DEFAULT NULL,
  `haz_material_mr` varchar(255) DEFAULT NULL,
  `statement_of_limitations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report` WRITE;
/*!40000 ALTER TABLE `tbl_report` DISABLE KEYS */;

INSERT INTO `tbl_report` (`reportID`, `siteID`, `introduction`, `scope_of_works`, `recommendations`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,17,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 123 Victoria Street. The risk assessment was performed by Nigel johnson on 01-01-2008. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(2,1,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 445. The risk assessment was performed by Andrew Pakeham on 10-08-2016.</p>','N','N','N','N','N','N','N','N'),
	(3,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(4,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(5,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(6,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(7,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(8,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(9,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(10,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(11,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(12,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(13,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(14,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(15,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(16,0,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(17,118,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(18,119,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(19,200,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(20,120,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(21,121,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(22,122,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(23,206,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 123. The risk assessment was performed by test on 11-10-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(24,123,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(25,124,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(26,125,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(27,126,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(28,127,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(29,178,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(30,179,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(31,220,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(32,221,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(33,222,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Test Client of the site located at 12 High Street. The risk assessment was performed by Bernard Day on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(34,223,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(35,224,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(36,225,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(37,226,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(38,227,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(39,228,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(40,229,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(41,230,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(42,231,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(43,232,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(44,233,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(45,234,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(46,235,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(47,236,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(48,237,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(49,238,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(50,239,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(51,240,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(52,241,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(53,242,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for  of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(54,243,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(55,244,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(56,245,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test123 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(57,246,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(58,258,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(59,261,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(60,262,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 25-10-2016.</p>','N','N','N','N','N','N','Y','N'),
	(61,263,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(62,264,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at Building name. The risk assessment was performed by 0 on 15-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(63,265,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 01-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(64,266,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at Building name. The risk assessment was performed by Inspected By on 01-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(65,267,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(66,268,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(67,269,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(68,270,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(69,271,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(70,272,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(71,273,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for testname3 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(72,274,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(73,275,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(74,276,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(75,277,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(76,278,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(77,279,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(78,280,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for Haztech of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(79,282,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','N','Y'),
	(80,283,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 15 Smith Street. The risk assessment was performed by GJH on 10-01-2017.</p>','N','N','N','N','N','N','N','N'),
	(81,284,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 85 Test Street. The risk assessment was performed by Inspected By on 28-03-2017. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(82,176,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 12. The risk assessment was performed by John Smith on .</p>','Y','Y','N','N','Y','N','N','N'),
	(83,281,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 15A. The risk assessment was performed by Inspected By on 22-11-2016. ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(84,177,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at 123. The risk assessment was performed by John Smith on 23-09-2016.</p>','N','N','N','N','N','N','N','N'),
	(85,285,'<p>This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at zzz. The risk assessment was performed by Fred Bloggs on 27-06-2012.</p>','Y','Y','Y','Y','Y','Y','Y','Y'),
	(86,286,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(87,287,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(88,288,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(89,289,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(90,290,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(91,294,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y'),
	(92,291,'This report presents the findings of an Hazardous Materials Risk Assessment conducted for test1 of the site located at . The risk assessment was performed by  on . ','Y','Y','Y','Y','Y','Y','Y','Y');

/*!40000 ALTER TABLE `tbl_report` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_report_common
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_report_common`;

CREATE TABLE `tbl_report_common` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recommendations` longtext,
  `scope_of_works` longtext,
  `methologies` longtext,
  `risk_factors` longtext,
  `priority_rating_system` longtext,
  `asbestos_mng_req` longtext,
  `haz_material_mr` longtext,
  `statement_of_limitations` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_report_common` WRITE;
/*!40000 ALTER TABLE `tbl_report_common` DISABLE KEYS */;

INSERT INTO `tbl_report_common` (`id`, `recommendations`, `scope_of_works`, `methologies`, `risk_factors`, `priority_rating_system`, `asbestos_mng_req`, `haz_material_mr`, `statement_of_limitations`)
VALUES
	(1,'<h3>Recommendations</h3>\r\n<ul>\r\n<li>Schedule periodic re-assessments of the asbestos-containing materials remaining in-situ to monitor their condition in accordance with the Code of Practice.</li>\r\n<li>Provide Asbestos Awareness training to staff and site personnel in accordance with the requirements of the Code of Practice.</li>\r\n<li>Consult with staff and health and safety representatives on the findings of this risk assessment and this report must be made available upon request, in accordance with the requirements of the Code of Practice.</li>\r\n<li>Areas highlighted in the Areas Not Accessed section as areas of \'no access\' should be presumed to contain hazardous materials. Appropriate management planning should be implemented in order to control access to and maintenance activities in these areas, until such a time as they can be inspected and the presence or absence of hazardous materials can be confirmed.</li>\r\n<li>Should any personnel come across any suspected asbestos or hazardous materials, work should cease immediately in the affected areas until further sampling and investigation is performed.</li>\r\n<li>Prior to demolition/refurbishment works undertake a destructive hazardous materials survey of the premises as per the requirements of AS 2601: 2001 The Demolition of Structures, Part 1.6.1.&nbsp;</li>\r\n<li>Synthetic Mineral Fibre (SMF) materials should be removed under controlled conditions prior to demolition/refurbishment works, in accordance with the requirements of the Code of Practice for the Safe Use of Synthetic Mineral Fibres [NOHSC:2006(1990)].</li>\r\n<li>Noel Arnold &amp; Associates can assist with the implementation of any of the above recommendations.</li>\r\n</ul>','<h3>Scope of Works</h3>\r\n<p class=\"p1\">The scope of works for this project was as follows:</p>\r\n<ul>\r\n<li>Inspect representative and accessible areas of the site to re-assess previously identified hazardous materials</li>\r\n<li>Identify the likelihood of hazardous materials in inaccessible areas</li>\r\n<li>Assess the current condition of hazardous materials at the site</li>\r\n<li>Assess the risks posed by the materials</li>\r\n<li>Compile an up-dated hazardous materials register for the site</li>\r\n<li>Recommend control measures and actions necessary to manage any hazardous material related risks</li>\r\n<li>Undertake a NATA-Accredited survey of the site in accordance with the requirements of ISO 17020</li>\r\n<li>Undertake representative lead paint identification using LeadCheck swabs</li>\r\n</ul>\r\n<p class=\"p1\">Refer to Methodology for full details.</p>','<p class=\"p1\"><strong>Asbestos</strong></p>\r\n<p class=\"p2\">This assessment was undertaken in accordance with the following documents and within the constraints of the scope of works:</p>\r\n<p class=\"p2\" style=\"padding-left: 30px;\">How to Manage and Control Asbestos in the Workplace: Code of Practice (Safe Work Australia, 2011)</p>\r\n<p class=\"p2\" style=\"padding-left: 30px;\">NSW Work Health &amp; Safety Regulation 2011</p>\r\n<p class=\"p2\">2 representative samples of suspected asbestos-containing material were collected and placed in plastic bags with clip-lock seals. These samples were analysed in Greencap\'s NATA-accredited laboratory for the presence of asbestos by Polarised Light Microscopy.</p>\r\n<p class=\"p2\">Where it was determined that asbestos was present, a risk and priority assessment was conducted in accordance with Greencap\'s standard Risk Assessment and Priority Ranking System. Refer to section on Priority Rating System for detailed information on this system.</p>\r\n<p class=\"p2\">Inaccessible areas that are likely to contain asbestos have been assumed to contain asbestos until further inspection and analysis of samples has been undertaken by an approved analyst.</p>\r\n<p class=\"p2\">Limited destructive sampling techniques have been used to gain access into restricted areas for the purpose of determining the likelihood of asbestos or other hazardous materials in these areas. Due to the nature of the survey&nbsp;methodology, it is possible that not every area of the site have been accessed. Reference should be made to the \'Areas Not Accessible\' section of this report for further details.</p>\r\n<p class=\"p2\">Subject to the limitations associated with the scope of works, this audit was conducted in accordance with the requirements of AS 2601-2001 The Demolition of Structures.</p>\r\n<p class=\"p2\">The survey methodology utilised in this assessment has been NATA-Accredited to meet the requirements of ISO 17020</p>\r\n<p class=\"p2\">Conformity assessment - General criteria for the operation of various types of bodies performing inspections</p>\r\n<p class=\"p1\"><strong>SMF</strong></p>\r\n<p class=\"p2\">Synthetic Mineral Fibre (SMF) Accessible areas where Synthetic Mineral Fibre (SMF) insulation was visually confirmed as being present were noted to give a general indication to the presence of materials throughout the building.</p>\r\n<p class=\"p1\"><strong>PCB</strong></p>\r\n<p class=\"p2\">Polychlorinated Biphenyls (PCBs) Representative light fittings containing capacitors were inspected where safely practicable and details noted for cross-referencing with the ANZECC Identification of PCB-Containing Capacitors - 1997.</p>\r\n<p class=\"p2\">Where metal capacitors were not listed on the database, these capacitors are noted as suspected to contain polychlorinated biphenyls.</p>\r\n<p class=\"p1\"><strong>Lead Paint</strong></p>\r\n<p class=\"p2\">Representative painted surfaces were tested unobtrusively for the presence of lead using the LeadCheck paint swab method. This method can give an instantaneous qualitative result and reproducibly detect lead in paints at concentrations of 0.5% (5,000ppm) and above, and may indicate lead in some paint films as low as 0.2% (2,000ppm).</p>\r\n<p class=\"p2\">The sampling program was representative of the various types of paints found within the site, concentrating on areas where lead based paints may have been used (Eg. Gloss paints on doors, railings, guttering and downpipes, columns, window and door architraves, skirting boards etc). The objective of lead paint identification in this survey is to highlight the presence of lead-based paints within the building, not to specifically quantify every source of lead-based paint.</p>','<h3 class=\"p1\">Risk Assessment Factors - Asbestos</h3>\r\n<p class=\"p2\">The presence of asbestos-containing materials (ACMs) does not necessarily constitute an exposure risk. However, if the ACM is sufficiently disturbed to cause the release of airborne respirable fibres, then an exposure risk may be posed to individuals. The assessment of the exposure risk posed by ACMs assesses (a) the material condition and friability, and (b) the disturbance potential.</p>\r\n<h3 class=\"p1\">Material Condition</h3>\r\n<p class=\"p2\">The assessment factors for material condition include:</p>\r\n<ul>\r\n<li>Evidence of physical deterioration and/or water damage.</li>\r\n<li>Degree of friability of the ACM.</li>\r\n<li>Surface treatment, lining or coating (if present).</li>\r\n<li>Likelihood to sustain damage or deterioration in its current location and state.</li>\r\n</ul>\r\n<p class=\"p3\"><strong>Physical Condition and Damage</strong></p>\r\n<p class=\"p2\">The condition of the ACM is rated as either being good, fair or poor.</p>\r\n<div><em>Good</em>&nbsp; &nbsp; refers to an ACM that has not been damaged or has not deteriorated</div>\r\n<div><em>Fair</em>&nbsp; &nbsp; &nbsp; refers to an ACM having suffered minor cracking or de-surfacing.</div>\r\n<div><em>Poor</em>&nbsp; &nbsp; &nbsp;describes an ACM which has been damaged or its condition has deteriorated over time.</div>\r\n<p class=\"p3\"><strong>Friability and Surface Treatment</strong></p>\r\n<p class=\"p2\">The degree of friability of ACMs describes the ease of which the material can be crumbled, and hence to release fibres, and takes into account surface treatment.</p>\r\n<p class=\"p2\"><em>Friable asbestos</em></p>\r\n<p class=\"p2\">(e.g. sprayed asbestos beam insulation (limpet), pipe lagging) can be easily crumbled and is more hazardous than non-friable asbestos products.</p>\r\n<p class=\"p2\"><em>Non-friable asbestos</em></p>\r\n<p class=\"p2\">also referred to as bonded asbestos, typically comprises asbestos fibres tightly bound in a stable non-asbestos matrix or impregnated with a coating. Examples of non-friable asbestos products include asbestos cement materials (sheeting, pipes etc), asbestos containing vinyl floor tiles, compressed gaskets and electrical backing boards.</p>\r\n<h3 class=\"p1\">Disturbance Potential</h3>\r\n<p class=\"p2\">In order to assess the disturbance potential, the following factors are considered:</p>\r\n<ul>\r\n<li>Requirement for access for either building work or maintenance operations.</li>\r\n<li>Likelihood and frequency of disturbance of the ACM.</li>\r\n<li>Accessibility of the ACM.</li>\r\n<li>Proximity of the ACM to air plenums and direct air stream.</li>\r\n<li>Quantity and exposed surface areas of ACM.</li>\r\n<li>Normal use and activity in area, and numbers of persons in vicinity of ACM.</li>\r\n</ul>\r\n<p class=\"p2\">These factors are used to determine (i) the potential for fibre generation, and (ii) the potential for exposure to person/s, as a rating of low, medium or high disturbance potential:</p>\r\n<h3 class=\"p1\">Risk Status</h3>\r\n<p class=\"p2\">The risk factors described previously are used to rank the asbestos exposure risk posed by the presence of the ACM.</p>\r\n<ul>\r\n<li>A low risk rating describes ACMs that pose a low exposure risk to personnel, employees and the general public providing they stay in a stable condition, for example asbestos materials that are in good condition and have low accessibility.</li>\r\n<li>A medium risk rating applies to ACMs that pose an increased exposure risk to people in the area.</li>\r\n<li>A high risk rating applies to ACMs that pose a higher exposure risk to personnel or the public in the vicinity of the material due to their condition or disturbance potential.</li>\r\n</ul>','<h3>Asbestos Priority Rating System for Control Recommendations</h3>\r\n<p>The following priority rating system is adopted to assist in the programming and budgeting of the control od asbestos risk identified at the site</p>\r\n<p><strong>Priority 1 (P1): Hazard with Elevated Risk Potential / Organise Remedial Works Immediately</strong></p>\r\n<p>Area has hazardous materials, which are either damaged or are being exposed to continual disturbance. Due to these conditions there is an increased potential for exposure and/or transfer of the material to other parts &nbsp;with continued unrestricted use of this area. It is recommended that the area be isolated, air monitoring be conducted (if relevant) and the hazardous material promptly removed.</p>\r\n<p><strong>Priority 2 (P2): Hazard with Moderate Risk Potential / Organise Remedial Works Within 3 Months</strong></p>\r\n<p>Area has hazardous materials with a potential for disturbance due to the following conditions:</p>\r\n<ol>\r\n<li>Material has been disturbed or damaged and its current condition, while not posing an immediate hazard, is unstable;</li>\r\n<li>The material is accessible and can, when disturbed, present a short-term exposure risk; or</li>\r\n<li>Demolition, refurbishment or maintenance works including new installations or modification to air-handling systems, ceilings, lighting, fire safety systems, or floor laylouts.</li>\r\n</ol>\r\n<p>Appropriate abatement measures to be taken as soon as is practical (within 3 months). Negligible health risks if&nbsp;materials remain undisturbed under the control of a hazardous materials management plan.</p>\r\n<p><strong>Priority 3 (P3): Hazard with Low Risk Potential</strong></p>\r\n<p>Area has hazardous materials where:</p>\r\n<ol>\r\n<li>The condition of any friable hazardous material is stable and has a low potential for distubance; or</li>\r\n<li>The hazardous material is in a non-friable condition and does not present an exposure risk unless cut, drilled, sanded or otherwise abraded.</li>\r\n</ol>\r\n<p>negligible health risks if the materials are left undisturbed under the control of a hazardous material management plan. Monitor condition during subsequent reviews. Defer abatement unless materials are to be disturbed as a result of maintenance, refurbisment or demolition activities.</p>\r\n<p><strong>Priority 4 (P4): Hazard with&nbsp;Negligible Risk Potential</strong></p>\r\n<p>The hazardous material is in non-friable form and in a good condition. It is most unlikely that the material can be disturbed under normal circumstances and can be safety subjected to normal traffic. Even id it were subjeted to minor disturbance the material poses a negligible health risk. Monitor condition during subsequent reviews. Defer abatement unless materials are to be disturbed as a reult of maintenance, refurbishment or demolition activities.</p>\r\n<h3>Labelling Requirements</h3>\r\n<p>Materials confirmed or suspected to contain asbestos should be clearly labelled in accordance with the reuirements outlined in current, relevant state health and safety regulations and Safe Work Australia Code of Practice for the Management and Control of Asbestos in Workplaces [NOHSC: 2018(2005)].</p>\r\n<h4>&nbsp;</h4>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','<p class=\"p1\">The Occupational Health and Safety Regulations of most Australian states &amp; territories refer to a Code of Practice for Guidance on identification and management of asbestos materials (ACMs) in workplaces. The requirements are summarised below.</p>\r\n<h3 class=\"p1\"><strong>&nbsp;Asbestos Management Plan (AMP)</strong></h3>\r\n<p class=\"p1\">&nbsp;An AMP should be developed for the site as per the Code of Practice. The AMP should be a broad ranging document detailing the following information:</p>\r\n<ul>\r\n<li>The site\'s asbestos material register.</li>\r\n<li>Responsibilities for relevant persons in the management of ACMs.</li>\r\n<li>Mechanisms for communicating the location, type and condition of ACMs, the risks posed by these and the controlmeasures adopted to minimise these risks.</li>\r\n<li>Training arrangements for workers and contractors.</li>\r\n<li>A Procedure for reviewing and updating the AMP and the register.</li>\r\n<li>Air Monitoring and clearance inspection arrangements.</li>\r\n<li>Timetable for action to review risk assessments and undertake asbestos management activities.</li>\r\n<li>Records of any maintenance or service work conducted on ACMs, including clearance certificates for removed items.</li>\r\n</ul>\r\n<h3 class=\"p2\"><strong>Updates to Register, AMP and Risk Assessments</strong></h3>\r\n<p class=\"p1\">&nbsp;The asbestos register and the AMP should be reviewed (via visual inspection by a competent person) and updated at least every 5 years or earlier where a risk assessment indicates the need for a re-assessment or if any ACMs have been removed or updated as per the requirements of the Code of Practice.</p>\r\n<p class=\"p1\">Risk assessments should be reviewed regularly and as specified by the Code of Practice, particularly when there is evidence that the risk assessment is no longer valid, control measures are shown to be ineffective or there is a significant change planned for the workplace or work practices or procedures relevant to the risk assessment; or there is a change in ACM condition or ACMs have since been enclosed, encapsulated or removed.</p>\r\n<h3 class=\"p2\"><strong>Labelling</strong></h3>\r\n<p class=\"p1\">&nbsp;All confirmed or presumed ACMs (or their enclosures) should be labelled to identify the material as asbestos-containing or presumed asbestos-containing and to warn that the items should not be disturbed as per the requirements of the Code of Practice.</p>\r\n<h3 class=\"p2\"><strong>Training</strong></h3>\r\n<p class=\"p1\">&nbsp;Staff and site personnel must be provided with Asbestos Awareness training in accordance with the Code of Practice.</p>\r\n<p class=\"p1\">Training should inform staff how to work safely alongside asbestos by instructing them of:</p>\r\n<ol>\r\n<li>The health risks associated with asbestos.</li>\r\n<li>Their roles and responsibilities under the AMP.</li>\r\n<li>Procedures for managing asbestos on-site.</li>\r\n<li>The correct use of control measures and safe work methods to minimise the risks from asbestos.</li>\r\n</ol>\r\n<h3 class=\"p2\"><strong>Refurbishment / Demolition Requirements</strong></h3>\r\n<p class=\"p1\">&nbsp;This audit is limited by the Scope of Works and Methodology outlined within this report.</p>\r\n<p class=\"p1\">Generally, a new audit or revised audit is required prior to any planned refurbishment, alteration, demotion or upgrade works that may disturb ACMs at the site in accordance with Australia Standard AS 2601: The Demolition of Structures and Demolition Work Code of Practice(Safe Work Australia 2013).</p>\r\n<h3 class=\"p2\"><strong>Removal of Asbestos Materials</strong></h3>\r\n<p class=\"p1\">&nbsp;Any works involving the removal of ACMs should be undertaken by a Licensed Asbestos Removal Contractor (LARC).</p>\r\n<p class=\"p1\">In addition, an appropriately qualified independent Asbestos Consultant / Occupational Hygienist should undertake asbestos fibre air monitoring during/after works, and issue a Clearance Certificate to validate the works have been undertaken safely.</p>\r\n<p class=\"p1\">All works should be conducted in accordance with legislative requirements and following the requirements of the document \'How to Safely Remove Asbestos: Code of Practice (Safe Work Australia, 2011)\'.</p>\r\n<p class=\"p1\">&nbsp;The Occupational Health and Safety Regulations of most Australian states &amp; territories have requirements for the identification and control of risks within workplaces. These broad requirements extends to the hazardous materials that may be present within buildings at the workplace. The requirements for management of hazardous materials is summarised below.</p>\r\n<h3 class=\"p2\"><strong>Synthetic Mineral Fibre (SMF)</strong></h3>\r\n<p class=\"p1\">&nbsp;Synthetic Mineral Fibre (SMF) is a man-made insulation material used extensively in industrial, commercial and residential sites as fire rating, reinforcement in construction materials and as acoustic and thermal insulators. Types of SMF materials include fibreglass, rockwool, ceramic fibres and continuous glass filaments.</p>\r\n<p class=\"p1\">There are two basic forms of Synthetic Mineral Fibre (SMF) insulation, bonded and un-bonded.</p>\r\n<ul>\r\n<li>Bonded SMF is where adhesives, binders or cements have been applied to the SMF before delivery and the SMF product has a specific shape.</li>\r\n<li>Un-bonded SMF has no adhesives, binders or cements and the SMF is loose material packed into a package.</li>\r\n</ul>\r\n<p class=\"p1\">Exposure to SMF can result in short-term skin, eye and respiratory irritation. SMF is also classified as a possible human carcinogen with a possible increase in risk in lung cancer from long-term exposure.</p>\r\n<p class=\"p1\">The use of and the safe removal of SMF materials should be conducted in accordance with the National Code of Practice for the safe use of Synthetic Mineral Fibres [NOHSC: 2006 (1990)].</p>\r\n<h3 class=\"p2\"><strong>Polychlorinated Biphenyls (PCBs)</strong></h3>\r\n<p class=\"p1\">&nbsp;Polychlorinated Biphenyls (PCBs) are a toxic organochlorine used as insulating fluids in electrical equipment such as transformers, capacitors and fluorescent light ballasts that were largely banned from importation in Australia in the 1970s.</p>\r\n<p class=\"p1\">PCBs are listed as a probable human carcinogen and should be managed in accordance with the ANZECC</p>\r\n<p class=\"p1\">Polychlorinated Biphenyls Management Plan, 2003. The handling and disposal of PCBs must be performed in accordance with applicable state and commonwealth environmental protection laws as scheduled PCB waste.</p>\r\n<p class=\"p1\">The following Personal Protective Equipment (PPE) should be worn when handling items containing or suspected to contain PCBs - nitrile gloves, eye protection, and disposable overalls. The PPE should be worn when removing capacitors from light fittings in case PCBs leak from the capacitor housing.</p>\r\n<h3 class=\"p2\"><strong>Lead Paint</strong></h3>\r\n<p class=\"p1\">&nbsp;Lead paint, as defined by the Australian Standard \"AS4361.2: 1998 Guide to Lead Paint Management; Part 2:</p>\r\n<p class=\"p1\">Residential and Commercial Buildings\", is that which contains in excess of 1% Lead by weight.</p>\r\n<p class=\"p1\">Lead carbonate (white lead) was once the main white pigment in paints for houses and public buildings. Paint with lead pigment was manufactured up until the late 1960\'s, and in 1969 the National Health and Medical Research Council\'s Uniform Paint Standard was amended to restrict lead content in domestic paint.</p>\r\n<p class=\"p1\">Lead in any form is toxic to humans when ingested or inhaled, with repeated transmission of particles cumulating in lead poisoning. Lead paint is assessed based on two potential routes of exposure. Firstly by the likelihood of inhalation or ingestion by people working in the vicinity of the paint and secondly by the condition of the paint. Paint that is flaking or in poor condition is more likely to be ingested than paint that is in a good, stable condition.</p>\r\n<p class=\"p1\">Any work relating to lead paint should be conducted in accordance with the \'National Code of Practice for the Control and Safe Use of Inorganic Lead at Work [NOHSC: 2015 (1994)]\'.</p>','<p class=\"p1\">&nbsp;The Occupational Health and Safety Regulations of most Australian states &amp; territories have requirements for the identification and control of risks within workplaces. These broad requirements extends to the hazardous materials that may be present within buildings at the workplace. The requirements for management of hazardous materials is summarised below.</p>\r\n<p class=\"p2\"><strong>Synthetic Mineral Fibre (SMF)</strong></p>\r\n<p class=\"p1\">&nbsp;Synthetic Mineral Fibre (SMF) is a man-made insulation material used extensively in industrial, commercial and residential sites as fire rating, reinforcement in construction materials and as acoustic and thermal insulators. Types of SMF materials include fibreglass, rockwool, ceramic fibres and continuous glass filaments.</p>\r\n<p class=\"p1\">There are two basic forms of Synthetic Mineral Fibre (SMF) insulation, bonded and un-bonded.</p>\r\n<ul>\r\n<li>Bonded SMF is where adhesives, binders or cements have been applied to the SMF before delivery and the SMF product has a specific shape.</li>\r\n<li>Un-bonded SMF has no adhesives, binders or cements and the SMF is loose material packed into a package.</li>\r\n</ul>\r\n<p class=\"p1\">Exposure to SMF can result in short-term skin, eye and respiratory irritation. SMF is also classified as a possible human carcinogen with a possible increase in risk in lung cancer from long-term exposure.</p>\r\n<p class=\"p1\">The use of and the safe removal of SMF materials should be conducted in accordance with the National Code of Practice for the safe use of Synthetic Mineral Fibres [NOHSC: 2006 (1990)].</p>\r\n<p class=\"p2\"><strong>Polychlorinated Biphenyls (PCBs)</strong></p>\r\n<p class=\"p1\">&nbsp;Polychlorinated Biphenyls (PCBs) are a toxic organochlorine used as insulating fluids in electrical equipment such as transformers, capacitors and fluorescent light ballasts that were largely banned from importation in Australia in the 1970s.</p>\r\n<p class=\"p1\">PCBs are listed as a probable human carcinogen and should be managed in accordance with the ANZECC Polychlorinated Biphenyls Management Plan, 2003. The handling and disposal of PCBs must be performed in accordance with applicable state and commonwealth environmental protection laws as scheduled PCB waste.</p>\r\n<p class=\"p1\">The following Personal Protective Equipment (PPE) should be worn when handling items containing or suspected to contain PCBs - nitrile gloves, eye protection, and disposable overalls. The PPE should be worn when removing capacitors from light fittings in case PCBs leak from the capacitor housing.</p>\r\n<p class=\"p2\"><strong>Lead Paint</strong></p>\r\n<p class=\"p1\">Lead paint, as defined by the Australian Standard \"AS4361.2: 1998 Guide to Lead Paint Management; Part 2:</p>\r\n<p class=\"p1\">Residential and Commercial Buildings\", is that which contains in excess of 1% Lead by weight.</p>\r\n<p class=\"p1\">Lead carbonate (white lead) was once the main white pigment in paints for houses and public buildings. Paint with lead pigment was manufactured up until the late 1960\'s, and in 1969 the National Health and Medical Research Council\'s Uniform Paint Standard was amended to restrict lead content in domestic paint.</p>\r\n<p class=\"p1\">Lead in any form is toxic to humans when ingested or inhaled, with repeated transmission of particles cumulating in lead poisoning. Lead paint is assessed based on two potential routes of exposure. Firstly by the likelihood of inhalation or ingestion by people working in the vicinity of the paint and secondly by the condition of the paint. Paint that is flaking or in poor condition is more likely to be ingested than paint that is in a good, stable condition.</p>\r\n<p class=\"p1\">Any work relating to lead paint should be conducted in accordance with the \'National Code of Practice for the Control and Safe Use of Inorganic Lead at Work [NOHSC: 2015 (1994)]\'.</p>','<p class=\"p1\">This report has been prepared in accordance with the agreement between Goodman Property and Noel Arnold &amp; Associates.</p>\r\n<p class=\"p1\">Within the limitations of the agreed upon scope of services, this work has been undertaken and performed in a professional manner, in accordance with generally accepted practices, using a degree of skill and care ordinarily exercised by members of its profession and consulting practice. No other warranty, expressed or implied, is made.</p>\r\n<p class=\"p1\">This report is solely for the use of Goodman Property and any reliance on this report by third parties shall be at such party\'s sole risk and may not contain sufficient information for purposes of other parties or for other uses. This report shall only be presented in full and may not be used to support any other objective than those set out in the report, except where written approval with comments are provided by Noel Arnold &amp; Associates.</p>\r\n<p class=\"p1\">This report relates only to the identification of asbestos containing materials used in the construction of the building and does not include the identification of dangerous goods or hazardous substances in the form of chemicals used, stored or manufactured within the building or plant.</p>\r\n<p class=\"p1\">The following should also be noted:</p>\r\n<p class=\"p1\">While the survey has attempted to locate the asbestos containing materials within the site it should be noted that the review was a visual inspection and a limited sampling program was conducted and/or the analysis results of the previous report were used.</p>\r\n<p class=\"p1\">Representative samples of suspect asbestos materials were collected for analysis. Other asbestos materials of similar appearance are assumed to have a similar content.</p>\r\n<p class=\"p1\">Not all suspected asbestos materials were sampled. Only those asbestos materials that were physically accessible could be located and identified. Therefore it is possible that asbestos materials, which may be concealed within inaccessible areas/voids, may not have been located during the audit. Such inaccessible areas fall into a number of categories.</p>\r\n<p class=\"p1\">(a) Locations behind locked doors;</p>\r\n<p class=\"p1\">(b) Inset ceilings or wall cavities;</p>\r\n<p class=\"p1\">(c) Those areas accessible only by dismantling equipment or performing minor localised demolition works;</p>\r\n<p class=\"p1\">(d) Service shafts, ducts etc., concealed within the building structure;</p>\r\n<p class=\"p1\">(e) Energised services, gas, electrical, pressurised vessel and chemical lines;</p>\r\n<p class=\"p1\">(f) Voids or internal areas of machinery, plant, equipment, air-conditioning ducts etc;</p>\r\n<p class=\"p1\">(g) Totally inaccessible areas such as voids and cavities created and intimately concealed within the building structure. These voids are only accessible during major demolition works;</p>\r\n<p class=\"p1\">(h) Height restricted areas</p>\r\n<p class=\"p1\">(i) Areas deemed unsafe or hazardous at time of audit.</p>\r\n<p class=\"p1\">In addition to areas that were not accessible, the possible presence of hazardous building materials may not have been assessed</p>\r\n<p class=\"p1\">because it was not considered practicable as:</p>\r\n<ol>\r\n<li>It would require unnecessary dismantling of equipment; and/or</li>\r\n<li>It was considered disruptive to the normal operations of the building; and/or</li>\r\n<li>It may have caused unnecessary damage to equipment, furnishings or surfaces; and/or</li>\r\n<li>The hazardous material was not considered to represent a significant exposure risk; and</li>\r\n<li>The time taken to determine the presence of the hazardous building material was considered prohibitive.</li>\r\n</ol>\r\n<p class=\"p1\">Only minor destructive auditing and sampling techniques were employed to gain access to those areas documented in the Hazardous Materials Register. Consequently, without substantial demolition of the building, it is not possible to guarantee that every source of hazardous material has been detected.</p>\r\n<p class=\"p1\">During the course of normal site works care should be exercised when entering any previously inaccessible areas or areas mentioned above and it is imperative that work cease pending further sampling if materials suspected of containing asbestos or unknown materials are encountered. Therefore during any refurbishment or demolition works, further investigations and assessment may be required should any suspect material be observed in previously inaccessible areas or areas not fully inspected previously, i.e. carpeted floors.</p>\r\n<p class=\"p1\">This report is not intended to be used for the purposes of tendering, programming of works, refurbishment works or demolition works unless used in conjunction with a specification detailing the extent of the works. To ensure its contextual integrity, the report must be read in its entirety and should not be copied, distributed or referred to in part only.</p>');

/*!40000 ALTER TABLE `tbl_report_common` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_document`;

CREATE TABLE `tbl_site_document` (
  `docID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`docID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_document` WRITE;
/*!40000 ALTER TABLE `tbl_site_document` DISABLE KEYS */;

INSERT INTO `tbl_site_document` (`docID`, `siteID`, `full_path`)
VALUES
	(52,177,'abbotsford.jpeg'),
	(53,177,'AustralianSuper_Master_RGB.jpg'),
	(54,17,''),
	(55,0,''),
	(56,0,'images.png'),
	(57,0,'images.png'),
	(58,0,'images.png'),
	(59,0,'images.png'),
	(60,290,'images.png'),
	(61,17,''),
	(62,1,''),
	(63,176,''),
	(64,176,''),
	(65,285,'AMP 50 Bridge Street, Sydney NSW Asbestos Sample Register.pdf'),
	(66,222,'');

/*!40000 ALTER TABLE `tbl_site_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_images`;

CREATE TABLE `tbl_site_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_images` WRITE;
/*!40000 ALTER TABLE `tbl_site_images` DISABLE KEYS */;

INSERT INTO `tbl_site_images` (`imageID`, `siteID`, `full_path`)
VALUES
	(2,17,'default-image.png'),
	(3,1,'abbotsford.jpeg'),
	(4,0,'default-image1.png'),
	(5,118,'default-image6.png'),
	(6,119,'default-image7.png'),
	(7,120,'default-image8.png'),
	(8,122,'default-image12.png'),
	(9,123,'default-image13.png'),
	(10,127,'image001.png'),
	(11,178,'default-image15.png'),
	(12,179,'default-image16.png'),
	(13,220,'default-image17.png'),
	(14,222,'IMG_2432.JPG'),
	(15,223,'checked1.png'),
	(16,224,'checked2.png'),
	(17,225,'reporting.png'),
	(18,226,'checked3.png'),
	(19,227,'checked4.png'),
	(20,228,'checked5.png'),
	(21,229,'checked6.png'),
	(22,230,'checked7.png'),
	(23,231,'checked8.png'),
	(24,232,'reporting2.png'),
	(25,236,'checked9.png'),
	(26,237,'checked10.png'),
	(27,238,'checked17.png'),
	(28,246,'checked18.png'),
	(29,258,'checked20.png'),
	(30,261,'checked21.png'),
	(31,262,'checked22.png'),
	(32,263,'reporting1.png'),
	(33,266,'checked23.png'),
	(34,267,'checked24.png'),
	(35,268,'reporting2.png'),
	(36,269,'checked26.png'),
	(37,271,'reporting3.png'),
	(38,272,'checked27.png'),
	(39,272,'checked28.png'),
	(40,273,'checked29.png'),
	(41,274,'reporting4.png'),
	(42,275,'reporting5.png'),
	(43,264,'checked30.png'),
	(44,280,'Greencap_logo.png'),
	(45,282,'risktech-logo.png'),
	(46,283,'024.jpg'),
	(47,284,'risktech-logo_(1).png'),
	(48,285,'006.jpg');

/*!40000 ALTER TABLE `tbl_site_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_plan`;

CREATE TABLE `tbl_site_plan` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_plan` WRITE;
/*!40000 ALTER TABLE `tbl_site_plan` DISABLE KEYS */;

INSERT INTO `tbl_site_plan` (`imageID`, `siteID`, `full_path`)
VALUES
	(1,230,'reporting1.png'),
	(2,233,'checked.png'),
	(3,235,'checked1.png'),
	(4,238,'reporting3.png'),
	(5,246,'reporting4.png'),
	(6,258,'reporting5.png'),
	(7,263,'checked2.png'),
	(8,268,'checked3.png'),
	(9,270,'image001.jpg'),
	(10,262,'reporting8.png'),
	(11,273,'reporting7.png'),
	(12,274,'checked4.png'),
	(13,275,'checked5.png'),
	(14,264,'reporting11.png'),
	(15,280,'Siteplan_test.png'),
	(16,282,'Siteplan-Example.jpg'),
	(17,283,'IMAG0337.jpg'),
	(18,17,'images.png'),
	(19,177,'images4.png');

/*!40000 ALTER TABLE `tbl_site_plan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) DEFAULT NULL,
  `site_url` text,
  `site_type` varchar(255) DEFAULT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `tenant` varchar(255) DEFAULT NULL,
  `site_contact` varchar(255) DEFAULT NULL,
  `description` longtext,
  `site` varchar(255) DEFAULT NULL,
  `property_number` int(11) DEFAULT NULL,
  `full_address` varchar(255) DEFAULT NULL,
  `survey_date` varchar(255) DEFAULT NULL,
  `levels` int(11) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `building_age` int(11) DEFAULT NULL,
  `construction_type` varchar(255) DEFAULT NULL,
  `previous_report` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roof_type` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT 'N',
  `clientID` int(11) DEFAULT NULL,
  `report_name` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `state` int(2) DEFAULT '0',
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `site_description` mediumtext,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `building_name`, `site_url`, `site_type`, `no_floors`, `tenant`, `site_contact`, `description`, `site`, `property_number`, `full_address`, `survey_date`, `levels`, `inspected_by`, `contact_title`, `building_age`, `construction_type`, `previous_report`, `company`, `date_inspected`, `area`, `roof_type`, `site_image`, `hide`, `clientID`, `report_name`, `city`, `post_code`, `state`, `date_updated`, `site_description`)
VALUES
	(1,'Test Site 1','test url','Test Type',4,'Cecilla Chapman','Cecilla Chapman','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','Abbotsford',320001,'445','26-08-2016',1,'Andrew Pakeham','Senior Personal Banker',1960,'Brick','Yes','XYZ Limited','10-08-2016','100 m2','metal','','N',8,'','City','0000',5,'2017-06-28 10:15:06',''),
	(17,'Ofiice',NULL,NULL,NULL,NULL,'John Smith',NULL,' Capital building',123456,'123 Victoria Street','',1,'Nigel johnson','Manager',1982,'Brick','Yes','ABC','01-01-2008','250m2','Metal',NULL,'N',8,'Asbestos Risk Assesment Report','','',0,'2017-06-28 10:14:15',''),
	(176,'Units 1 - 3, 12 Mars Road',NULL,NULL,NULL,NULL,'',NULL,'AU019 Trans tech Business Park',0,'12','',3,'John Smith','',1989,'Concrete','','NAA','','80000m2','Concrete',NULL,'N',1,'','','',0,'2017-06-28 10:16:50',''),
	(222,'Main Building',NULL,NULL,NULL,NULL,'Joe Smith',NULL,'12 High Street, Big Town',12,'12 High Street','24-05-2017',2,'Bernard Day','Building Manager',1978,'Brick','No','','','2000','Concrete',NULL,'N',1,'Report Name','Big Town','2000',1,'2017-08-24 10:03:43','<p>The site was built in 1978 is a 2 storey office wbuilding with a warehouse style rear carperk area.</p>'),
	(262,'Building name',NULL,NULL,NULL,NULL,'Inspected By',NULL,'test timestamp2',0,'Building name','01-11-2016',0,'Inspected By','test',0,'brick','','Company','25-10-2016','100m2','metal',NULL,'Y',14,'Test','City','0000',5,'2017-05-09 11:46:40','This is a test site description'),
	(264,'Building name',NULL,NULL,NULL,NULL,'Inspected By',NULL,'test timestamp2',1,'Building name','01-11-2016',0,'0','test',1999,'test','Yes','haztech test','15-11-2016','100m2','metal',NULL,'N',8,'Report Name','City','0000',5,'2017-05-09 11:19:24','<p>This is a test site description</p>'),
	(265,'Building name',NULL,NULL,NULL,NULL,'Inspected By',NULL,'test timestamp2',1,'Building name','08-11-2016',0,'Inspected By','test',1999,'test','Yes','haztech test','01-11-2016','','',NULL,'Y',1,'','City','0000',1,'2016-10-26 12:36:47',NULL),
	(266,'Building name',NULL,NULL,NULL,NULL,'Inspected By',NULL,'Test',1,'Building name','08-11-2016',0,'Inspected By','test',1999,'test','Yes','haztech test','01-11-2016','100m2','metal',NULL,'Y',1,'Test','City]','0000',1,'2016-10-26 12:36:44',NULL),
	(281,'Building name',NULL,NULL,NULL,NULL,'Test Site Contact',NULL,'Test site 1',1891,'15A','16-11-2016',12,'Inspected By','Manager',1620,'brick','No','greencap test','22-11-2016','5000m2','concrete',NULL,'N',1,'Test Report Name','Test City','3000',5,'2017-05-04 17:16:16',NULL),
	(282,'Building name',NULL,NULL,NULL,NULL,'Inspected By',NULL,'Test site 2',1891,'15A','21-11-2016',12,'Inspected By','test',1999,'brick','No','haztech test','22-11-2016','100m2','metal',NULL,'N',1,'Test','Test City','3000',5,'2017-05-04 17:16:12',NULL),
	(283,'Goldsmith House',NULL,NULL,NULL,NULL,'Fred Bloggs',NULL,'Test site GH',88888,'15 Smith Street','12-01-2017',8,'GJH','Property Manager',1976,'Steel + Concrete','Yes','Knight Frank - change heading to Client','10-01-2017','6500','flat concrete',NULL,'N',1,'Remove','Sydney','2000',1,'2017-05-04 17:16:14',NULL),
	(284,'Building name',NULL,NULL,NULL,NULL,'Test Site Contact',NULL,'Test Site',456,'85 Test Street','27-03-2017',12,'Inspected By','Test Contact Title',1885,'Brick','No','Company Test','28-03-2017','100m2','roof type ',NULL,'Y',8,'Report Name','City','0000',5,'2017-03-06 11:53:32',NULL),
	(285,'City CBD',NULL,NULL,NULL,NULL,'John Smith',NULL,'June 2017',123,'zzz','',88,'Fred Bloggs','Mr',1970,'mud','Yes','Hibbs','27-06-2012','50000','Straw',NULL,'N',24,'????? Why','Sydney','2000',0,'2017-06-28 13:27:01','<p>Why is this a dialogue box.??? &nbsp;with File/Edit/etc/etc.. and there is a \"p\" at the bottom.?</p>');

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`, `hide`)
VALUES
	(3,'B','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',1,'September 21, 2017, 1:56 pm','N'),
	(16,'F','clientgeneral@swim.com.au','d28d4ecc1fa0fddc6efcce683e834b59','Client General','clientgeneral@swim.com.au','A',8,'October 26, 2016, 2:31 pm','N'),
	(17,'F','clientadmin@swim.com.au','d3b3da8d67d2a7bdbbb88447e8466d93','Client Admin','clientadmin@swim.com.au','A',1,'September 29, 2016, 2:58 pm','N'),
	(18,'B','risktechadmin@swim.com.au','b3d803edd45aff5a9196c0a43811cbef','Risktech Admin','risktechadmin@swim.com.au','A',NULL,NULL,'N'),
	(19,'C','risktechgeneral@swim.com.au','75d71d93550ab3911ee592363a77080c','Risktech General','risktechgeneral@swim.com.au','A',8,NULL,'N'),
	(20,'D','risktechcontractor@swim.com.au','60db2811e337c3eabd1ee88d6dcd8550','Risktech Contractor','risktechcontractor@swim.com.au','A',NULL,NULL,'N'),
	(21,'A','superadmin@swim.net.au','17c4520f6cfd1ab53d8745e84681eb49','Super Admin','superadmin@swim.net.au','A',8,'August 24, 2017, 9:58 am','N'),
	(34,'F','testclientgeneral@swim.com.au','0cec358bdaaabfeaf7b4d18c4695b89b','testclientgeneral','testclientgeneral@swim.com.au','A',NULL,'October 25, 2016, 3:04 pm','Y'),
	(35,'E','clientgeneraltest@swim.com.au','7cf1c46862a09971261f1f4866d60620','clientgeneraltest','clientgeneraltest@swim.com.au','A',8,'October 25, 2016, 2:56 pm','N'),
	(36,'F','testclientgeneral@swim.com.au','0cec358bdaaabfeaf7b4d18c4695b89b','testclientgeneral@swim.co.au','testclientgeneral@swim.com.au','A',1,'October 25, 2016, 3:04 pm','N'),
	(37,'C','adminuser@swim.com.au','098f6bcd4621d373cade4e832627b4f6','adminuser','adminuser@swim.com.au','I',14,NULL,'N'),
	(38,'F','clientgeneralhaztech@swim.com.au','264b1f6b792c42a5ae3b1ac3a1bd968c','clientgeneralhaztech','clientgeneralhaztech@swim.com.au','A',8,'October 26, 2016, 2:51 pm','N');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Risktech General','C'),
	(4,'Risktech Contractor','D'),
	(5,'Client Admin','E'),
	(6,'Client General','F');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

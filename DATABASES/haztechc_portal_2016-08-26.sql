# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: haztechc_portal
# Generation Time: 2016-08-26 04:08:39 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`)
VALUES
	(1,'Sample Blog Post 1','29/04/2016','5','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','Array'),
	(2,'Sample Blog Post','29/04/2016','4','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','public'),
	(3,'Sample Blog Post','29/04/2016','demouser','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','daa796b804bb9411820d6c7cec7e1b16ABR-Online-Issue-17.pdf','public'),
	(7,'this is test 2','2016/08/16',NULL,'<p>This is test 2</p>','Test image',NULL);

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_access`;

CREATE TABLE `tbl_blog_access` (
  `blogaccessID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(5) DEFAULT NULL,
  `type_letter` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`blogaccessID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_access` WRITE;
/*!40000 ALTER TABLE `tbl_blog_access` DISABLE KEYS */;

INSERT INTO `tbl_blog_access` (`blogaccessID`, `blogID`, `type_letter`)
VALUES
	(5,2,'0'),
	(8,1,'B'),
	(9,1,'D');

/*!40000 ALTER TABLE `tbl_blog_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `item_no` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `room_specific` varchar(255) DEFAULT NULL,
  `hazard_type` varchar(255) DEFAULT NULL,
  `samples_taken` varchar(11) DEFAULT NULL,
  `sample_no` varchar(255) DEFAULT NULL,
  `sample_status` varchar(11) DEFAULT NULL,
  `extent` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_number` mediumint(25) DEFAULT NULL,
  `recommendations` longtext,
  `item_condition` varchar(255) DEFAULT NULL,
  `disturb_potential` varchar(255) DEFAULT NULL,
  `risk_rating` varchar(255) DEFAULT NULL,
  `current_label` varchar(11) DEFAULT NULL,
  `control_priority` varchar(255) DEFAULT NULL,
  `work_records` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` text,
  `location_level` varchar(255) DEFAULT NULL,
  `friability` varchar(255) DEFAULT NULL,
  `control_recommendation` longtext,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_items` WRITE;
/*!40000 ALTER TABLE `tbl_items` DISABLE KEYS */;

INSERT INTO `tbl_items` (`itemID`, `siteID`, `item_no`, `item_name`, `room_specific`, `hazard_type`, `samples_taken`, `sample_no`, `sample_status`, `extent`, `contact`, `contact_number`, `recommendations`, `item_condition`, `disturb_potential`, `risk_rating`, `current_label`, `control_priority`, `work_records`, `description`, `image`, `location_level`, `friability`, `control_recommendation`)
VALUES
	(6,1,1,'Material','Meeting Room','test','yes','1','Negative','300 m2','Test',0,NULL,'Good','Low','Low','Yes','Low','No Action','Roof - Corrugated cement','','External - Ground','Friable','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim<span style=\"color: #666666; font-family: Verdana, Geneva, sans-serif; font-size: 10px;\">.</span></p>'),
	(7,5,2,'Test','Meeting Room','Type','Yes','1','Positive','1','Contact Name',1231212,NULL,'Good','test','Test','Test','Test','Test',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) DEFAULT NULL,
  `site_url` text,
  `site_type` varchar(255) DEFAULT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `tenant` varchar(255) DEFAULT NULL,
  `site_contact` varchar(255) DEFAULT NULL,
  `description` longtext,
  `site` varchar(255) DEFAULT NULL,
  `property_number` int(11) DEFAULT NULL,
  `full_address` double DEFAULT NULL,
  `survey_date` varchar(255) DEFAULT NULL,
  `levels` int(11) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `building_age` int(11) DEFAULT NULL,
  `construction_type` varchar(255) DEFAULT NULL,
  `previous_report` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roof_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `building_name`, `site_url`, `site_type`, `no_floors`, `tenant`, `site_contact`, `description`, `site`, `property_number`, `full_address`, `survey_date`, `levels`, `inspected_by`, `contact_title`, `building_age`, `construction_type`, `previous_report`, `company`, `date_inspected`, `area`, `roof_type`)
VALUES
	(1,'Test Site 1','test url','Test Type',4,'Cecilla Chapman','Cecilla Chapman','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','Abbotsford',320001,445,'26-08-2016',1,'Andrew Pakeham','Senior Personal Banker',1960,'Brick','Yes','NAA','10-08-2016','100 m2','metal'),
	(5,'Test 2','test','test',3,'Test','Test','<p>Test</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`)
VALUES
	(3,'B','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',NULL,'August 26, 2016, 12:31 pm'),
	(6,'F','Test_password','Password','Test password','test',NULL,NULL,NULL),
	(7,'F','testforpw','Test','Test For PW','testemail','A',NULL,'A'),
	(10,'F','test','test','test pass','test',NULL,NULL,NULL),
	(11,'F','test','test','test','test',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Risktech General','C'),
	(4,'Risktech Contractor','D'),
	(5,'Client Admin','E'),
	(6,'Client General','F');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

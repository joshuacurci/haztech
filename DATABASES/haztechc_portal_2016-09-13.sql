# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: haztechc_portal
# Generation Time: 2016-09-13 05:52:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_posted` timestamp NULL DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `content` text,
  `image` text,
  `access` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog` WRITE;
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;

INSERT INTO `tbl_blog` (`blogID`, `title`, `date_posted`, `author`, `content`, `image`, `access`, `hide`)
VALUES
	(1,'Sample Blog Post 1','0000-00-00 00:00:00','5','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','Array',NULL),
	(2,'Sample Blog Post 3','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(3,'Sample Blog Post','0000-00-00 00:00:00','3','<p>The 2016 addition of the National Construction Code &ndash; Building Code of Australia (BCA) has been published. Changes affecting Volume 1 and 2 include new verification methods for structural robustness and ventilation. Clarification has also been provided for what constitutes as \"constant\" for stair going and risers. A number of new referenced documents including Australian standards have been adopted for NCC 2016 as well as amendments to existing referenced documents.</p>\r\n<p>Changes are specifically affecting Volume 1 relates to clarification added to the definition of \"effective height\" and provides a determination Method used to identify the lower story included in a calculation of rise in stories. New provisions have been included to permit the construction of timber midrise buildings which are Class 2, 3 or 5, sprinkler protected and not more than 25 m effective height.</p>','','public',NULL),
	(7,'Sample Blog Post 2','2016-08-16 00:00:00','3','<p>This is test 2</p>','',NULL,NULL);

/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_access`;

CREATE TABLE `tbl_blog_access` (
  `blogaccessID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(5) DEFAULT NULL,
  `type_letter` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`blogaccessID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_blog_access` WRITE;
/*!40000 ALTER TABLE `tbl_blog_access` DISABLE KEYS */;

INSERT INTO `tbl_blog_access` (`blogaccessID`, `blogID`, `type_letter`)
VALUES
	(5,2,'0'),
	(8,1,'B'),
	(9,1,'D');

/*!40000 ALTER TABLE `tbl_blog_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_blog_images`;

CREATE TABLE `tbl_blog_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_blog_images` WRITE;
/*!40000 ALTER TABLE `tbl_blog_images` DISABLE KEYS */;

INSERT INTO `tbl_blog_images` (`imageID`, `blogID`, `full_path`)
VALUES
	(3,2,'greencap-logo.png');

/*!40000 ALTER TABLE `tbl_blog_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `site_address`, `date_inspected`, `inspected_by`, `hide`)
VALUES
	(1,'GreenCap','Hawthorn West 3122','08-09-2016','Bernard Day','N'),
	(7,'Goodman Property','12 Mars Road, lane Cove NSW 2066','01-02-2013','','Y'),
	(8,'Haztech','123 Lean Road, VIC 2210','23-09-2016','John Doe','N');

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_images`;

CREATE TABLE `tbl_client_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_client_images` WRITE;
/*!40000 ALTER TABLE `tbl_client_images` DISABLE KEYS */;

INSERT INTO `tbl_client_images` (`imageID`, `clientID`, `full_path`)
VALUES
	(1,1,'greencap-logo2.png'),
	(2,8,'risktech-logo1.png');

/*!40000 ALTER TABLE `tbl_client_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_hazard_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_hazard_types`;

CREATE TABLE `tbl_hazard_types` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_hazard_types` WRITE;
/*!40000 ALTER TABLE `tbl_hazard_types` DISABLE KEYS */;

INSERT INTO `tbl_hazard_types` (`typeID`, `type_name`)
VALUES
	(1,'Asbestos'),
	(2,'SMF Products'),
	(3,'Lead Products'),
	(4,'PCBs');

/*!40000 ALTER TABLE `tbl_hazard_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_item_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_item_images`;

CREATE TABLE `tbl_item_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_item_images` WRITE;
/*!40000 ALTER TABLE `tbl_item_images` DISABLE KEYS */;

INSERT INTO `tbl_item_images` (`imageID`, `itemID`, `full_path`)
VALUES
	(1,20,'default-image.png'),
	(2,24,'default-image1.png');

/*!40000 ALTER TABLE `tbl_item_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `item_no` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `room_specific` varchar(255) DEFAULT NULL,
  `hazard_type` int(11) DEFAULT NULL,
  `samples_taken` varchar(225) DEFAULT NULL,
  `sample_no` varchar(255) DEFAULT NULL,
  `sample_status` varchar(11) DEFAULT NULL,
  `extent` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_number` mediumint(25) DEFAULT NULL,
  `recommendations` longtext,
  `item_condition` varchar(255) DEFAULT NULL,
  `disturb_potential` varchar(255) DEFAULT NULL,
  `risk_rating` varchar(255) DEFAULT NULL,
  `current_label` varchar(11) DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `control_priority` varchar(255) DEFAULT NULL,
  `work_records` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` text,
  `location_level` varchar(255) DEFAULT NULL,
  `friability` varchar(255) DEFAULT NULL,
  `control_recommendation` longtext,
  `hide` char(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_items` WRITE;
/*!40000 ALTER TABLE `tbl_items` DISABLE KEYS */;

INSERT INTO `tbl_items` (`itemID`, `siteID`, `item_no`, `item_name`, `room_specific`, `hazard_type`, `samples_taken`, `sample_no`, `sample_status`, `extent`, `contact`, `contact_number`, `recommendations`, `item_condition`, `disturb_potential`, `risk_rating`, `current_label`, `date_updated`, `control_priority`, `work_records`, `description`, `image`, `location_level`, `friability`, `control_recommendation`, `hide`, `clientID`)
VALUES
	(6,1,1,'Material','Meeting Room',0,'yes','1','Negative','300 m2','Test',0,NULL,'Good','Non-selected','Low','Yes','2016-09-08 13:45:27','Non-selected','No Action','Roof - Corrugated cement','','External - Ground','Friable','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim<span style=\"color: #666666; font-family: Verdana, Geneva, sans-serif; font-size: 10px;\">.</span></p>','N',1),
	(7,5,2,'Test','Meeting Room',0,'Yes','1','Positive','1','Contact Name',1231212,NULL,'Good','test','Test','Test','2016-09-01 16:58:37','Test','Test',NULL,NULL,NULL,NULL,NULL,'N',NULL),
	(18,17,NULL,NULL,'',NULL,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 11:09:31','',NULL,'',NULL,'','','','Y',NULL),
	(19,1,NULL,NULL,'',NULL,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 15:35:57','',NULL,'',NULL,'','','','Y',1),
	(20,17,NULL,NULL,'Below Sink',4,NULL,'J108740-AU019-50','Negative','Level 1 - Throughout',NULL,NULL,NULL,'Fair','High Accessibility','Low','Suspect','2016-09-09 13:04:34','P1',NULL,'Beams - Sprayed Vermiculite',NULL,'kitchenettes','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolitaion or refurbishment.</p>','N',17),
	(21,1,NULL,NULL,'',1,NULL,'','','',NULL,NULL,NULL,'','','','','2016-09-08 15:36:01','',NULL,'',NULL,'','','','Y',1),
	(22,1,NULL,NULL,'Ceiling',4,NULL,'J108740-AU019-52','','1 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Medium','Suspect','2016-09-08 15:41:30','P1',NULL,'Compressed cement sheet',NULL,'Level 2','Friable','<p>Engage a license asbestos conrtactor to undertake remedial/removal works on this item as soon as practicable (within 3 months)</p>','N',1),
	(23,1,NULL,NULL,'South',4,NULL,'Similar to J108740-AU019-52','','7 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect','2016-09-08 15:43:21','P2',NULL,'Service Riser - Moulded Fibre Cement Flue',NULL,'Wasrehouse - Underside Of  Roof','Friable','<p>Maintain in current condition, label and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',1),
	(24,17,NULL,NULL,'Underside of roof',4,NULL,'','','1000m2',NULL,NULL,NULL,'Bad','High Accessibility','Low','Suspect','2016-09-09 13:07:54','P3',NULL,'Roof Lining - Insulation Material',NULL,'Warehouse','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Removed under controlled conditions prior to demolition or refurbishment</p>','N',17),
	(25,177,NULL,NULL,'North',1,NULL,'','Assumed Neg','500m2',NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect',NULL,'P3',NULL,'Beams - Sprayed Vermiculite',NULL,'Warehouse','Friable','<p>Maintain in Good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment.</p>','N',177),
	(26,177,NULL,NULL,'Interior - Unit 14',2,NULL,'','Suspected P','3 Unit/s',NULL,NULL,NULL,'Good','Low Accessibility','Low','Suspect',NULL,'P3',NULL,'Hot water sevice insulation - Insulation material',NULL,'Ground Level','Non-Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under contolled conditions prior to demolition or refurbishment.</p>','N',177),
	(27,177,NULL,NULL,'Office Area - Throughout',2,NULL,'','Suspected P','4000m2',NULL,NULL,NULL,'Fair','Medium Accessibility','Low','Suspect',NULL,'P3',NULL,'Ceiling Tiles - INsulation Material',NULL,'Level One','Friable','<p>Maintain in good condition and incorporate into a HMMP. Remove under controlled conditions prior to demolition or refurbishment&nbsp;</p>','N',177);

/*!40000 ALTER TABLE `tbl_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_site_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_site_images`;

CREATE TABLE `tbl_site_images` (
  `imageID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `full_path` text,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_site_images` WRITE;
/*!40000 ALTER TABLE `tbl_site_images` DISABLE KEYS */;

INSERT INTO `tbl_site_images` (`imageID`, `siteID`, `full_path`)
VALUES
	(2,17,'default-image.png');

/*!40000 ALTER TABLE `tbl_site_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) DEFAULT NULL,
  `site_url` text,
  `site_type` varchar(255) DEFAULT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `tenant` varchar(255) DEFAULT NULL,
  `site_contact` varchar(255) DEFAULT NULL,
  `description` longtext,
  `site` varchar(255) DEFAULT NULL,
  `property_number` int(11) DEFAULT NULL,
  `full_address` double DEFAULT NULL,
  `survey_date` varchar(255) DEFAULT NULL,
  `levels` int(11) DEFAULT NULL,
  `inspected_by` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `building_age` int(11) DEFAULT NULL,
  `construction_type` varchar(255) DEFAULT NULL,
  `previous_report` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_inspected` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `roof_type` varchar(255) DEFAULT NULL,
  `site_image` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `building_name`, `site_url`, `site_type`, `no_floors`, `tenant`, `site_contact`, `description`, `site`, `property_number`, `full_address`, `survey_date`, `levels`, `inspected_by`, `contact_title`, `building_age`, `construction_type`, `previous_report`, `company`, `date_inspected`, `area`, `roof_type`, `site_image`, `hide`, `clientID`)
VALUES
	(1,'Test Site 1','test url','Test Type',4,'Cecilla Chapman','Cecilla Chapman','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>','Abbotsford',320001,445,'26-08-2016',1,'Andrew Pakeham','Senior Personal Banker',1960,'Brick','Yes','NAA','10-08-2016','100 m2','metal','','N',1),
	(17,'Ofiice',NULL,NULL,NULL,NULL,'John Smith',NULL,' Capital building',123456,123,'',1,'Nigel johnson','Manager',1982,'Brick','Yes','Noel Arnold & Associates','01-01-2008','250m2','Metal',NULL,'N',8),
	(176,'Units 1 - 3, 12 Mars Road',NULL,NULL,NULL,NULL,'',NULL,'AU019 Trans tech Business Park',0,12,NULL,3,'Shane Morris','',1989,'Concrete','','Noel Arnold & Associates','','80000m2','Concrete',NULL,'N',0),
	(177,'Building 1',NULL,NULL,NULL,NULL,'John Smith',NULL,'Haztech Site',0,123,NULL,5,'John Smith','Manager',1979,'Concrete','No previous report','Risktech','23-09-2016','2000m2','Concrete',NULL,'N',8);

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  `hide` char(11) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `usr_client`, `lastlogin`, `hide`)
VALUES
	(3,'B','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',1,'September 13, 2016, 1:55 pm','N'),
	(16,'F','testclientgeneral@swim.com.au','a01a08880fb84a11f444af8541ba41c5','testclientgeneral','testclientgeneral@swim.com.au','A',1,'September 12, 2016, 12:31 pm','N'),
	(17,'E','testclientadmin@swim.com.au','7e5043457c2f299970df08fb5432eaae','Test Client Admin','testclientadmin@swim.com.au','A',NULL,NULL,'N');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Risktech Admin','B'),
	(3,'Risktech General','C'),
	(4,'Risktech Contractor','D'),
	(5,'Client Admin','E'),
	(6,'Client General','F');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

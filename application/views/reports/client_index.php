<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
			<h1>Dashboard</h1>
		</div>
		<div class="col-sm-6">
			<h3>Sites List</h3>
			<a href="<? echo base_url(); ?>index.php/sites/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a>
			<a href="<? echo base_url(); ?>index.php/sites/index/" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> View All Site</a>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						
						<th>Site</th>
						<th>Area</th>
						<th>Company</th>
						<th>Levels</th>
						<th></th>
						<th></th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id="myTable"> 
					
					<? if(isset($_SESSION['loginuser'])) { ?>
						<?php $usertype = $_SESSION['usertype']; ?>
						<?php 
						$usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
						$usertype_admin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'); 
						?>
						<?php if ($usertype_admin) { ?>
							<?php $i = 0; ?>
							<? foreach ($sitelist as $sitesdata) { ?>
								<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
									<td><? echo $sitesdata['site']; ?></td>
									<td><? echo $sitesdata['area']; ?></td>
									<td><? echo $sitesdata['company']; ?></td>
									<td><? echo $sitesdata['levels']; ?></td>
									<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
									<td><a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $sitesdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
								</tr>
								<? if (++$i == 5) break;
							} ?>
							<? } else { ?>
								<?php $i = 0; ?>
								<? foreach ($siteclientlist as $sitesdata) { ?>
									<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
										<td><? echo $sitesdata['site']; ?></td>
										<td><? echo $sitesdata['area']; ?></td>
										<td><? echo $sitesdata['company']; ?></td>
										<td><? echo $sitesdata['levels']; ?></td>
										<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
										<td><a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $sitesdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
									</tr>
									<? if (++$i == 5) break;
								} ?>
								<? } ?>
								<? } ?>
							</tbody>
						</table>
					</div>

					<div class="col-sm-6">

						<h3>Items List</h3>
						<a href="<? echo base_url(); ?>index.php/sites/index/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Item</a>
						<a href="<? echo base_url(); ?>index.php/items/all" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> View All Material</a>
						<br/><br/>
						<table class="table table-striped table-responsive admin-table" id="User-data">
							<thead
							<tr>
								<th>Location</th>
								<th>Condition</th>
								<th>Risk Rating</th>
								<th></th>
								<th></th>
								<? if (isset($searchresults)) { echo '<th></th>'; } ?>
							</tr>
						</thead>
						<tbody id="myTable"> 
							<? if(isset($_SESSION['loginuser'])) { ?>
								<?php $usertype = $_SESSION['usertype']; ?>
								<?php 
								$usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
								$usertype_admin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'); 
								?>
								<?php if ($usertype_admin) { ?>
									<?php $i = 0; ?>
									<? foreach ($allitemlist as $itemsdata) { ?>
										<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
											<td><? echo $itemsdata['location_level']; ?></td>
											<td><? echo $itemsdata['item_condition']; ?></td>
											<td><a href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['risk_rating']; ?>/"><? echo $itemsdata['risk_rating']; ?></a></td>
											<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
											<td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
										</tr>
										<? if (++$i == 5) break; 
									}?>
										<? } else { ?>
											<?php $i = 0; ?>
											<? foreach ($allclientitemlist as $itemsdata) { ?>
												<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
													<td><? echo $itemsdata['location_level']; ?></td>
													<td><? echo $itemsdata['item_condition']; ?></td>
													<td><a href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['risk_rating']; ?>/"><? echo $itemsdata['risk_rating']; ?></a></td>
													<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</span></a></td>
													<td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
												</tr>
												<? if (++$i == 5) break; ?>
												<? } ?>
												<? } ?>
												<? } ?>

											</tbody>
										</table>
									</div>

				<!-- <div class="col-md-12 text-center">
					<ul class="pagination pagination-lg" id="myPager"></ul>
				</div> -->
			</div>
		</div>
	</div>
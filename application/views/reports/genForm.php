<div class="container">
	<a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>

	<div class="col_md-12">
		<h1>Edit Pre-filled Report Data</h1>

	<div class="row">
		<div class="col-sm-12"> 
		  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/sites/generated_report_dl/<? echo $siteID; ?>/<? echo $clientID; ?>">
			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Scope of Works:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="scope_of_works"><? echo $reportTemplate[0]['scope_of_works']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>

			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Recommendations:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="recommendations"><? echo $reportTemplate[0]['recommendations']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>

			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Methodology:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="methologies"><? echo $reportTemplate[0]['methologies']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>

			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Control Priority System:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="priority_rating_system"><? echo $reportTemplate[0]['priority_rating_system']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>

			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Statement of Limitations:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="statement_of_limitations"><? echo $reportTemplate[0]['statement_of_limitations']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>

			<div class="form-group">
				<label for="inputrecNum1" class="col-sm-3 control-label">Areas not accessed:</label>
				<div class="col-sm-12">
					<textarea class="form-control" name="areas_not_accessed"><? echo $reportTemplate[0]['areas_not_accessed']; ?></textarea>
				</div>
				<div style="clear:both"></div>
			</div>        

        	<button type="submit" name="action" value="update_prefilled_data" class="btn btn-success"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save and Generate</button>
      	  </form>
    	</div>
  	</div>

	</div>
</div>


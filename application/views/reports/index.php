<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
			<div class="col-sm-7 sites-title">
				<h1>Reports</h1>
			</div>
			<div class="col-sm-5 sites-title">
				<br />				
<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/sites/report_data_view" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Pre-filled Report Data</a><? } ?>
				<? if(isset($_SESSION['loginuser'])) { ?>
					<?php $usertype = $_SESSION['usertype']; ?>
					<?php $clientID = $_SESSION['clientID']; ?>
					<?php $userID = $_SESSION['userID']; ?>
					<?php 
					$usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
					$usertype_alladmin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'E'); 
					?>
					<? if ($usertype_alladmin) { ?>
						<a href="<? echo base_url(); ?>index.php/sites/report_data_edit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Pre-filled Report Data</a>
						<? } ?>
						<? } ?>
					</div>
				</div>
				<div class="col-sm-12">
					<h3>Site List</h3>
					
					<table class="table table-striped table-responsive admin-table" id="User-data">
						<thead>
							<tr>
								<th></th>
								<th>Site</th>
								<th>Property Number</th>
								<th>Survey Date</th>
								<th>building_name</th>
								<th>Levels</th>
								<th></th>
								<th></th>
								<th></th>
								<? if (isset($searchresults)) { echo '<th></th>'; } ?>
							</tr>
						</thead>
						<tbody id="myTable"> 
							<? foreach ($sitelist as $sitesdata) { ?>
								<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
									<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
									<td><? echo $sitesdata['site']; ?></td>
									<td><? echo $sitesdata['property_number']; ?></td>
									<td><? echo $sitesdata['survey_date']; ?></td>
									<td><? echo $sitesdata['building_name']; ?></td>
									<td><? echo $sitesdata['levels']; ?></td>
									<td>
										<!-- Download the report  -->
										<a href="<? echo base_url(); ?>index.php/sites/view_site/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View the Report</a></td>
										<td>
											<!-- Download the report  -->
											<a href="<? echo base_url(); ?>index.php/sites/download_site/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" target="_blank" class="btn btn-primary"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download the Report</a></td>							
											<td><a href="<? echo base_url(); ?>index.php/sites/report_wizard/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" target="_blank" class="btn btn-info"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Report Wizard</a></td>
										</tr>
										<? } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
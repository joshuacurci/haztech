<div class="container">
	<a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>

	<div class="col_md-12">
		<h1>Saved Reports for <? echo $sitename[0]['site']; ?></h1>

	<div class="row">
		<div class="col-sm-12"> 
		<table class="table table-striped table-responsive admin-table" id="User-data">
			<thead>
				<tr>
					<th>Date</th>
					<th>File Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="myTable"> 
			<? foreach ($savedReports as $reportData) { ?>
				<tr>
					<td><? echo date('d/m/Y h:ia', $reportData['dateGenerated']); ?></td>
					<td><? echo $reportData['reportName']; ?></td>
					<td><a href="<? echo base_url(); ?>uploads/genReport/<? echo $reportData['reportName']; ?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download/View</a></td>
				</tr>
			<? } ?>
			</tbody>

			
		  </table>
    	</div>
  	</div>

	</div>
</div>


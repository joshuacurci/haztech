<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items List</h3>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Location-Level</th>
						<th>Extent</th>
						<th>Dist. Potential</th>
						<th>Friability</th>
						<th>Sample No.</th>
						<th>Current Label</th>
						<th>Condition</th>
						<th>Control Priority</th>
						<th>Risk Rating</th>
						<th></th>
						<th></th>
						<th></th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id="myTable"> 
					<? if(isset($_SESSION['loginuser'])) { ?>
						<?php $usertype = $_SESSION['usertype']; ?>
						<?php $clientID = $_SESSION['clientID']; ?>
						<?php $userID = $_SESSION['userID']; ?>
						<?php 
						$usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
						$usertype_alladmin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'E'); 
						?>
						<?php
						if ($usertype_notadmin) { ?>
							<? foreach ($itemclientlist as $itemsdata) { ?>
							<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
								<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/" role="button">View</span></a></td>
								<td><? echo $itemsdata['location_level']; ?></td>
								<td><? echo $itemsdata['extent']; ?></td>
								<td><? echo $itemsdata['disturb_potential']; ?></td>
								<td><? echo $itemsdata['friability']; ?></td>
								<td><? echo $itemsdata['sample_no']; ?></td>
								<td><? echo $itemsdata['current_label']; ?></td>
								<td><? echo $itemsdata['item_condition']; ?></td>
								<td><? echo $itemsdata['control_priority']; ?></td>
								<td><? echo $itemsdata['risk_rating']; ?></td>
								<td></td>
								<td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
								<td><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $itemsdata['itemID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
							</tr>
							<? } ?>
							<? } else { ?>
							<? foreach ($itemlist as $itemsdata) { ?>
								<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
									<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/" role="button">View</span></a></td>
									<td><? echo $itemsdata['location_level']; ?></td>
									<td><? echo $itemsdata['extent']; ?></td>
									<td><? echo $itemsdata['disturb_potential']; ?></td>
									<td><? echo $itemsdata['friability']; ?></td>
									<td><? echo $itemsdata['sample_no']; ?></td>
									<td><? echo $itemsdata['current_label']; ?></td>
									<td><? echo $itemsdata['item_condition']; ?></td>
									<td><? echo $itemsdata['control_priority']; ?></td>
									<td><? echo $itemsdata['risk_rating']; ?></td>
									<td></td>
									<td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
									<td><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
								</tr>
								<? } ?>
								<? } ?>
								<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
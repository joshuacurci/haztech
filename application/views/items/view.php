<div class="container-fluid">
 <br/>
 <div class="col-sm-10">
   <a href="<? echo base_url(); ?>index.php/sites/view/<? echo $item[0]['siteID']; ?>/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
   <? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/items/add/<? echo $item[0]['siteID']; ?>/<? echo $item[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Hazardous Item</a><? } ?>
 </div>

 <div class="col-sm-2">
 <? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $item[0]['itemID']; ?>/<? echo $item[0]['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a><? } ?>
  <? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $item[0]['itemID']; ?>/<? echo $item[0]['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a><? } ?>
</div>
<br/>
<hr />
<div class="col_md-12">
<?php if (!empty($error)) { echo $error;  } ?>
 <h1>Hazardous Item material</h1>
 <div style="clear:both"></div>
 <div class="row">

  <div class="col-sm-4"> 
    <label for="inputrecNum1" class="col-sm-5 control-label">Location-Level:</label>
    <div class="col-sm-7">
      <? echo $item[0]['location_level']; ?>
    </div>
    <div style="clear:both"></div>

    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Room-Specific Location:</label>
    <div class="col-sm-7">
      <? echo $item[0]['room_specific']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Building:</label>
    <div class="col-sm-7">
      <? echo $item[0]['buildingName']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Material:</label>
    <div class="col-sm-7">
      <? 
      if ($item[0]['materialID'] == 0) {
        echo $item[0]['materialOther'];
      } else {
        $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_basevalues WHERE materialID = ".$item[0]['materialID']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { echo $row['material']; }
        } else { } 
      }
      ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Feature - Description:</label>
    <div class="col-sm-7">
      <? echo $item[0]['description']; ?>
    </div>
    <div style="clear:both"></div>
    
    <label for="inputrecNum1" class="col-sm-5 control-label">Sample Number:</label>
    <div class="col-sm-7">
      <? echo $item[0]['sample_no']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Sample Status:</label>
    <div class="col-sm-7">
      <? echo $item[0]['sample_status']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Photo number:</label>
    <div class="col-sm-7">
      <? echo $item[0]['photo_no']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Extent:</label>
    <div class="col-sm-7">
      <? echo $item[0]['extent']; ?> <? echo $item[0]['extent_mesurement']; ?>
    </div>
    <div style="clear:both"></div>

    <label for="inputrecNum1" class="col-sm-5 control-label">Hazardous Item Status:</label>
    <div class="col-sm-7">
      <? echo $item[0]['item_status']; ?>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="col-sm-4">

   <label for="inputrecNum1" class="col-sm-5 control-label">Condition:</label>
   <div class="col-sm-7">
    <? echo $item[0]['item_condition']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Friability:</label>
  <div class="col-sm-7">
    <? echo $item[0]['friability']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Dist. Potential:</label>
  <div class="col-sm-7">
    <? echo $item[0]['disturb_potential']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Risk Rating:</label>
  <div class="col-sm-7">
    <? echo $item[0]['risk_rating']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Current Label:</label>
  <div class="col-sm-7">
    <? echo $item[0]['current_label']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Control Priority:</label>
  <div class="col-sm-7">
    <? echo $item[0]['control_priority']; ?>
  </div>
  <div style="clear:both"></div>

  <label for="inputrecNum1" class="col-sm-5 control-label">Hazard Type:</label>
  <div class="col-sm-7">
   <? foreach ($hazardtype as $hazarddata) { ?>
     <? if($hazarddata['typeID'] == $item[0]['hazard_type']) { ?><? echo $hazarddata['type_name']; } ?>
     <? } ?>
   </div>
   <div style="clear:both"></div>

   <label for="inputrecNum1" class="col-sm-5 control-label">Cost for repair:</label>
    <div class="col-sm-7">
      <? 
      if ($finalcost == 0) {
        echo 'Please contact for a quote';
      } else {
        echo '$'.$finalcost; 
      }
      ?>
    </div>
    <div style="clear:both"></div>

 </div>
 <div class="col-sm-4">
   <label for="inputrecNum1" class="col-sm-12 control-label">Hazardous Items Documents:</label>
   <?php if (empty($itemDocumentlist)) { ?>
    No Documents
    <?php } else { ?>
     <?php foreach ($itemDocumentlist as $itemDocumentlist) { ?>
      <a href="<?php echo base_url('uploads/item_document/'.$itemDocumentlist['full_path']); ?>"><? echo $itemDocumentlist['full_path']; ?>
      </a><br/>
      <?php } } ?>
      <hr/>
      <div class="col-sm-7">
       <?php if (empty($image)) { ?>
        No Image
        <?php } else {?>
         <img class="sites_image" src="<?php echo base_url('uploads/items/'.$image[0]['full_path']); ?>" />
         <?php } ?>
       </div>
       <div style="clear:both"></div>
     </div>

     <div style="clear:both"></div>
     <div class="col-sm-10 "> 
      <label for="inputrecNum1" class="col-sm-2 control-label">Control Recommendation:</label>
      <div class="col-sm-10 site-view-desc">
        <? echo $item[0]['control_recommendation']; ?>
      </div>
      <div style="clear:both"></div>
    </div>
    <div class="col-sm-10 "> 
      <label for="inputrecNum1" class="col-sm-2 control-label">Work Action:</label>
      <div class="col-sm-10 site-view-desc">
        <? echo $item[0]['work_action']; ?>
      </div>
      <div style="clear:both"></div>
    </div>
  </div>

</div>

</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items List</h3>
<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/items/add/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Hazardous Item</a><? } ?>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Building</th>
						<th>Location-Level</th>
						<th>Room Specific</th>
						<th>Material</th>
						<th>Hazard Type</th>
						<th>Description</th>
						<th>Extent</th>
						<th>Sample No.</th>
						<th>Condition</th>
						<th>Control Priority</th>
						<th></th>
						<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><th></th>
						<th></th><? } ?>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id=""> 
					<? foreach ($itemlist as $itemsdata) { ?>
						<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
							<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" role="button">View</span></a></td>
							<td><? echo $itemsdata['buildingName']; ?></td>
							<td><? echo $itemsdata['location_level']; ?></td>
							<td><? echo $itemsdata['room_specific']; ?></td>
							<td>
							<? 
      if ($itemsdata['materialID'] == 0) {
        echo $itemsdata['materialOther'];
      } else {
        $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_basevalues WHERE materialID = ".$itemsdata['materialID']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { echo $row['material']; }
        } else { } 
      }
      ?>
	  						</td>
							  <td>
							<? 

        $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_hazard_types WHERE typeID = ".$itemsdata['hazard_type']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { echo $row['type_name']; }
        } else { } 

      ?>
	  						</td>
							<td><? echo $itemsdata['description']; ?></td>
							<td><? echo $itemsdata['extent']; ?><? echo $itemsdata['extent_mesurement']; ?></td>
							<td><? echo $itemsdata['sample_no']; ?></td>
							<td><? echo $itemsdata['item_condition']; ?></td>
							<td><? echo $itemsdata['control_priority']; ?></td>

							<td></td>
							<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
							<td><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td><? } ?>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
  <br/>
  <a href="<? echo base_url(); ?>index.php/sites/view/<? echo $site[0]['siteID']; ?>/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back to Site</a>
  <a href="<? echo base_url(); ?>index.php/items/add/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add Hazardous Item</a>
  <br/>
  <hr />
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/items/save/">
    <input type="hidden" name="siteID" value="<? echo $item[0]['siteID']; ?>" >
    <input type="hidden" name="itemID" value="<? echo $item[0]['itemID']; ?>" >
    <input type="hidden" name="photo_no" value="<? echo $item[0]['photo_no']; ?>" >
    <div class="col_md-12">
     <h1>Edit Hazardous Item</h1>

     <div class="row">
     
           <div class="col-sm-4"> 
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Location-Level:</label>
               <div class="col-sm-7">
                 <input type="text" class="form-control" placeholder="Location - Level" id="capital-text" name="location_level" value="<? echo $item[0]['location_level']; ?>"/>
               </div>
               <div style="clear:both"></div>
             </div>
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Room-Specific Location:</label>
               <div class="col-sm-7">
                 <input type="text" class="form-control" placeholder="Room-Specific Location" name="room_specific" value="<? echo $item[0]['room_specific']; ?>"/>
               </div>
               <div style="clear:both"></div>
             </div>


             <?php 
                $buildingArray = explode('///', $site[0]['building_name']);
              ?>
              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label" required>Building:</label>
                <div class="col-sm-7">
                  <select class="form-control" id="buildingName" name="buildingName">
                  <option value="">Select Building</option>
                  <? foreach ($buildingArray as $buildingName){ ?>
                    <option <? if($item[0]['buildingName'] == $buildingName){ echo "selected"; } ?> value="<? echo $buildingName ?>"><? echo $buildingName ?></option>
                    <? } ?>
                  </select>
                </div>
                <div style="clear:both"></div>
              </div>



             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label" required>Material:</label>
               <div class="col-sm-7">
                 <select class="form-control" id="material_option" class="onchange_function" name="materialID">
                  <option value="">Select Material</option>
                  <? foreach ($materials as $materialdata) { ?>
                    <option <? if($item[0]['materialID'] == $materialdata['materialID']){ echo "selected"; } ?> value="<? echo $materialdata['materialID'] ?>"><? echo $materialdata['material'] ?></option>
                    <? } ?>
                    <option <? if($item[0]['materialID'] == '0'){ echo "selected"; } ?> value="0">Other</option>
                  </select>
               </div>
               <div style="clear:both"></div>
             </div>

             <div class="form-group" <? if($item[0]['materialID'] != 0){ echo 'style="display:none;"'; } ?> id="other_material">
  <label for="inputrecNum1" class="col-sm-5 control-label">Other Material:</label>
  <div class="col-sm-7">
   <input type="text" class="form-control" placeholder="Please enter material" id="materialOther" name="materialOther" value="<? echo $item[0]['materialOther']; ?>"/>
 </div>
 <div style="clear:both"></div>
</div>



             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Feature - Description:</label>
               <div class="col-sm-7">
                 <input type="text" class="form-control" placeholder="Feature - Description" name="description" value="<? echo $item[0]['description']; ?>"/>
               </div>
               <div style="clear:both"></div>
             </div>
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Sample No.:</label>
               <div class="col-sm-7">
                 <input type="text" class="form-control" placeholder="Sample No." name="sample_no" value="<? echo $item[0]['sample_no']; ?>"/>
               </div>
               <div style="clear:both"></div>
             </div>
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Sample Status:</label>
               <div class="col-sm-7">
                 <select class="form-control" id="inputorg1" name="sample_status">
                   <option value="">Select Sample Status</option>
                   <option <? if($item[0]['sample_status'] == 'Positive'){ echo "selected"; } ?> value="Positive">Positive</option>
                   <option <? if($item[0]['sample_status'] == 'Negative'){ echo "selected"; } ?> value="Negative">Negative</option>
                   <option <? if($item[0]['sample_status'] == 'Suspected Positive'){ echo "selected"; } ?> value="Suspected Positive">Suspected Positive</option>
                   <option <? if($item[0]['sample_status'] == 'Suspected Negative'){ echo "selected"; } ?> value="Suspected Negative">Suspected Negative</option>
                   <option <? if($item[0]['sample_status'] == 'Assumed Positive'){ echo "selected"; } ?> value="Assumed Positive">Assumed Positive</option>
                   <option <? if($item[0]['sample_status'] == 'Assumed Negative'){ echo "selected"; } ?> value="Assumed Negative">Assumed Negative</option>
                 </select>
               </div>
               <div style="clear:both"></div>
             </div>
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Hazard Type:</label>
               <div class="col-sm-7">
                <select class="form-control" id="hazard_type_option" name="hazard_type" class="onchange_function">
                  <option value="0">Select Hazard Type</option>
                  <? foreach ($hazardtype as $hazarddata) { ?>
                    <option <? if($item[0]['hazard_type'] == $hazarddata['typeID']){ echo "selected"; } ?> value="<? echo $hazarddata['typeID'] ?>"><? echo $hazarddata['type_name'] ?></option>
                    <? } ?>
                  </select>
                </div>
                <div style="clear:both"></div>
              </div>
              <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Hazardous Material Status:</label>
               <div class="col-sm-7">
                 <select class="form-control" id="inputorg1" name="item_status">
                   <option value="">Select Status</option>
                   <option <? if($item[0]['item_status'] == 'Outstanding'){ echo "selected"; } ?> value="Outstanding">Outstanding</option>
                   <option <? if($item[0]['item_status'] == 'Rectified'){ echo "selected"; } ?> value="Rectified">Rectified</option>
                 </select>
               </div>
               <div style="clear:both"></div>
             </div>
     
            </div>
     
            <div class="col-sm-4">
             <div class="form-group">
               <label for="inputrecNum1" class="col-sm-5 control-label">Extent (eg units/lm/sqm/volume):</label>
               <div class="col-sm-7">
                <input type="text" class="form-control" style="width: 70%; float: left" placeholder="Extent" id="capital-text" name="extent" value="<? echo $item[0]['extent']; ?>"/>
     
                <select class="form-control col-sm-3" style="width: 28%; float: right" id="item_condition" name="extent_mesurement">
                 <option <? if($item[0]['extent_mesurement'] == 'm'){ echo "selected"; } ?> value="m">m</option>
                <option <? if($item[0]['extent_mesurement'] == 'lm'){ echo "selected"; } ?> value="lm">lm</option>
                <option <? if($item[0]['extent_mesurement'] == 'm&sup2;' || $item[0]['extent_mesurement'] == 'm²'){ echo "selected"; } ?> value="m&sup2;">m&sup2;</option>
                <option <? if($item[0]['extent_mesurement'] == 'unit(s)'){ echo "selected"; } ?> value="unit(s)">unit(s)</option>
                </select>
     
                <i style="color: grey; clear: both;">Please only include the number not the measurement</i>
              </div>
              <div style="clear:both"></div>
            </div>
            <div class="form-group">
             <label for="inputrecNum1" class="col-sm-5 control-label">Item Condition:</label>
             <div class="col-sm-7">
               <select class="form-control" id="item_condition" name="item_condition">
                 <option value="">Please Select Condition</option>
                 <option <? if($item[0]['item_condition'] == 'Good'){ echo "selected"; } ?> value="Good">Good</option>
                 <option <? if($item[0]['item_condition'] == 'Fair'){ echo "selected"; } ?> value="Fair">Fair</option>
                 <option <? if($item[0]['item_condition'] == 'Poor'){ echo "selected"; } ?> value="Poor">Poor</option>
               </select>
             </div>
             <div style="clear:both"></div>
           </div>
           <div class="form-group">
             <label for="inputrecNum1" class="col-sm-5 control-label">Friability:</label>
             <div class="col-sm-7">
              <select class="form-control" name="friability" id="friability">
               <option value="">Please Select Friability</option>
               <option <? if($item[0]['friability'] == 'Friable'){ echo "selected"; } ?> value="Friable">Friable</option>
               <option <? if($item[0]['friability'] == 'Non-Friable'){ echo "selected"; } ?> value="Non-Friable">Non-friable</option>
               <option <? if($item[0]['friability'] == 'Bonded'){ echo "selected"; } ?> value="Bonded">Bonded</option>
               <option <? if($item[0]['friability'] == 'Non-Bonded'){ echo "selected"; } ?> value="Non-Bonded">Non-Bonded</option>
             </select>
           </div>
           <div style="clear:both"></div>
         </div>
         <div class="form-group">
           <label for="inputrecNum1" class="col-sm-5 control-label">Accessibility/Dist. Potential:</label>
           <div class="col-sm-7">
            <select class="form-control" id="inputorg1" name="disturb_potential">
             <option value="">Select Disturb Potential</option>
             <option <? if($item[0]['disturb_potential'] == 'Low'){ echo "selected"; } ?> value="Low">Low</option>
             <option <? if($item[0]['disturb_potential'] == 'Medium'){ echo "selected"; } ?> value="Medium">Medium</option>
             <option <? if($item[0]['disturb_potential'] == 'High'){ echo "selected"; } ?> value="High">High</option>
           </select>
         </div>
         <div style="clear:both"></div>
       </div>
       <div class="form-group">
         <label for="inputrecNum1" class="col-sm-5 control-label">Risk Rating:</label>
         <div class="col-sm-7">
          <select class="form-control" id="inputorg1" name="risk_rating">
           <option value="">Select Risk Rating</option>
           <option <? if($item[0]['risk_rating'] == 'Low'){ echo "selected"; } ?> value="Low">Low</option>
           <option <? if($item[0]['risk_rating'] == 'Medium'){ echo "selected"; } ?> value="Medium">Medium</option>
           <option <? if($item[0]['risk_rating'] == 'High'){ echo "selected"; } ?> value="High">High</option>
         </select>
       </div>
       <div style="clear:both"></div>
     </div>
     <div class="form-group">
       <label for="inputrecNum1" class="col-sm-5 control-label">Current Label:</label>
       <div class="col-sm-7">
        <input type="text" class="form-control" placeholder="Current Label" id="current_label" name="current_label" value="<? echo $item[0]['current_label']; ?>"/>
      </div>
      <div style="clear:both"></div>
     </div>
     
     <div class="form-group">
       <label for="inputrecNum1" class="col-sm-5 control-label">Control Priority:</label>
       <div class="col-sm-7">
        <select class="form-control" id="control_priority_option" name="control_priority" class="onchange_function">
         <option value="">Select Control Priority</option>
         <option <? if($item[0]['control_priority'] == 'P1'){ echo "selected"; } ?> value="P1">P1</option>
         <option <? if($item[0]['control_priority'] == 'P2'){ echo "selected"; } ?> value="P2">P2</option>
         <option <? if($item[0]['control_priority'] == 'P3'){ echo "selected"; } ?> value="P3">P3</option>
         <option <? if($item[0]['control_priority'] == 'P4'){ echo "selected"; } ?> value="P4">P4</option>
       </select>
     </div>
     <div style="clear:both"></div>
     </div>
     
     </div>
     <div class="col-sm-4">
      <div class="form-group">
         <label for="inputrecNum1" class="col-sm-5 control-label">Upload Document:</label>
         <div class="col-sm-7">
            <?php if (empty($itemDocumentlist)) { ?>
              No Documents
            <?php } else { ?>
              <?php foreach ($itemDocumentlist as $itemDocumentlist) { ?>
                <a href="<?php echo base_url('uploads/item_document/'.$itemDocumentlist['full_path']); ?>"><? echo $itemDocumentlist['full_path']; ?></a>
                <br/>
              <?php } ?>
            <?php } ?>
         <input type="file" multiple name="item_document[]" size="20" placeholder="Upload Document" />
         </div>
         <div style="clear:both"></div>
       </div>
       <hr/>
       <div class="form-group">
         <label for="inputrecNum1" class="col-sm-5 control-label">Upload Image:</label>
         <div class="col-sm-7">
          <?php if (empty($image)) { ?>
            No Image
          <?php } else { ?>
            <img class="sites_image" src="<?php echo base_url('uploads/items/'.$image[0]['full_path']); ?>" />
          <?php } ?>
           <input type="file" name="item_image" size="20" placeholder="Upload Image" />
         </div>
         <div style="clear:both"></div>
       </div>
     </div>
     </div>
     <div style="clear:both"></div>
     
     <div class="col-sm-10 items_add_bottom"> 
      <div class="form-group">
       <label for="inputrecNum1" class="col-sm-2 control-label">Control Recommendation:</label>
       <div class="col-sm-10 site-view-desc">
        <input type="text" class="form-control" id="control_recommendationID" name="control_recommendation" placeholder="This field will be auto populated based the selected fields above" value="<? echo $item[0]['control_recommendation']; ?>"/>
      </div>
      <div style="clear:both"></div>
     </div>
     
     </div>
     <div class="col-sm-10 items_add_bottom">  
      <div class="form-group">
       <label for="inputrecNum1" class="col-sm-2 control-label">Work Action:</label>
       <div class="col-sm-10 site-view-desc">
        <textarea value="" id="capital-text2" name="work_action"><? echo $item[0]['work_action']; ?></textarea>
     
      </div>
      <div style="clear:both"></div>
     </div>
     </div>
     </div>
     <br />
     <div class="form-group">
       <div class="col-sm-9">
         <button type="submit" class="btn btn-primary" id="submititem">Submit</button>
         <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
       </div>
     </div>
     </form>
     </div>
     
     
     
     </div>

     <script>
$(document).ready(function($){
    $('#material_option').change(function(){
       var setvalue = $(this).val();
       if (setvalue == 0) {
        $('#other_material').show();
       } else {
        $('#other_material').hide();
       }
        
    });
});
</script>
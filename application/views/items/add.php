<div class="container-fluid">
  <br/>
  <a href="<? echo base_url(); ?>index.php/sites/view/<? echo $site[0]['siteID']; ?>/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/items/add/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Material</a>
  <br/>
  <hr />
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/items/newitem/">
    <div class="col_md-12">
     <h1>Add New Hazardous Item</h1>
     <input type="hidden" name="photo_no" value="<? echo $current_photo_no[0]['value']+1; ?>" >
     <input type="hidden" class="form-control" name="siteID" value="<? echo $site[0]['siteID']; ?>">
     <input type="hidden" class="form-control" name="hide" value="N">
     <input type="hidden" class="form-control" name="clientID" value="<? echo $site[0]['clientID']; ?>">
     <div class="row">

      <div class="col-sm-4"> 
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Location-Level:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" placeholder="Location - Level" id="capital-text" name="location_level"/>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Room-Specific Location:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" placeholder="Room-Specific Location" name="room_specific"/>
          </div>
          <div style="clear:both"></div>
        </div>

        <?php 
          $buildingArray = explode('///', $site[0]['building_name']);
        ?>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label" required>Building:</label>
          <div class="col-sm-7">
            <select class="form-control" id="buildingName" name="buildingName">
             <option value="">Select Building</option>
             <? foreach ($buildingArray as $buildingName){ ?>
               <option value="<? echo $buildingName ?>"><? echo $buildingName ?></option>
               <? } ?>
             </select>
          </div>
          <div style="clear:both"></div>
        </div>




        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label" required>Material:</label>
          <div class="col-sm-7">
            <select class="form-control" id="material_option" class="onchange_function" name="materialID">
             <option value="">Select Material</option>
             <? foreach ($materials as $materialdata) { ?>
               <option value="<? echo $materialdata['materialID'] ?>"><? echo $materialdata['material'] ?></option>
               <? } ?>
               <option value="0">Other</option>
             </select>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group" style="display:none;" id="other_material">
  <label for="inputrecNum1" class="col-sm-5 control-label">Other Material:</label>
  <div class="col-sm-7">
   <input type="text" class="form-control" placeholder="Please enter material" id="materialOther" name="materialOther"/>
 </div>
 <div style="clear:both"></div>
</div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Feature - Description:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" placeholder="Feature - Description" name="description"/>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Sample No.:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" placeholder="Sample No." name="sample_no"/>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Sample Status:</label>
          <div class="col-sm-7">
            <select class="form-control" id="inputorg1" name="sample_status">
              <option value="">Select Sample Status</option>
              <option value="Positive">Positive</option>
              <option value="Negative">Negative</option>
              <option value="Suspected Positive">Suspected Positive</option>
              <option value="Suspected Negative">Suspected Negative</option>
              <option value="Assumed Positive">Assumed Positive</option>
              <option value="Assumed Negative">Assumed Negative</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Hazard Type:</label>
          <div class="col-sm-7">
           <select class="form-control" id="hazard_type_option" name="hazard_type" class="onchange_function">
             <option value="0">Select Hazard Type</option>
             <? foreach ($hazardtype as $hazarddata) { ?>
               <option value="<? echo $hazarddata['typeID'] ?>"><? echo $hazarddata['type_name'] ?></option>
               <? } ?>
             </select>
           </div>
           <div style="clear:both"></div>
         </div>
         <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Hazardous Material Status:</label>
          <div class="col-sm-7">
            <select class="form-control" id="inputorg1" name="item_status">
              <option value="">Select Status</option>
              <option value="Outstanding">Outstanding</option>
              <option value="Rectified">Rectified</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

       </div>

       <div class="col-sm-4">
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Extent (eg units/lm/sqm/volume):</label>
          <div class="col-sm-7">
           <input type="text" class="form-control" style="width: 70%; float: left" placeholder="Extent" id="capital-text" name="extent"/>

           <select class="form-control col-sm-3" style="width: 28%; float: right" id="item_condition" name="extent_mesurement">
            <option value="m">m</option>
            <option value="lm">lm</option>
            <option value="m&sup2;">m&sup2;</option>
            <option value="unit(s)">unit(s)</option>
           </select>

           <i style="color: grey; clear: both;">Please only include the number not the measurement</i>
         </div>
         <div style="clear:both"></div>
       </div>
       <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Item Condition:</label>
        <div class="col-sm-7">
          <select class="form-control" id="item_condition" name="item_condition">
            <option value="">Please Select Condition</option>
            <option value="Good">Good</option>
            <option value="Fair">Fair</option>
            <option value="Poor">Poor</option>
          </select>
        </div>
        <div style="clear:both"></div>
      </div>
      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Friability:</label>
        <div class="col-sm-7">
         <select class="form-control" name="friability" id="friability">
          <option value="">Please Select Friability</option>
          <option value="Friable">Friable</option>
          <option value="Non-Friable">Non-friable</option>
          <option value="Bonded">Bonded</option>
          <option value="Non-Bonded">Non-Bonded</option>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Accessibility/Dist. Potential:</label>
      <div class="col-sm-7">
       <select class="form-control" id="inputorg1" name="disturb_potential">
        <option value="">Select Disturb Potential</option>
        <option value="Low">Low</option>
        <option value="Medium">Medium</option>
        <option value="High">High</option>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Risk Rating:</label>
    <div class="col-sm-7">
     <select class="form-control" id="inputorg1" name="risk_rating">
      <option value="">Select Risk Rating</option>
      <option value="Low">Low</option>
      <option value="Medium">Medium</option>
      <option value="Medium">High</option>
    </select>
  </div>
  <div style="clear:both"></div>
</div>
<div class="form-group">
  <label for="inputrecNum1" class="col-sm-5 control-label">Current Label:</label>
  <div class="col-sm-7">
   <input type="text" class="form-control" placeholder="Current Label" id="current_label" name="current_label"/>
 </div>
 <div style="clear:both"></div>
</div>

<div class="form-group">
  <label for="inputrecNum1" class="col-sm-5 control-label">Control Priority:</label>
  <div class="col-sm-7">
   <select class="form-control" id="control_priority_option" name="control_priority" class="onchange_function">
    <option value="">Select Control Priority</option>
    <option value="P1">P1</option>
    <option value="P2">P2</option>
    <option value="P3">P3</option>
    <option value="P4">P4</option>
  </select>
</div>
<div style="clear:both"></div>
</div>

</div>
<div class="col-sm-4">
 <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Upload Document:</label>
    <div class="col-sm-7">
    <input type="file" multiple name="item_document[]" size="20" placeholder="Upload Document" />
    </div>
    <div style="clear:both"></div>
  </div>
  <hr/>
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Upload Image:</label>
    <div class="col-sm-7">
      <input type="file" name="item_image" size="20" placeholder="Upload Image" />
    </div>
    <div style="clear:both"></div>
  </div>
</div>
</div>
<div style="clear:both"></div>

<div class="col-sm-10 items_add_bottom"> 
 <div class="form-group">
  <label for="inputrecNum1" class="col-sm-2 control-label">Control Recommendation:</label>
  <div class="col-sm-10 site-view-desc">
   <input type="text" class="form-control" id="control_recommendationID" name="control_recommendation" placeholder="This field will be auto populated based the selected fields above"/>
 </div>
 <div style="clear:both"></div>
</div>

</div>
<div class="col-sm-10 items_add_bottom">  
 <div class="form-group">
  <label for="inputrecNum1" class="col-sm-2 control-label">Work Action:</label>
  <div class="col-sm-10 site-view-desc">
   <textarea value="" id="capital-text2" name="work_action"></textarea>

 </div>
 <div style="clear:both"></div>
</div>
</div>
</div>
<br />
<div class="form-group">
  <div class="col-sm-9">
    <button type="submit" class="btn btn-primary" id="submititem">Submit</button>
    <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
  </div>
</div>
</form>
</div>



</div>

<script>
$(document).ready(function($){
    $('#material_option').change(function(){
       var setvalue = $(this).val();
       if (setvalue == 0) {
        $('#other_material').show();
       } else {
        $('#other_material').hide();
       }
        
    });
});
</script>
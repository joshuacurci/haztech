<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items List</h3>
			
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Location-Level</th>
						<th>Extent</th>
						<th>Dist. Potential</th>
						<th>Friability</th>
						<th>Sample No.</th>
						<th>Current Label</th>
						<th>Condition</th>
						<th>Control Priority</th>
						<th>Risk Rating</th>
						<th></th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id="myTable"> 
					<? foreach ($itemlist as $itemsdata) { ?>
						<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
							<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" role="button">View</span></a></td>
							<td><? echo $itemsdata['location_level']; ?></td>
							<td><? echo $itemsdata['extent']; ?></td>
							<td><? echo $itemsdata['disturb_potential']; ?></td>
							<td><? echo $itemsdata['friability']; ?></td>
							<td><? echo $itemsdata['sample_no']; ?></td>
							<td><? echo $itemsdata['current_label']; ?></td>
							<td><? echo $itemsdata['item_condition']; ?></td>
							<td><? echo $itemsdata['control_priority']; ?></td>
							<td><? echo $itemsdata['risk_rating']; ?></td>
							<td></td>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
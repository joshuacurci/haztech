<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items List</h3>
			<a href="<? echo base_url(); ?>index.php/dashboard/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Site</th>
						<th>Building</th>
						<th>Location-Level</th>
						<th>Extent</th>
						<th>Dist. Potential</th>
						<th>Friability</th>
						<th>Sample No.</th>
						<th>Current Label</th>
						<th>Condition</th>
						<th>Control Priority</th>
						<th>Risk Rating</th>
						<th></th>
						<th></th>
						<th></th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id=""> 
					<? foreach ($itemlist as $itemsdata) { ?>
						<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
							<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" role="button">View</span></a></td>
							<td>
								<?
							            $servername = $this->db->hostname;
							            $username = $this->db->username;
							            $password = $this->db->password;
							            $dbname = $this->db->database;

							            // Create connection
							            $conn = new mysqli($servername, $username, $password, $dbname);
							            // Check connection
							            if ($conn->connect_error) {
							                die("Connection failed: " . $conn->connect_error);
							            }

							            $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$itemsdata['siteID']."";
							            $result = $conn->query($sql);
							            
							            $num_rec = $result->num_rows;

							            if ($result->num_rows > 0) {
							                while($row = $result->fetch_assoc()) { ?>
							                  <? echo $row['site']; ?>
							                <? }
							            } else { ?>
							                No Assigned Site
							  <? } ?>
									
							</td>
							<td><? echo $itemsdata['buildingName']; ?></td>
							<td><? echo $itemsdata['location_level']; ?></td>
							<td><? echo $itemsdata['extent']; ?></td>
							<td><? echo $itemsdata['disturb_potential']; ?></td>
							<td><? echo $itemsdata['friability']; ?></td>
							<td><? echo $itemsdata['sample_no']; ?></td>
							<td><? echo $itemsdata['current_label']; ?></td>
							<td><? echo $itemsdata['item_condition']; ?></td>
							<td><? echo $itemsdata['control_priority']; ?></td>
							<td><? echo $itemsdata['risk_rating']; ?></td>
							<td></td>
							<?php if ($_SESSION['usertype']!='C') { ?>
							<td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
							<td><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
							<?php } ?>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
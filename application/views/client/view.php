<div class="container-fluid">
  <div class="row">
    <br/>

    <div class="col-sm-10">
    <h1> <? echo $client[0]['client_name']; ?></h1>
      <!--       <a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back to Client</a> -->
      <? if(isset($_SESSION['loginuser'])) { ?>
          <?php $usertype = $_SESSION['usertype']; ?>
          <?php $clientID = $_SESSION['clientID']; ?>
          <?php $useradmin = ($usertype=="A" || $usertype== "B" || $usertype== "C" || $usertype== "D"); ?>
          <?php
          if ($useradmin) { ?>
      <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New client</a>
      <?php } ?>
      

    </div>

    <div class="col-sm-2">
      <a href="<? echo base_url(); ?>index.php/client/edit/<? echo $client[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
      <?php if ($useradmin) { ?>
      <a href="<? echo base_url(); ?>index.php/client/delete/<? echo $client[0]['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
      <?php } ?>
    </div>
    <br />
    <hr />
    <div class="col-sm-12">
    <br />
      
      <div class="col-sm-8"> 
        <label for="inputrecNum1" class="col-sm-5 control-label">Site Address:</label>
        <div class="col-sm-7">
          <? echo $client[0]['site_address']; ?>
        </div>
        <div style="clear:both"></div>


        <label for="inputrecNum1" class="col-sm-5 control-label">Contact Person:</label>
        <div class="col-sm-7">
          <? echo $client[0]['contact_name']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Contact Number:</label>
        <div class="col-sm-7">
          <? echo $client[0]['contact_number']; ?>
        </div>
      </div>
      <div class="col-sm-4">
        <?php if (empty($image)) { ?>
          No Image
          <?php } else {?>
           <img class="sites_image" src="<?php echo base_url('uploads/client/'.$image[0]['full_path']); ?>" />
           <?php } ?>
         </div>

       </div>

     </div>
     <?php } ?>
   </div>

 </div>
 <br />
 <br />
 <div class="container-fluid">
  <div class="row">
    <div role="main" class="container-fluid main-wrapper theme-showcase">
    </div>
    <br />
    <h3>Sites List</h3>

      <a href="<? echo base_url(); ?>index.php/sites/addonclient/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a>
      <br/><br/>
      <table class="table table-striped table-responsive admin-table" id="User-data">
        <thead>
          <tr>
            <th></th>
            <th>Site</th>
            <th>Property Number</th>
            <th>Survey Date</th>
            <th>Building Name</th>
            <th>Levels</th>
            <th>Inspected By</th>
            <th>Site Contact</th>
            <th></th>
            <th></th>
            <th></th>
            <? if (isset($searchresults)) { echo '<th></th>'; } ?>
          </tr>
        </thead>
        <tbody id="myTable"> 
          <? foreach ($sitelist as $sitesdata) { ?>
                <tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
                  <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
                  <td><? echo $sitesdata['site']; ?></td>
                  <td><? echo $sitesdata['property_number']; ?></td>
                  <td><? echo $sitesdata['survey_date']; ?></td>
                  <td><? echo $sitesdata['building_name']; ?></td>
                  <td><? echo $sitesdata['levels']; ?></td>
                  <td><? echo $sitesdata['inspected_by']; ?></td>
                  <td><? echo $sitesdata['site_contact']; ?></td>
                  <td><a href="<? echo base_url(); ?>index.php/items/add/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add item</a></td>
                  <td><a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $sitesdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                  <td><a href="<? echo base_url(); ?>index.php/sites/delete_client_site/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
                </tr>
                <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
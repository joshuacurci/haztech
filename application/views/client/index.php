<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
			<h1>Client List</h1>
		</div>
		<div class="col-sm-12">
			<div class="search-box">

				<div class="client-search-box col-sm-12">
					<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
						<h3>Search By Name</h3>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" class="form-control" id="inputrecNum1" placeholder="Client Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
							</div>
							<div style="clear:both"></div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary">Search</button>
							</div>
							<div style="clear:both"></div>
						</div>
					</form>
				</div>

			</div>
			<div style="clear:both"></div>
			<? if(isset($_SESSION['loginuser'])) { ?>
				<?php $usertype = $_SESSION['usertype']; ?>
				<?php $clientID = $_SESSION['clientID']; ?>
				<?php $useradmin = ($usertype=="A" || $usertype== "B"); ?>
				
				<?php
				if ($useradmin) { ?>
					<a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Client</a>
					<?php } ?>

					<br/>
				</div>
				
				<div class="col-md-12 text-center">
    <?php 
      $letters = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
      if(!isset($filter)){$filter = "";}
    ?>
      <ul class="pagination pagination-lg">
        <? foreach ($letters as $letter){ ?>
          <li <? if ($letter == $filter){ ?> class="active" <? } ?>><a href="<? echo base_url(); ?>index.php/client/filter/<? echo $letter; ?>/"><? echo $letter; ?></a></li>
        <? } ?>
      </ul>
    </div>
    <br/>
				<table class="table table-striped table-responsive" id="client-data">
					<thead>
						<tr>
							<th></th>
							<th>Client Name</th>
							<th>Address</th>
							<th>Contact Name</th>
							<th>Phone Number</th>
							<th></th>
							<th></th>
							<? if (isset($searchresults)) { echo '<th></th>'; } ?>
						</tr>
					</thead>
					<tbody id="myTable">
						<? foreach ($client as $clientdata) { ?>
							<tr <? if (isset($searchresults) && $clientdata['hide'] == "N") { echo 'class="info"'; } ?>>
								<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/client/view/<? echo $clientdata['clientID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
								<td><? echo $clientdata['client_name']; ?></td>
								<td><? echo $clientdata['site_address']; ?></td>
								<td><? echo $clientdata['contact_name']; ?></td>
								<td><? echo $clientdata['contact_number']; ?></td>
								<td><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientdata['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
								<?php
								if ($useradmin) { ?>
									<td><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientdata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
									<?php } ?>
									<? if (isset($searchresults) && $clientdata['hide'] == "N") { echo '<td>Archived</td>'; } else {echo '<td></td>';} ?>
								</tr>
								<? } ?>

							</tbody>
						</table>
						<? } ?>

						<div class="col-md-12 text-center">
							<ul class="pagination pagination-lg" id="myPager"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
<div class="container-fluid">
  <br/>

  <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
  <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Client</a>
  <br/>
  <hr />
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/save/">
    <input type="hidden" name="clientID" value="<? echo $client[0]['clientID']; ?>" >
    <div class="col_md-12">
     <h1>Edit client</h1>

     <div class="row">

      <div class="col-sm-8"> 
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<? echo $client[0]['client_name']; ?>" name="client_name">
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Full Address:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<? echo $client[0]['site_address']; ?>" name="site_address">
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Person:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<? echo $client[0]['contact_name']; ?>" name="contact_name">
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Number:</label>
          <div class="col-sm-9">
           <input type="text" class="form-control" value="<? echo $client[0]['contact_number']; ?>" name="contact_number">
         </div>
         <div style="clear:both"></div>
       </div>

       
     </div>
     <div class="col-sm-4">
      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-12 control-label">Client Image:</label>
        <div class="col-sm-12">

          <?php if (empty($image)) { ?>
            No Image
            <?php } else {?>
             <img class="sites_image" src="<?php echo base_url('uploads/client/'.$image[0]['full_path']); ?>" />
             <?php } ?>
           </div>
           <div style="clear:both"></div>
         </div>
         <div class="form-group">
          <label for="inputrecNum1" class="col-sm-12 control-label">Upload/Replace Image:</label>
          <div class="col-sm-12">
            <input type="file" name="client_image" size="20" />
          </div>
          <div style="clear:both"></div>
        </div>
        
      </div>
      <br />
      <div class="form-group">
        <div class="col-sm-9">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
        </div>
      </div>
    </form>
  </div>



</div>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Under Construction</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css">
	
</head>
<body>


<div class="construction-wrapper" style="margin:0 auto; padding:20px; text-align:center;">
	<div class="construction-image">
		<img src="<?php echo base_url(); ?>images/logo-login.png" />
	</div>
	<div class="construction-fields" style="font-size:30px">
		SWiM Greenbook is currently undergoing scheduled maintenance<br/>
		The system should be online shortly<br/>
		Please try again later
	</div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</body>
</html>
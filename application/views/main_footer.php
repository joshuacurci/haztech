<div class="footer-wrapper">
<div class="container">
  <div class="row">
    <footer class="footer-section">
      
            </br>
            <i>&copy; <? echo date('Y'); ?> RiskTech Pty Limited ABN 88 098 796 331. All Rights Reserved</i>
          </footer>
        </div>
      </div>
      </div>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      
      <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
      <script type="text/javascript">
        jQuery(".date-picker").datepicker({
          format: "dd-mm-yyyy",
          todayBtn: "linked",
          autoclose: true,
          todayHighlight: true
        });

        $("#capital-text").keyup(function(evt){
              // to capitalize all words  
              var cp_value= ucwords($(this).val(),true) ;
              $(this).val(cp_value );
            });


          </script>
          <script src="<?php echo base_url(); ?>js/pagenation-script.js"></script>
          <script src="<?php echo base_url(); ?>js/capital.js"></script>
          <script src="<?php echo base_url(); ?>js/pw-validation.js"></script>
          <script src="<?php echo base_url(); ?>js/recommendation.js"></script>

        </body>
        </html>
<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
			<h1>Dashboard</h1>
		</div>
		<div class="col-sm-6">
			<div class="pie-chart-wrapper">
	        <div id="pie-chart"></div>
	    	<script>
	            $("#pie-chart").kendoChart({
	                title: {
	                    position: "top",
	                    text: "Risk Rating Summary"
	                },
	                legend: {
	                    visible: false
	                },
	                chartArea: {
	                    background: ""
	                },
	                seriesDefaults: {
	                    labels: {
	                        visible: true,
	                        background: "transparent",
	                        template: "#= category #: \n #= value#"
	                    }
	                },
	                series: [{
	                    type: "pie",
	                    startAngle: 0,
	                    data: [{
	                        category: "<a href='<? echo base_url(); ?>index.php/items/listrate/P1/' class='graph-links'>P1 - Extreme</a>",
	                        value: <? echo $extrame; ?>,
	                        color: "#303030"
	                    },{
	                        category: "<a href='<? echo base_url(); ?>index.php/items/listrate/P2/' class='graph-links'>P2 - High</a>",
	                        value: <? echo $high ?>,
	                        color: "#e43f3f"
	                    },{
	                        category: "<a href='<? echo base_url(); ?>index.php/items/listrate/P3/' class='graph-links'>P3 - Med</a>",
	                        value: <? echo $medium ?>,
	                        color: "#f4b441"
	                    },{
	                        category: "<a href='<? echo base_url(); ?>index.php/items/listrate/P4/' class='graph-links'>P4 - Low</a>",
	                        value: <? echo $low ?>,
	                        color: "#ebeb30"
	                    }]
	                }],
	                tooltip: {
	                    visible: true,
	                    format: "{0}"
	                }
	            });
	        
	    	</script>
            </div>

		</div>
	
		<div class="col-sm-6">
			<div class="line-chart-wrapper">
	        <div id="line-chart"></div>
	    	<script>
	            $("#line-chart").kendoChart({
                title: {
                    text: "No. Asbestos Items"
                },
                chartArea: {
				    background: "transparent"
				  },
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    type: "bar",
                    stack: true
                },
                series: [{
                    name: "Low",
                    data: [
                    <? foreach ($stacked as $low) {
                        echo $low['low'].',';
                    } ?>],
                    color: "#ebeb30"
                }, {
                    name: "Medium",
                    data: [
                    <? foreach ($stacked as $medium) {
                        echo $medium['medium'].',';
                    } ?>],
                    color: "#f4b441"
                }, {
                    name: "High",
                    data: [
                    <? foreach ($stacked as $high) {
                        echo $high['high'].',';
                    } ?>],
                    color: "#e43f3f"
                }, {
                    name: "Extreme",
                    data: [
                    <? foreach ($stacked as $topline) {
                    	echo $topline['extreme'].',';
                    } ?>],
                    color: "#303030"
                }],
                valueAxis: {
                    max: <? echo $total; ?>,
                    line: {
                        visible: false
                    },
                    minorGridLines: {
                        visible: true
                    }
                },
                categoryAxis: {
                    categories: [
                    <? foreach ($stacked as $names) {
						echo "'";
						echo '<a href="'.base_url().'sites/view/'.$names['locationID'].'/" class="graph-links">'.$names['name'].'</a>';
						echo "',";
                    } ?>
                    ],
                    majorGridLines: {
                        visible: false
                    }
				},
				chartArea: {
			        height: <? echo 20*$locations_count; ?>
			    },
                tooltip: {
                    visible: true,
                    template: "#= series.name #: #= value #"
                }
            });
	        
	    	</script>
            </div>
		</div>
		
		</div>
	</div>
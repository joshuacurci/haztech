<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items Search</h3>
			<div class="search-box item-search-box">
			<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/search/result/">

			<div class="form-group col-sm-3">
				<label for="inputrecNum1" class="search-label control-label">Sites:</label>
				<div class="">
					<select class="form-control" id="material_option" class="onchange_function" name="siteID[]" multiple>
          <option value="" selected>All</option>
					<? foreach ($sites as $siteData) { ?>
					<option value="<? echo $siteData['siteID'] ?>"><? echo $siteData['site'] ?></option>
					<? } ?>
					</select>
				</div>
			</div>
			
			<div class="form-group col-sm-3">
				<label for="inputrecNum1" class="search-label control-label">Material:</label>
				<div class="">
					<select class="form-control" id="material_option" class="onchange_function" name="materialID[]" multiple>
          <option value="" selected>All</option>
					<? foreach ($materials as $materialdata) { ?>
					<option value="<? echo $materialdata['materialID'] ?>"><? echo $materialdata['material'] ?></option>
					<? } ?>
					</select>
				</div>
			</div>

			<div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Sample Status:</label>
          <div class="">
            <select class="form-control" id="inputorg1" name="sample_status[]" multiple>
            <option value="" selected>All</option>
              <option value="Positive">Positive</option>
              <option value="Negative">Negative</option>
              <option value="Suspected Positive">Suspected Positive</option>
              <option value="Suspected Negative">Suspected Negative</option>
              <option value="Assumed Positive">Assumed Positive</option>
              <option value="Assumed Negative">Assumed Negative</option>
            </select>
          </div>
        </div>

		<div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Hazard Type:</label>
          <div class="">
           <select class="form-control" id="hazard_type_option" name="hazard_type[]" class="onchange_function" multiple>
           <option value="" selected>All</option>
             <? foreach ($hazardtype as $hazarddata) { ?>
               <option value="<? echo $hazarddata['typeID'] ?>"><? echo $hazarddata['type_name'] ?></option>
               <? } ?>
             </select>
           </div>
         </div>

         <div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Hazardous Material Status:</label>
          <div class="">
            <select class="form-control" id="inputorg1" name="item_status[]" multiple>
            <option value="" selected>All</option>
              <option value="Outstanding">Outstanding</option>
              <option value="Rectified">Rectified</option>
            </select>
          </div>
        </div>

		<div class="form-group col-sm-3">
        <label for="inputrecNum1" class="search-label control-label">Item Condition:</label>
        <div class="">
          <select class="form-control" id="item_condition" name="item_condition[]" multiple>
          <option value="" selected>All</option>
            <option value="Good">Good</option>
            <option value="Fair">Fair</option>
            <option value="Poor">Poor</option>
          </select>
        </div>
      </div>

      <div class="form-group col-sm-3">
        <label for="inputrecNum1" class="search-label control-label">Friability:</label>
        <div class="">
         <select class="form-control" name="friability[]" id="friability" multiple>
         <option value="" selected>All</option>
          <option value="Friable">Friable</option>
          <option value="Non-Friable">Non-friable</option>
        </select>
      </div>
    </div>

    <div class="form-group col-sm-3">
      <label for="inputrecNum1" class="search-label control-label">Accessibility/Dist. Potential:</label>
      <div class="">
       <select class="form-control" id="inputorg1" name="disturb_potential[]" multiple>
       <option value="" selected>All</option>
        <option value="Low Accessibility">Low Accessibility</option>
        <option value="Medium Accessibility">Medium Accessibility</option>
        <option value="High Accessibility">High Accessibility</option>
      </select>
    </div>
  </div>

  <div class="form-group col-sm-3">
    <label for="inputrecNum1" class="search-label control-label">Risk Rating:</label>
    <div class="">
     <select class="form-control" id="inputorg1" name="risk_rating[]" multiple>
     <option value="" selected>All</option>
      <option value="Low">Low</option>
      <option value="Medium">Medium</option>
      <option value="Medium">High</option>
    </select>
  </div>
</div>

<div style="clear:both;"></div>

    <div style="text-align:right;">
    <button type="submit" class="btn btn-primary" id="">Submit</button>
    <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
		</div>
      </form>
      </div>
			</div>
		</div>
	</div>
</div>
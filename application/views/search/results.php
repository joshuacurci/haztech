<div style="display:none">
<? print_r($searchedTerms); ?>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h3>Hazardous Items Search</h3>
      <div class="search-box item-search-box">
			<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/search/result/">

			<div class="form-group col-sm-3">
				<label for="inputrecNum1" class="search-label control-label">Sites:</label>
				<div class="">
					<select class="form-control" id="material_option" class="onchange_function" name="siteID[]" multiple>
          <option value="">All</option>
					<? foreach ($sites as $siteData) { ?>
					<option value="<? echo $siteData['siteID'] ?>"><? echo $siteData['site'] ?></option>
					<? } ?>
					</select>
				</div>
			</div>
			
			<div class="form-group col-sm-3">
				<label for="inputrecNum1" class="search-label control-label">Material:</label>
				<div class="">
					<select class="form-control" id="material_option" class="onchange_function" name="materialID[]" multiple>
          <option value="">All</option>
					<? foreach ($materials as $materialdata) { ?>
					<option value="<? echo $materialdata['materialID'] ?>"><? echo $materialdata['material'] ?></option>
					<? } ?>
					</select>
				</div>
			</div>

			<div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Sample Status:</label>
          <div class="">
            <select class="form-control" id="inputorg1" name="sample_status[]" multiple>
            <option value="">All</option>
              <option value="Positive">Positive</option>
              <option value="Negative">Negative</option>
              <option value="Suspected Positive">Suspected Positive</option>
              <option value="Suspected Negative">Suspected Negative</option>
              <option value="Assumed Positive">Assumed Positive</option>
              <option value="Assumed Negative">Assumed Negative</option>
            </select>
          </div>
        </div>

		<div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Hazard Type:</label>
          <div class="">
           <select class="form-control" id="hazard_type_option" name="hazard_type[]" class="onchange_function" multiple>
           <option value="">All</option>
             <? foreach ($hazardtype as $hazarddata) { ?>
               <option value="<? echo $hazarddata['typeID'] ?>"><? echo $hazarddata['type_name'] ?></option>
               <? } ?>
             </select>
           </div>
         </div>

         <div class="form-group col-sm-3">
          <label for="inputrecNum1" class="search-label control-label">Hazardous Material Status:</label>
          <div class="">
            <select class="form-control" id="inputorg1" name="item_status[]" multiple>
            <option value="">All</option>
              <option value="Outstanding">Outstanding</option>
              <option value="Rectified">Rectified</option>
            </select>
          </div>
        </div>

		<div class="form-group col-sm-3">
        <label for="inputrecNum1" class="search-label control-label">Item Condition:</label>
        <div class="">
          <select class="form-control" id="item_condition" name="item_condition[]" multiple>
          <option value="">All</option>
            <option value="Good">Good</option>
            <option value="Fair">Fair</option>
            <option value="Poor">Poor</option>
          </select>
        </div>
      </div>

      <div class="form-group col-sm-3">
        <label for="inputrecNum1" class="search-label control-label">Friability:</label>
        <div class="">
         <select class="form-control" name="friability[]" id="friability" multiple>
         <option value="">All</option>
          <option value="Friable">Friable</option>
          <option value="Non-Friable">Non-friable</option>
        </select>
      </div>
    </div>

    <div class="form-group col-sm-3">
      <label for="inputrecNum1" class="search-label control-label">Accessibility/Dist. Potential:</label>
      <div class="">
       <select class="form-control" id="inputorg1" name="disturb_potential[]" multiple>
       <option value="">All</option>
        <option value="Low Accessibility">Low Accessibility</option>
        <option value="Medium Accessibility">Medium Accessibility</option>
        <option value="High Accessibility">High Accessibility</option>
      </select>
    </div>
  </div>

  <div class="form-group col-sm-3">
    <label for="inputrecNum1" class="search-label control-label">Risk Rating:</label>
    <div class="">
     <select class="form-control" id="inputorg1" name="risk_rating[]" multiple>
     <option value="">All</option>
      <option value="Low">Low</option>
      <option value="Medium">Medium</option>
      <option value="High">High</option>
    </select>
  </div>
</div>

<div style="clear:both;"></div>

    <div style="text-align:right;">
    <button type="submit" class="btn btn-primary" id="submititem">Submit</button>
    <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
		</div>
      </form>
      </div>

      <div class="search-results">
              <h3>Search criteria selected</h3>
          <?php
            if (isset($searchedTerms['siteID'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Sites:</span> <span class='keyterms'>";
                foreach ($searchedTerms['siteID'] as $siteID) {
                  foreach ($sites as $siteData) {
                    if ($siteID == $siteData['siteID']) { echo '"'.$siteData['building_name'].'" '; }
                  }
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Sites:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['materialID'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Material:</span> <span class='keyterms'>";
                foreach ($searchedTerms['materialID'] as $materialID) {
                  foreach ($materials as $materialdata) {
                    if ($materialID == $materialdata['materialID']) { echo '"'.$materialdata['material'].'" '; }
                  }
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Material:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['sample_status'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Sample Status:</span> <span class='keyterms'>";
                foreach ($searchedTerms['sample_status'] as $sample_status) {
                  echo '"'.$sample_status.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Sample Status:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['hazard_type'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Hazard Type:</span> <span class='keyterms'>";
                foreach ($searchedTerms['hazard_type'] as $hazard_type) {
                  foreach ($hazardtype as $hazarddata) {
                    if ($hazard_type == $hazarddata['typeID']) { echo '"'.$hazarddata['type_name'].'" '; }
                  }
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Hazard Type:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['item_status'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Hazardous Material Status:</span> <span class='keyterms'>";
                foreach ($searchedTerms['item_status'] as $item_status) {
                  echo '"'.$item_status.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Hazardous Material Status:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['item_condition'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Item Condition:</span> <span class='keyterms'>";
                foreach ($searchedTerms['item_condition'] as $item_condition) {
                  echo '"'.$item_condition.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Item Condition:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['friability'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Friability:</span> <span class='keyterms'>";
                foreach ($searchedTerms['friability'] as $friability) {
                  echo '"'.$friability.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Friability:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['disturb_potential'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Accessibility/Dist. Potential:</span> <span class='keyterms'>";
                foreach ($searchedTerms['disturb_potential'] as $disturb_potential) {
                  echo '"'.$disturb_potential.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Accessibility/Dist. Potential:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

<?php
            if (isset($searchedTerms['risk_rating'])) {
              echo "<div class='searchresults-items'><span class='keytitle'>Risk Rating:</span> <span class='keyterms'>";
                foreach ($searchedTerms['risk_rating'] as $risk_rating) {
                  echo '"'.$risk_rating.'" ';
                }
              echo "</span></div>";
            } else {
              echo "<div class='searchresults-items'><span class='keytitle'>Risk Rating:</span> <span class='keyterms'>All</span></div>";
            }
          ?>

      </div>

      <div>
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/search/export_report/">
          <? if (isset($searchedTerms['siteID'])) { foreach ($searchedTerms['siteID'] as $siteID) { ?> <input type="hidden" value="<? echo $siteID; ?>" name="siteID[]"/><? } } else { ?><input type="hidden" value="all" name="siteID[]"/><? } ?>
          <? if (isset($searchedTerms['materialID'])) { foreach ($searchedTerms['materialID'] as $materialID) { ?> <input type="hidden" value="<? echo $materialID; ?>" name="materialID[]"/><? } } else { ?><input type="hidden" value="all" name="materialID[]"/><? } ?>
          <? if (isset($searchedTerms['sample_status'])) { foreach ($searchedTerms['sample_status'] as $sample_status) { ?> <input type="hidden" value="<? echo $sample_status; ?>" name="sample_status[]"/><? } } else { ?><input type="hidden" value="all" name="sample_status[]"/><? } ?>
          <? if (isset($searchedTerms['hazard_type'])) { foreach ($searchedTerms['hazard_type'] as $hazard_type) { ?> <input type="hidden" value="<? echo $hazard_type; ?>" name="hazard_type[]"/><? } } else { ?><input type="hidden" value="all" name="hazard_type[]"/><? } ?>
          <? if (isset($searchedTerms['item_status'])) { foreach ($searchedTerms['item_status'] as $item_status) { ?> <input type="hidden" value="<? echo $item_status; ?>" name="item_status[]"/><? } } else { ?><input type="hidden" value="all" name="item_status[]"/><? } ?>
          <? if (isset($searchedTerms['item_condition'])) { foreach ($searchedTerms['item_condition'] as $item_condition) { ?> <input type="hidden" value="<? echo $item_condition; ?>" name="item_condition[]"/><? } } else { ?><input type="hidden" value="all" name="item_condition[]"/><? } ?>
          <? if (isset($searchedTerms['friability'])) { foreach ($searchedTerms['friability'] as $friability) { ?> <input type="hidden" value="<? echo $friability; ?>" name="friability[]"/><? } } else { ?><input type="hidden" value="all" name="friability[]"/><? } ?>
          <? if (isset($searchedTerms['disturb_potential'])) { foreach ($searchedTerms['disturb_potential'] as $disturb_potential) { ?> <input type="hidden" value="<? echo $disturb_potential; ?>" name="disturb_potential[]"/><? } } else { ?><input type="hidden" value="all" name="disturb_potential[]"/><? } ?>
          <? if (isset($searchedTerms['risk_rating'])) { foreach ($searchedTerms['risk_rating'] as $risk_rating) { ?> <input type="hidden" value="<? echo $risk_rating; ?>" name="risk_rating[]"/><? } } else { ?><input type="hidden" value="all" name="risk_rating[]"/><? } ?>
          <?php $temp=$items; ?>
          <input type="hidden" value='<?php echo serialize((object) $temp); ?>' name="result"/>
        
          <?php if ($returnedItems !== 0) { ?>
          <button type="submit" class="btn btn-primary" name="reportexport" id="submititem"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Export report</button>
          <?php } ?>
        </form>
      </div>

      <?php 

      if ($returnedItems !== 0) { ?>     
      <div style="height:1000px;overflow:auto;">
      <table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Site</th>
            <th>Building Name</th>
            <th>Location-Level</th>
						<th>Extent</th>
						<th>Dist. Potential</th>
						<th>Friability</th>
						<th>Current Label</th>
						<th>Condition</th>
						<th>Control Priority</th>
						<th>Risk Rating</th>
						<th></th>
						<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><th></th>
						<th></th><? } ?>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody> 
					<? foreach ($items as $itemsdata) { ?>
						<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
							<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/items/view/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" role="button">View</span></a></td>
							<?
      $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$itemsdata['siteID']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { ?>
          <? 
          echo "<td>".$row['site']."</td>"; 
          ?>
          <? }
        } else { ?>
          <td></td>
          <? } ?>
              <td><? echo $itemsdata['buildingName']; ?></td>
              <td><? echo $itemsdata['location_level']; ?></td>
							<td><? echo $itemsdata['extent']; ?></td>
							<td><? echo $itemsdata['disturb_potential']; ?></td>
							<td><? echo $itemsdata['friability']; ?></td>
							<td><? echo $itemsdata['current_label']; ?></td>
							<td><? echo $itemsdata['item_condition']; ?></td>
							<td><? echo $itemsdata['control_priority']; ?></td>
							<td><? echo $itemsdata['risk_rating']; ?></td>
							<td></td>
							<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><td><a href="<? echo base_url(); ?>index.php/items/edit/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
							<td><a href="<? echo base_url(); ?>index.php/items/delete/<? echo $itemsdata['itemID']; ?>/<? echo $itemsdata['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td><? } ?>
						</tr>
						<? } ?>
					</tbody>
				</table>
				</div>
        <?php } else { ?>
          <h3 style="text-align: center;">No results found</h3>
          <?php } ?>
			</div>
		</div>
	</div>
</div>
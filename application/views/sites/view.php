<div class="container-fluid">
  <div class="row">
    <br/>

    <div class="col-sm-10">
<!--       <a href="<? echo base_url(); ?>index.php/sites/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back to Site</a>
-->
<a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/sites/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a><? } ?>
</div>

<div class="col-sm-2">
<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $site[0]['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a><? } ?>
<? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/sites/delete/<? echo $site[0]['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a><? } ?>
</div>
<br/>

<hr />
<div class="col-sm-12">

 <h1> <? echo $site[0]['site']; ?></h1>

 <div class="col-sm-8 site-view"> 

   <div class="col-sm-6"> 


    <label for="inputrecNum1" class="col-sm-5 control-label">Site:</label>
    <div class="col-sm-7">
      <? echo $site[0]['site']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Property Number:</label>
    <div class="col-sm-7">
      <? echo $site[0]['property_number']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Street Address:</label>
    <div class="col-sm-7">
      <? echo $site[0]['full_address']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">City:</label>
    <div class="col-sm-7">
      <? echo $site[0]['city']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
    <div class="col-sm-7">
      <?
      $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_au_states WHERE id = ".$site[0]['state']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { ?>
          <? echo ''; echo $row['state_name']; ?>
          <? }
        } else { ?>
          No Assigned State
          <? } ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Post Code:</label>
        <div class="col-sm-7">
          <? echo $site[0]['post_code']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Survey Date:</label>
        <div class="col-sm-7">
          <? echo $site[0]['survey_date']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Bulding Name:</label>
        <div class="col-sm-7">
          <? $buildingArray = explode('///', $site[0]['building_name']); foreach ($buildingArray as $buildingName){ echo $buildingName.'<br/>'; } ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Levels:</label>
        <div class="col-sm-7">
          <? echo $site[0]['levels']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Inspected By:</label>
        <div class="col-sm-7">
          <? echo $site[0]['inspected_by']; ?>
        </div>
      </div>

      <div class="col-sm-6">
        <label for="inputrecNum1" class="col-sm-5 control-label">Site Contact:</label>
        <div class="col-sm-7">
          <? echo $site[0]['site_contact']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Title:</label>
        <div class="col-sm-7">
          <? echo $site[0]['contact_title']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Building Age:</label>
        <div class="col-sm-7">
          <? echo $site[0]['building_age']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Site Use:</label>
        <div class="col-sm-7">
          <? echo $site[0]['site_use']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Construction Type:</label>
        <div class="col-sm-7">
          <? echo $site[0]['construction_type']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Previous Report:</label>
        <div class="col-sm-7">
          <? echo $site[0]['previous_report']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Company:</label>
        <div class="col-sm-7">
          <? echo $site[0]['company']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Area:</label>
        <div class="col-sm-7">
          <? echo $site[0]['area']; ?>
        </div>
        <div style="clear:both"></div>
        <label for="inputrecNum1" class="col-sm-5 control-label">Roof Type:</label>
        <div class="col-sm-7">
          <? echo $site[0]['roof_type']; ?>
        </div>
        <div style="clear:both"></div>

      </div>
      <div class="col-sm-12"> 
        <br/>
        <div class="col-sm-9">
          <label for="inputrecNum1" class="control-label">Site Description: </label>

          <? echo $site[0]['site_description']; ?>
        </div>
        <div style="clear:both"></div>
      </div>
      <div class="col-sm-12 site_view_buttons"> 
        <br />    
        <!-- View the report -->
        <? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/sites/view_site/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View the Report</a><? } ?>
        <!-- Download the report  -->
        <? if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?><a href="<? echo base_url(); ?>index.php/reports/generate_report/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Generate Report</a><? } ?>

        <? if ($_SESSION['usertype'] == "C" && $previousReportTest == "Y") { ?><a href="<? echo base_url(); ?>index.php/download/lastReport/<? echo $site[0]['siteID']; ?>/" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Report</a><? } ?>
        <!-- Download the report  -->
        <? if ($_SESSION['usertype'] == "C") { ?><a href="<? echo base_url(); ?>index.php/sites/generated_report_client/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download Report</a><? } ?>

				<!--<a href="<? //echo base_url(); ?>index.php/reports/viewsaved/<? //echo $site[0]['siteID']; ?>/<? //echo $site[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Previous Reports</a>-->
      </div>
      <div class="col_md-6 right">

        <br />

      </div>
    </div>
    <div class="col-sm-4">
    <label for="inputrecNum1" class="col-sm-12 control-label">Site Documents:</label>
      <?php if (empty($siteDocumentlist)) { ?>
        No Documents
        <?php } else { ?>
           <?php foreach ($siteDocumentlist as $siteDocumentlist) { ?>
          <a href="<?php echo base_url('uploads/site_document/'.$siteDocumentlist['full_path']); ?>"><? echo $siteDocumentlist['full_path']; ?>
</a><br/>
          <?php } } ?>
     <hr/>
      <label for="inputrecNum1" class="col-sm-12 control-label">Site Image/Logo:</label>
      <?php if (empty($image)) { ?>
        No Image
        <?php } else {?>
          <img class="sites_image" src="<?php echo base_url('uploads/sites/'.$image[0]['full_path']); ?>" />
          <?php } ?>
          <hr/>
          <label for="inputrecNum1" class="col-sm-12 control-label">Site Plan:</label>
          <?php if (empty($siteplan)) { ?>
            No siteplan included
            <?php } else {?>
              <img class="sites_image" src="<?php echo base_url('uploads/siteplan/'.$siteplan[0]['full_path']); ?>" />
              <?php } ?>

              <br />    <br/>
              <?php if (empty($siteplan)) {
                echo 'No site plan included';
              }  else { ?>
                <!-- View the siteplan -->
                <a href="<? echo base_url(); ?>index.php/sites/view_siteplan/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a>
                <!-- Download the siteplan  -->
                <a href="<? echo base_url(); ?>index.php/sites/download_siteplan/<? echo $site[0]['siteID']; ?>/<? echo $site[0]['clientID']; ?>/" target="_blank" class="btn btn-primary"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download</a>
                <?php }
                ?>
                <br />
              </div>


            </div>

          </div>
        </div>

      </div>
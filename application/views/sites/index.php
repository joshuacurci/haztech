<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
			<div class="col-sm-12 sites-title">
				<h1>Site List</h1>
			</div>
		</div>
		<div class="col-sm-12">
			<a href="<? echo base_url(); ?>index.php/sites/add" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Site</th>
						<th>Property Number</th>
						<th>Survey Date</th>
						<th>Building Name/s</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id=""> 
					<? foreach ($sitelist as $sitesdata) { ?>
						<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
							<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
							<td><? echo $sitesdata['site']; ?></td>
							<td><? echo $sitesdata['property_number']; ?></td>
							<td><? echo $sitesdata['survey_date']; ?></td>
							<td><? $buildingArray = explode('///', $sitesdata['building_name']); foreach ($buildingArray as $buildingName){ echo $buildingName.'<br/>'; } ?></td>
							<td><a href="<? echo base_url(); ?>index.php/reports/generate_report/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Generate Report</a></td>
							<td><a href="<? echo base_url(); ?>index.php/reports/viewsaved/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Previous Reports</a></td>
							<td><a href="<? echo base_url(); ?>index.php/items/add/<? echo $sitesdata['siteID']; ?>/<? echo $sitesdata['clientID']; ?>/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add item</a></td>
							<td><a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $sitesdata['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>							
							<td><a href="<? echo base_url(); ?>index.php/sites/delete/<? echo $sitesdata['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
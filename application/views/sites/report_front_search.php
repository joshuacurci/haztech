<style>
  .container-client-vview {
    width:200px;
  }
</style>
<div class="container-client-view" style="height: 100%;">
  <div class="row">
    <div class="col-sm-4">
      <img style="width: 100px;" class="sites_image" src="<?php echo base_url('uploads/client/risktech-logo.png'); ?>" />

       </div>
         <div class="col-sm-10">
           <h1 style="font-size: 22px;">Hazardous Materials Risk Assessment</h1>
           <h1 style="font-size: 18px;">Based on the following search results</h1>      
           <p style="font-size: 12px;"><?php
            if (isset($searchedTerms['siteID'])) {
              echo "<b>Sites:</b> ";
              // var_dump($searchedTerms['siteID']);
    
                foreach ($searchedTerms['siteID'] as $siteID) {
                  foreach ($sitelist as $siteData) {
                    if ($siteID == $siteData['siteID']) { echo '"'.$siteData['building_name'].'" '; }
                  }
                }
              echo "</p>";
            } else {
              echo "<b>Sites:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['materialID'])) {
              echo "<b>Material:</b> ";
                foreach ($searchedTerms['materialID'] as $materialID) {
                  foreach ($materials as $materialdata) {
                    if ($materialID == $materialdata['materialID']) { echo '"'.$materialdata['material'].'" '; }
                  }
                }
              echo "</p>";
            } else {
              echo "<b>Material:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['sample_status'])) {
              echo "<b>Sample Status:</b> ";
                foreach ($searchedTerms['sample_status'] as $sample_status) {
                  echo '"'.$sample_status.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Sample Status:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['hazard_type'])) {
              echo "<b>Hazard Type:</b> ";
                foreach ($searchedTerms['hazard_type'] as $hazard_type) {
                  foreach ($hazardtype as $hazarddata) {
                    if ($hazard_type == $hazarddata['typeID']) { echo '"'.$hazarddata['type_name'].'" '; }
                  }
                }
              echo "</p>";
            } else {
              echo "<b>Hazard Type:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['item_status'])) {
              echo "<b>Hazardous Material Status:</b> ";
                foreach ($searchedTerms['item_status'] as $item_status) {
                  echo '"'.$item_status.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Hazardous Material Status:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['item_condition'])) {
              echo "<b>Item Condition:</b> ";
                foreach ($searchedTerms['item_condition'] as $item_condition) {
                  echo '"'.$item_condition.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Item Condition:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['friability'])) {
              echo "<b>Friability:</b> ";
                foreach ($searchedTerms['friability'] as $friability) {
                  echo '"'.$friability.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Friability:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['disturb_potential'])) {
              echo "<b>Accessibility/Dist. Potential:</b> ";
                foreach ($searchedTerms['disturb_potential'] as $disturb_potential) {
                  echo '"'.$disturb_potential.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Accessibility/Dist. Potential:</b> All</p>";
            }
          ?>
<p style="font-size: 12px;">
<?php
            if (isset($searchedTerms['risk_rating'])) {
              echo "<b>Risk Rating:</b> ";
                foreach ($searchedTerms['risk_rating'] as $risk_rating) {
                  echo '"'.$risk_rating.'" ';
                }
              echo "</p>";
            } else {
              echo "<b>Risk Rating:</b> All</p>";
            }
          ?>


           <br />
           <br />
           <br />
           <p style="font-size: 12; font-weight: bold;">Date: <? echo date("F j, Y"); ?></p>
          </div>
        </div>

      </div>
      <br />
      <br />
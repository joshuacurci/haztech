<? if(isset($_SESSION['loginuser'])) { ?>
  <?php $usertype = $_SESSION['usertype']; ?>
  <?php 
  $usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
  $usertype_alladmin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'C' || $_SESSION['usertype'] == 'D'); 
  ?>
  <div class="container-fluid">
    <br/>

    <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
    <a href="<? echo base_url(); ?>index.php/sites/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a>
    <br/>
    <hr />
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/sites/save/">
      <input type="hidden" name="siteID" value="<? echo $site[0]['siteID']; ?>" >
      <input type="hidden" id="capital-text" class="form-control" value="<? echo $site[0]['report_name']; ?>" name="report_name" placeholder="Report Name"/>
      <select style="display: none;" class="form-control" id="inputorg1" name="client_name">
        <? foreach ($client as $clientdata) { ?>
         <option <? if($clientdata['clientID'] == $clientdata['clientID']) {echo 'selected';} ?> value="<? echo $clientdata['client_name'] ?>"><? echo $clientdata['client_name'] ?></option>
         <? } ?>
       </select>
       <div class="col_md-12">
         <h1>Edit Site</h1>

         <div class="row">

           <div class="col-sm-8 site-view"> 
            <div class="col-sm-6"> 
              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Site:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['site']; ?>" name="site">
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Property Number:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['property_number']; ?>" name="property_number">
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Address:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['full_address']; ?>" name="full_address">
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">City:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['city']; ?>" name="city" placeholder="City" />
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
                <div class="col-sm-7">
                  <select class="form-control" id="inputorg1" name="state">
                    <option value="0">State/Territory</option> 
                    <? foreach ($states as $statedata) { ?>
                     <option  <? if($site[0]['state'] == $statedata['id']) {echo 'selected';} ?> value="<? echo $statedata['id'] ?>"><? echo $statedata['state_name'] ?></option>
                     <? } ?>
                   </select>

                 </div>
                 <div style="clear:both"></div>
               </div>

               <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Post Code:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="post_code" value="<? echo $site[0]['post_code']; ?>" placeholder="Post Code" />
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Survey Date:</label>
                <div class="col-sm-7">
                  <div class="controls">
                    <div class="input-group">
                      <input id="date-picker-1" type="text" class="date-picker form-control" value="<? echo $site[0]['survey_date']; ?>" name="survey_date"/>
                      <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
                    </div>
                  </div>
                </div>
                <div style="clear:both"></div>
              </div>

              <style>
          .building-class {
            float: left;
            width: 88%;
          }

          .building-class-button {
              float: right;
          }

          .building-wrapper {
            margin-top: 5px;
          }
        </style>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Building Name:</label>
          <div class="col-sm-7" id="building_wrapper">
          <? 
           $buildingArray = explode('///', $site[0]['building_name']); 
           foreach ($buildingArray as $buildingName){ ?>
            <div class="building-wrapper"><input type="text" class="form-control building-class" name="building_name[]" placeholder="Building Name" value="<? echo $buildingName; ?>" /><a class="btn building-class-button" onclick="removebuilding(this);">X</a></div>
           <? } ?>
          
          </div>

          <div class="col-sm-7 col-sm-offset-5" style="margin-top:5px;">
            <a class="btn btn-default" onclick="addnewbuilding()">Add new building</a>
          </div>
          <div style="clear:both"></div>
        </div>

        <script>
          function addnewbuilding() {
            $('#building_wrapper').append('<div class="building-wrapper"><input type="text" class="form-control building-class" name="building_name[]" placeholder="Building Name" /><a class="btn building-class-button" onclick="removebuilding(this);">X</a></div>');
          }

          function removebuilding(btn){
            ((btn.parentNode).parentNode).removeChild(btn.parentNode);
        }
        </script>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Levels:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['levels']; ?>" name="levels"/>
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Inpected By:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['inspected_by']; ?>" name="inspected_by"/>
                </div>
                <div style="clear:both"></div>
              </div>

            </div>
            <div class="col-sm-6"> 
              <?php if ($usertype_alladmin) { ?>
                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Client:</label>
                  <div class="col-sm-7">
                    <select class="form-control" id="inputorg1" name="clientID">
                      <? foreach ($client as $clientdata) { ?>
                       <option <? if($clientdata['clientID'] == $site[0]['clientID']) {echo 'selected';} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
                       <? } ?>
                     </select>
                   </div>
                   <div style="clear:both"></div>
                 </div>
                 <?php } else { ?>
                  <input type="hidden" class="form-control" value="<? echo $site[0]['clientID']; ?>" name="clientID"/>
                  <?php } ?>

                  <div class="form-group">
                    <label for="inputrecNum1" class="col-sm-5 control-label">Site Contact:</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" value="<? echo $site[0]['site_contact']; ?>" name="site_contact"/>
                    </div>
                    <div style="clear:both"></div>
                  </div>

                  <div class="form-group">
                    <label for="inputrecNum1" class="col-sm-5 control-label">Contact Title:</label>
                    <div class="col-sm-7">
                     <input type="text" class="form-control" value="<? echo $site[0]['contact_title']; ?>" name="contact_title">
                   </div>
                   <div style="clear:both"></div>
                 </div>

                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Construction Type:</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" value="<? echo $site[0]['construction_type']; ?>" name="construction_type"/>
                  </div>
                  <div style="clear:both"></div>
                </div>

                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Previous Report:</label>
                  <div class="col-sm-7">
                    <select class="form-control" id="inputorg1" name="previous_report">
                      <option value="">Select Previous Report</option>
                      <option <?php if ($site[0]['previous_report'] == 'Yes') { echo 'selected'; }?> value="Yes">Yes</option>
                      <option <?php if ($site[0]['previous_report'] == 'No') { echo 'selected'; }?> value="No">No</option>
                    </select>
                  </div>
                  <div style="clear:both"></div>
                </div>

                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Company:</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" value="<? echo $site[0]['company']; ?>" name="company"/>
                  </div>
                  <div style="clear:both"></div>
                </div>


               <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Area:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['area']; ?>" name="area"/>
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Roof Type:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['roof_type']; ?>" name="roof_type"/>
                </div>
                <div style="clear:both"></div>
              </div>

               <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Building Age:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['building_age']; ?>" name="building_age"/>
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Site Use:</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" value="<? echo $site[0]['site_use']; ?>" name="site_use"/>
                </div>
                <div style="clear:both"></div>
              </div>

            </div>
            <div class="col-sm-12"> 
              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-12 control-label">Site Description:</label>
                <div class="col-sm-12">
                  <textarea type="text" class="form-control" name="site_description"><? echo $site[0]['site_description']; ?> </textarea>
                </div>
                <div style="clear:both"></div>
              </div>
            </div>

          </div>
          <div class="col-sm-4">
           <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Site Documents:</label>
            <div class="col-sm-7">
            <div id="removeDocumentsForm"></div>

              <?php if (empty($siteDocumentlist)) { ?>
                No Documents
                <?php } else {?>
                  <?php foreach ($siteDocumentlist as $siteDocumentlist) { ?>
                  <div style="margin-bottom:5px;" id="siteDocument-<? echo $siteDocumentlist['docID']; ?>">
                    <a href="<?php echo base_url('uploads/site_document/'.$siteDocumentlist['full_path']); ?>"><? echo $siteDocumentlist['full_path']; ?></a>
                    <button type="button" class="btn btn-default" aria-label="Remove" onclick="removefile(<? echo $siteDocumentlist['docID']; ?>);"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  </div>
                  <?php } } ?>
                </div>
                <div style="clear:both"></div>
              </div>
              <div class="form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Upload Documents:</label>
                <div class="col-sm-7">
                  <input type="file" multiple="multiple" name="site_document[]" size="20" />
                </div>
                <div style="clear:both"></div>
              </div>
              <hr/>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Site Image:</label>
                <div class="col-sm-7">

                  <?php if (empty($image)) { ?>
                    No Image
                    <?php } else {?>
                     <img class="sites_image" src="<?php echo base_url('uploads/sites/'.$image[0]['full_path']); ?>" />
                     <?php } ?>
                   </div>
                   <div style="clear:both"></div>
                 </div>
                 <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Replace Image:</label>
                  <div class="col-sm-7">
                    <input type="file" name="site_image" size="20" />
                  </div>
                  <div style="clear:both"></div>
                </div>
                <hr/>

                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-5 control-label">Site Plan:</label>
                  <div class="col-sm-7">

                    <?php if (empty($siteplan)) { ?>
                      No Siteplan Included
                      <?php } else {?>
                       <img class="sites_image" src="<?php echo base_url('uploads/siteplan/'.$siteplan[0]['full_path']); ?>" />
                       <?php } ?>
                     </div>
                     <div style="clear:both"></div>
                   </div>
                   <div class="form-group">
                    <label for="inputrecNum1" class="col-sm-5 control-label">Replace/Upload Siteplan:</label>
                    <div class="col-sm-7">
                      <input type="file" name="site_plan" size="20" />
                    </div>
                    <div style="clear:both"></div>
                  </div>

                </div>
                <br />
                <div class="form-group">
                  <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
                  </div>
                </div>
              </form>
            </div>



          </div>
          <?php } ?>
<script>

function removefile(docID){
  jQuery('#siteDocument-'+docID).hide();
  jQuery('#removeDocumentsForm').append('<input type="hidden" value="'+docID+'" name="removeDocument[]" />');
}

</script>

<div class="container">
  <div class="row">
    <br/>

    <div class="col-sm-10">
      <a href="<? echo base_url(); ?>index.php/sites/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back to Site</a>
      <a href="<? echo base_url(); ?>index.php/sites/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Site</a>
    </div>

    <div class="col-sm-2">
      <a href="<? echo base_url(); ?>index.php/sites/edit/<? echo $site[0]['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
      <a href="<? echo base_url(); ?>index.php/blogs/delete/<? echo $site[0]['siteID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
    </div>
    <br/>

    <hr />
    <div class="col-sm-12">
     <h1> <? echo $site[0]['site']; ?></h1>



     <div class="col-sm-4"> 

      <label for="inputrecNum1" class="col-sm-5 control-label">Site:</label>
      <div class="col-sm-7">
        <? echo $site[0]['site']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Property Number:</label>
      <div class="col-sm-7">
        <? echo $site[0]['property_number']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Full Address:</label>
      <div class="col-sm-7">
        <? echo $site[0]['full_address']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Survey Date:</label>
      <div class="col-sm-7">
        <? echo $site[0]['survey_date']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Bulding Name:</label>
      <div class="col-sm-7">
        <? echo $site[0]['building_name']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Levels:</label>
      <div class="col-sm-7">
        <? echo $site[0]['levels']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Inspected By:</label>
      <div class="col-sm-7">
        <? echo $site[0]['inspected_by']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Site Contact:</label>
      <div class="col-sm-7">
        <? echo $site[0]['site_contact']; ?>
      </div>
      <div style="clear:both"></div>
      <label for="inputrecNum1" class="col-sm-5 control-label">Title:</label>
      <div class="col-sm-7">
        <? echo $site[0]['contact_title']; ?>
      </div>
    </div>

    <div class="col-sm-4">

     <label for="inputrecNum1" class="col-sm-5 control-label">Building Age:</label>
     <div class="col-sm-7">
      <? echo $site[0]['building_age']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Construction Type:</label>
    <div class="col-sm-7">
      <? echo $site[0]['construction_type']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Previous Report:</label>
    <div class="col-sm-7">
      <? echo $site[0]['previous_report']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Company:</label>
    <div class="col-sm-7">
      <? echo $site[0]['company']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Date Inspected:</label>
    <div class="col-sm-7">
      <? echo $site[0]['date_inspected']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Area:</label>
    <div class="col-sm-7">
      <? echo $site[0]['area']; ?>
    </div>
    <div style="clear:both"></div>
    <label for="inputrecNum1" class="col-sm-5 control-label">Roof Type:</label>
    <div class="col-sm-7">
      <? echo $site[0]['roof_type']; ?>
    </div>
    <div style="clear:both"></div>

  </div>
  <div class="col-sm-4">
    IMAGE HERE
  </div>

</div>

</div>
</div>

</div>
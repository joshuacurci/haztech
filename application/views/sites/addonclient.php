<div class="container-fluid">
  <br/>
  <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
  <br/>
  <hr />
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/sites/newsiteclient/">
    <!-- <input type="hidden" name="siteID" > -->
    <input type="hidden" name="hide" value="N" >
    <input type="hidden" name="clientID" value="<?php echo $client[0]['clientID'] ?>" >
    <select style="display: none;" class="form-control" id="inputorg1" name="client_name">
      <? foreach ($client as $clientdata) { ?>
       <option <? if($clientdata['clientID'] == $clientdata['clientID']) {echo 'selected';} ?> value="<? echo $clientdata['client_name'] ?>"><? echo $clientdata['client_name'] ?></option>
       <? } ?>
     </select>
     <div class="col_md-12">
       <h1>Add New Site</h1>

       <div class="row">

        <div class="col-sm-4"> 
          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Site:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="site" placeholder="Site">
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Property Number:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="property_number" placeholder="Property Number">
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Street Address:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="full_address" placeholder="Street Address" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">City:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="city" placeholder="City" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg1" name="state">
                <option value="0">State/Territory</option> 
                <? foreach ($states as $statedata) { ?>
                 <option value="<? echo $statedata['id'] ?>"><? echo $statedata['state_name'] ?></option>
                 <? } ?>
               </select>
             </div>
             <div style="clear:both"></div>
           </div>

           <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Post Code:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="post_code" placeholder="Post Code" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Survey Date:</label>
            <div class="col-sm-7">
              <div class="input-group">
                <input id="date-picker-1" type="text" class="date-picker form-control" name="survey_date" />
                <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
              </div>
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Building Name:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="building_name" placeholder="Building Name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Levels:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="levels" placeholder="Levels"/>
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Inspected By:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" name="inspected_by" placeholder="Inspected By" />
            </div>
            <div style="clear:both"></div>
          </div>

        </div>

        <div class="col-sm-4">
         <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Site Contact:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" name="site_contact" placeholder="Site Contact"/>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Contact Title:</label>
          <div class="col-sm-7">
           <input type="text" class="form-control" name="contact_title" placeholder="Title" />
         </div>
         <div style="clear:both"></div>
       </div>


       <div class="form-group">
         <label for="inputrecNum1" class="col-sm-5 control-label">Building Age:</label>
         <div class="col-sm-7">
           <input type="text" class="form-control" name="building_age" placeholder="Building Age"/>
         </div>
         <div style="clear:both"></div>
       </div>

       <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Construction Type:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" name="construction_type" placeholder="Construction Type" />
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Previous Report:</label>
        <div class="col-sm-7">
          <select class="form-control" id="inputorg1" name="previous_report">
            <option value="">Select Previous Report</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Company:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" name="company" placeholder="Company"/>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Date Inspected:</label>
        <div class="col-sm-7">
         <div class="controls">
          <div class="input-group">
            <input id="date-picker-2" type="text" class="date-picker form-control" name="date_inspected" />
            <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Area:</label>
      <div class="col-sm-7">
       <input type="text" class="form-control" name="area" placeholder="Area"/>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Roof Type:</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" name="roof_type" placeholder="Roof Type" />
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Report Name:</label>
    <div class="col-sm-7">
      <input type="text" id="capital-text" class="form-control" name="report_name" placeholder="Report Name" />
    </div>
    <div style="clear:both"></div>
  </div>

</div>

<div class="col-sm-4"> 
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Upload Document:</label>
    <div class="col-sm-7">
    <input type="file" multiple name="site_document[]" size="20" placeholder="Upload Document" />
    </div>
    <div style="clear:both"></div>
  </div>
  <hr/>
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Upload Image:</label>
    <div class="col-sm-7">
      <input type="file" name="site_image" size="20" placeholder="Upload Image" />
    </div>
    <div style="clear:both"></div>
  </div>
  <hr/>
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Upload Site Plan:</label>
    <div class="col-sm-7">
      <input type="file" name="site_plan" size="20" placeholder="Upload Image" />
    </div>
    <div style="clear:both"></div>
  </div>
  <hr/>
  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-12 control-label">Site Description:</label>
    <div class="col-sm-12">
      <textarea type="file" name="site_description" size="20" placeholder="Site Description" ></textarea>
    </div>
    <div style="clear:both"></div>
  </div>
</div>
<div style="clear:both"></div>
</div>


</div>
<br />
<div class="form-group">
  <div class="col-sm-9">
    <button type="submit" class="btn btn-primary">Submit</button>
    <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
  </div>
</div>
</form>
</div>



</div>


<style>
	h3 {
		font-size: 14px;
	}
	p {
		font-size: 11px;
	}
	.site_report_header {
		width: 80%;
		text-align: center;
	}
	.site_report_image {
		width: 10%;
		text-align: right;
	}
	.intro-body, .intro-body td {
		border: 1px solid black;
		padding: 5px;
		font-size: 12px;

	}
	.intro-body .label {
		background-color: #D3D3D3;
	}
</style>
<table class="report_header">
	<tr>
		<td class="site_report_image">
			<?php if (empty($client_image)) { ?>
				<img class="sites_image" src="<?php echo base_url('uploads/client/risktech-logo.png'); ?>" />
				<?php } else {?>
					<img class="sites_image" src="<?php echo base_url('uploads/client/'.$client_image[0]['full_path']); ?>" />
					<?php } ?>
				</td>
				<td class="site_report_header">
					<h1>Findings and Recommendations</h1>
				</td>
				<td class="site_report_image">

				</td>
	</tr>
</table>
				<br />
				<br />

				<div>
				<h3>Introduction</h3>
					<p>
						This report presents the findings of an Hazardous Materials Risk Assessment based on the following search results: 
					</p>
					<table class="intro-body">
						<tr>
							<td class="label">
								Sites:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['siteID'] as $siteID) {
									foreach ($sitelist as $siteData) {
										if ($siteID == $siteData['siteID']) { echo '"'.$siteData['building_name'].'" '; }
									}
								}
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Material:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['materialID'] as $materialID) {
									foreach ($materials as $materialdata) {
									  if ($materialID == $materialdata['materialID']) { echo '"'.$materialdata['material'].'" '; }
									}
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Sample Status:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								 foreach ($searchedTerms['sample_status'] as $sample_status) {
									echo '"'.$sample_status.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr> 
						<tr>
							<td class="label">
							Hazard Type:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['hazard_type'] as $hazard_type) {
									foreach ($hazardtype as $hazarddata) {
									  if ($hazard_type == $hazarddata['typeID']) { echo '"'.$hazarddata['type_name'].'" '; }
									}
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Hazardous Material Status:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['item_status'] as $item_status) {
									echo '"'.$item_status.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Item Condition:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['item_condition'] as $item_condition) {
									echo '"'.$item_condition.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Friability:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['friability'] as $friability) {
									echo '"'.$friability.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Accessibility/Dist. Potential:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['disturb_potential'] as $disturb_potential) {
									echo '"'.$disturb_potential.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
						<tr>
							<td class="label">
							Risk Rating:
							</td>
							<td colspan="2">
							<?php if (isset($searchedTerms['siteID'])) {
								foreach ($searchedTerms['risk_rating'] as $risk_rating) {
									echo '"'.$risk_rating.'" ';
								  }
							} else {
								echo "All";
							}
							?>
							</td>
						</tr>
					</table>
							<p><? echo $report_info[0]['scope_of_works']; ?></p>


					<!-- <img class="sites_image" src="<?php echo base_url('uploads/client/'.$client_image[0]['full_path']); ?>" /> -->

				</div>
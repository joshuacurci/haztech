<style>
  .report_body, .report_body > th, .report_body > td {
    border: 0px none gray;
    padding: 1px 1px;
  }
  .report_top_section, .report_top_section > th, .report_top_section > td {
    border: 0px none gray;
    padding: 1px 1px;
  }
  .items_body, .items_body > th, .items_body > td{
    border: 0.1px solid gray;
    padding: 4px 2px;
  }
  table{
    width:100%;
  }
  .items_body > th {
    color: #fff;
  }
  .site_report_header {
    width: 70%;
    text-align: center;
  }
  .site_report_image {
    width: 15%;
    text-align: right;
  }
</style>
<!-- <?php 
// foreach ($sitelist as $sitedata): ?>    -->
<div class="container">
  <div class="row">

    <table class="report_body" style="width:100%;">
      <tr nobr="true" style="width:100%;">
        <td>
          <table class="report_top_section" style="width:100%;">
            <tr>
              <td><strong>Client</strong></td>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;
              $conn = new mysqli($servername, $username, $password, $dbname);
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_client WHERE clientID = ".$site[0]['clientID']."";
              $result = $conn->query($sql);

              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td colspan="2"><? echo $row['client_name']; ?></td> 
                <? }
              } else { ?>
                <td colspan="2">No Client</td> 
              <? } ?>              
            </tr>
            <tr>
              <td><strong>Site</strong></td>
              <td colspan="2"><? echo $site[0]['site']; ?></td>
            </tr>
            <tr>
              <td><strong>Company</strong></td>
              <td colspan="2"><? echo $site[0]['company']; ?></td>
            </tr>
          </table>
        </td>

        <td>
          <table class="report_top_section" style="width:100%;">
            <tr>
              <td><strong>Full Address</strong></td>
              <td colspan="2"><? echo $site[0]['full_address']; ?>, <br/><? echo $site[0]['city']; ?>, <br/><?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

                    // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

              $sql = "SELECT * FROM tbl_au_states WHERE id = ".$site[0]['state']."";
              $result = $conn->query($sql);

              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?><? echo $row['state_name']; ?><? }
                } else { ?>
                No Assigned State
                <? } ?>, <? echo $site[0]['post_code']; ?></td>
            </tr>
            <tr>
              <td><strong>Levels</strong></td>
              <td colspan="2"><? echo $site[0]['levels']; ?></td>
            </tr>
            <tr>
              <td></td>
              <td colspan="2"></td>
            </tr>
          </table>
        </td>

        <td>
          <table class="report_top_section" style="width:100%;">
            <tr>
              <td><strong>Area</strong></td>
              <td colspan="2"><? echo $site[0]['area']; ?></td>
            </tr>
            <tr>
              <td><strong>Constr. Type</strong></td>
              <td colspan="2"><? echo $site[0]['construction_type']; ?></td>
            </tr>
            <tr>
              <td><strong>Roof Type</strong></td>
              <td colspan="2"><? echo $site[0]['roof_type']; ?></td>
            </tr>
          </table>
        </td>

        <td>
          <table class="report_top_section" style="width:100%;">
            <tr>
              <td><strong>Inspected By</strong></td>
              <td colspan="2"><? echo $site[0]['inspected_by']; ?></td>
            </tr>
            <tr>
              <td><strong>Survey Date</strong></td>
              <td colspan="2"><? echo $site[0]['survey_date']; ?></td>
            </tr>
            <tr>
              <td><strong>Property #</strong></td>
              <td colspan="2"><? echo $site[0]['property_number']; ?></td>
            </tr>
          </table>
        </td>
      </tr>
     </table> 
     <br />  
     <table class="items_body" style="width:100%;">
      <thead style="width:100%;">
        <tr style="background-color: #003c7d;color: #fff; width:100%;">
          <th colspan="2"><strong>Location-Level</strong></th>
          <th colspan="2"><strong>Room Specific</strong></th>
          <th colspan="2"><strong>Material</strong></th>
          <th><strong>Hazard Type</strong></th>
          <th colspan="2"><strong>Description</strong></th>
          <th><strong>Sample No.</strong></th>
          <th><strong>Sample Status</strong></th>
          <th><strong>Photo No.</strong></th>
          <th><strong>Extent</strong></th>
          <th><strong>Condition</strong></th>
          <th><strong>Friability</strong></th>
          <th><strong>Dist. Potential</strong></th>
          <th><strong>Risk Rating</strong></th>
          <th><strong>Current Label</strong></th>
          <th><strong>Control Priority</strong></th>          
          <th colspan="4"><strong>Control Recommendation</strong></th>

        </tr>
      </thead>
      <tbody id="myTable"> 
      <?php 
          $buildingArray = explode('///', $site[0]['building_name']);
        ?>
        <? foreach ($buildingArray as $buildingName){ ?>
          <tr style="background-color: #d0d1fe; color: #000000;" nobr="true">
          <td colspan="23"><? echo $buildingName; ?></td>
          </tr>
        <? foreach ($itemlist as $itemsdata) { ?>
          <? if ($itemsdata['buildingName'] == $buildingName) { ?>
          <tr nobr="true">
            <td colspan="2"><? echo $itemsdata['location_level']; ?></td>
            <td colspan="2"><? $displaying = str_replace(',', '&#44;', $itemsdata['room_specific']); echo $displaying; ?></td>
            <td colspan="2"><? if($itemsdata['materialID'] == 0) { 
              echo $itemsdata['materialOther'];
            } else {
              
      $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_basevalues WHERE materialID = ".$itemsdata['materialID']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { echo $row['material']; }
        } else { } 
            }              
            ?>
            </td>
            <td><? foreach ($hazardtype as $hazarddata) { 
               if($hazarddata['typeID'] == $itemsdata['hazard_type']) { ?><? echo $hazarddata['type_name'];
             } } ?></td>
            <td colspan="2"><? echo $itemsdata['description']; ?></td>
            <td><? echo $itemsdata['sample_no']; ?></td>
            <td><? echo $itemsdata['sample_status']; ?></td>
            <? if ($itemsdata['sample_status'] == 'Negative') { ?>
              <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>          
          <th colspan="4"></th>
            <? } else { ?>
            <td><? if ($itemsdata['photo_no'] != ''){echo $photoNumberList[$itemsdata['photo_no']];} ?></td>
            <td>
            <? if($itemsdata['extent'] != '' || $itemsdata['extent'] < 0){ ?>
              <? if($itemsdata['extent'] == 1 && $itemsdata['extent_mesurement'] == 'unit(s)'){ ?> 
                1 unit
              <? } else { ?>
                <? echo $itemsdata['extent']; ?> <? echo $itemsdata['extent_mesurement']; ?>
                <? } ?>
            <? } ?>
            </td>
            <td><? echo $itemsdata['item_condition']; ?></td>
            <td><? echo $itemsdata['friability']; ?></td>
            <td><? echo $itemsdata['disturb_potential']; ?></td>
            <td><? echo $itemsdata['risk_rating']; ?></td>
            <td><? echo $itemsdata['current_label']; ?></td>
            <td><? echo $itemsdata['control_priority']; ?></td>
             <td colspan="4" 
              <? 
                if ($itemsdata['control_priority'] == 'P1'){ echo 'style="background-color:#d53939"'; }
                elseif ($itemsdata['control_priority'] == 'P2'){ echo 'style="background-color:#f6912e"'; }
                elseif ($itemsdata['control_priority'] == 'P3'){ echo 'style="background-color:#ffff00"'; }
                elseif ($itemsdata['control_priority'] == 'P4'){ echo 'style="background-color:#ffff00"'; }
                else {}

              ?>
             ><? echo $itemsdata['control_recommendation']; ?></td> 
                <? } ?>            
           </tr>
           <? } ?>
           <? } ?>
           <? } ?>
         </tbody>
       </table>
     </div>
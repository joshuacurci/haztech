<div class="container">
  <br/>

  <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
   <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/sites/update_report_data/">
  <br/>
  <hr />
  <div class="col_md-12">
   <h1>Edit Pre-filled Report Data</h1>

   <div class="row">

      <div class="col-sm-12"> 
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Scope of Works:</label>


          <div class="col-sm-12">
            <textarea class="form-control" name="scope_of_works"><? echo $report_common[0]['scope_of_works']; ?></textarea>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in Scope of Works field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_scopeofworks_data">Submit</button>
        </div>
      </form>

      <div style="clear:both"></div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Findings & Recommendations:</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="recommendations"><? echo $report_common[0]['recommendations']; ?></textarea> 
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in Recommendations field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_recommendations_data">Submit</button>
        </div>
      </form>

      <div style="clear:both"></div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Methodology:</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="methologies"><? echo $report_common[0]['methologies']; ?></textarea> 
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in Methologies field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_methologies_data">Submit</button>
        </div>
      </form>

      <div style="clear:both"></div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Priority Rating System:</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="priority_rating_system"><? echo $report_common[0]['priority_rating_system']; ?></textarea>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in Priority Rating System field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_psystem_data">Submit</button>
        </div>
      </form>

      <div style="clear:both"></div>

      <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">State of Limitations:</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="statement_of_limitations"><? echo $report_common[0]['statement_of_limitations']; ?></textarea>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in State of Limitations field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_stateoflimitations_data">Submit</button>
        </div>
      </form>

      <div style="clear:both"></div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Areas not accessed:</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="areas_not_accessed"><? echo $report_common[0]['areas_not_accessed']; ?></textarea>
          </div>
          <div style="clear:both"></div>
        </div>
        <div class="col-sm-12 reportdata-buttons">
          <button type="submit" onclick="return confirm('IMPORTANT! This will only save the changes made in Areas not accessed field. Do you want to continue?');" class="btn btn-primary" name="action" value="update_areasnotaccessed_data">Submit</button>
        </div>
        <button type="submit" onclick="return confirm('IMPORTANT! This will save the changes made in ALL of fields. Do you want to continue?');" name="action" value="update_prefilled_data" class="btn btn-danger">Submit ALL</button>
      </form>
    </div>
  </div>
  <br />
</div>



</div>


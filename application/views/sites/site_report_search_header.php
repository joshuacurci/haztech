<style>
  .report_body, .report_body > th, .report_body > td {
    border: 0.03px solid gray;
    padding: 4px 2px;
  }
  .items_body, .items_body > th, .items_body > td{
    border: 0.1px solid gray;
    padding: 4px 2px;
  }
  .items_body > th {
    color: #fff;
  }
  .control_recommendation {
    background-color: #FFFF00;
  }
  .site_report_header {
    width: 70%;
    text-align: center;
  }
  .site_report_image {
    width: 15%;
    text-align: right;
  }
</style>
<div class="container">
  <div class="row">
     <table class="items_body">
      <thead>
        <tr style="background-color: #003c7d;color: #fff;">
          <th><strong>Site</strong></th>
          <th><strong>Building Name</strong></th>
          <th><strong>Location-Level</strong></th>
          <th><strong>Room Specific</strong></th>
          <th><strong>Description</strong></th>
          <th><strong>Sample No.</strong></th>
          <th><strong>Sample Status</strong></th>
          <th><strong>Photo No.</strong></th>
          <th><strong>Extent</strong></th>
          <th><strong>Status</strong></th>
          <th><strong>Condition</strong></th>
          <th><strong>Friability</strong></th>
          <th><strong>Dist. Potential</strong></th>
          <th><strong>Risk Rating</strong></th>
          <th><strong>Current Label</strong></th>
          <th><strong>Control Priority</strong></th>
          <th><strong>Material</strong></th>
          <th><strong>Hazard Type</strong></th>
          <th><strong>Cost of Repair</strong></th>
          <th colspan="3"><strong>Control Recommendation</strong></th>
          <th colspan="2"><strong>Work Action</strong></th>

        </tr>
      </thead>
   </table>
</div>
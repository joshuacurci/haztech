<style>
	h3 {
		font-size: 14px;
	}
	p {
		font-size: 11px;
	}
	.site_report_header {
		width: 80%;
		text-align: center;
	}
	.site_report_image {
		width: 10%;
		text-align: right;
	}

	.summary-table td {
		border-left: 0px none #000;
		border-right: 0px none #000;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
	}
</style>
<table class="report_header">
	<tr>
		<td class="site_report_image">

				</td>
				<td class="site_report_header">
					<h1>Control Priority Rating System</h1>
				</td>
				<td class="site_report_image">
				<img class="sites_image" src="<?php echo base_url('images/new-doc-logo.png'); ?>" />
						</td>
					</tr>
				</table>
				<div>
				<p><? echo $report_info[0]['priority_rating_system']; ?></p>

				<table style="font-size:11px; width:100%" class="summary-table">
  <tr style="background-color:#e92626;">
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/><strong>Priority 1 (P1): Hazard with Elevated Risk Potential / Organise   Remedial Works Immediately</strong><br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
  </tr>
  <tr>
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/>Area has hazardous materials, which are either damaged or are being exposed to continual disturbance. Due to these conditions there is an   increased potential for exposure and/or transfer of the material to other   parts with continued unrestricted use of this area. It is recommended that   the area be isolated, air monitoring be conducted (if relevant) and the   hazardous material promptly removed.<br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
  </tr>
  <tr style="background-color:#ecb030;">
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/><strong>Priority 2 (P2): Hazard with Moderate Risk Potential / Organise   Remedial Works Within 3 Months</strong><br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
  </tr>
  <tr>
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/>Area has hazardous materials with a potential for disturbance   due to the following conditions:<br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Material has been disturbed or damaged and its current   condition, while not posing an immediate hazard, is unstable;<br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. The material is accessible and can, when disturbed, present a   short-term exposure risk; or<br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Demolition, refurbishment or maintenance works including new   installations or modification to air-handling systems, ceilings, lighting,   fire safety systems, or floor layouts.<br />
      Appropriate abatement measures to be taken as soon as is   practical (within 3 months). Negligible health risks if materials remain   undisturbed under the control of a management plan.<br/></td>
			<td style="width:1%; border-right: 1px solid #000;"></td>
  </tr>
  <tr style="background-color:#f2eb37;">
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/><strong>Priority 3 (P3): Hazard with Low   Risk Potential</strong><br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
	</tr>
  <tr>
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/>Area has hazardous materials where:<br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. The condition of any friable hazardous   material is stable and has a low potential for disturbance; or<br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. The hazardous material is in a non-friable condition and does   not present an exposure risk unless cut, drilled, sanded or otherwise   abraded.<br />
      Negligible health risks if the materials are left undisturbed   under the control of a hazardous material management plan. Monitor condition   during subsequent reviews. Defer abatement unless materials are to be   disturbed as a result of maintenance, refurbishment or demolition activities. <br/></td>
			<td style="width:1%; border-right: 1px solid #000;"></td>
	</tr>
  <tr style="background-color:#f2eb37;">
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/><strong>Priority 4 (P4): Hazard with Negligible Risk Potential</strong><br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
	</tr>
  <tr>
	<td style="width:1%; border-left: 1px solid #000;"></td>
    <td style="width:98%;"><br/><br/>The hazardous material is in non-friable   form and in a good condition and poses a negligible health risk. Monitor   condition during subsequent reviews. Defer abatement unless materials are to   be disturbed as a result of maintenance, refurbishment or demolition   activities.<br/></td>
		<td style="width:1%; border-right: 1px solid #000;"></td>
	</tr>
</table>
				</div>
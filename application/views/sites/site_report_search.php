<style>
  .report_body, .report_body > th, .report_body > td {
    border: 0.03px solid gray;
    padding: 0px 0px;
  }
  .items_body, .items_body > th, .items_body > td{
    border: 0.1px solid gray;
    padding: 0px 0px;
  
  }
  .items_body > th {
    color: #fff;
  }
  .control_recommendation {
    background-color: #FFFF00;
  }
  .site_report_header {
    width: 70%;
    text-align: center;
  }
  .site_report_image {
    width: 15%;
    text-align: right;
  }
</style>
<div class="container">
  <? foreach ($itemdata as $fullData) { 
    $itemlist = $fullData['itemlist'];
    }
    ?>


    
  <div class="row">
     <table class="items_body">
      <tbody id="myTable"> 

  <?    


              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
foreach ($itemlist as $item){
  
  $sql = "SELECT DISTINCT site FROM tbl_sites WHERE siteID = ".$item['siteID']."";
  $result = $conn->query($sql);

  $num_rec = $result->num_rows; 
  echo "<tr>";
        if ($result->num_rows > 0) {
          
    while($row = $result->fetch_assoc()) { ?>
      <? 
      echo "<td>".$row['site'].$counter."</td>"; 
      
 
      ?>
      <? }
    } else { ?>
      <td></td>
      <? } ?>
     
        <td><? echo $item['buildingName']; ?></td>
        <td><? echo $item['location_level']; ?></td>
        <td><? echo $item['room_specific']; ?></td>
        <td><? echo $item['description']; ?></td>
        <td><? echo $item['sample_no']; ?></td>
        <td><? echo $item['sample_status']; ?></td>
        <td><? echo $item['photo_no']; ?></td>
        <td><? echo $item['extent']; ?></td>
        <td><? echo $item['item_status']; ?></td>
        <td><? echo $item['item_condition']; ?></td>
        <td><? echo $item['friability']; ?></td>
        <td><? echo $item['disturb_potential']; ?></td>
        <td><? echo $item['risk_rating']; ?></td>
        <td><? echo $item['current_label']; ?></td>
        <td><? echo $item['control_priority']; ?></td>
      
        <td>
  
          <!-- Get the Material -->
          <? foreach ($materials as $materialdata) { ?>
           <? if($materialdata['materialID'] == $item['materialID']) { 
            echo $materialdata['material'];
          } } ?>
        </td>
        <td>
         <!-- Get the Hazard Type -->
         <? foreach ($hazardtype as $hazarddata) { ?>
           <? if($hazarddata['typeID'] == $item['hazard_type']) { ?><? echo $hazarddata['type_name'];
         } } ?></td>
         <td>
           <!-- Get the Calculate the cost based on Material costper, calloutfee and Hazardous Item extent  -->
           <? foreach ($materials as $materialdata) { 
             if($materialdata['materialID'] == $item['materialID']) { ?><? echo (($item['extent'] * $materialdata['costper']) + $materialdata['calloutfee']);
           } } ?>
         </td>
         <td colspan="3" class="control_recommendation"><? echo $item['control_recommendation']; ?></td>        
         <td colspan="2" class="control_recommendation"><? echo $item['work_action']; ?></td> 
           
       </tr>
<?}?>

      


         </tbody>
       </table>
     </div>



<style>
	h3 {
		font-size: 14px;
	}
	p {
		font-size: 11px;
	}
	.site_report_header {
		width: 80%;
		text-align: center;
	}
	.site_report_image {
		width: 10%;
		text-align: right;
	}
	.intro-body, .intro-body td {
		border: 1px solid black;
		padding: 5px;
		font-size: 12px;

	}
	.intro-body .label {
		background-color: #D3D3D3;
	}

	table.summary-table, .summary-table tr, .summary-table td {
		border:1px solid #000;
	}

	.summary-table td {
		padding: 4px 2px;
	}
</style>
<table class="report_header">
	<tr>
		<td class="site_report_image">

				</td>
				<td class="site_report_header">
					<h1>Executive Summary</h1>
				</td>
				<td class="site_report_image">
				<img class="sites_image" src="<?php echo base_url('images/new-doc-logo.png'); ?>" />
						</td>
					</tr>
				</table>
				<br />
				<br />

				<div>
					<table class="intro-body">
						<tr>
							<td class="label">
								Report for:
							</td>
							<td colspan="2">
								<?php echo $site[0]['site']; ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								Site Address:
							</td>
							<td colspan="2">
								<? echo $site[0]['full_address']; ?>, <? echo $site[0]['city']; ?>, <? if ($site[0]['state'] == 1) { echo 'NSW';} 
      elseif ($site[0]['state'] == 2) { echo 'QLD';} 
      elseif ($site[0]['state'] == 3) { echo 'SA';} 
      elseif ($site[0]['state'] == 4) { echo 'TAS';} 
      elseif ($site[0]['state'] == 5) { echo 'VIC';} 
      elseif ($site[0]['state'] == 6) { echo 'WA';} 
      elseif ($site[0]['state'] == 7) { echo 'ACT';} 
      elseif ($site[0]['state'] == 8) { echo 'NT';} 
      ?> <? echo $site[0]['post_code']; ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								Inspected By:
							</td>
							<td colspan="2">
								<?php echo $site[0]['inspected_by']; ?>
							</td>
						</tr> 
						<tr>
							<td class="label">
								Survey Date:
							</td>
							<td colspan="2">
								<?php echo $site[0]['survey_date']; ?>
							</td>
						</tr> 
						<tr>
							<td class="label">
								Building Age:
							</td>
							<td colspan="2">
								<?php echo $site[0]['building_age']; ?>
							</td>
						</tr> 
						<tr>
							<td class="label">
								Site Use:
							</td>
							<td colspan="2">
								<?php echo $site[0]['site_use']; ?>
							</td>
						</tr> 
					</table>
					
						<?php echo '<p>'.$report_info[0]['scope_of_works'].'</p>'; ?>
					</div>
				<div>
				<h3>Summary of Findings</h3>
				<table style="font-size:12px; width:100%" class="summary-table">
				<thead>
					<tr>
						<td style="width:40%; background-color: #e7e7e7; font-weight: bold;">Building/Area</td>
						<td style="width:24%; text-align:center; background-color: #e7e7e7; font-weight: bold;" colspan="2">Asbestos</td>
						<td style="width:36%; text-align:center; background-color: #e7e7e7; font-weight: bold;" colspan="3">Hazardous Materials</td>
					</tr>

					<tr>
						<td style="width:40%; background-color: #e7e7e7; font-weight: bold;"></td>
						<td style="width:12%; text-align:center; background-color: #e7e7e7; font-weight: bold;">Friable</td>
						<td style="width:12%; text-align:center; background-color: #e7e7e7; font-weight: bold;">Non-Friable</td>
						<td style="width:12%; text-align:center; background-color: #e7e7e7; font-weight: bold;">SMF</td>
						<td style="width:12%; text-align:center; background-color: #e7e7e7; font-weight: bold;">PCBs</td>
						<td style="width:12%; text-align:center; background-color: #e7e7e7; font-weight: bold;">Lead Paint</td>
					</tr>
				</thead>
				<tbody>
				<? $buildingArray = explode('///', $site[0]['building_name']); sort($buildingArray); ?>
				<? foreach ($buildingArray as $buildingName){ if($buildingName !== ''){?>
					<tr nobr="true">
						<td style="width:40%"><? echo $buildingName; ?></td>

<?php 
$value1 = '-';
$value2 = '-';
$value3 = '-';
$value4 = '-';
$value5 = '-';

	$servername = $this->db->hostname;
	$username = $this->db->username;
	$password = $this->db->password;
	$dbname = $this->db->database;

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	$sql1 = "SELECT * FROM tbl_items WHERE siteID = ".$site[0]['siteID']." AND hide = 'N' AND buildingName = '".$buildingName."' AND hazard_type = '1' AND (sample_status = 'Positive' OR sample_status = 'Suspected Positive' OR sample_status = 'Assumed Positive') AND friability = 'Friable'";
	$result1 = $conn->query($sql1);
	$value1Num = $result1->num_rows;
	if ($value1Num > 0){$value1 = 'Yes';}

	$sql2 = "SELECT * FROM tbl_items WHERE siteID = ".$site[0]['siteID']." AND hide = 'N' AND buildingName = '".$buildingName."' AND hazard_type = '1' AND (sample_status = 'Positive' OR sample_status = 'Suspected Positive' OR sample_status = 'Assumed Positive') AND friability = 'Non-Friable'";
	$result2 = $conn->query($sql2);
	$value2Num = $result2->num_rows;
	if ($value2Num > 0){$value2 = 'Yes';}

	$sql3 = "SELECT * FROM tbl_items WHERE siteID = ".$site[0]['siteID']." AND hide = 'N' AND buildingName = '".$buildingName."' AND hazard_type = '2' AND (sample_status = 'Positive' OR sample_status = 'Suspected Positive' OR sample_status = 'Assumed Positive')";
	$result3 = $conn->query($sql3);
	$value3Num = $result3->num_rows;
	if ($value3Num > 0){$value3 = 'Yes';}

	$sql4 = "SELECT * FROM tbl_items WHERE siteID = ".$site[0]['siteID']." AND hide = 'N' AND buildingName = '".$buildingName."' AND hazard_type = '4' AND (sample_status = 'Positive' OR sample_status = 'Suspected Positive' OR sample_status = 'Assumed Positive')";
	$result4 = $conn->query($sql4);
	$value4Num = $result4->num_rows;
	if ($value4Num > 0){$value4 = 'Yes';}

	$sql5 = "SELECT * FROM tbl_items WHERE siteID = ".$site[0]['siteID']." AND hide = 'N' AND buildingName = '".$buildingName."' AND hazard_type = '3' AND (sample_status = 'Positive' OR sample_status = 'Suspected Positive' OR sample_status = 'Assumed Positive')";
	$result5 = $conn->query($sql5);
	$value5Num = $result5->num_rows;
	if ($value5Num > 0){$value5 = 'Yes';}

/*foreach($findingssummary as $valuefound) {
	if ($valuefound['buildingName'] == $buildingName &&
		$valuefound['hazard_type'] == '1' &&
		($valuefound['sample_status'] == 'Positive' || $valuefound['sample_status'] == 'Assumed Positive' || $valuefound['sample_status'] == 'Suspected Positive') &&
		$valuefound['friability'] == 'Friable') 
	{
		$value1 = 'Yes';
	} elseif ($valuefound['buildingName'] == $buildingName &&
		$valuefound['hazard_type'] == '1' &&
		($valuefound['sample_status'] == 'Positive' || $valuefound['sample_status'] == 'Assumed Positive' || $valuefound['sample_status'] == 'Suspected Positive') &&
		$valuefound['friability'] == 'Non-Friable') 
	{
		$value2 = 'Yes';
	} elseif ($valuefound['buildingName'] == $buildingName &&
		$valuefound['hazard_type'] == '2' &&
		($valuefound['sample_status'] == 'Positive' || $valuefound['sample_status'] == 'Assumed Positive' || $valuefound['sample_status'] == 'Suspected Positive')) 
	{
		$value3 = 'Yes';
	} elseif ($valuefound['buildingName'] == $buildingName &&
		$valuefound['hazard_type'] == '4' &&
		($valuefound['sample_status'] == 'Positive' || $valuefound['sample_status'] == 'Assumed Positive' || $valuefound['sample_status'] == 'Suspected Positive')) 
	{
		$value4 = 'Yes';
	} elseif ($valuefound['buildingName'] == $buildingName &&
		$valuefound['hazard_type'] == '3' &&
		($valuefound['sample_status'] == 'Positive' || $valuefound['sample_status'] == 'Assumed Positive' || $valuefound['sample_status'] == 'Suspected Positive')) 
	{
		$value5 = 'Yes';
	}

}/**/?>

						<td style="width:12%; text-align:center;">
							<?php echo $value1; ?>
						</td>
						<td style="width:12%; text-align:center;">
							<?php echo $value2; ?>
						</td>
						<td style="width:12%; text-align:center;">
							<?php echo $value3; ?>
						</td>
						<td style="width:12%; text-align:center;">
							<?php echo $value4; ?>
						</td>
						<td style="width:12%; text-align:center;">
							<?php echo $value5; ?>
						</td>
					</tr>
				<? } } ?>
				</tbody>
				</table>
				</div>

				<? //echo '<pre>'; print_r($findingssummary); echo '</pre>'; exit; ?>
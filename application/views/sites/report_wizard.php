<div class="container-fluid">
  <br/>

  <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
  <br/>
  <hr />
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/sites/generate_report/">
    <input type="hidden" name="siteID" value="<? echo $site[0]['siteID']; ?>" >
    <input type="hidden" name="clientID" value="<? echo $site[0]['clientID']; ?>" >
    <div class="col_md-12">
     
     <div class="row">
<h1>Report Wizard</h1>
      <div class="col-sm-6"> 
      
        <div class="form-group">
          <h4>Introduction</h4>
          <div class="col-sm-12">
            <textarea class="form-control" value="<? echo $report[0]['introduction']; ?>" name="introduction"><? echo $report[0]['introduction']; ?></textarea>
          </div>
          <div style="clear:both"></div>
        </div>


      </div>
      <div class="col-sm-6"> 
      <h4>Please Select to include</h4>
      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label"> Scope of Works:</label>
        <div class="col-sm-7">
          <input type="radio" name="scope_of_works" value="Y"> Yes  
          <input type="radio" name="scope_of_works" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

       <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Recommendations:</label>
        <div class="col-sm-7">
          <input type="radio" name="recommendations" value="Y"> Yes  
          <input type="radio" name="recommendations" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">How to use register:</label>
        <div class="col-sm-7">
          <input type="radio" name="methologies" value="Y"> Yes  
          <input type="radio" name="methologies" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Risk Assesment Factors:</label>
        <div class="col-sm-7">
          <input type="radio" name="risk_factors" value="Y"> Yes  
          <input type="radio" name="risk_factors" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Priority Rating System:</label>
        <div class="col-sm-7">
          <input type="radio" name="priority_rating_system" value="Y"> Yes  
          <input type="radio" name="priority_rating_system" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Hazardous Material Management Requirements:</label>
        <div class="col-sm-7">
          <input type="radio" name="asbestos_mng_req" value="Y"> Yes  
          <input type="radio" name="asbestos_mng_req" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Asbestos Management Requirement:</label>
        <div class="col-sm-7">
          <input type="radio" name="haz_material_mr" value="Y"> Yes  
          <input type="radio" name="haz_material_mr" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Statement of Limitations:</label>
        <div class="col-sm-7">
          <input type="radio" name="statement_of_limitations" value="Y"> Yes  
          <input type="radio" name="statement_of_limitations" checked="checked" value="N"> No<br>
        </div>
        <div style="clear:both"></div>
      </div>
    </div>
    <br />
    <div class="form-group">
      <div class="col-sm-9">
        <button type="submit" name="action" value="reportwiz_view" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>  Generate & View Report</button>
        <button type="submit" name="action" value="reportwiz_download" class="btn btn-primary"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>  Generate & Download Report</button>
    </div>
  </form>
</div>



</div>
</div>
</div>

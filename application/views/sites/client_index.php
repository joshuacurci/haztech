<div class="container-fluid">
	<div class="row">
		<div role="main" class="container-fluid main-wrapper theme-showcase">
		</div>
		<br />
		<h1>Sites List</h1>
			<br/><br/>
			<table class="table table-striped table-responsive admin-table" id="User-data">
				<thead>
					<tr>
						<th></th>
						<th>Site</th>
						<th>Property Number</th>
						<th>Survey Date</th>
						<th>Building Names</th>
						<th>Levels</th>
						<th>Inspected By</th>
						<th>Site Contact</th>
						<? if (isset($searchresults)) { echo '<th></th>'; } ?>
					</tr>
				</thead>
				<tbody id=""> 
					<? foreach ($sitelist as $sitesdata) { ?>
								<tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
									<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/sites/view/<? echo $sitesdata['siteID']; ?>/" role="button">View</span></a></td>
									<td><? echo $sitesdata['site']; ?></td>
									<td><? echo $sitesdata['property_number']; ?></td>
									<td><? echo $sitesdata['survey_date']; ?></td>
									<td><? $buildingArray = explode('///', $sitesdata['building_name']); foreach ($buildingArray as $buildingName){ echo $buildingName.'<br/>'; } ?></td>
									<td><? echo $sitesdata['levels']; ?></td>
									<td><? echo $sitesdata['inspected_by']; ?></td>
									<td><? echo $sitesdata['site_contact']; ?></td>
								</tr>
								<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
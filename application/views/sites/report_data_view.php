<? if(isset($_SESSION['loginuser'])) { ?>
  <?php $usertype = $_SESSION['usertype']; ?>
  <?php $clientID = $_SESSION['clientID']; ?>
  <?php $userID = $_SESSION['userID']; ?>
  <?php 
  $usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
  $usertype_alladmin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'C' || $_SESSION['usertype'] == 'D'); 
  ?>
  <div class="container">
    <br/>
    <div class="col-sm-8 sites-title-left">
      <a onclick="goBack()" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a> <script> function goBack() { window.history.back(); } </script>
    </div>

    <div class="col-sm-4 sites-title-right">
      <?php
      if ($usertype_alladmin) { ?>
        <a href="<? echo base_url(); ?>index.php/sites/report_data_edit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Report Data</a>
        <?php } ?>
      </div>
      <br/>
      <hr />
      <div class="col_md-12">
       <h1>Pre-filled Report data</h1>

       <div class="row report_data">

        <div class="col-sm-12"> 
          <div class="form-group report-data-section">
            <h3 class="report_data_h3">Scope of Works:</h3>
            <div class="col-sm-12">
              <div class="report-data-view-div"><? echo $report_common[0]['scope_of_works']; ?></div>
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group report-data-section">
            <h3 class="report_data_h3">Recommendations:</h3>
            <div class="col-sm-12">
             <div class="report-data-view-div"><? echo $report_common[0]['recommendations']; ?></div>
           </div>
           <div style="clear:both"></div>
         </div>

         <div class="form-group report-data-section">
          <h3 class="report_data_h3">Methodology:</h3>
          <div class="col-sm-12">
            <div class="report-data-view-div"><? echo $report_common[0]['methologies']; ?></div>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group report-data-section">
          <h3 class="report_data_h3">Priority Rating System:</h3>
          <div class="col-sm-12">
            <div class="report-data-view-div"><? echo $report_common[0]['priority_rating_system']; ?></div>
          </div>
          <div style="clear:both"></div>
        </div>

      <div class="form-group report-data-section">
        <h3 class="report_data_h3">State of Limitations:</h3>
        <div class="col-sm-12">
          <div class="report-data-view-div"><? echo $report_common[0]['statement_of_limitations']; ?></div>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group report-data-section">
        <h3 class="report_data_h3">Areas not accessed:</h3>
        <div class="col-sm-12">
          <div class="report-data-view-div"><? echo $report_common[0]['areas_not_accessed']; ?></div>
        </div>
        <div style="clear:both"></div>
      </div>
    </div>
  </div>
  <br />
</div>



</div>
<?php } ?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
  echo '<span style="z-index: 5000; position: absolute; top: 0; right: 1.5em; padding: 0.3rem; font-size: 16px; background: green; color: #fff; border-radius: 0 0 5px 5px;">Local</span>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>404 Page Not Found</title>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css">
  <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="/css/main.css">
  <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
  <script>tinymce.init({selector:'textarea', relative_urls: false, remove_script_host: false});</script>
</head>


<body>
  <div class="header-wrapper">
	  <div class="container">
		  <div class="row">
			  <div class="col-md-4">
				  <a href="/dashboard/index">
					  <img src="/images/risktech-logo.png">
				  </a>
			  </div>
			  <div class="col-md-8 menu-button">

						  <nav class="navbar navbar-default">
							  <div class="container-fluid">
								  <!-- Brand and toggle get grouped for better mobile display -->
								  <div class="navbar-header">
									  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
										  <span class="sr-only">Toggle navigation</span>
										  <span class="icon-bar"></span>
										  <span class="icon-bar"></span>
										  <span class="icon-bar"></span>
									  </button>
								  </div>
							  </div>


							  <!-- Collect the nav links, forms, and other content for toggling -->
							  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								  <ul class="nav navbar-nav navbar-right">
									  <li><a href="/index.php/dashboard/index">Home</a></li>
								  </ul>
							  </div><!-- /.navbar-collapse -->

						  </div><!-- /.container-fluid -->
					  </nav>
				  </div>
			  </div>
		  </div>
	  </div>
	<div id="container" style="text-align:center;">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
	<div class="footer-wrapper">
<div class="container">
  <div class="row">
    <footer class="footer-section">
      
            </br>
            <i>&copy; <? echo date('Y'); ?> RiskTech Pty Limited ABN 88 098 796 331. All Rights Reserved</i>
          </footer>
        </div>
      </div>
      </div>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      
      <script src="/js/bootstrap.min.js"></script>
      

        </body>
        </html>
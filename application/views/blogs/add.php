<div class="container-fluid">
  <div class="row">
  <div class="col-sm-12">
      <h1>Add New Blog</h1>
      <a href="<? echo base_url(); ?>index.php/blogs/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
      <a href="<? echo base_url(); ?>index.php/blogs/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Blog</a>
      <br/><br/>
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/blogs/newblog/">
        <input type="hidden" name="userID" value="<? echo $userlist[0]['userID']; ?>" >
        <input type="hidden" name="date_posted" value="<? echo date('Y/m/d'); ?>" >
        <input type="hidden" name="author" value="<? echo $_SESSION['userID']; ?>" >


        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Title:</label>
            <div class="col-sm-9">
              <input class="form-control" type="text" id="" name="title" class="">
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Author:</label>
            <div class="col-sm-9">
             <? echo $_SESSION['username']; ?>
           </div>
           <div style="clear:both"></div>
         </div>

         <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Content:</label>
          <div class="col-sm-9">
            <textarea class="form-control" id="" placeholder="Blog Content" name="content"></textarea>
          </div>
          <div style="clear:both"></div>
        </div>
        
      </div>

      <div class="col-sm-4">

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Upload Image:</label>
          <div class="col-sm-7">
            <input type="file" name="blog_image" size="20" placeholder="Upload Image" />
          </div>
          <div style="clear:both"></div>
        </div>

      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <h1>Edit Blog</h1>
      <a href="<? echo base_url(); ?>index.php/blogs/view/<? echo $blogs[0]['blogID']; ?>/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
      <a href="<? echo base_url(); ?>index.php/blogs/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Blog</a>
      <br/><br/>
      
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/blogs/save/">
        <input type="hidden" name="blogID" value="<? echo $blogs[0]['blogID']; ?>" >
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Title:</label>
            <div class="col-sm-9">
              <input class="form-control" id="" type="text" value="<?php echo $blogs[0]['title'] ?>" name="title">
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Author:</label>
            <div class="col-sm-9">
              <? foreach ($userlist as $userdata){ ?>
                <? if($userdata['userID'] == $blogs[0]['author']) { ?> <input name="author" type="hidden" value="<? echo $blogs[0]['author'];  ?>"><? echo $userdata['name'];  }?>
                <? } ?>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">Content:</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="" placeholder="Blog Content" name="content"><? echo $blogs[0]['content']; ?></textarea>
              </div>
              <div style="clear:both"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Image:</label>
              <div class="col-sm-7">

                <?php if (empty($image)) { ?>
                  No Image
                  <?php } else {?>
                   <img class="sites_image" src="<?php echo base_url('uploads/blog/'.$image[0]['full_path']); ?>" />
                   <?php } ?>
                 </div>
                 <div style="clear:both"></div>
               </div>
               <div class="form-group">
                <label for="inputrecNum1" class="col-sm-5 control-label">Replace Image:</label>
                <div class="col-sm-7">
                  <input type="file" name="blog_image" size="20" />
                </div>
                <div style="clear:both"></div>
              </div>
            </div>
          </div>
          <br/>
          <div class="form-group">
            <div class="col-sm-9">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

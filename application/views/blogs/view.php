<? if(isset($_SESSION['loginuser'])) { ?>
  <?php $usertype = $_SESSION['usertype']; ?>
  <?php $clientID = $_SESSION['clientID']; ?>
  <?php $usertype_admin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B');?>
  <?php } ?>
  <div class="container-fluid">
   <div class="col_md-12">
     <h1><?php echo $blogs[0]['title'] ?></h1>     
     <a href="<? echo base_url(); ?>index.php/blogs/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
     <? if(isset($_SESSION['loginuser'])) { ?>
      <? if($usertype_admin) { ?>
       <a href="<? echo base_url(); ?>index.php/blogs/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New BLog</a>
       <?php } ?>
       <?php } ?>
       <br/><br/>
       <div class="blog-item">
        <h3 class="blogheading"></h3>
        <div class="blogcontent">
          <div class="col-md-12">
            <div class="col-md-8">          
              <span class="auth-date">Posted on <?php echo $blogs[0]['date_posted'] ?></span>
              <?php
              echo $blogs[0]['content']; 
              ?>
            </div>
            <div class="col-md-4">
              <?php if (empty($image)) { ?>
                No Image
                <?php } else {?>
                 <img class="sites_image" src="<?php echo base_url('uploads/blog/'.$image[0]['full_path']); ?>" />
                 <?php } ?>
               </div>

             </div>

             <? if(isset($_SESSION['loginuser'])) { ?>
              <? if($usertype_admin) { ?>
                <div class="blog_buttons">
                 <a href="<? echo base_url(); ?>index.php/blogs/delete/<? echo $blogs[0]['blogID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                 <a href="<? echo base_url(); ?>index.php/blogs/edit/<? echo $blogs[0]['blogID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
                 <?php } ?>
                 <?php } ?>
               </div>
             </div>
           </div>
         </div>
       </div>
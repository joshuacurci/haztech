<? if(isset($_SESSION['loginuser'])) { ?>
  <?php $usertype = $_SESSION['usertype']; ?>
  <?php $clientID = $_SESSION['clientID']; ?>
  <?php $usertype_admin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'); ?>
  <?php } ?>
  <div class="container-fluid">
   <div class="col_md-12">
     <h1>Latest News</h1>
     <? if(isset($_SESSION['loginuser'])) { ?>
     <?php if ($usertype_admin)  { ?>
       <a href="<? echo base_url(); ?>index.php/blogs/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New Blog</a>
       <?php } ?>
       <?php } ?>
       <br/><br/>
       <?php foreach ($blog_posts as $news_item): ?>
        <div class="col-md-6">
        <div class="blog-item" style="width: 100%;">
          <h3 class="blogheading"><a href="<?php echo base_url(); ?>index.php/blogs/view/<?php echo $news_item['blogID'] ?>" target="_blank"><?php echo $news_item['title'] ?></a></h3>
          <div class="blogcontent">
            <span class="auth-date">Posted on <?php echo $news_item['date_posted'] ?></span>
            <?php
            $rest = substr($news_item['content'], 0, 300); 
            echo $rest."...";
            ?>
            <div>
              <a href="<? echo base_url(); ?>index.php/blogs/view/<? echo $news_item['blogID']; ?>/" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a>
              <? if(isset($_SESSION['loginuser'])) { ?>
              <?php
              if ($usertype_admin)  { ?>
                <a href="<? echo base_url(); ?>index.php/blogs/edit/<? echo $news_item['blogID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
                <a href="<? echo base_url(); ?>index.php/blogs/delete/<? echo $news_item['blogID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                <?php } ?>
                <?php } ?>
                <br/><br/>
              </div>
            </div>
          </div>
          </div>
        <?php endforeach ?>
      </div>
      </div>
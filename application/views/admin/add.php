<div class="container">
  <div class="row">
    <div role="main" class="container-fluid main-wrapper theme-showcase">
      <div class="col-sm-8">
      <?php echo $this->session->flashdata('msg'); ?>
        <div class="row">
          <div class="col-sm-8">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-4">
            <p id="validate-status"></p>
          </div>
        </div>
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/admin/newuser/" name="add_admin">
          <input type="hidden" value="N" name="hide" />
          <input type="hidden" value="A" name="usr_status" />

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Name" value="" name="name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Role:</label>
            <div class="col-sm-9">
              <select class="form-control" id="role_type_letter" name="type_letter">
                <!-- loop through each usertype -->
                <option value="0">Select Role </option>
                <? foreach ($usertypes as $userdata){ ?>
                  <option value="<? echo $userdata['type_letter'] ?>"><? echo $userdata['type_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">User Email:</label>
              <div class="col-sm-9">
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="User Email" name="username" />
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Password:</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="password1" placeholder="Password" name="password" />                
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Confirm Password:</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="password2" placeholder="Confirm Password" name="confirm_password" onblur="validate()" />
                </div>
                <div style="clear:both"></div>
              </div>



              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
                <div class="col-sm-9">
                 <select class="form-control" id="inputorg1" name="usr_client" required="true">
                   <!-- loop for clients -->
                   <option value="0">Please Select Client</option>
                   <? foreach ($client as $clientdata) { ?>
                     <option value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
                     <? } ?>
                   </select>
                 </div>
                 <div style="clear:both"></div>
               </div>

               <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
                </div>
              </div>

            </form>


          </div>
        </div>
      </div>
    </div>
  </div>
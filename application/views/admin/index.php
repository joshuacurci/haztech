<div class="container">
  <div class="row">
    <div role="main" class="container-fluid main-wrapper theme-showcase">
      <h1>Admin</h1>
    </div>
    <a href="<? echo base_url(); ?>index.php/sites/report_data_view" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View  Pre-filled Report Data</a>
				
					<? if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B') { ?>
				<a href="<? echo base_url(); ?>index.php/sites/report_data_edit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Pre-filled Report Data</a>
				<? } ?>
    <div class="col-sm-12">
      <h3>User List</h3>
      <a href="<? echo base_url(); ?>index.php/admin/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New User</a>
      <br/><br/>
      <table class="table table-striped table-responsive admin-table" id="User-data">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>User Email</th>
            <th></th>
            <th></th>
            <? if (isset($searchresults)) { echo '<th></th>'; } ?>
          </tr>
        </thead>
        <tbody id="myTable"> 
          <? foreach ($userlist as $userdata) { ?>
            <tr <?php if (isset($searchresults)) { echo 'class="info"'; } ?>>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/admin/view/<? echo $userdata['userID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View</a></td>
              <td><a href="<? echo base_url(); ?>index.php/ad min/view/<? echo $userdata['userID']; ?>/"><? echo $userdata['name']; ?></a></td>
              <td><a href="<? echo base_url(); ?>index.php/admin/view/<? echo $userdata['userID']; ?>/"><? echo $userdata['username']; ?></a></td>
              <td><a href="<? echo base_url(); ?>index.php/admin/edit/<? echo $userdata['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/admin/delete/<? echo $userdata['userID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
            <? } ?>
          </tbody>
        </table>
      </div>


    </div>
  </div>
<div class="container">
  <div class="row">
    <div class="col-sm-8">

      <div class="row">
        <?php echo $this->session->flashdata('msg'); ?>
        <div class="col-sm-8">
          <h1>Edit User</h1>
        </div>
        <div class="col-sm-4">
          <p id="validate-status-change">
          </div>
        </div>
        <a href="<? echo base_url(); ?>index.php/admin/view/<? echo $user[0]['userID']; ?>/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
        <!-- set user variables -->
        <? if(isset($_SESSION['loginuser'])) {
          // declare usertype
          $usertype = $_SESSION['usertype']; ?>
          <?php $clientID = $_SESSION['clientID']; ?>
          <?php $usertype_admin = ($usertype=="A" || $usertype== "B");?>
          <?php
          if ($usertype_admin) { ?>
            <a href="<? echo base_url(); ?>index.php/admin/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new User</a>
            <?php } ?>
            <?php } ?>
            <br/><br/>
            <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/admin/save/">
              <input type="hidden" name="userID" value="<? echo $user[0]['userID']; ?>" >
              <input type="hidden" name="password_correct" value="<? echo $user[0]['password']; ?>" >

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" placeholder="Name" name="name" value="<? echo $user[0]['name']; ?>" >
                </div>
                <div style="clear:both"></div>
              </div>

              <?php
          if ($usertype_admin) { ?>
              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Role:</label>
                <div class="col-sm-9">
                  <select class="form-control" id="role_type_letter" name="type_letter" onchange="valueChanged()">
                    <? foreach ($usertypes as $userdata){ ?>
                      <option <? if($user[0]['type_letter'] == $userdata['type_letter']) {echo "selected=selected";} ?> value="<? echo $userdata['type_letter'] ?>"><? echo $userdata['type_name'] ?></option>
                      <? } ?>
                    </select>
                  </div>
                  <div style="clear:both"></div>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-3 control-label">User Email:</label>
                  <div class="col-sm-9">
                    <input type="email" id="exampleInputEmail1" class="form-control" id="capital-text" placeholder="User Email" name="username" value="<? echo $user[0]['username']; ?>" >
                  </div>
                  <div style="clear:both"></div>
                </div>

 

                <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">User Status:</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="usr_status">
                      <option <? if($user[0]['usr_status'] == 'A') {echo "selected=selected";} ?> value="A">Active</option>
                       <option <? if($user[0]['usr_status'] == 'I') {echo "selected=selected";} ?> value="I">Inactive</option>
                    </select>
                  </div>
                  <div style="clear:both"></div>
                </div>

                <?php
          if ($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B") { ?>

              <div class="form-group">
                <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
                <div class="col-sm-9">
                 <select class="form-control" id="inputorg1" name="usr_client" required="true">
                  <? foreach ($client as $clientdata) { ?>
                   <option <? if($clientdata['clientID'] == $user[0]['usr_client']) {echo 'selected';} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
                   <? } ?>
                 </select>
               </div>
               <div style="clear:both"></div>
             </div>

             <?php } ?>
                
                <div class="form-group">
                  <label for="inputrecNum1" class="col-sm-3 control-label">Change Password?</label>
                  <div class="col-sm-9">
                   <input class="change_pass" type="checkbox" name="change_pass" value="1" onchange="valueChanged()"/>
                 </div>
                 <div style="clear:both"></div>
               </div>

               <div class="form-group pw-change-show">
                <label for="inputrecNum1" class="col-sm-3 control-label">Current Password:</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="password1" value="<?php $user[0]['password']; ?>" placeholder="Password" name="password" />                
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group pw-change-show">
                <label for="inputrecNum1" class="col-sm-3 control-label"> New Password:</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="password_change" placeholder="Change Password (Only if changing password)" name="password_change" />                
                </div>
                <div style="clear:both"></div>
              </div>

              <div class="form-group pw-change-show">
                <label for="inputrecNum1" class="col-sm-3 control-label"> Confirm New Password:</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" id="password_change1" placeholder="Confirm Password (Only if changing password)" name="password_change1" onblur="validate_change()" />                
                </div>
                <div style="clear:both"></div>
              </div>

          

             <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
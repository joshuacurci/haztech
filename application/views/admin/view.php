 <? if(isset($_SESSION['loginuser'])) { ?>
  <?php $usertype = $_SESSION['usertype']; ?>
  <?php $clientID = $_SESSION['clientID']; ?>
  <?php $userID = $_SESSION['userID']; ?>
  <?php 
  $usertype_notadmin = ($_SESSION['usertype'] == 'E' || $_SESSION['usertype'] == 'F');
  $usertype_admin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'); 
  ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h1>User</h1>
        <? if ($usertype=="A" || $usertype== "B") { ?>
        <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
        <?php } ?>
        <? if(isset($_SESSION['loginuser'])) { ?>
          <?php $usertype = $_SESSION['usertype']; ?>
          <?php $clientID = $_SESSION['clientID']; ?>
          <?php
          if ($usertype=="A" || $usertype== "B") { ?>
            <a href="<? echo base_url(); ?>index.php/admin/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add New User</a>
            <?php } ?>
            <?php } ?>
            <br/><br/>

            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
              <div class="col-sm-9">
                <? echo $user[0]['name']; ?>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">User Email:</label>
              <div class="col-sm-9">
                <? echo $user[0]['username']; ?>
              </div>
              <div style="clear:both"></div>
            </div>



            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">Status:</label>
              <div class="col-sm-9">
               <? if($user[0]['usr_status'] == "A") {echo 'Active';} ?>
               <? if($user[0]['usr_status'] == "I") {echo 'Inactive';} ?>
             </div>
             <div style="clear:both"></div>
           </div>

           <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Role:</label>
            <div class="col-sm-9">
              <? if($user[0]['type_letter'] == "A") {echo 'Super Admin';} ?>
              <? if($user[0]['type_letter'] == "B") {echo 'Risktech Admin';} ?>
              <? if($user[0]['type_letter'] == "C") {echo 'Clients';} ?>
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Last Login:</label>
            <div class="col-sm-9">
              <? echo $user[0]['lastlogin']; ?>
            </div>
            <div style="clear:both"></div>
          </div>

          <br/>
          <br/>
          <a href="<? echo base_url(); ?>index.php/admin/edit/<? echo $user[0]['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
          <?php if ($usertype_admin) { ?>
            <a href="<? echo base_url(); ?>index.php/admin/delete/<? echo $user[0]['userID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
            <?php } ?>

          </div>
        </div>
        <?php } ?>


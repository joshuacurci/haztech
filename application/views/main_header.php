<?php
  if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
    echo '<span style="z-index: 5000; position: absolute; top: 0; right: 1.5em; padding: 0.3rem; font-size: 16px; background: green; color: #fff; border-radius: 0 0 5px 5px;">Local</span>';
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>HazTech</title>

	<?php if($menuitem=="1"){ ?>
	    <link rel="stylesheet" href="<?php echo base_url(); ?>kendo/styles/kendo.common.min.css" />
	    <link rel="stylesheet" href="<?php echo base_url(); ?>kendo/styles/kendo.default.min.css" />
	    <link rel="stylesheet" href="<?php echo base_url(); ?>kendo/styles/kendo.dataviz.min.css" />
	    <link rel="stylesheet" href="<?php echo base_url(); ?>kendo/styles/kendo.dataviz.default.min.css" /> 
    <?php } ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <?php if($menuitem=="1"){ ?><script src="<?php echo base_url(); ?>kendo/js/kendo.all.min.js"></script><?php } ?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-datepicker.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css">
	<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
	<script>tinymce.init({selector:'textarea', relative_urls: false, remove_script_host: false});</script>
</head>


<body>
	<div class="header-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<a href="<? echo base_url();?>dashboard/index">
						<img src="<? echo base_url();?>/images/risktech-logo.png">
					</a>
				</div>
				<div class="col-md-8 menu-button">
					<? if(isset($_SESSION['loginuser'])) { ?>
						<a class="btn btn-default loggout" href="<?php echo base_url(); ?>index.php/login/logout" role="button">Logout</a>
						<? } else { ?>
							<a class="btn btn-default loggout" href="<?php echo base_url(); ?>index.php/login/index" role="button">Login</a>
							<? } ?>

							<nav class="navbar navbar-default">
								<div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>
									</div>
								</div>


								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li <? if($menuitem=="1"){ echo 'class="active"';} ?> ><a href="<? echo base_url(); ?>index.php/dashboard/index">Dashboard</a></li>
										<!-- Think of sites as locations -->
										<? if(isset($_SESSION['loginuser'])) { 
											$usertype = $_SESSION['usertype']; 
											$clientID = $_SESSION['clientID']; 
											$userID = $_SESSION['userID']; 
											$usertype_notadmin = ($_SESSION['usertype'] == 'C');
											$usertype_alladmin = ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'); 

											if ($usertype_notadmin) { ?>
												<li <? if($menuitem=="5"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/sites/client_index/<? echo $clientID; ?>/">Sites</a></li>
												<li <? if($menuitem=="8"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/search/index/">Item Search</a></li>												
											<?php } else { ?>
												<li <? if($menuitem=="5"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/sites/index">Sites</a></li>
												<li <? if($menuitem=="6"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/client/index">Client</a></li>
												<li <? if($menuitem=="8"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/search/index">Item Search</a></li>											
											<?php } ?>
													<!-- Visible only to admin users including client -->
											<?php if ($usertype_alladmin) { ?>
												<li <? if($menuitem=="2"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/admin/index">Admin</a></li>
											<?php } else { ?>
												<li <? if($menuitem=="2"){ echo 'class="active"'; } ?> ><a href="<? echo base_url(); ?>index.php/admin/view/<? echo $userID; ?>/">Profile</a></li>
											<?php }	?>
												
												<li><a class="nolink">Welcome <?php echo $_SESSION['username']; ?></a></li>
										<?php } ?>
									</ul>
								</div><!-- /.navbar-collapse -->

							</div><!-- /.container-fluid -->
						</nav>
					</div>
				</div>
			</div>
		</div>
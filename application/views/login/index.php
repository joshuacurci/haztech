<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1>Login</h1>
			<p>Please fill in your login details below. If you are having trouble logging in please contact us at <a href="mailto:admin@risktech.com.au">admin@risktech.com.au</a></p>
			<?php echo $this->session->flashdata('msg'); ?>
			<?php 
	          $attributes = array("id" => "loginform", "name" => "loginform");
	          echo form_open("login/login", $attributes);?>
			  <div class="form-group">
			    <input type="email" class="form-control" id="exampleInputEmail1" name="txt_username" placeholder="Email">
			  </div>
			  <div class="form-group">
			    <input type="password" class="form-control" id="exampleInputPassword1" name="txt_password" placeholder="Password">
			  </div>
			  <input id="btn_login" name="btn_login" type="submit" class="btn btn-default right-align-button" value="Login" />
	          <?php echo form_close(); ?>
	          
		</div>
		
	</div>
</div>
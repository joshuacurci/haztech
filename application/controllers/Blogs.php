<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('admin_model');
		$this->load->model('blogs_model');
		$this->load->model('news_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
	}

	public function index()
	{
		$header['menuitem'] = '3';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'News';

		$data['blogs'] = $this->blogs_model->get_blogs();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('blogs/index', $data);
		$this->load->view('main_footer');
	}

	public function adminSide()
	{
		$header['menuitem'] = '3';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'News';

		$data['blogs'] = $this->blogs_model->get_blogs();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('blogs/adminSide', $data);
		$this->load->view('main_footer');
	}

	public function view($blogID)
	{
		$header['menuitem'] = '3';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'News';

		$data['blogs'] = $this->blogs_model->get_blog_info($blogID);
		$data['image'] = $this->blogs_model->get_image_info($blogID);

		$this->load->view('main_header', $header);
		$this->load->view('blogs/view', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$postdata = $this->input->post();
		$data['user'] = $this->admin_model->search_users($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($blogID)
	{
		$header['menuitem'] = '3';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'News';

		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['useraccess'] = $this->blogs_model->get_access_info($blogID);
		$data['blogs'] = $this->blogs_model->get_blog_info($blogID);
		$data['image'] = $this->blogs_model->get_image_info($blogID);

		$this->load->view('main_header', $header);
		$this->load->view('blogs/edit', $data);
		$this->load->view('main_footer');
	}

	public function add()
	{
		$header['menuitem'] = '3';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'News';
		$userID = $this->input->post('userID');

		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('blogs/add', $data);
		$this->load->view('main_footer');
	}

	public function save()
	{
		$postdata = $this->input->post();
		$blogID = $postdata['blogID'];

		$imageInfo = $this->blogs_model->get_image_info($blogID);
		$this->blogs_model->update_blog_info($postdata);

		if (empty($imageInfo)) {
			$this->do_upload($blogID);
		} else {
			$this->replace_upload($blogID);
		}

		redirect('/blogs/view/'.$blogID);	
	}

	public function newblog()
	{
		$postdata = $this->input->post();
		
		// get the last inserted blogID
		$blogID = $this->blogs_model->new_blog_info($postdata);

		// Check image Data
		$imageInfo = $this->blogs_model->get_image_info($blogID);

		// Pass the blog ID to the upload image function
		if (empty($imageInfo)){
			$this->do_upload($blogID);
		} else {
			$this->replace_upload($blogID);
		}

		redirect('/blogs/view/'.$blogID.'/');
	}

	public function do_upload($blogID)
	{
		$postdata = $this->input->post();
		$data['blog'] = $this->blogs_model->get_blog_info($blogID);

		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/blog/'))
		{
			mkdir('./uploads/blog/', 0777, true);
		}
		$config['upload_path']          = './uploads/blog/';
		$config['allowed_types']        = 'gif|jpg|png';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('blog_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/blogs/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->blogs_model->upload_image($blogID, $value['file_name']);
			}
			redirect('/blogs/view/'.$blogID);
		}
	}

	public function replace_upload($blogID)
	{
		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/blog/'))
		{
			mkdir('./uploads/blog/', 0777, true);
		}
		$config['upload_path']          = './uploads/blog/';
		$config['allowed_types']        = 'gif|jpg|png';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('blog_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/blogs/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->blogs_model->update_image($blogID, $value['file_name']);
			}
			redirect('/blogs/view/'.$blogID);
		}
	}

	public function delete($blogID)
	{
		$this->blogs_model->delete_blog_info($blogID);

		redirect('/blogs/index/');
	}

}

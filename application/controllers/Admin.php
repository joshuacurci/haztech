<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('admin_model');
		$this->load->model('login_model');
		$this->load->model('news_model');
		$this->load->model('blogs_model');
		$this->load->model('client_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users_list();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function view($userID)
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users_list();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['user'] = $this->admin_model->get_user_info($userID);
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('admin/view', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$postdata = $this->input->post();
		$data['user'] = $this->admin_model->search_users($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($userID)
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';


		$data['user'] = $this->admin_model->get_user_info($userID);
		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();
		$data['client'] = $this->client_model->get_client_list();

		$this->load->view('main_header', $header);
		$this->load->view('admin/edit', $data);
		$this->load->view('main_footer');
	}

	public function add()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();
		$data['client'] = $this->client_model->get_client_list();

		$this->load->view('main_header', $header);
		$this->load->view('admin/add', $data);
		$this->load->view('main_footer');
	}

	public function save()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$postdata = $this->input->post();
		$userID = $this->input->post('userID');
		$password_correct = $_POST["password_correct"];
		$password = $_POST["password"];

		$data['user'] = $this->admin_model->get_user_info($userID);
		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();
		$usertype = $_SESSION['usertype'];

		$this->load->view('main_header', $header);
		// check if password field is not empty
		if ($_POST["password_change"] != NULL) {
			// check if usertype is admin
			if ($usertype == 'A' || $usertype == 'B') {
				// check if password matches
				if ($_POST["password_change"] == $_POST["password_change1"]) {

					$this->admin_model->update_password_info($postdata);

					$this->load->view('admin/updated', $data);
					$this->load->view('admin/view', $data);

				} 
				// if password didnt match
				else {
					$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">"Change Password" and "Confirm password Change" did not match.</div>');
					$this->load->view('admin/edit', $data);
				}
			}
			// if not admin 
			else {
				// check if password for user is correct
				if ( $password_correct == $password ) {
					// check if new password matches
					if ($_POST["password_change"] == $_POST["password_change1"]) {

						$this->admin_model->update_password_info($postdata);

						$this->load->view('admin/updated', $data);
						$this->load->view('admin/view', $data);
					}
					// if password didn't match
					else {
						$this->session->set_flashdata('msg', '<br /><div class="alert alert-danger text-center">"Change Password" and "Confirm password Change" did not match.</div>');
						$this->load->view('admin/edit', $data);
					}
				}
				// if password for user is incorrect 
				else {
					$this->session->set_flashdata('msg', '<br /><div class="alert alert-danger text-center">User password incorrect! Please try again.</div>');
					$this->load->view('admin/edit', $data);
				}
			} 
		} 
		// if password field is empty, dont update password
		else {

			$this->admin_model->update_user_info($postdata);

			$this->load->view('admin/updated', $data);
			$this->load->view('admin/view', $data);
		}

		$this->load->view('main_footer');
	}

	public function newuser()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$postdata = $this->input->post();

		$type_letter = $_POST['type_letter'];
		$usr_client = $_POST['usr_client'];

		$this->load->view('main_header', $header);
		if ($type_letter == 'E' || $type_letter == 'F') {
			if ($usr_client == '0') { 
				$this->session->set_flashdata('msg', '<br /><div class="alert alert-danger text-center">Client Required For This User</div>');
				$this->load->view('admin/add', $data);
			} else{
				$this->admin_model->new_user_info($postdata);
				redirect('/admin/index/');
			}
		} else {
			if ($_POST["password"] == $_POST["confirm_password"]) {
				$this->admin_model->new_user_info($postdata);
				redirect('/admin/index/');
			} else {
				$this->session->set_flashdata('msg', '<br /><div class="alert alert-danger text-center">"Change Password" and "Confirm password Change" did not match.</div>');
				$this->load->view('admin/add', $data);
			}
		}

		$this->load->view('main_footer');


		

	}

	public function delete($userID)
	{
		$this->admin_model->delete_user_info($userID);

		redirect('/admin/index/');
	}

}

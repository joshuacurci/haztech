<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Pdf');
		$this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');
		$this->load->model('items_model');
		$this->load->model('client_model');
		$this->load->model('admin_model');
		$this->load->model('news_model');
		$this->load->model('login_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}


	public function index()
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$data['client'] = $this->client_model->get_clients();
		$data['clientlist'] = $this->client_model->get_client_list();

		$this->load->view('main_header', $header);
		$this->load->view('client/index', $data);
		$this->load->view('main_footer');
	}

	public function view($clientID)
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$data['sitelist'] = $this->sites_model->get_client_site_list($clientID);
		$data['client'] = $this->client_model->get_client_info($clientID);
		$data['image'] = $this->client_model->get_image_info($clientID);

		$this->load->view('main_header', $header);
		$this->load->view('client/view', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$postdata = $this->input->post();
		$data['client'] = $this->client_model->search_clients($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['date_inspected'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('client/index', $data);
		$this->load->view('main_footer');
	}

	public function clientsearch()
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$postdata = $this->input->post();
		$data['client'] = $this->client_model->search_client_name($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['client_name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('client/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($clientID)
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$data['client'] = $this->client_model->get_client_info($clientID);
		$data['image'] = $this->client_model->get_image_info($clientID);

		$this->load->view('main_header', $header);
		$this->load->view('client/edit', $data);
		$this->load->view('main_footer');
	}

	public function add()
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$this->load->view('main_header', $header);
		$this->load->view('client/add');
		$this->load->view('main_footer');
	}


	public function save()
	{
		$postdata = $this->input->post();
		$clientID = $this->input->post('clientID');

		$imageInfo = $this->client_model->get_image_info($clientID);

		$this->client_model->update_client_info($postdata);

		if (empty($imageInfo)) {
			$this->do_upload($clientID);
		} else {
			$this->replace_upload($clientID);
		}

		redirect('/client/view/'.$clientID.'/');
	}

	public function newclient()
	{
		$postdata = $this->input->post();

		// get last client ID
		$clientID = $this->client_model->new_client_info($postdata);

		// Get image information
		$imageInfo = $this->client_model->get_image_info($clientID);

		// Pass the client ID to the upload image function
		if (empty($imageInfo)) {
			$this->do_upload($clientID);
		} else {
			$this->replace_upload($clientID);
		}

		redirect('/client/view/'.$clientID.'/');
		
	}

	public function do_upload($clientID)
	{
		$postdata = $this->input->post();

		$data['client'] = $this->client_model->get_client_info($clientID);

		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/client/'))
		{
			mkdir('./uploads/client/', 0777, true);
		}
		$config['upload_path']          = './uploads/client/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('client_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/client/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->client_model->upload_image($clientID, $value['file_name']);
			}
		}
	}

	public function replace_upload($clientID)
	{
		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/client/'))
		{
			mkdir('./uploads/client/', 0777, true);
		}
		$config['upload_path']          = './uploads/client/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('client_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/client/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->client_model->update_image($clientID, $value['file_name']);
			}
		}
	}

	public function delete($clientID)
	{
		$this->client_model->delete_client_info($clientID);

		redirect('/client/index/');
	}

	public function filter($filterletter)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->filter_letter_clients($filterletter);
          $data['filter'] = $filterletter;

          $this->load->view('main_header', $header);
          $this->load->view('client/index', $data);
          $this->load->view('main_footer');
  }

}

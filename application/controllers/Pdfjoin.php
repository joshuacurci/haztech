<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  
   class Pdfjoin extends CI_Controller {
	
      public function __construct() { 
        parent::__construct(); 
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Pdfmurge');
        $this->load->model('sites_model');
      }
		
      public function index($pdfName, $pdfNameFINAL, $pdfName2, $siteID) { 
        
        $filePath = str_replace('/application/controllers', '/uploads/genReport/', dirname(__FILE__));

        // PDF joinning script to then download
        $pdfMerge = new PDFMerger;
        //$pdfMerge->setPrintHeader(false);
        $pdfMerge->addPDF($filePath.$pdfName2, 'all'); // with links
        //echo $filePath.$pdfName2.'</br>';
        $siteDocs = $this->sites_model->get_sitedoc_removeblank($siteID);
        $countArray = count($siteDocs);
        if ($countArray > 0){
          foreach ($siteDocs as $document){
            $pdfMerge->addPDF('uploads/site_document/'.$document['full_path'], 'all');
            //echo 'uploads/site_document/'.$document['full_path'].'</br>';
          }
        }	
        $pdfMerge->merge('file', $filePath.$pdfNameFINAL); // generate the file
        $pdfMerge->merge('download', $pdfName); /**/
      } 
		
   } 
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class download extends CI_Controller
{

     public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          $this->load->model('login_model');
          $this->load->model('download_model');
          //$this->load->model('logs_model');
          
          if ( ! $this->session->userdata('loginuser'))
          { 
              redirect('login/index');
          }
     }

     public function file($location, $fileID)
     {
        $fileName = $this->download_model->get_file($location, $fileID);
        $file = 'uploads/genReport/'.$fileName;

        $this->logs_model->add_new_log($this->session->userdata('userID'),'Downloaded file - '.$fileName,'D');

        // Force the download
        header("Content-Disposition: attachment; filename=" . basename($file) . "");
        header("Content-Length: " . filesize($file));
        header("Content-Type: application/octet-stream;");
        readfile($file);/**/
          
     }

     public function lastReport($siteID)
     {
        $fileName = $this->download_model->get_lastReport_file($siteID);
        $file = 'uploads/genReport/'.$fileName;

        //$this->logs_model->add_new_log($this->session->userdata('userID'),'Downloaded file - '.$fileName,'D');

        // Force the download
        header("Content-Disposition: attachment; filename=" . basename($file) . "");
        header("Content-Length: " . filesize($file));
        header("Content-Type: application/octet-stream;");
        readfile($file);/**/
          
     }
}?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Pdf');
		$this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');
		$this->load->model('items_model');
		$this->load->model('admin_model');
		$this->load->model('news_model');
		$this->load->model('blogs_model');
		$this->load->model('MathEngine_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['itemlist'] = $this->items_model->get_items_list();
		$data['current_photo_no'] = $this->items_model->get_photo_no();

		$this->load->view('main_header', $header);
		$this->load->view('items/index', $data);
		$this->load->view('main_footer');
	}

	public function all()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$clientID = $_SESSION['clientID'];

		$data['itemlist'] = $this->items_model->get_all_items();
		$data['current_photo_no'] = $this->items_model->get_photo_no();
		$data['itemclientlist'] = $this->items_model->get_client_items_list($clientID);

		$this->load->view('main_header', $header);
		$this->load->view('items/all', $data);
		$this->load->view('main_footer');
	}

	public function clientall($clientID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['itemlist'] = $this->items_model->get_all_items();
		$data['itemclientlist'] = $this->items_model->get_client_items_list($clientID);
		$data['current_photo_no'] = $this->items_model->get_photo_no();

		$this->load->view('main_header', $header);
		$this->load->view('items/all', $data);
		$this->load->view('main_footer');
	}

	public function view($itemID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['item'] = $this->items_model->get_item_info($itemID);
		$data['image'] = $this->items_model->get_image_info($itemID);
		$data['hazardtype'] = $this->items_model->get_hazards();
		$data['materials'] = $this->items_model->get_materials();
		$data['current_photo_no'] = $this->items_model->get_photo_no();
		$data['itemDocumentlist'] = $this->items_model->get_item_documentlist($itemID);

		$data['finalcost'] = $this->MathEngine_model->calculate_repair_cost($data['item'][0]['materialID'],$data['item'][0]['extent']);
		
		$this->load->view('main_header', $header);
		$this->load->view('items/view', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$postdata = $this->input->post();		
		$data['user'] = $this->admin_model->search_users($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($itemID, $siteID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['item'] = $this->items_model->get_item_info($itemID);
		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['image'] = $this->items_model->get_image_info($itemID);
		$data['hazardtype'] = $this->items_model->get_hazards();
		$data['materials'] = $this->items_model->get_materials();
		$data['itemDocumentlist'] = $this->items_model->get_item_documentlist($itemID);

		$this->load->view('main_header', $header);
		$this->load->view('items/edit', $data);
		$this->load->view('main_footer');
	}

	public function add($siteID, $clientID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['hazardtype'] = $this->items_model->get_hazards();
		$data['materials'] = $this->items_model->get_materials();
		$data['current_photo_no'] = $this->items_model->get_photo_no();
		
		$this->load->view('main_header', $header);
		$this->load->view('items/add', $data);
		$this->load->view('main_footer');
	}

	public function save()
	{
		$postdata = $this->input->post();
		// var_dump($postdata); 
		$itemID = $postdata['itemID'];
		$siteID = $postdata['siteID'];
		$files = $_FILES;

		$imageInfo = $this->items_model->get_image_info($itemID);
		$this->items_model->update_item_info($postdata);

		// Pass the site ID to the upload image function
		if (empty($imageInfo)) {
			$this->do_upload($itemID);
			$this->items_model->update_photo_no($postdata);
		} else {
			$this->replace_upload($itemID);
		}

		// Upload documents (array)
		$this->do_upload_itemDocument($itemID, $files);		

		redirect('/items/view/'.$itemID);
	}

	public function newitem()
	{
		$postdata = $this->input->post();
		$siteID = $this->input->post('siteID');
		
		$files = $_FILES;

		// get last item ID
		$itemID = $this->items_model->new_item_info($postdata);
		
		// Check image information
		$imageInfo = $this->items_model->get_image_info($itemID);
		// Pass the site ID to the upload image function
		if (empty($imageInfo)) {
			$this->do_upload($itemID);
		} else {
			$this->replace_upload($itemID);
		}

		// Upload documents (array)
		$this->do_upload_itemDocument($itemID, $files);
		

		redirect('/items/view/'.$itemID.'/');
	}

	public function do_upload_itemDocument($itemID, $files)
	{
		//Upload document
		$this->load->library('upload');
		$cpt = count($_FILES['item_document']['name']);

    	// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/item_document/'))
		{
			mkdir('./uploads/item_document/', 0777, true);

		}
		// Define folder to upload and file types
		$config = array(
			'upload_path'   => './uploads/item_document/',
			'allowed_types' => 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx|zip',
			'overwrite'     => 1,                       
			);

		$this->upload->initialize($config);
		$this->load->library('upload', $config);
		
		// loop through files
		for($i=0; $i<$cpt; $i++) {
			$_FILES['item_document']['name']= $files['item_document']['name'][$i];
			$_FILES['item_document']['type']= $files['item_document']['type'][$i];
			$_FILES['item_document']['tmp_name']= $files['item_document']['tmp_name'][$i];
			$_FILES['item_document']['error']= $files['item_document']['error'][$i];
			$_FILES['item_document']['size']= $files['item_document']['size'][$i];

			if ($this->upload->do_upload('item_document')) {
				$this->_uploaded[$i] = $this->upload->data();
			} else {
				$this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
				//echo $this->upload->display_errors();
        		//return FALSE;
			}
			$file_name = $_FILES['item_document']['name'];
			$this->items_model->upload_itemDocument($itemID, $file_name);
		}
	}


	public function do_upload($itemID)
	{
		$postdata = $this->input->post();
		$siteID = $this->input->post('siteID');

		$data['item'] = $this->items_model->get_item_info($itemID);

		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/items/'))
		{
			mkdir('./uploads/items/', 0777, true);
		}
		$config['upload_path']          = './uploads/items/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('item_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$insertID = $this->items_model->upload_image($itemID, $value['file_name']);
				$this->items_model->add_image_number($itemID, $insertID);
			}
		}
	}

	public function replace_upload($itemID)
	{
		$postdata = $this->input->post();
		$siteID = $this->input->post('siteID');
		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/items/'))
		{
			mkdir('./uploads/items/', 0777, true);
		}
		$config['upload_path']          = './uploads/items/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('item_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->items_model->update_image($itemID, $value['file_name']);
			}
		}
	}

	public function delete($itemID,$siteID)
	{
		$this->items_model->delete_item_info($itemID);

		redirect('sites/view/'.$siteID);
	}

	public function listrate($rating)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['itemlist'] = $this->items_model->getControlPriorityData($rating);
		$data['current_photo_no'] = $this->items_model->get_photo_no();

		$this->load->view('main_header', $header);
		$this->load->view('items/indexRate', $data);
		$this->load->view('main_footer');
	}

}

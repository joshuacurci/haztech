<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');

		$this->load->model('newreport_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{
		$header['menuitem'] = '7';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Reports';

		$data['sitelist'] = $this->sites_model->get_sites_list();

		$this->load->view('main_header', $header);
		$this->load->view('reports/index', $data);
		$this->load->view('main_footer');
	}

		public function client_index($clientID)
	{
		$header['menuitem'] = '7';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Reports';

		$data['sitelist'] = $this->sites_model->get_client_site_list($clientID);

		$this->load->view('main_header', $header);
		$this->load->view('reports/index', $data);
		$this->load->view('main_footer');
	}

	public function viewsaved($siteID)
	{
		$header['menuitem'] = '7';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Reports';

		$data['savedReports'] = $this->newreport_model->get_report_saved($siteID);
		$data['sitename'] = $this->newreport_model->get_sitename($siteID);

		$this->load->view('main_header', $header);
		$this->load->view('reports/viewsaved', $data);
		$this->load->view('main_footer');
	}

	public function generate_report($siteID, $clientID)
	{
		$header['menuitem'] = '7';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Reports';

		$data['reportTemplate'] = $this->newreport_model->get_report_template($siteID);
		$data['siteID'] = $siteID;
		$data['clientID'] = $clientID;

		$this->load->view('main_header', $header);
		$this->load->view('reports/genForm', $data);
		$this->load->view('main_footer');
	}

}

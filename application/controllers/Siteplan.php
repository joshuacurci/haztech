<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siteplan extends CI_Controller 
{

     public function __construct()
     {
          parent::__construct();

          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          $this->load->library('Pdf');
          $this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
          $this->load->model('sites_model');
          $this->load->model('items_model');
          $this->load->model('admin_model');
          $this->load->model('news_model');
          $this->load->model('blogs_model');
          $this->load->model('client_model');

          if($this->config->item('maintenance_mode') == TRUE) {
               $this->load->view('under_construction');
               $content = $this->load->view('under_construction', '', TRUE); 
               echo $content;
               die();
          }
            //$this->load->model('news_model');
            //$this->load->model('admin_model');

          if ( ! $this->session->userdata('loginuser')) { 
               redirect('login/index');
          }
     }


     public function do_upload($siteID)
     {
          $postdata = $this->input->post();

          $data['site'] = $this->sites_model->get_site_info($siteID);

          // Make sure the upload folder exists before uploading to it
          if (!is_dir('uploads/siteplan/'))
          {
               mkdir('./uploads/siteplan/', 0777, true);
          }
          $config['upload_path']          = './uploads/siteplan/';
          $config['allowed_types']        = 'gif|jpg|png|jpeg';

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if (!$this->upload->do_upload('site_plan'))
          {
               $error = array('error' => $this->upload->display_errors());
               // Display an error or var_log($error);
               $this->load->view('/sites/view', $error);
          }
          else
          {
               $data = array('upload_data' => $this->upload->data());

               // loop through data image array to get image data
               // echo '<pre>';
               // var_dump($data);
               foreach($data as $value) {
                    $this->sites_model->upload_siteplan($siteID, $value['file_name']);
               }
               redirect('/sites/view/'.$siteID);
          }
     }

     public function replace_upload($siteID)
     {
          // Make sure the upload folder exists before uploading to it
          if (!is_dir('uploads/siteplan/'))
          {
               mkdir('./uploads/siteplan/', 0777, true);
          }
          $config['upload_path']          = './uploads/siteplan/';
          $config['allowed_types']        = 'gif|jpg|png|jpeg';

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if (!$this->upload->do_upload('site_plan'))
          {
               $error = array('error' => $this->upload->display_errors());
               // Display an error or var_log($error);
               $this->load->view('/sites/view', $error);
          }
          else
          {
               $data = array('upload_data' => $this->upload->data());

               // loop through data image array to get image data
               // echo '<pre>';
               // var_dump($data);
               foreach($data as $value) {
                    $this->sites_model->update_siteplan($siteID, $value['file_name']);
               }
               redirect('/sites/view/'.$siteID);
          }
     }
     
}?>
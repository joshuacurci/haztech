	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Sites extends CI_Controller 
	{
	// View Report
		public function view_site($siteID, $clientID)
		{
			$data['sitelist'] = $this->sites_model->get_sites_list();
			$data['site'] = $this->sites_model->get_site_info($siteID);
			$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
			$data['itemlist'] = $this->items_model->get_items_list($siteID);
			$data['image'] = $this->sites_model->get_image_info($siteID);
			$data['client_image'] = $this->client_model->get_image_info($clientID);
			$data['client'] = $this->client_model->get_client_info($clientID);
			$reportheader = $this->load->view('sites/report_header', $data, true);
			$this->load->library("Pdf");
	// create new PDF document
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);   
	// remove default header/footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(true);
	// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
	// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}   
			$first_column_width = 85;
			$second_column_width = 85;
			$column_space = 25; 
			$pdf->SetY(28);
			$current_y_position = $pdf->getY();
			$line_height = 4.2;
			$siteview = $this->load->view('sites/site_report', $data, true);
			$clientview = $this->load->view('sites/report_front', $data, true);
			$itemimages = $this->load->view('sites/report_images', $data, true);
			$itemimages2 = $this->load->view('sites/report_images2', $data, true);
			$reportheader = $this->load->view('sites/report_header', $data, true);
			$pdf->setFontSubsetting(true);   
			$pdf->SetFont('dejavusans', '', 6, '', true);   
// set header data
			$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
// declare paper orientation and size. P for Portrait and L for landscape
			$pdf->AddPage('P', 'A4');
			$pdf->SetY(50);
// front page of report
			$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, 'C', true); 
// landscape
			$pdf->AddPage('L', 'A4');
// header
			$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
// content
			$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);
			$pdf->AddPage('P', 'A4');
			$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
// set auto page break
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
// 2 columns for the images
// column 1
			$pdf->writeHTMLCell($first_column_width, '', '', $current_y_position, $itemimages, true, 0, 0);
// continue page break
			if (($current_y_position + $line_height) >= 147) {
				$pdf->AddPage('P', 'A4');
				$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
				$y = $this->pdf->GetY();
				$y = 0;

			}
$pdf->Cell(0); // THIS LINE SOLVES THE SECOND COLUMN SPACE PROBLEM
// write second column 
$pdf->writeHTMLCell($second_column_width, '', $first_column_width+$column_space, $current_y_position, $itemimages2, true, 0, 0, 0, true, '', true);
if (($current_y_position + $line_height) >= 147) {
	$y = $this->pdf->GetY();
	$y = 0;
	$pdf->AddPage('P', 'A4');
}
}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
    $this->load->model('login_model');
    $this->load->model('news_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
    
  }

  public function index()
  {

    $header['menuitem'] = '0';
    $header['usergroup'] = '';
    $header['pagetitle'] = 'Login';

    $data['blog_posts'] = $this->news_model->get_blog_posts();

    $this->load->view('main_header', $header);
    $this->load->view('login/index', $data);
    $this->load->view('main_footer');
  }

  public function login() {
          //get the posted values
    $username = $this->input->post("txt_username");
    $password = $this->input->post("txt_password");

          //set validations
    $this->form_validation->set_rules("txt_username", "Username", "trim|required");
    $this->form_validation->set_rules("txt_password", "Password", "trim|required");

    if ($this->form_validation->run() == FALSE) {
            //validation fails
      $data['blog_posts'] = $this->news_model->get_blog_posts();
      $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Please ensure you have entered your email and password</div>');
      $this->load->view('main_header', $header);
      $this->load->view('login/index', $data);
      $this->load->view('main_footer');
    } else {
               //validation succeeds
      if ($this->input->post('btn_login') == "Login") {
                    //check if the master is logging in
        if ($username == 'master@swim.com.au' && $password == 'master') {
                      //set the session variables
          $sessiondata['username'] = $username;
          $sessiondata['loginuser'] = TRUE;
          $sessiondata['userID'] = 0;
          $sessiondata['usertype'] = 'A';
          $sessiondata['usersname'] = 'The Creator';
          $sessiondata['useremail'] = 'master@swim.com.au';
                      //$this->login_model->setlastlogin($username, $password);
          $this->session->set_userdata($sessiondata);
          redirect('home/index');
        } else {
                      //check if username and password is correct
          $usr_result = $this->login_model->get_user($username, $password);
                      //active user record is present
          if ($usr_result['rownumber'] > 0) {
                        //set the session variables
            $sessiondata['username'] = $username;
            $sessiondata['loginuser'] = TRUE;

            foreach ($usr_result['userdetails'] as $userdata) {
              $sessiondata['userID'] = $userdata['userID'];
              $sessiondata['usertype'] = $userdata['type_letter'];
              $sessiondata['username'] = $userdata['name'];
              $sessiondata['useremail'] =  $userdata['email'];
              $sessiondata['clientID'] =  $userdata['usr_client'];
            }
            $this->login_model->setlastlogin($username, $password);  
            $this->session->set_userdata($sessiondata);
            redirect('dashboard/index');
          } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
            redirect('login/index');
          }
        }
      } else {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
        redirect('login/index');
      }
    }
  }

  public function logout() {
    session_destroy();
    redirect('login/index');
  }

}

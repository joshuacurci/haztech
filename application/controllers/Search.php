<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Pdf');
		$this->load->library('Upload');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');
		$this->load->model('items_model');
		$this->load->model('admin_model');
		$this->load->model('news_model');
		$this->load->model('blogs_model');
		$this->load->model('MathEngine_model');

		$this->load->model('search_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{
		$header['menuitem'] = '8';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Item Search';

		$data['hazardtype'] = $this->items_model->get_hazards();
		$data['materials'] = $this->items_model->get_materials();
		$data['sites'] = $this->sites_model->get_sites();

		$this->load->view('main_header', $header);
		$this->load->view('search/index', $data);
		$this->load->view('main_footer');
	}

	public function test()
	{
		$postdata = $this->input->post();
		echo '<pre>';
		print_r($postdata);
		echo '</pre>';
	}

	public function result()
	{
		$header['menuitem'] = '8';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Item Search';

		$postdata = $this->input->post();

		if ($postdata['siteID'] == '' || empty($postdata['siteID'][0])){ unset($postdata['siteID']); }
		if ($postdata['materialID'] == '' || empty($postdata['materialID'][0])){ unset($postdata['materialID']); }
		if ($postdata['sample_status'] == '' || empty($postdata['sample_status'][0])){ unset($postdata['sample_status']); }
		if ($postdata['hazard_type'] == '' || empty($postdata['hazard_type'][0])){ unset($postdata['hazard_type']); }
		if ($postdata['item_status'] == '' || empty($postdata['item_status'][0])){ unset($postdata['item_status']); }
		if ($postdata['item_condition'] == '' || empty($postdata['item_condition'][0])){ unset($postdata['item_condition']); }
		if ($postdata['friability'] == '' || empty($postdata['friability'][0])){ unset($postdata['friability']); }
		if ($postdata['disturb_potential'] == '' || empty($postdata['disturb_potential'][0])){ unset($postdata['disturb_potential']); }
		if ($postdata['risk_rating'] == '' || empty($postdata['risk_rating'][0])){ unset($postdata['risk_rating']); }

		$data['hazardtype'] = $this->items_model->get_hazards();
		$data['materials'] = $this->items_model->get_materials();
		$data['sites'] = $this->sites_model->get_sites();

		if ($_SESSION['usertype'] == 'C') {
			$data['items'] = $this->search_model->get_searched_itemsClient($postdata, $_SESSION['clientID']);
		} else {
			$data['items'] = $this->search_model->get_searched_items($postdata);
		}
		$data['searchedTerms'] = $postdata;
		$data['returnedItems'] = count($data['items']);

		$this->load->view('main_header', $header);
		$this->load->view('search/results', $data);
		$this->load->view('main_footer');
	}

	public function export_report()
	{
	    $data['searchedTerms'] = $this->input->post();
		$data['sitelist'] = $this->sites_model->get_sites_list();
		$data['report_info'] = $this->sites_model->get_reportcommon_info();
		$data['materials'] = $this->items_model->get_materials();
		$data['hazardtype'] = $this->items_model->get_hazards();
        $SearchResult = filter_input(INPUT_POST, 'result');
        $data['SearchResult'] = unserialize($SearchResult);
	    if (isset($_POST['reportexport'])) {
	    $postdata = $this->input->post();
	    $this->search_report_dl($data, unserialize($postdata['result']));
        redirect('search/index', $data);

        } 
	}
	
	public function search_report_dl($data, $resultlist)
	{	    
	    ob_start();
		$data['itemdata'][$siteID]['itemlist']=$resultlist;
		$data['counter']=0;
		$reportheader = $this->load->view('sites/report_header_search', $data, true);
		$titlename = 'Search Results Export';
	
		$this->load->library("Pdf");
			// Get the value from report where values can be set to Y or N for each site

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true); $pdf->SetTitle($titlename);  
		 // remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
			// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}   
		$first_column_width = 85;
		$second_column_width = 85;
		$column_space = 25; 
		$pdf->SetY(28);
		$current_y_position = $pdf->getY();
		$line_height = 4.2;
			// Get the template
		$siteviewheader = $this->load->view('sites/site_report_search_header', $data, true);
		$siteview = $this->load->view('sites/site_report_search', $data, true);
		$siteinfo = $this->load->view('sites/site_information_search', $data, true);
		$clientview = $this->load->view('sites/report_front_search', $data, true);
		$itemimages = $this->load->view('sites/report_images', $data, true);
		$itemimages2 = $this->load->view('sites/report_images2', $data, true);
		$reportheader = $this->load->view('sites/report_header', $data, true);
		$introduction = $this->load->view('sites/report_introduction_search', $data, true);
		$risk_factor = $this->load->view('sites/report_riskfactor', $data, true);
		$risk_factors = $this->load->view('sites/report_risk_factors', $data, true);
		$howtouseregister = $this->load->view('sites/report_howtouseregister', $data, true);
		$pdf->setFontSubsetting(true);   
		$pdf->SetFont('dejavusans', '', 5, '', true);   
		$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		$pdf->AddPage('P', 'A4');

		$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
		$pdf->SetY(-50);
		$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 


			// How to use register
		$pdf->AddPage('L', 'A4');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $howtouseregister, 0, 1, 0, true, '', true); 

         foreach ($data['itemdata'] as $fullData) 
        { 
            $itemlist = (array)$fullData['itemlist'];
			$listsize= sizeof($itemlist);
	
			
            
		}
		$data['listsize'] = sizeof($itemlist);
		$pdf->AddPage('L', 'A4');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		$pdf->writeHTMLCell(0, 0, '', '', $siteviewheader, 0, 1, 0, true, '', true);
		
	

		$siteview = $this->load->view('sites/site_report_search', $data, true);

		$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);

		
		
 
			// Check if there is any item has image
		if (!empty($item_photolist['photo_no'])) {
				// Add page for the item photos
			$pdf->AddPage('P', 'A4');
				// Header (haztech and site logo)
			$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
				// set auto page break
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
			// 2 columns for the images
			// column 1
			$pdf->writeHTMLCell($first_column_width, '', '', $current_y_position, $itemimages, true, 0, 0);
			// continue page break
			if (($current_y_position + $line_height) >= 147) {
				$pdf->AddPage('P', 'A4');
				$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
				$y = $this->pdf->GetY();
				$y = 0;
	
			}
			$pdf->Cell(0); // THIS LINE SOLVES THE SECOND COLUMN SPACE PROBLEM
			// write second column 
			$pdf->writeHTMLCell($second_column_width, '', $first_column_width+$column_space, $current_y_position, $itemimages2, true, 0, 0, 0, true, '', true);
			if (($current_y_position + $line_height) >= 147) {
				$y = $this->pdf->GetY();
				$y = 0;
				$pdf->AddPage('P', 'A4');
			}
	
		}
		ob_end_clean();
		$pdf->Output("report.pdf",'D');
		 ob_end_flush();
	}

	

}

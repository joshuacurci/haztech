<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('Pdf');
		$this->load->library('Upload');
		//$this->load->library('Pdfmurge');
		//$this->load->library('Pdfmurgeupdate');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');
		$this->load->model('items_model');
		$this->load->model('admin_model');
		$this->load->model('news_model');
		$this->load->model('blogs_model');
		$this->load->model('client_model');
		$this->load->model('newreport_model');
		$this->load->model('MathEngine_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{

		if ($this->session->userdata('usertype') == 'C') {
			redirect('sites/client_index/'.$this->session->userdata('clientID'));
		}

		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['sitelist'] = $this->sites_model->get_sites_list();

		$this->load->view('main_header', $header);
		$this->load->view('sites/index', $data);
		$this->load->view('main_footer');
	}

	public function client_index($clientID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['sitelist'] = $this->sites_model->get_client_site_list($clientID);
		$data['client'] = $this->client_model->get_client_info($clientID);


		$this->load->view('main_header', $header);
		$this->load->view('sites/client_index', $data);
		$this->load->view('main_footer');
	}

	public function view($siteID)
	{

		if (!$siteID) {
			redirect('sites/index');
		}

		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['itemlist'] = $this->items_model->get_items_list($siteID);
		$data['sitelist'] = $this->sites_model->get_sites_list();
		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['image'] = $this->sites_model->get_image_info($siteID);
		$data['siteplan'] = $this->sites_model->get_site_plan($siteID);
		$data['siteDocument'] = $this->sites_model->get_site_document($siteID);
		$data['siteDocumentlist'] = $this->sites_model->get_site_documentlist($siteID);

		$data['previousReportTest'] = $this->sites_model->test_ifprevious_reports($siteID);

		$this->load->view('main_header', $header);
		$this->load->view('sites/view', $data);
		$this->load->view('sites/site_report_button', $data);
		$this->load->view('items/index', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$postdata = $this->input->post();
		$data['user'] = $this->admin_model->search_users($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($siteID)
	{

		if (!$siteID) {
			redirect('sites/index');
		}

		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['states'] = $this->client_model->get_au_states();
		$data['image'] = $this->sites_model->get_image_info($siteID);
		$data['client'] = $this->client_model->get_clients();
		$data['siteplan'] = $this->sites_model->get_site_plan($siteID);
		$data['siteDocument'] = $this->sites_model->get_site_document($siteID);
		$data['siteDocumentlist'] = $this->sites_model->get_site_documentlist($siteID);

		$this->load->view('main_header', $header);
		$this->load->view('sites/edit', $data);
		$this->load->view('main_footer');
	}

	public function report_data_view()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['report_common'] = $this->sites_model->get_reportcommon_info();


		$this->load->view('main_header', $header);
		$this->load->view('sites/report_data_view', $data);
		$this->load->view('main_footer');
	}

	public function report_data_edit()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['report_common'] = $this->sites_model->get_reportcommon_info();


		$this->load->view('main_header', $header);
		$this->load->view('sites/report_data_edit', $data);
		$this->load->view('main_footer');
	}

	public function report_wizard($siteID, $clientID)
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['image'] = $this->sites_model->get_image_info($siteID);
		$data['client'] = $this->client_model->get_clients();
		$data['report'] = $this->sites_model->get_report_info($siteID);
		$data['reportcommon'] = $this->sites_model->get_reportcommon_info();

		$this->load->view('main_header', $header);
		$this->load->view('sites/report_wizard', $data);
		$this->load->view('main_footer');
	}

	public function add()
	{
		$header['menuitem'] = '5';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Sites';

		$data['states'] = $this->client_model->get_au_states();
		$data['client'] = $this->client_model->get_clients();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");

		$this->load->view('main_header', $header);
		$this->load->view('sites/add', $data);
		$this->load->view('main_footer');
	}

	public function addonclient($clientID)
	{
		$header['menuitem'] = '6';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Client';

		$data['client'] = $this->client_model->get_client_info($clientID);

		$this->load->view('main_header', $header);
		$this->load->view('sites/addonclient', $data);
		$this->load->view('main_footer');
	}
	public function save()
	{
		$postdata = $this->input->post();
		// var_dump($postdata); 
		$siteID = $postdata['siteID'];
		$client_name = $postdata['client_name'];
		$full_address = $postdata['full_address'];
		$inspected_by = $postdata['inspected_by'];
		$files = $_FILES;

		$imageInfo = $this->sites_model->get_image_info($siteID);
		$siteplanInfo = $this->sites_model->get_site_plan($siteID);
		$reportinfo = $this->sites_model->get_report_info($siteID);

		$this->sites_model->update_site_info($postdata);

		// Check if there is data
		$this->newreport_model->get_report_template($siteID);

		// Pass the site ID to the upload image function
		if (empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->do_upload_siteplan($siteID);
		} else if (empty($imageInfo) && !empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->replace_site_plan($siteID);
		} else if (!empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload_siteplan($siteID);
			$this->replace_upload($siteID);
		} else {
			$this->replace_upload($siteID);
			$this->replace_site_plan($siteID);
		}

		// Upload Documents (array)
		//$this->add_report_info($siteID);
		$this->do_upload_siteDocument($siteID, $files);
		
		if(isset($postdata['removeDocument'])){
			foreach ($postdata['removeDocument'] as $documentInfo) {
				$this->sites_model->remove_site_file($documentInfo);
			}
		}
		
		redirect('/sites/view/'.$siteID);
	}

	public function newsite()
	{
		
		$postdata = $this->input->post();
		// var_dump($postdata); 
		$clientID = $postdata['clientID'];
		$client_name = $postdata['client_name'];
		$full_address = $postdata['full_address'];
		$inspected_by = $postdata['inspected_by'];
		$files = $_FILES;

		//Get last Inserted ID
		$siteID = $this->sites_model->new_site_info($postdata, $clientID);

		// Get image image and document informations
		$imageInfo = $this->sites_model->get_image_info($siteID);
		$siteplanInfo = $this->sites_model->get_site_plan($siteID);
		$sitedocument = $this->sites_model->get_site_document($siteID);
		$reportinfo = $this->sites_model->get_report_info($siteID);	

		// Check if there is data
		$this->newreport_model->get_report_template($siteID);

		// Pass the site ID to the upload image function
		if (empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->do_upload_siteplan($siteID);
		} else if (empty($imageInfo) && !empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->replace_site_plan($siteID);
		} else if (!empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload_siteplan($siteID);
			$this->replace_upload($siteID);
		} else {
			$this->replace_upload($siteID);
			$this->replace_site_plan($siteID);
		}

		// Upload Documents (array)
		$this->do_upload_siteDocument($siteID, $files);

		redirect('/sites/view/'.$siteID);
	}

	public function newsiteclient()
	{
		$postdata = $this->input->post();
		$clientID = $postdata['clientID'];

		// var_dump($postdata); 
		$client_name = $postdata['client_name'];
		$full_address = $postdata['full_address'];
		$inspected_by = $postdata['inspected_by'];
		$date_inspected = $postdata['date_inspected'];
		$document = $_FILES['site_document']['name'];
		$files = $_FILES; 

		// get last ID
		$siteID = $this->sites_model->new_site_info($postdata, $clientID);	
		
		// check image data
		$imageInfo = $this->sites_model->get_image_info($siteID);
		$siteplanInfo = $this->sites_model->get_site_plan($siteID);
		$reportinfo = $this->sites_model->get_report_info($siteID);

		// Check if there is data
		if (empty($reportinfo)) {
			$this->sites_model->add_report_info($postdata, $client_name, $full_address, $inspected_by, $date_inspected, $siteID);
		} else {
			$this->sites_model->update_report_info($postdata, $client_name, $full_address, $inspected_by, $date_inspected, $siteID);
		}

		// Pass the site ID to the upload image function
		if (empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->do_upload_siteplan($siteID);
		} else if (empty($imageInfo) && !empty($siteplanInfo)) {
			$this->do_upload($siteID);
			$this->replace_site_plan($siteID);
		} else if (!empty($imageInfo) && empty($siteplanInfo)) {
			$this->do_upload_siteplan($siteID);
			$this->replace_upload($siteID);
		} else {
			$this->replace_upload($siteID);
			$this->replace_site_plan($siteID);
		}

		// Upload Documents (array)
		$this->do_upload_siteDocument($siteID, $files);

		redirect('/sites/view/'.$siteID);
	}

	public function do_upload_siteDocument($siteID, $files)
	{
		//Upload files
		$this->load->library('upload');
		$cpt = count($_FILES['site_document']['name']);

    	// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/site_document/'))
		{
			mkdir('./uploads/site_document/', 0777, true);

		}
		// Define folder to upload and file types
		$config = array(
			'upload_path'   => './uploads/site_document/',
			'allowed_types' => 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx|zip',
			'overwrite'     => 1,                       
			);

		$this->upload->initialize($config);
		$this->load->library('upload', $config);
		
		// loop through files
		for($i=0; $i<$cpt; $i++) {
			$_FILES['site_document']['name']= $files['site_document']['name'][$i];
			$_FILES['site_document']['type']= $files['site_document']['type'][$i];
			$_FILES['site_document']['tmp_name']= $files['site_document']['tmp_name'][$i];
			$_FILES['site_document']['error']= $files['site_document']['error'][$i];
			$_FILES['site_document']['size']= $files['site_document']['size'][$i];

			if ($this->upload->do_upload('site_document')) {
				$this->_uploaded[$i] = $this->upload->data();
			} else {
				$this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
				//echo $this->upload->display_errors();
        		//return FALSE;
			}
			$file_name = $_FILES['site_document']['name'];
			$this->sites_model->upload_siteDocument($siteID, $file_name);
		}
	}

	public function do_upload_siteplan($siteID)
	{
		$postdata = $this->input->post();

		$data['site'] = $this->sites_model->get_site_info($siteID);

          // Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/siteplan/'))
		{
			mkdir('./uploads/siteplan/', 0777, true);
		}
		$config['upload_path']          = './uploads/siteplan/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('site_plan'))
		{
			$error = array('error' => $this->upload->display_errors());
               // Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

               // loop through data image array to get image data
               // echo '<pre>';
               // var_dump($data);
			foreach($data as $value) {
				$this->sites_model->upload_siteplan($siteID, $value['file_name']);
			}
		}
	}

	public function replace_site_plan($siteID)
	{
          // Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/siteplan/'))
		{
			mkdir('./uploads/siteplan/', 0777, true);
		}
		$config['upload_path']          = './uploads/siteplan/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('site_plan'))
		{
			$error = array('error' => $this->upload->display_errors());
               // Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

               // loop through data image array to get image data
               // echo '<pre>';
               // var_dump($data);
			// var_dump($data);
			// 	die();
			foreach($data as $value) {
				$this->sites_model->update_siteplan($siteID, $value['file_name']);
			}
		}
	}
	

	public function do_upload($siteID)
	{
		$postdata = $this->input->post();

		$data['site'] = $this->sites_model->get_site_info($siteID);

		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/sites/'))
		{
			mkdir('./uploads/sites/', 0777, true);
		}
		$config['upload_path']          = './uploads/sites/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('site_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->sites_model->upload_image($siteID, $value['file_name']);
			}
		}
	}

	public function replace_upload($siteID)
	{
		// Make sure the upload folder exists before uploading to it
		if (!is_dir('uploads/sites/'))
		{
			mkdir('./uploads/sites/', 0777, true);
		}
		$config['upload_path']          = './uploads/sites/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('site_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			// Display an error or var_log($error);
			$this->load->view('/sites/view', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// loop through data image array to get image data
			// echo '<pre>';
			// var_dump($data);
			foreach($data as $value) {
				$this->sites_model->update_image($siteID, $value['file_name']);
			}
		}
	}

	public function delete($siteID)
	{
		if (!$siteID) {
			redirect('sites/index');
		}

		$this->sites_model->delete_site_info($siteID);

		redirect('/sites/index/');
	}

	public function delete_client_site($siteID,$clientID)
	{
		$this->sites_model->delete_site_info($siteID);

		redirect('/client/view/'.$clientID);
	}

	public function generate_report()
	{
		$postdata = $this->input->post();
		// var_dump($postdata); 
		$siteID = $postdata['siteID'];
		$clientID = $postdata['clientID'];

		$this->sites_model->update_reportwiz_info($postdata);

		if ($postdata['action'] == 'reportwiz_view') {
			redirect('/sites/generated_report/'.$siteID.'/'.$clientID);
		} else if ($postdata['action'] == 'reportwiz_download') {
			redirect('/sites/generated_report_dl/'.$siteID.'/'.$clientID);
		}
	}

	public function update_report_data(){

		$postdata = $this->input->post();

		if ($postdata['action'] == 'update_scopeofworks_data') {
			$this->sites_model->update_scopeofworks_info($postdata);
			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_recommendations_data') {
			$this->sites_model->update_recommendations_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_methologies_data') {
			$this->sites_model->update_methologies_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_risk_factors_data') {
			$this->sites_model->update_risk_factors_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_psystem_data') {
			$this->sites_model->update_psystem_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_amr_data') {
			$this->sites_model->update_amr_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_hmr_data') {
			$this->sites_model->update_hmr_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_stateoflimitations_data') {
			$this->sites_model->update_stateoflimitations_info($postdata);

			redirect('/sites/report_data_view');
		} else if ($_POST['action'] == 'update_areasnotaccessed_data') {
			$this->sites_model->update_areasnotaccessed_info($postdata);

			redirect('/sites/report_data_view');
		} else {
			$this->sites_model->update_prefilled_info($postdata);

			redirect('/sites/report_data_view');
		}
	}

	// View Report after report wizard
	public function generated_report($siteID, $clientID)
	{
		$data['sitelist'] = $this->sites_model->get_sites_list();
		$data['site'] = $this->sites_model->get_site_info($siteID);
		$data['sitereport'] = $this->sites_model->get_report_info($siteID);
		$data['report_info'] = $this->sites_model->get_reportcommon_info($siteID);
		$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
		$data['itemlist'] = $this->items_model->get_items_list($siteID);
		$data['image'] = $this->sites_model->get_image_info($siteID);
		$data['client_image'] = $this->client_model->get_image_info($clientID);
		$data['client'] = $this->client_model->get_client_info($clientID);
		$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
		$data['materials'] = $this->items_model->get_materials();
		$data['hazardtype'] = $this->items_model->get_hazards();
		$reportheader = $this->load->view('sites/report_header', $data, true);
		$title = $this->sites_model->get_site_info($siteID);
		$titlename = $title[0]['site'];

		$this->load->library("Pdf");
		// Get the value from report where values can be set to Y or N for each site
		$report_info = $this->sites_model->get_report_info($siteID);
		$recommendations_field = $report_info[0]['recommendations'];
		$methologies_field = $report_info[0]['methologies'];
		$riskfactors_field = $report_info[0]['risk_factors'];
		$priority_rating_system_field = $report_info[0]['priority_rating_system'];
		$asbestos_mng_req_field = $report_info[0]['asbestos_mng_req'];
		$haz_material_mr_field = $report_info[0]['haz_material_mr'];
		$statement_of_limitations_field = $report_info[0]['statement_of_limitations'];
    // create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true); $pdf->SetTitle($titlename);  
     // remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);
    // set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}   
		$first_column_width = 85;
		$second_column_width = 85;
		$column_space = 25; 
		$pdf->SetY(28);
		$current_y_position = $pdf->getY();
		$line_height = 1;
		// Get the template
		$siteview = $this->load->view('sites/site_report', $data, true);
		$siteinfo = $this->load->view('sites/site_information', $data, true);
		$clientview = $this->load->view('sites/report_front', $data, true);
		$itemimages = $this->load->view('sites/report_images', $data, true);
		$itemimages2 = $this->load->view('sites/report_images2', $data, true);
		$reportheader = $this->load->view('sites/report_header', $data, true);
		$introduction = $this->load->view('sites/report_introduction', $data, true);
		$risk_factor = $this->load->view('sites/report_riskfactor', $data, true);
		$risk_factors = $this->load->view('sites/report_risk_factors', $data, true);
		$recommendations = $this->load->view('sites/report_recommendations', $data, true);
		$methologies = $this->load->view('sites/report_methologies', $data, true);
		$priority_rating_system = $this->load->view('sites/report_priority_rating', $data, true);
		$asbestos_mng_req = $this->load->view('sites/report_asbestos_mng_req', $data, true);
		$haz_material_mr = $this->load->view('sites/report_haz_material_mr', $data, true);
		$statement_of_limitations = $this->load->view('sites/report_statement_of_limitations', $data, true);
		$howtouseregister = $this->load->view('sites/report_howtouseregister', $data, true);
		$pdf->setFontSubsetting(true);   
		$pdf->SetFont('dejavusans', '', 5, '', true);   
		$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		$pdf->AddPage('P', 'A4');
		// $pdf->SetY(50);
		// Align Center 
		// $pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, 'C', true); 
		$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
		$pdf->SetY(-50);
		$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 
		$pdf->AddPage('P', 'A4');
		$pdf->writeHTMLCell(0, 0, '', '', $introduction, 0, 1, 0, true, '', true); 
		// If set to 'Y' = show recommendations
		if ($recommendations_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $recommendations, 0, 1, 0, true, '', true); 
		}
		
		// If set to 'Y' = shwo methologies
		if ($methologies_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $methologies, 0, 1, 0, true, '', true); 
		}
		// If set to 'Y' = shwo methologies
		if ($riskfactors_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $risk_factor, 0, 1, 0, true, '', true); 
		}
		// If set to 'Y' = show Priority Rating System
		if ($priority_rating_system_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $priority_rating_system, 0, 1, 0, true, '', true); 
		}
		// If set to 'Y' = show asbestos management requirement
		if ($asbestos_mng_req_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $asbestos_mng_req, 0, 1, 0, true, '', true); 
		}
		// If set to 'Y' = show hazardous material management requirement
		if ($haz_material_mr_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $haz_material_mr, 0, 1, 0, true, '', true); 
		}
		// If set to 'Y' = show statement of limitations
		if ($statement_of_limitations_field == 'Y') {
			$pdf->AddPage('P', 'A4');
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
			$pdf->writeHTMLCell(0, 0, '', '', $statement_of_limitations, 0, 1, 0, true, '', true); 
		}
		// How to use register
		$pdf->AddPage('L', 'A4');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $howtouseregister, 0, 1, 0, true, '', true); 

		$pdf->AddPage('L', 'A4');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+6);
		$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);

		$item_photolist = $this->items_model->get_items_photolist($siteID);

		// Check if there is any item has image
		if (!empty($item_photolist['photo_no'])) {
			// Add page for the item photos
			$pdf->AddPage('P', 'A4');
			// Header (haztech and site logo)
			$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
			// set auto page break
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
		// 2 columns for the images
		// column 1
			$pdf->writeHTMLCell($first_column_width, '', '', $current_y_position, $itemimages, true, 0, 0);
		// continue page break
			if (($current_y_position + $line_height) >= 147) {
				$pdf->AddPage('P', 'A4');
				$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
				$y = $this->pdf->GetY();
				$y = 0;

			}
		$pdf->Cell(0); // THIS LINE SOLVES THE SECOND COLUMN SPACE PROBLEM
		// write second column 
		$pdf->writeHTMLCell($second_column_width, '', $first_column_width+$column_space, $current_y_position, $itemimages2, true, 0, 0, 0, true, '', true);
		if (($current_y_position + $line_height) >= 147) {
			$y = $this->pdf->GetY();
			$y = 0;
			$pdf->AddPage('P', 'A4');
		}

	}

	$pdf->Output("report.pdf",'I');
}

	// Download Report after report wizard
public function generated_report_dl($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['client'] = $this->client_model->get_client_info($clientID);

	$postdata['introduction'] = 'This report presents the findings of an Hazardous Materials Risk Assessment conducted for '.$data['client'][0]['client_name'].' of the site located at '.$data['site'][0]['site'].'. This risk assessment was generated on '.date('d/m/Y');
	$postdata['scope_of_works'] = 'Y';
	$postdata['recommendations'] = 'Y';
	$postdata['methologies'] = 'Y';
	$postdata['risk_factors'] = 'Y';
	$postdata['priority_rating_system'] = 'Y';
	$postdata['asbestos_mng_req'] = 'Y';
	$postdata['statement_of_limitations'] = 'Y';
	$postdata['haz_material_mr'] = 'Y';
	$postdata['siteID'] = $siteID;
	$this->sites_model->update_reportwiz_info($postdata);

	$data['sitereport'] = $this->sites_model->get_report_info($siteID);
	
	$data['item_photolist'] = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$data['itemlist'] = $this->items_model->get_REPORTitems_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);

	$data['itemImage'] = $this->items_model->get_items_photoinformation();
	$data['findingssummary'] = $this->sites_model->get_findings_summary();
	
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['materials'] = $this->items_model->get_materials();
	$data['hazardtype'] = $this->items_model->get_hazards();
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);

	$item_photolistNumbers = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$photoNumberList = array();
	$photoNumberBase = '1';
	foreach ($item_photolistNumbers as $indItems) {
		$photoNumberList[$indItems['photo_no']] = $photoNumberBase;
		$photoNumberBase++;
	}
	$data['photoNumberList'] = $photoNumberList;

	$titlename = $title[0]['site'];

	$this->load->library("Pdf");
	$postdata = $this->input->post();
		// Get the value from report where values can be set to Y or N for each site
	$recommendations_field = $postdata['recommendations'];
	$methologies_field = $postdata['methologies'];
	$priority_rating_system_field = $postdata['priority_rating_system'];
	$statement_of_limitations_field = $postdata['statement_of_limitations'];
	$areas_not_accessed = $postdata['areas_not_accessed'];

	$report_info[0] = $postdata;

	$data['report_info'] = $report_info;
	$this->sites_model->updateReportPlan($siteID, $report_info);
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true); $pdf->SetTitle($titlename);  
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;
		// Get the template
	$siteview = $this->load->view('sites/site_report', $data, true);
	$siteinfo = $this->load->view('sites/site_information', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$fullimages = $this->load->view('sites/report_FULL_images', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$reportTOCheader = $this->load->view('sites/report_toc_header', $data, true);
	$reportIMGheader = $this->load->view('sites/report_IMGheader', $data, true);
	$introduction = $this->load->view('sites/report_introduction', $data, true);
	$risk_factor = $this->load->view('sites/report_riskfactor', $data, true);
	$risk_factors = $this->load->view('sites/report_risk_factors', $data, true);
	$recommendations = $this->load->view('sites/report_recommendations', $data, true);
	$methologies = $this->load->view('sites/report_methologies', $data, true);
	$priority_rating_system = $this->load->view('sites/report_priority_rating', $data, true);
	$asbestos_mng_req = $this->load->view('sites/report_asbestos_mng_req', $data, true);
	$haz_material_mr = $this->load->view('sites/report_haz_material_mr', $data, true);
	$statement_of_limitations = $this->load->view('sites/report_statement_of_limitations', $data, true);
	$howtouseregister = $this->load->view('sites/report_howtouseregister', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);  
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', '8'));  

	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
	$pdf->AddPage('P', 'A4');
	$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
	$pdf->SetY(-50);
	$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 

	$pdf->AddPage('P', 'A4');
	$pdf->Bookmark('Executive Summary', 0, 0, '', 'B', '');
	$pdf->Bookmark('Scope of Works', 1, 0, '', 'B', '');
	$pdf->Bookmark('Summary of Findings', 1, 0, '', 'B', '');
	$pdf->writeHTMLCell(0, 0, '', '', $introduction, 0, 1, 0, true, '', true);
	 
		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Recommendations', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $recommendations, 0, 1, 0, true, '', true); 
	

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Methodology', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $methologies, 0, 1, 0, true, '', true); 

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Control Priority Rating System', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $priority_rating_system, 0, 1, 0, true, '', true); 

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Statement of Limitations', 0, 0, '', 'B', '');
		$pdf->Bookmark('Areas not accessed', 1, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $statement_of_limitations, 0, 1, 0, true, '', true); 
	
		// How to use register
	$pdf->AddPage('L', 'A4');
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
	$pdf->writeHTMLCell(0, 0, '', '', $howtouseregister, 0, 1, 0, true, '', true); 

	$pdf->AddPage('L', 'A4');
	$pdf->Bookmark('Hazardous Materials Register', 0, 0, '', 'B', '');
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+6);
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
	$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);

	$item_photolist = $this->items_model->get_items_photolist($siteID);

	/*if ($item_photolist[0]['photo_no'] != '') {
		echo "if the first doesn't equal blank";
	} else {
		echo "First equals blank";
	}
	echo '<pre>';
	print_r($item_photolist);
	echo '</pre>';
	exit();/**/

		// Check if there is any item has image
	if ($item_photolist){
		if ($item_photolist[0]['photo_no'] != '') {
			$pdf->AddPage('P', 'A4');
			$pdf->Bookmark('Photographs', 0, 0, '', 'B', '');
			$pdf->writeHTMLCell(0, 0, '', '', $reportIMGheader, 0, 1, 0, true, '', true);
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
			$pdf->writeHTMLCell(0, 0, '', '', $fullimages, 0, 1, 0, true, '', true); /**/
		}
	}

	// add a new page for TOC
	$pdf->addTOCPage('P', 'A4');

	// write the TOC title
	//$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

	$pdf->SetFont('dejavusans', '', 11);

	// add a simple Table Of Content at first page
	// (check the example n. 59 for the HTML version)
	//$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
	$pdf->writeHTMLCell(0, 0, '', '', $reportTOCheader, 0, 1, 0, true, '', true);
	$pdf->addTOC(2, 'dejavusans', '.', 'INDEX', 'B', array(128,0,0));
	$pdf->endTOCPage();

	$pdfNamerough = date("Y_m_d").'-'.$data['site'][0]['site'].'-report.pdf';
	$pdfName = str_replace(' ', '_', $pdfNamerough);

	$pdfNamerough2 = time().'-'.$data['site'][0]['site'].'-preMurge.pdf';
	$pdfName2 = str_replace(' ', '_', $pdfNamerough2);

	$pdfNameroughFINAL = time().'-'.$data['site'][0]['site'].'-report.pdf';
	$pdfNameFINAL = str_replace(' ', '_', $pdfNameroughFINAL);

	$filePath = str_replace('/application/controllers', '/uploads/genReport/', dirname(__FILE__));

	$this->sites_model->save_report_location($siteID,$pdfNameFINAL);
	
	$pdf->Output($filePath.$pdfName2,'F');
	//$pdf->Output($pdfName,'D');

	
	// PDF joinning script to then download
	/*$pdfMerge = new PDFMerger;
	//$pdfMerge->setPrintHeader(false);
	$pdfMerge->addPDF($filePath.$pdfName2, 'all'); // with links
	$siteDocs = $this->sites_model->get_sitedoc_removeblank($siteID);
	$countArray = count($siteDocs);
	if ($countArray > 0){
		foreach ($siteDocs as $document){
			$pdfMerge->addPDF('uploads/site_document/'.$document['full_path'], 'all');
		}
	}	
	$pdfMerge->merge('file', $filePath.$pdfNameFINAL); // generate the file
	$pdfMerge->merge('download', $pdfName); /**/
	redirect('/pdfjoin/index/'.$pdfName.'/'.$pdfNameFINAL.'/'.$pdfName2.'/'.$siteID);

}

public function generated_report_client($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['client'] = $this->client_model->get_client_info($clientID);

	$postdata['introduction'] = 'This report presents the findings of an Hazardous Materials Risk Assessment conducted for '.$data['client'][0]['client_name'].' of the site located at '.$data['site'][0]['site'].'. This risk assessment was generated on '.date('d/m/Y');
	$postdata['scope_of_works'] = 'Y';
	$postdata['recommendations'] = 'Y';
	$postdata['methologies'] = 'Y';
	$postdata['risk_factors'] = 'Y';
	$postdata['priority_rating_system'] = 'Y';
	$postdata['asbestos_mng_req'] = 'Y';
	$postdata['statement_of_limitations'] = 'Y';
	$postdata['haz_material_mr'] = 'Y';
	$postdata['siteID'] = $siteID;
	$this->sites_model->update_reportwiz_info($postdata);

	$data['sitereport'] = $this->sites_model->get_report_info($siteID);
	
	$data['item_photolist'] = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$data['itemlist'] = $this->items_model->get_REPORTitems_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);

	$data['itemImage'] = $this->items_model->get_items_photoinformation();
	$data['findingssummary'] = $this->sites_model->get_findings_summary();
	
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['materials'] = $this->items_model->get_materials();
	$data['hazardtype'] = $this->items_model->get_hazards();
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);

	$item_photolistNumbers = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$photoNumberList = array();
	$photoNumberBase = '1';
	foreach ($item_photolistNumbers as $indItems) {
		$photoNumberList[$indItems['photo_no']] = $photoNumberBase;
		$photoNumberBase++;
	}
	$data['photoNumberList'] = $photoNumberList;

	$titlename = $title[0]['site'];

	$this->load->library("Pdf");
	$postdata = $this->newreport_model->get_reporttext_data($siteID);
		// Get the value from report where values can be set to Y or N for each site
	$recommendations_field = $postdata['recommendations'];
	$methologies_field = $postdata['methologies'];
	$priority_rating_system_field = $postdata['priority_rating_system'];
	$statement_of_limitations_field = $postdata['statement_of_limitations'];
	$areas_not_accessed = $postdata['areas_not_accessed'];

	$report_info[0] = $postdata;

	$data['report_info'] = $report_info;
	//$this->sites_model->updateReportPlan($siteID, $report_info);
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true); $pdf->SetTitle($titlename);  
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;
		// Get the template
	$siteview = $this->load->view('sites/site_report', $data, true);
	$siteinfo = $this->load->view('sites/site_information', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$fullimages = $this->load->view('sites/report_FULL_images', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$reportTOCheader = $this->load->view('sites/report_toc_header', $data, true);
	$reportIMGheader = $this->load->view('sites/report_IMGheader', $data, true);
	$introduction = $this->load->view('sites/report_introduction', $data, true);
	$risk_factor = $this->load->view('sites/report_riskfactor', $data, true);
	$risk_factors = $this->load->view('sites/report_risk_factors', $data, true);
	$recommendations = $this->load->view('sites/report_recommendations', $data, true);
	$methologies = $this->load->view('sites/report_methologies', $data, true);
	$priority_rating_system = $this->load->view('sites/report_priority_rating', $data, true);
	$asbestos_mng_req = $this->load->view('sites/report_asbestos_mng_req', $data, true);
	$haz_material_mr = $this->load->view('sites/report_haz_material_mr', $data, true);
	$statement_of_limitations = $this->load->view('sites/report_statement_of_limitations', $data, true);
	$howtouseregister = $this->load->view('sites/report_howtouseregister', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);  
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', '8'));  

	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
	$pdf->AddPage('P', 'A4');
	$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
	$pdf->SetY(-50);
	$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 

	$pdf->AddPage('P', 'A4');
	$pdf->Bookmark('Executive Summary', 0, 0, '', 'B', '');
	$pdf->Bookmark('Scope of Works', 1, 0, '', 'B', '');
	$pdf->Bookmark('Summary of Findings', 1, 0, '', 'B', '');
	$pdf->writeHTMLCell(0, 0, '', '', $introduction, 0, 1, 0, true, '', true);
	 
		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Recommendations', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $recommendations, 0, 1, 0, true, '', true); 
	

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Methodology', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $methologies, 0, 1, 0, true, '', true); 

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Control Priority Rating System', 0, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $priority_rating_system, 0, 1, 0, true, '', true); 

		$pdf->AddPage('P', 'A4');
		$pdf->Bookmark('Statement of Limitations', 0, 0, '', 'B', '');
		$pdf->Bookmark('Areas not accessed', 1, 0, '', 'B', '');
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
		$pdf->writeHTMLCell(0, 0, '', '', $statement_of_limitations, 0, 1, 0, true, '', true); 
	
		// How to use register
	$pdf->AddPage('L', 'A4');
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
	$pdf->writeHTMLCell(0, 0, '', '', $howtouseregister, 0, 1, 0, true, '', true); 

	$pdf->AddPage('L', 'A4');
	$pdf->Bookmark('Hazardous Materials Register', 0, 0, '', 'B', '');
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+6);
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
	$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);

	$item_photolist = $this->items_model->get_items_photolist($siteID);

	/*if ($item_photolist[0]['photo_no'] != '') {
		echo "if the first doesn't equal blank";
	} else {
		echo "First equals blank";
	}
	echo '<pre>';
	print_r($item_photolist);
	echo '</pre>';
	exit();/**/

		// Check if there is any item has image
	if ($item_photolist){
		if ($item_photolist[0]['photo_no'] != '') {
			$pdf->AddPage('P', 'A4');
			$pdf->Bookmark('Photographs', 0, 0, '', 'B', '');
			$pdf->writeHTMLCell(0, 0, '', '', $reportIMGheader, 0, 1, 0, true, '', true);
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
			$pdf->writeHTMLCell(0, 0, '', '', $fullimages, 0, 1, 0, true, '', true); /**/
		}
	}

	// add a new page for TOC
	$pdf->addTOCPage('P', 'A4');

	// write the TOC title
	//$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

	$pdf->SetFont('dejavusans', '', 11);

	// add a simple Table Of Content at first page
	// (check the example n. 59 for the HTML version)
	//$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
	$pdf->writeHTMLCell(0, 0, '', '', $reportTOCheader, 0, 1, 0, true, '', true);
	$pdf->addTOC(2, 'dejavusans', '.', 'INDEX', 'B', array(128,0,0));
	$pdf->endTOCPage();

	$pdfNamerough = date("Y_m_d").'-'.$data['site'][0]['site'].'-report.pdf';
	$pdfName = str_replace(' ', '_', $pdfNamerough);

	$pdfNamerough2 = time().'-'.$data['site'][0]['site'].'-preMurge.pdf';
	$pdfName2 = str_replace(' ', '_', $pdfNamerough2);

	$pdfNameroughFINAL = time().'-'.$data['site'][0]['site'].'-report.pdf';
	$pdfNameFINAL = str_replace(' ', '_', $pdfNameroughFINAL);

	$filePath = str_replace('/application/controllers', '/uploads/genReport/', dirname(__FILE__));

	$this->sites_model->save_report_location($siteID,$pdfNameFINAL);
	
	$pdf->Output($filePath.$pdfName2,'F');
	//$pdf->Output($pdfName,'D');

	
	// PDF joinning script to then download
	/*$pdfMerge = new PDFMerger;
	//$pdfMerge->setPrintHeader(false);
	$pdfMerge->addPDF($filePath.$pdfName2, 'all'); // with links
	$siteDocs = $this->sites_model->get_sitedoc_removeblank($siteID);
	$countArray = count($siteDocs);
	if ($countArray > 0){
		foreach ($siteDocs as $document){
			$pdfMerge->addPDF('uploads/site_document/'.$document['full_path'], 'all');
		}
	}	
	$pdfMerge->merge('file', $filePath.$pdfNameFINAL); // generate the file
	$pdfMerge->merge('download', $pdfName); /**/
	redirect('/pdfjoin/index/'.$pdfName.'/'.$pdfNameFINAL.'/'.$pdfName2.'/'.$siteID);

}

	// View Report
public function view_siteplan($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
	$data['itemlist'] = $this->items_model->get_items_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['client'] = $this->client_model->get_client_info($clientID);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);
	$titlename = $title[0]['site'];
	$this->load->library("Pdf");
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;
	$siteplan = $this->load->view('sites/site_plan', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);   
		// set header data
	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		// landscape
	$pdf->AddPage('L', 'A4');
		// header
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		// content
	$pdf->writeHTMLCell(0, 0, '', '', $siteplan, 0, 1, 0, true, '', true);

	$pdf->Output("siteplan.pdf",'I');

}

	// Download Report
public function download_siteplan($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
	$data['itemlist'] = $this->items_model->get_items_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['client'] = $this->client_model->get_client_info($clientID);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);
	$titlename = $title[0]['site'];
	$this->load->library("Pdf");
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);   $pdf->SetTitle($titlename);  
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 

	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;
	$siteplan = $this->load->view('sites/site_plan', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);   
		// set header data
	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		// landscape
	$pdf->AddPage('L', 'A4');
		// header
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		// content
	$pdf->writeHTMLCell(0, 0, '', '', $siteplan, 0, 1, 0, true, '', true);

	$pdf->Output("siteplan.pdf",'D');
}	
	// View Report
public function view_site($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
	$data['itemlist'] = $this->items_model->get_items_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);
	$data['client'] = $this->client_model->get_client_info($clientID);
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['materials'] = $this->items_model->get_materials();
	$data['hazardtype'] = $this->items_model->get_hazards();
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);
	$titlename = $title[0]['site'];
	$this->load->library("Pdf");
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);   $pdf->SetTitle($titlename);  
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;

	$data['item_photolist'] = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$data['itemlist'] = $this->items_model->get_REPORTitems_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);

	$data['itemImage'] = $this->items_model->get_items_photoinformation();
	$data['findingssummary'] = $this->sites_model->get_findings_summary();
	
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['materials'] = $this->items_model->get_materials();
	$data['hazardtype'] = $this->items_model->get_hazards();
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);

	$item_photolistNumbers = $this->items_model->get_items_photolistTESTnotblank($siteID);
	$photoNumberList = array();
	$photoNumberBase = '1';
	foreach ($item_photolistNumbers as $indItems) {
		$photoNumberList[$indItems['photo_no']] = $photoNumberBase;
		$photoNumberBase++;
	}
	$data['photoNumberList'] = $photoNumberList;

	$siteview = $this->load->view('sites/site_report', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$siteinfo = $this->load->view('sites/site_information', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);   
		// set header data
	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		// declare paper orientation and size. P for Portrait and L for landscape
	$pdf->AddPage('P', 'A4');
		// $pdf->SetY(50);
		// front page of report
		// Align Center
		// $pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, 'C', true);
	$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
	$pdf->SetY(-50);
	$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 
		// landscape
	$pdf->AddPage('L', 'A4');
		// header
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		// content
	$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);
	
	$fullimages = $this->load->view('sites/report_FULL_images', $data, true);
$reportIMGheader = $this->load->view('sites/report_IMGheader', $data, true);

$item_photolist = $this->items_model->get_items_photolist($siteID);


		// Check if there is any item has image
	if ($item_photolist){
		if ($item_photolist[0]['photo_no'] != '') {
			$pdf->AddPage('P', 'A4');
			$pdf->Bookmark('Photographs', 0, 0, '', 'B', '');
			$pdf->writeHTMLCell(0, 0, '', '', $reportIMGheader, 0, 1, 0, true, '', true);
			$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
			$pdf->writeHTMLCell(0, 0, '', '', $fullimages, 0, 1, 0, true, '', true); /**/
		}
	}


	$pdf->Output("report.pdf",'I');

}
	// Download Report
public function download_site($siteID, $clientID)
{
	$data['sitelist'] = $this->sites_model->get_sites_list();
	$data['site'] = $this->sites_model->get_site_info($siteID);
	$data['item_photolist'] = $this->items_model->get_items_photolist($siteID);
	$data['itemlist'] = $this->items_model->get_items_list($siteID);
	$data['image'] = $this->sites_model->get_image_info($siteID);
	$data['client_image'] = $this->client_model->get_image_info($clientID);
	$data['client'] = $this->client_model->get_client_info($clientID);
	$data['site_plan'] = $this->sites_model->get_site_plan($siteID);
	$data['materials'] = $this->items_model->get_materials();
	$data['hazardtype'] = $this->items_model->get_hazards();
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$title = $this->sites_model->get_site_info($siteID);
	$titlename = $title[0]['site'];
	$this->load->library("Pdf");
    // create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);   $pdf->SetTitle($titlename);  
     // remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
    // set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
		// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}   
	$first_column_width = 85;
	$second_column_width = 85;
	$column_space = 25; 
	$pdf->SetY(28);
	$current_y_position = $pdf->getY();
	$line_height = 4.2;
	$siteview = $this->load->view('sites/site_report', $data, true);
	$clientview = $this->load->view('sites/report_front', $data, true);
	$itemimages = $this->load->view('sites/report_images', $data, true);
	$itemimages2 = $this->load->view('sites/report_images2', $data, true);
	$reportheader = $this->load->view('sites/report_header', $data, true);
	$siteinfo = $this->load->view('sites/site_information', $data, true);
	$pdf->setFontSubsetting(true);   
	$pdf->SetFont('dejavusans', '', 5, '', true);   
		// set header data
	$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
		// declare paper orientation and size. P for Portrait and L for landscape
	$pdf->AddPage('P', 'A4');
		// $pdf->SetY(50);
		// front page of report
		// Align Center
		// $pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, 'C', true);
	$pdf->writeHTMLCell(0, 0, '', '', $clientview, 0, 1, 0, true, '', true); 
	$pdf->SetY(-50);
	$pdf->writeHTMLCell(0, 0, '', '', $siteinfo, 0, 1, 0, true, '', true); 
		// landscape
	$pdf->AddPage('L', 'A4');
		// header
	$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
		// content
	$pdf->writeHTMLCell(0, 0, '', '', $siteview, 0, 1, 0, true, '', true);
	
	$item_photolist = $this->items_model->get_items_photolist($siteID);

		// Check if there is any item has image
	if (!empty($item_photolist['photo_no'])) {
			// Add page for the item photos
		$pdf->AddPage('P', 'A4');
			// Header (haztech and site logo)
		$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
			// set auto page break
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
		// 2 columns for the images
		// column 1
		$pdf->writeHTMLCell($first_column_width, '', '', $current_y_position, $itemimages, true, 0, 0);
		// continue page break
		if (($current_y_position + $line_height) >= 147) {
			$pdf->AddPage('P', 'A4');
			$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
			$y = $this->pdf->GetY();
			$y = 0;

		}
		$pdf->Cell(0); // THIS LINE SOLVES THE SECOND COLUMN SPACE PROBLEM
		// write second column 
		$pdf->writeHTMLCell($second_column_width, '', $first_column_width+$column_space, $current_y_position, $itemimages2, true, 0, 0, 0, true, '', true);
		if (($current_y_position + $line_height) >= 147) {
			$y = $this->pdf->GetY();
			$y = 0;
			$pdf->AddPage('P', 'A4');
		}

	}

	$pdf->Output("report.pdf",'D');

}
}
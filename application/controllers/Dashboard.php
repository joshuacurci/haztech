<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		$this->load->model('sites_model');
		$this->load->model('items_model');
		$this->load->model('client_model');

		if($this->config->item('maintenance_mode') == TRUE) {
			$this->load->view('under_construction');
			$content = $this->load->view('under_construction', '', TRUE); 
			echo $content;
			die();
		}
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

		if ( ! $this->session->userdata('loginuser')) { 
			redirect('login/index');
		}
	}

	public function index()
	{
		$header['menuitem'] = '1';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Dashboard';

		$userID = $_SESSION['userID'];
		$clientID = $_SESSION['clientID'];
		if ($_SESSION['usertype'] == 'C') {

			$data['low'] = $this->items_model->getControlPriorityCLIENT('P4', $clientID);
			$data['medium'] = $this->items_model->getControlPriorityCLIENT('P3', $clientID);
			$data['high'] = $this->items_model->getControlPriorityCLIENT('P2', $clientID);
			$data['extrame'] = $this->items_model->getControlPriorityCLIENT('P1', $clientID);
			$data['rateTotal'] = $this->items_model->getControlPriorityAllCLIENT($clientID);
			$data['stacked'] = $this->items_model->getControlPriorityLocationCLIENT($clientID);
		} else {
			$data['sitelist'] = $this->sites_model->get_sites_list();
			$data['siteclientlist'] = $this->sites_model->get_client_site_list($clientID);
			$data['allitemlist'] = $this->items_model->get_all_items();
			$data['allclientitemlist'] = $this->items_model->get_client_items_list($clientID);
			$data['client'] = $this->client_model->get_client_info($clientID);

			// How many of a particular rating
			$data['low'] = $this->items_model->getControlPriority('P4');
			$data['medium'] = $this->items_model->getControlPriority('P3');
			$data['high'] = $this->items_model->getControlPriority('P2');
			$data['extrame'] = $this->items_model->getControlPriority('P1');
			$data['rateTotal'] = $this->items_model->getControlPriorityAll();
			$data['stacked'] = $this->items_model->getControlPriorityLocation();
		}

		$data['total'] = $this->items_model->getCalculatedAmount($data['stacked']);
		$data['locations_count'] = $this->items_model->getTotalLocations();

		$this->load->view('main_header', $header);
		$this->load->view('dashboard/index', $data);
		$this->load->view('main_footer');
	}

	public function view($userID)
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users_list();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['user'] = $this->admin_model->get_user_info($userID);
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('admin/view', $data);
		$this->load->view('main_footer');
	}

	public function search()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$postdata = $this->input->post();
		$data['user'] = $this->admin_model->search_users($postdata);
		$data['searchresults'] = $postdata;
		$data['searchresults']['name'] = "";

		$this->load->view('main_header', $header);
		$this->load->view('admin/index', $data);
		$this->load->view('main_footer');
	}

	public function edit($userID)
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';


		$data['user'] = $this->admin_model->get_user_info($userID);
		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('admin/edit', $data);
		$this->load->view('main_footer');
	}

	public function add()
	{
		$header['menuitem'] = '2';
		$header['usergroup'] = '';
		$header['pagetitle'] = 'Admin';

		$data['userlist'] = $this->admin_model->get_users();
		$data['usertypes'] = $this->admin_model->get_user_types();
		$data['blog_posts'] = $this->news_model->get_blog_posts();

		$this->load->view('main_header', $header);
		$this->load->view('admin/add', $data);
		$this->load->view('main_footer');
	}

	public function save()
	{
		$postdata = $this->input->post();
		$userID = $this->input->post('userID');

		$this->admin_model->update_user_info($postdata);

		redirect('/admin/index/'.$userID);
	}

	public function newuser()
	{
		$postdata = $this->input->post();
		if ($_POST["password"] == $_POST["confirm_password"]) {
			$this->admin_model->new_user_info($postdata);
			redirect('/admin/index/');
		} else {
			$header['menuitem'] = '2';
			$header['usergroup'] = '';
			$header['pagetitle'] = 'Admin';

			$data['userlist'] = $this->admin_model->get_users();
			$data['usertypes'] = $this->admin_model->get_user_types();
			$data['blog_posts'] = $this->news_model->get_blog_posts();

			$this->load->view('main_header', $header);
			$this->load->view('admin/pw_mismatch');
			$this->load->view('admin/add', $data);
			$this->load->view('main_footer');
		}
		
	}

	public function delete($userID)
	{
		$this->admin_model->delete_user_info($userID);

		redirect('/admin/index/');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  if ( ! $this->session->userdata('loginuser')) { 
        redirect('login/index');
      }
    }

	public function index()
	{
        $header['menuitem'] = '1';
        $header['usergroup'] = '';
        $header['pagetitle'] = 'Dashboard';

        $userID = $_SESSION['userID'];

        // $data['projects'] = $this->projects_model->get_user_projects($userID);
		$this->load->view('main_header', $header);
		$this->load->view('home/index');
		$this->load->view('main_footer');
	}


}

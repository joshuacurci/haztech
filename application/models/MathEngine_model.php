<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MathEngine_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     /*
          Base formulae is
          Extent (a) x Removal Cost per (b) + Call out fee (c) = Total (T)
               [a*b+c=T]


     */
     function calculate_repair_cost($materialID, $amount) {
         if ($materialID != 0){
          $sql = "SELECT * FROM tbl_basevalues WHERE materialID = '".$materialID."'";
          $query = $this->db->query($sql);
          $values = $query->result_array();

          $total = $amount*$values[0]['costper']+$values[0]['calloutfee'];
          return $total;
         } else {
            return 0;
         }
     }

     
}?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download_model extends CI_Model
{
     public function __construct()
     {
          // Call the Model constructor
          $this->load->database();
     }

     //get the username & password from tbl_usrs
     function get_file($location, $fileID)
     {
        if($location == 'A') {
            $sql = "SELECT * FROM tbl_accountdocs WHERE docID = '".$fileID."'";
        } elseif($location == 'L') {
            $sql = "SELECT * FROM tbl_locationdocs WHERE docID = '".$fileID."'";
        } else {
            $sql = "SELECT * FROM tbl_upload WHERE fileID = '".$fileID."'";
        }
          
          $query = $this->db->query($sql);
          $filename = $query->result_array();

          if($location != 'U') {
            return $filename[0]['doc_filename'];
          } else {
            return $filename[0]['filename'];
          }
     }

     function get_lastReport_file($siteID)
     {
        $sql = "SELECT * FROM tbl_report_saved WHERE siteID = '".$siteID."' ORDER BY dateGenerated DESC";
          
        $query = $this->db->query($sql);
        $filename = $query->result_array();

        return $filename[0]['reportName'];
     }
	 
}?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_blog_posts() {
          $sql = "SELECT * FROM tbl_blog ORDER BY date_posted LIMIT 3";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     
}?>
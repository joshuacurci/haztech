<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_users() {
          $sql = "SELECT * FROM tbl_users ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_types() {
          $sql = "SELECT * FROM tbl_usertypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_users($searchData) {
          $sql = "SELECT * FROM tbl_users WHERE name LIKE '%".$searchData['name']."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_users_list() {
          $sql = "SELECT * FROM tbl_users ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_users($filterletter) {
          $sql = "SELECT * FROM tbl_users WHERE name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_archived_users($filterletter) {
          $sql = "SELECT * FROM tbl_users WHERE user_archive = 'Y' AND name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_info($userID) {
          $sql = "SELECT * FROM tbl_users WHERE userID = '".$userID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_user_info ($userdata) {
          $sql = "UPDATE tbl_users SET 
          name = '".$userdata['name']."',
          type_letter = '".$userdata['type_letter']."',
          email = '".$userdata['email']."',
          name = '".$userdata['name']."'
          WHERE userID = ".$userdata['userID']."";
          $query = $this->db->query($sql);
     }

     function new_user_info ($userdata) {
          $sql = "INSERT INTO tbl_users (name,type_letter,username,password,email) 
          VALUES 
          ('".$userdata['name']."','".$userdata['type_letter']."','".$userdata['username']."','".$userdata['password']."','".$userdata['email']."')";
          $query = $this->db->query($sql);
     }


     function delete_user_info($userID) {
          $sql = "DELETE FROM tbl_users WHERE userID = '".$userID."'";
          $query = $this->db->query($sql);
     }
}?>
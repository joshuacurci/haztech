<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_blogs() {
          $sql = "SELECT * FROM tbl_blog ORDER BY date_posted DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_blog_info($blogID) {
          $sql = "SELECT * FROM tbl_blog WHERE blogID = '".$blogID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_access_info($blogID) {
          $sql = "SELECT type_letter FROM tbl_blog_access WHERE blogID = '".$blogID."'";
          $query = $this->db->query($sql);

          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_blog_posts() {
          $sql = "SELECT * FROM tbl_blog ORDER BY date_posted LIMIT 3";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_blog_info ($userdata) {
           $data_upload_files = $this->upload->data();          
          $image = $data_upload_files['full_path']; 
          $sql = "UPDATE tbl_blog SET 
          title = '".$userdata['title']."',
          author = '".$userdata['author']."',
          content = '".$userdata['content']."'
          WHERE blogID = ".$userdata['blogID']."";
          $query = $this->db->query($sql);
     }

     function get_image_info($blogID) {
          $sql = "SELECT full_path FROM tbl_blog_images WHERE blogID = '".$blogID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function new_blog_info ($userdata) {
          $sql = "INSERT INTO tbl_blog (title,content,image,date_posted) 
          VALUES 
          ('".$userdata['title']."','".$userdata['content']."','".$userdata['image']."','".$userdata['date_posted']."')";
          $query = $this->db->query($sql);
     }

     function new_blog_access ($userdata,$blogID) {
          foreach ($userdata['access'] as $newaccess) {
               $sql = "INSERT INTO tbl_blog_access (blogID,type_letter) 
               VALUES
               ('".$blog[0]['blogID']."','".$newaccess."')";
               $query = $this->db->query($sql);
          }
     }

     function update_blog_access ($userdata,$blogID) {
          $sql = "DELETE FROM tbl_blog_access WHERE blogID = '".$blogID."'";
          $query = $this->db->query($sql);

          foreach ($userdata['type_letter'] as $newaccess) {
               $sql = "INSERT INTO tbl_blog_access (blogID,type_letter) 
               VALUES 
               ('".$blogID."','".$newaccess."')";
               $query = $this->db->query($sql);
          }
     }

     function upload_image($blogID, $fullPath) {
          $sql = "INSERT INTO tbl_blog_images (blogID, full_path) VALUES ('".$blogID."', '".$fullPath."')";
          $query = $this->db->query($sql);
     }

     function update_image($blogID, $fullPath) {
          $sql = "UPDATE tbl_blog_images SET blogID = ".$blogID.", full_path = '".$fullPath."'
          WHERE blogID = ".$blogID."";
          $query = $this->db->query($sql);
     }

     function delete_blog_info($blogID) {
          $sql = "UPDATE tbl_blog SET hide = 'Y' WHERE blogID = '".$blogID."'";
          $query = $this->db->query($sql);
     }
     
}?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_clients() {
          $sql = "SELECT * FROM tbl_client WHERE hide= 'N' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_types() {
          $sql = "SELECT * FROM tbl_usertypes";
          $query = $this->db->query($sql);
          return $query->result_array();     
     }

     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states";
          $query = $this->db->query($sql);
          return $query->result_array();     
     }

     function search_clients($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND hide= 'N' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_clients($filterletter) {
          $sql = "SELECT * FROM tbl_client WHERE (hide = 'N' OR hide IS NULL) AND client_name LIKE '".$filterletter."%' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     function search_client_name($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE clientID  LIKE '%".$searchData['client_name']."%' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_client_list() {
          $sql = "SELECT * FROM tbl_client WHERE hide= 'N' ORDER  BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_users($filterletter) {
          $sql = "SELECT * FROM tbl_users WHERE name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_archived_users($filterletter) {
          $sql = "SELECT * FROM tbl_users WHERE user_archive = 'Y' AND name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_info($clientID) {
          $sql = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."' AND hide = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_client_info ($clientdata) {
          $sql = "UPDATE tbl_client SET 
          client_name = '".$clientdata['client_name']."',
          site_address = '".$clientdata['site_address']."',
          contact_name = '".$clientdata['contact_name']."',
          contact_number = '".$clientdata['contact_number']."'
          WHERE clientID = ".$clientdata['clientID']."";
          $query = $this->db->query($sql);
     }

     function new_client_info ($clientdata) {
          $sql = "INSERT INTO tbl_client (client_name,site_address,contact_name,contact_number,hide) 
          VALUES 
          ('".$clientdata['client_name']."','".$clientdata['site_address']."','".$clientdata['contact_name']."','".$clientdata['contact_number']."','N')";
          $query = $this->db->query($sql);
          return $this->db->insert_id();
     }

     function upload_image($clientID, $fullPath) {
          $sql = "INSERT INTO tbl_client_images (clientID, full_path) VALUES ('".$clientID."', '".$fullPath."')";
          $query = $this->db->query($sql);
     }

     function update_image($clientID, $fullPath) {
          $sql = "UPDATE tbl_client_images SET clientID = ".$clientID.", full_path = '".$fullPath."'
          WHERE clientID = ".$clientID."";
          $query = $this->db->query($sql);
     }

     function get_image_info($clientID) {
          $sql = "SELECT full_path FROM tbl_client_images WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_client_info($clientID) {
          $sql = "UPDATE tbl_client SET hide = 'Y' WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
     }
}?>
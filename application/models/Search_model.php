<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_searched_items($postdata){
        $sql = "SELECT * FROM tbl_items WHERE hide = 'N'";

        // ---------- Building SQL Query ----------

        // ----- Site ID -----
        if (isset($postdata['siteID'])) {
            $i = 0;
            $len = count($postdata['siteID']);

            if ($len == '1') {
                $sql .= " AND siteID = '".$postdata['siteID'][0]."'";
            } else {
                $sql .= " AND siteID IN (";
                foreach ($postdata['siteID'] as $siteID) {
                    if ($i == $len - 1) {
                        $sql .= "'".$siteID."'";
                    } else{
                        $sql .= "'".$siteID."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Material -----
        if (isset($postdata['materialID'])) {
            $i = 0;
            $len = count($postdata['materialID']);

            if ($len == '1') {
                $sql .= " AND materialID = '".$postdata['materialID'][0]."'";
            } else {
                $sql .= " AND materialID IN (";
                foreach ($postdata['materialID'] as $materialID) {
                    if ($i == $len - 1) {
                        $sql .= "'".$materialID."'";
                    } else{
                        $sql .= "'".$materialID."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Sample Status -----
        if (isset($postdata['sample_status'])) {
            $i = 0;
            $len = count($postdata['sample_status']);

            if ($len == '1') {
                $sql .= " AND sample_status = '".$postdata['sample_status'][0]."'";
            } else {
                $sql .= " AND sample_status IN (";
                foreach ($postdata['sample_status'] as $sample_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$sample_status."'";
                    } else{
                        $sql .= "'".$sample_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazard Type -----
        if (isset($postdata['hazard_type'])) {
            $i = 0;
            $len = count($postdata['hazard_type']);

            if ($len == '1') {
                $sql .= " AND hazard_type = '".$postdata['hazard_type'][0]."'";
            } else {
                $sql .= " AND hazard_type IN (";
                foreach ($postdata['hazard_type'] as $hazard_type) {
                    if ($i == $len - 1) {
                        $sql .= "'".$hazard_type."'";
                    } else{
                        $sql .= "'".$hazard_type."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazardous Material Status -----
        if (isset($postdata['item_status'])) {
            $i = 0;
            $len = count($postdata['item_status']);

            if ($len == '1') {
                $sql .= " AND item_status = '".$postdata['item_status'][0]."'";
            } else {
                $sql .= " AND item_status IN (";
                foreach ($postdata['item_status'] as $item_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_status."'";
                    } else{
                        $sql .= "'".$item_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Item Condition -----
        if (isset($postdata['item_condition'])) {
            $i = 0;
            $len = count($postdata['item_condition']);

            if ($len == '1') {
                $sql .= " AND siteID = '".$postdata['item_condition'][0]."'";
            } else {
                $sql .= " AND siteID IN (";
                foreach ($postdata['item_condition'] as $item_condition) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_condition."'";
                    } else{
                        $sql .= "'".$item_condition."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Friability -----
        if (isset($postdata['friability'])) {
            $i = 0;
            $len = count($postdata['friability']);

            if ($len == '1') {
                $sql .= " AND friability = '".$postdata['friability'][0]."'";
            } else {
                $sql .= " AND friability IN (";
                foreach ($postdata['friability'] as $friability) {
                    if ($i == $len - 1) {
                        $sql .= "'".$friability."'";
                    } else{
                        $sql .= "'".$friability."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Accessibility/Dist. Potential -----
        if (isset($postdata['disturb_potential'])) {
            $i = 0;
            $len = count($postdata['disturb_potential']);

            if ($len == '1') {
                $sql .= " AND disturb_potential = '".$postdata['disturb_potential'][0]."'";
            } else {
                $sql .= " AND disturb_potential IN (";
                foreach ($postdata['disturb_potential'] as $disturb_potential) {
                    if ($i == $len - 1) {
                        $sql .= "'".$disturb_potential."'";
                    } else{
                        $sql .= "'".$disturb_potential."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Risk Rating -----
        if (isset($postdata['risk_rating'])) {
            $i = 0;
            $len = count($postdata['risk_rating']);

            if ($len == '1') {
                $sql .= " AND risk_rating = '".$postdata['risk_rating'][0]."'";
            } else {
                $sql .= " AND risk_rating IN (";
                foreach ($postdata['risk_rating'] as $risk_rating) {
                    if ($i == $len - 1) {
                        $sql .= "'".$risk_rating."'";
                    } else{
                        $sql .= "'".$risk_rating."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        $query = $this->db->query($sql);
        
        return $query->result_array();

     }

     function get_searched_itemsClient($postdata, $clientID){
        $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND clientID = '".$clientID."'";

        // ---------- Building SQL Query ----------

        // ----- Site ID -----
        if (isset($postdata['siteID'])) {
            $i = 0;
            $len = count($postdata['siteID']);

            if ($len == '1') {
                $sql .= " AND siteID = '".$postdata['siteID'][0]."'";
            } else {
                $sql .= " AND siteID IN (";
                foreach ($postdata['siteID'] as $siteID) {
                    if ($i == $len - 1) {
                        $sql .= "'".$siteID."'";
                    } else{
                        $sql .= "'".$siteID."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Material -----
        if (isset($postdata['materialID'])) {
            $i = 0;
            $len = count($postdata['materialID']);

            if ($len == '1') {
                $sql .= " AND materialID = '".$postdata['materialID'][0]."'";
            } else {
                $sql .= " AND materialID IN (";
                foreach ($postdata['materialID'] as $materialID) {
                    if ($i == $len - 1) {
                        $sql .= "'".$materialID."'";
                    } else{
                        $sql .= "'".$materialID."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Sample Status -----
        if (isset($postdata['sample_status'])) {
            $i = 0;
            $len = count($postdata['sample_status']);

            if ($len == '1') {
                $sql .= " AND sample_status = '".$postdata['sample_status'][0]."'";
            } else {
                $sql .= " AND sample_status IN (";
                foreach ($postdata['sample_status'] as $sample_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$sample_status."'";
                    } else{
                        $sql .= "'".$sample_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazard Type -----
        if (isset($postdata['hazard_type'])) {
            $i = 0;
            $len = count($postdata['hazard_type']);

            if ($len == '1') {
                $sql .= " AND hazard_type = '".$postdata['hazard_type'][0]."'";
            } else {
                $sql .= " AND hazard_type IN (";
                foreach ($postdata['hazard_type'] as $hazard_type) {
                    if ($i == $len - 1) {
                        $sql .= "'".$hazard_type."'";
                    } else{
                        $sql .= "'".$hazard_type."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazardous Material Status -----
        if (isset($postdata['item_status'])) {
            $i = 0;
            $len = count($postdata['item_status']);

            if ($len == '1') {
                $sql .= " AND item_status = '".$postdata['item_status'][0]."'";
            } else {
                $sql .= " AND item_status IN (";
                foreach ($postdata['item_status'] as $item_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_status."'";
                    } else{
                        $sql .= "'".$item_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Item Condition -----
        if (isset($postdata['item_condition'])) {
            $i = 0;
            $len = count($postdata['item_condition']);

            if ($len == '1') {
                $sql .= " AND siteID = '".$postdata['item_condition'][0]."'";
            } else {
                $sql .= " AND siteID IN (";
                foreach ($postdata['item_condition'] as $item_condition) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_condition."'";
                    } else{
                        $sql .= "'".$item_condition."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Friability -----
        if (isset($postdata['friability'])) {
            $i = 0;
            $len = count($postdata['friability']);

            if ($len == '1') {
                $sql .= " AND friability = '".$postdata['friability'][0]."'";
            } else {
                $sql .= " AND friability IN (";
                foreach ($postdata['friability'] as $friability) {
                    if ($i == $len - 1) {
                        $sql .= "'".$friability."'";
                    } else{
                        $sql .= "'".$friability."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Accessibility/Dist. Potential -----
        if (isset($postdata['disturb_potential'])) {
            $i = 0;
            $len = count($postdata['disturb_potential']);

            if ($len == '1') {
                $sql .= " AND disturb_potential = '".$postdata['disturb_potential'][0]."'";
            } else {
                $sql .= " AND disturb_potential IN (";
                foreach ($postdata['disturb_potential'] as $disturb_potential) {
                    if ($i == $len - 1) {
                        $sql .= "'".$disturb_potential."'";
                    } else{
                        $sql .= "'".$disturb_potential."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Risk Rating -----
        if (isset($postdata['risk_rating'])) {
            $i = 0;
            $len = count($postdata['risk_rating']);

            if ($len == '1') {
                $sql .= " AND risk_rating = '".$postdata['risk_rating'][0]."'";
            } else {
                $sql .= " AND risk_rating IN (";
                foreach ($postdata['risk_rating'] as $risk_rating) {
                    if ($i == $len - 1) {
                        $sql .= "'".$risk_rating."'";
                    } else{
                        $sql .= "'".$risk_rating."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        $query = $this->db->query($sql);
        
        return $query->result_array();

     }

     function get_items_list($postdata, $siteID){
        $sql = "SELECT * FROM tbl_items WHERE hide = 'N'";

        // ---------- Building SQL Query ----------

        // ----- Site ID -----
       
        $sql .= " AND siteID = '".$siteID."'";
            

        // ----- Material -----
        if ($postdata['materialID'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['materialID']);

            if ($len == '1') {
                $sql .= " AND materialID = '".$postdata['materialID'][0]."'";
            } else {
                $sql .= " AND materialID IN (";
                foreach ($postdata['materialID'] as $materialID) {
                    if ($i == $len - 1) {
                        $sql .= "'".$materialID."'";
                    } else{
                        $sql .= "'".$materialID."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Sample Status -----
        if ($postdata['sample_status'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['sample_status']);

            if ($len == '1') {
                $sql .= " AND sample_status = '".$postdata['sample_status'][0]."'";
            } else {
                $sql .= " AND sample_status IN (";
                foreach ($postdata['sample_status'] as $sample_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$sample_status."'";
                    } else{
                        $sql .= "'".$sample_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazard Type -----
        if ($postdata['hazard_type'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['hazard_type']);

            if ($len == '1') {
                $sql .= " AND hazard_type = '".$postdata['hazard_type'][0]."'";
            } else {
                $sql .= " AND hazard_type IN (";
                foreach ($postdata['hazard_type'] as $hazard_type) {
                    if ($i == $len - 1) {
                        $sql .= "'".$hazard_type."'";
                    } else{
                        $sql .= "'".$hazard_type."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Hazardous Material Status -----
        if ($postdata['item_status'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['item_status']);

            if ($len == '1') {
                $sql .= " AND item_status = '".$postdata['item_status'][0]."'";
            } else {
                $sql .= " AND item_status IN (";
                foreach ($postdata['item_status'] as $item_status) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_status."'";
                    } else{
                        $sql .= "'".$item_status."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Item Condition -----
        if ($postdata['item_condition'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['item_condition']);

            if ($len == '1') {
                $sql .= " AND siteID = '".$postdata['item_condition'][0]."'";
            } else {
                $sql .= " AND siteID IN (";
                foreach ($postdata['item_condition'] as $item_condition) {
                    if ($i == $len - 1) {
                        $sql .= "'".$item_condition."'";
                    } else{
                        $sql .= "'".$item_condition."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Friability -----
        if ($postdata['friability'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['friability']);

            if ($len == '1') {
                $sql .= " AND friability = '".$postdata['friability'][0]."'";
            } else {
                $sql .= " AND friability IN (";
                foreach ($postdata['friability'] as $friability) {
                    if ($i == $len - 1) {
                        $sql .= "'".$friability."'";
                    } else{
                        $sql .= "'".$friability."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Accessibility/Dist. Potential -----
        if ($postdata['disturb_potential'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['disturb_potential']);

            if ($len == '1') {
                $sql .= " AND disturb_potential = '".$postdata['disturb_potential'][0]."'";
            } else {
                $sql .= " AND disturb_potential IN (";
                foreach ($postdata['disturb_potential'] as $disturb_potential) {
                    if ($i == $len - 1) {
                        $sql .= "'".$disturb_potential."'";
                    } else{
                        $sql .= "'".$disturb_potential."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        // ----- Risk Rating -----
        if ($postdata['risk_rating'][0] !== 'all') {
            $i = 0;
            $len = count($postdata['risk_rating']);

            if ($len == '1') {
                $sql .= " AND risk_rating = '".$postdata['risk_rating'][0]."'";
            } else {
                $sql .= " AND risk_rating IN (";
                foreach ($postdata['risk_rating'] as $risk_rating) {
                    if ($i == $len - 1) {
                        $sql .= "'".$risk_rating."'";
                    } else{
                        $sql .= "'".$risk_rating."', ";
                    }
                    $i++;
                }
                $sql .= ")";
            }
        }

        $query = $this->db->query($sql);
        
        return $query->result_array();

     }
}?>
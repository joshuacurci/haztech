<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newreport_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_report_saved($siteID) {
          $sql = "SELECT * FROM tbl_report_saved WHERE siteID = '".$siteID."' ORDER BY dateGenerated DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_report_template ($siteID) {
            $sql = "SELECT * FROM tbl_report_common WHERE id = '".$siteID."'";
            $query = $this->db->query($sql);
            $getFinal = $query->result_array();
            $totalCount = count($getFinal);

            if ($totalCount == 0){
                  $db = get_instance()->db->conn_id; 
                  $sql2 = "SELECT * FROM tbl_report_common WHERE id = 0";
                  $query2 = $this->db->query($sql2);
                  $commonValue = $query2->result_array();
                  
                  $sql2 = "INSERT INTO tbl_report_common (
                        id,
                        scope_of_works,
                        recommendations,
                        methologies,
                        priority_rating_system,
                        statement_of_limitations,
                        areas_not_accessed
                  ) VALUES (
                        '".mysqli_real_escape_string($db, $siteID)."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['scope_of_works'])."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['recommendations'])."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['methologies'])."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['priority_rating_system'])."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['statement_of_limitations'])."',
                        '".mysqli_real_escape_string($db, $commonValue[0]['areas_not_accessed'])."'
                  );";
                  $query2 = $this->db->query($sql2);

                  return $commonValue;
            } else {
                 return $getFinal; 
            }
     }

     function get_reporttext_data ($siteID) {
      $sql = "SELECT * FROM tbl_report_common WHERE id = '".$siteID."'";
      $query = $this->db->query($sql);
      $getFinal = $query->result_array();
      $totalCount = count($getFinal);

      if ($totalCount == 0){
            $db = get_instance()->db->conn_id; 
            $sql2 = "SELECT * FROM tbl_report_common WHERE id = 0";
            $query2 = $this->db->query($sql2);
            $commonValue = $query2->result_array();
            
            $sql2 = "INSERT INTO tbl_report_common (
                  id,
                  scope_of_works,
                  recommendations,
                  methologies,
                  priority_rating_system,
                  statement_of_limitations,
                  areas_not_accessed
            ) VALUES (
                  '".mysqli_real_escape_string($db, $siteID)."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['scope_of_works'])."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['recommendations'])."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['methologies'])."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['priority_rating_system'])."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['statement_of_limitations'])."',
                  '".mysqli_real_escape_string($db, $commonValue[0]['areas_not_accessed'])."'
            );";
            $query2 = $this->db->query($sql2);

            $prebuilt = $commonValue;
      } else {
            $prebuilt = $getFinal; 
      }

      $finalValue = array();
      $finalValue['scope_of_works'] = $prebuilt[0]['scope_of_works'];
      $finalValue['recommendations'] = $prebuilt[0]['recommendations'];
      $finalValue['methologies'] = $prebuilt[0]['methologies'];
      $finalValue['priority_rating_system'] = $prebuilt[0]['priority_rating_system'];
      $finalValue['statement_of_limitations'] = $prebuilt[0]['statement_of_limitations'];
      $finalValue['areas_not_accessed'] = $prebuilt[0]['areas_not_accessed'];
      return $finalValue;

}

     function get_sitename ($siteID) {
      $sql = "SELECT * FROM tbl_sites WHERE siteID = '".$siteID."'";
      $query = $this->db->query($sql);
      return $query->result_array(); 
}
     
}?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the sitename & password from tbl_usrs
     function get_sites() {
          $sql = "SELECT * FROM tbl_sites ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_site_types() {
          $sql = "SELECT * FROM tbl_sitetypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_sites($searchData) {
          $sql = "SELECT * FROM tbl_sites WHERE hide = 'N' AND name LIKE '%".$searchData['name']."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function getControlPriority($rateing) {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND control_priority = '".$rateing."'";
          $query = $this->db->query($sql);
          return $query->num_rows();
     }

     function getControlPriorityCLIENT($rateing, $clientID) {
        $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND control_priority = '".$rateing."' AND clientID = '".$clientID."'";
        $query = $this->db->query($sql);
        return $query->num_rows();
   }

     function getControlPriorityData($rateing) {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND control_priority = '".$rateing."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function getControlPriorityAll() {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N'";
          $query = $this->db->query($sql);
          return $query->num_rows();
     }

     function getControlPriorityAllCLIENT($clientID) {
        $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND clientID = '".$clientID."'";
        $query = $this->db->query($sql);
        return $query->num_rows();
   }

     //get the sitename & password from tbl_usrs
     function get_items_list($siteID) {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$siteID."' ORDER BY buildingName ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_REPORTitems_list($siteID) {
      $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$siteID."' ORDER BY location_level ASC";
      $query = $this->db->query($sql);
      return $query->result_array();
 }

     //get the sitename & password from tbl_usrs
     function get_items_photolist($siteID) {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$siteID."' AND sample_status != 'Negative' ORDER BY photo_no DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_items_photolistTEST($siteID) {
      $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$siteID."' ORDER BY photo_no ASC";
      $query = $this->db->query($sql);
      return $query->result_array();
 }

 function get_items_photolistTESTnotblank($siteID) {
  $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$siteID."' AND photo_no != '' AND sample_status != 'Negative' ORDER BY photo_no ASC";
  $query = $this->db->query($sql);
  return $query->result_array();
}

     function get_items_photoinformation() {
      $sql = "SELECT * FROM tbl_item_images";
      $query = $this->db->query($sql);
      $array = $query->result_array();
      $finalArray = array();

      foreach ($array as $image) {
        $finalArray[$image['imageID']] = array('imageID' => $image['imageID'], 'itemID' => $image['itemID'], 'full_path' => $image['full_path']);
      }

      return $finalArray;
 }

     //get all items under ClientID
     function get_client_items_list($clientID) {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' AND clientID = '".$clientID."' ORDER BY date_updated DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the sitename & password from tbl_usrs
     function get_all_items() {
          $sql = "SELECT * FROM tbl_items WHERE hide = 'N' ORDER BY date_updated DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the sitename & password from tbl_usrs
     function get_hazards() {
          $sql = "SELECT * FROM tbl_hazard_types";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_materials() {
          $sql = "SELECT * FROM tbl_basevalues ORDER BY material ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_sites($filterletter) {
          $sql = "SELECT * FROM tbl_sites WHERE name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_archived_sites($filterletter) {
          $sql = "SELECT * FROM tbl_sites WHERE site_archive = 'Y' AND name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_photo_no() {
          $sql = "SELECT * FROM tbl_global WHERE id = '1'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_item_info($itemID) {
          $sql = "SELECT * FROM tbl_items WHERE itemID = '".$itemID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_image_info($itemID) {
          $sql = "SELECT full_path FROM tbl_item_images WHERE itemID = '".$itemID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get siteplan info of website
     function get_item_documentlist($itemID) {
      $sql2 = "DELETE FROM tbl_item_document WHERE full_path = ''";
      $query2 = $this->db->query($sql2);

          $sql = "SELECT * FROM tbl_item_document WHERE itemID = '".$itemID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_item_info($itemdata) {
          $data_upload_files = $this->upload->data();          
          $image = $data_upload_files['full_path']; 
          $sql = "UPDATE tbl_items SET 
          location_level = '".$itemdata['location_level']."',
          room_specific = '".$itemdata['room_specific']."',
          description = '".$itemdata['description']."',
          sample_no = '".$itemdata['sample_no']."',
          sample_status = '".$itemdata['sample_status']."',
          extent = '".$itemdata['extent']."',
          item_condition = '".$itemdata['item_condition']."',
          friability = '".$itemdata['friability']."',
          disturb_potential = '".$itemdata['disturb_potential']."',
          risk_rating = '".$itemdata['risk_rating']."',
          current_label = '".$itemdata['current_label']."',
          control_priority = '".$itemdata['control_priority']."',
          hazard_type = '".$itemdata['hazard_type']."',
          work_action = '".$itemdata['work_action']."',
          item_status = '".$itemdata['work_action']."',
          materialID = '".$itemdata['materialID']."',
          buildingName = '".$itemdata['buildingName']."',
          materialOther = '".$itemdata['materialOther']."',
          extent_mesurement = '".$itemdata['extent_mesurement']."',
          date_updated = NOW(),
          control_recommendation = '".$itemdata['control_recommendation']."'
          WHERE itemID = ".$itemdata['itemID']."";
          $query = $this->db->query($sql);
     }

     function upload_itemDocument($itemID, $file_name) {
          $sql = "INSERT INTO tbl_item_document (itemID, full_path) VALUES ('".$itemID."', '".$file_name."')";
          $query = $this->db->query($sql);
     }

     function upload_image($itemID, $fullPath) {
          $sql = "INSERT INTO tbl_item_images (itemID, full_path) VALUES ('".$itemID."', '".$fullPath."')";
          $query = $this->db->query($sql);
          return $this->db->insert_id();
     }

     function update_image($itemID, $fullPath) {
          $sql = "UPDATE tbl_item_images SET full_path = '".$fullPath."'
          WHERE itemID = ".$itemID."";
          $query = $this->db->query($sql);
     }

     function add_image_number($itemID, $imageID) {
      $sql = "UPDATE tbl_items SET photo_no = '".$imageID."'
      WHERE itemID = ".$itemID."";
      $query = $this->db->query($sql);
 }

     function update_photo_no($itemdata) {
          $sql = "UPDATE tbl_global SET value = '".$itemdata['photo_no']."' WHERE id = '1';";
          $query = $this->db->query($sql);

     }

     function new_item_info ($itemdata) {
          $data_upload_files = $this->upload->data();          
          $image = $data_upload_files['full_path']; 
          $sql = "INSERT INTO tbl_items (location_level,hazard_type,buildingName,room_specific,description,sample_no,sample_status,extent,item_condition,friability,disturb_potential,risk_rating,current_label,control_priority,control_recommendation,siteID,clientID,hide,work_action,item_status,date_updated,materialID,materialOther,extent_mesurement) 
          VALUES 
          ('".$itemdata['location_level']."','".$itemdata['hazard_type']."','".$itemdata['buildingName']."','".$itemdata['room_specific']."','".$itemdata['description']."','".$itemdata['sample_no']."','".$itemdata['sample_status']."','".$itemdata['extent']."','".$itemdata['item_condition']."','".$itemdata['friability']."','".$itemdata['disturb_potential']."','".$itemdata['risk_rating']."','".$itemdata['current_label']."','".$itemdata['control_priority']."','".$itemdata['control_recommendation']."','".$itemdata['siteID']."','".$itemdata['clientID']."','".$itemdata['hide']."','".$itemdata['work_action']."','".$itemdata['item_status']."',NOW(),'".$itemdata['materialID']."','".$itemdata['materialOther']."','".$itemdata['extent_mesurement']."')";
          $query = $this->db->query($sql);
          return $this->db->insert_id();
     }


     function delete_item_info($itemID) {
          $sql = "UPDATE tbl_items SET hide = 'Y' WHERE itemID = '".$itemID."'";
          $query = $this->db->query($sql);          
     }

     function getControlPriorityLocation() {
          $sql = "SELECT * FROM tbl_sites WHERE hide = 'N' ORDER BY site ASC";
          $query = $this->db->query($sql);
          $locations = $query->result_array();
          $combinedList = '';

          foreach ($locations as $location) {
               $sql1 = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P1'";
               $sql2 = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P2'";
               $sql3 = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P3'";
               $sql4 = "SELECT * FROM tbl_items WHERE hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P4'";
               $query1 = $this->db->query($sql1);
               $query2 = $this->db->query($sql2);
               $query3 = $this->db->query($sql3);
               $query4 = $this->db->query($sql4);
               $ext = $query1->num_rows();
               $hig = $query2->num_rows();
               $med = $query3->num_rows();
               $low = $query4->num_rows();

               $combinedList[] = array('name' => $location['site'],'locationID' => $location['siteID'],'extreme' => $ext,'high' => $hig,'medium' => $med,'low' => $low);
          }


          return $combinedList;
     }

     function getControlPriorityLocationCLIENT($clientID) {
        $sql = "SELECT * FROM tbl_sites WHERE hide = 'N' AND clientID = '".$clientID."' ORDER BY site ASC";
        $query = $this->db->query($sql);
        $locations = $query->result_array();
        $combinedList = '';

        foreach ($locations as $location) {
             $sql1 = "SELECT * FROM tbl_items WHERE  hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P1'";
             $sql2 = "SELECT * FROM tbl_items WHERE  hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P2'";
             $sql3 = "SELECT * FROM tbl_items WHERE  hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P3'";
             $sql4 = "SELECT * FROM tbl_items WHERE  hide = 'N' AND siteID = '".$location['siteID']."' AND control_priority = 'P4'";
             $query1 = $this->db->query($sql1);
             $query2 = $this->db->query($sql2);
             $query3 = $this->db->query($sql3);
             $query4 = $this->db->query($sql4);
             $ext = $query1->num_rows();
             $hig = $query2->num_rows();
             $med = $query3->num_rows();
             $low = $query4->num_rows();

             $combinedList[] = array('name' => $location['site'],'locationID' => $location['siteID'],'extreme' => $ext,'high' => $hig,'medium' => $med,'low' => $low);
        }


        return $combinedList;
   }

   function getCalculatedAmount($fullData){
    $allNumbers = array();
    
    foreach ($fullData as $varInfo) {
      $allNumbers[] = $varInfo['extreme']+$varInfo['high']+$varInfo['medium']+$varInfo['low'];
    }

    $maxNumber = max($allNumbers);
    $finalTotal = $maxNumber+5;

    return $finalTotal;

   }

   function getTotalLocations(){
      $sql1 = "SELECT * FROM tbl_sites WHERE hide = 'N'";
      $query1 = $this->db->query($sql1);
      $finalAmount = $query1->num_rows();

    return $finalAmount;

   }
}?>
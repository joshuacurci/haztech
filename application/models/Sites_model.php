<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sites_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the sitename & password from tbl_usrs
     function get_sites() {
          $sql = "SELECT * FROM tbl_sites WHERE hide = 'N' ORDER BY site";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_site_types() {
          $sql = "SELECT * FROM tbl_sitetypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_site_docs($siteID) {
        $sql = "SELECT * FROM tbl_site_document WHERE siteID = '".$siteID."' AND full_path != ''";
        $query = $this->db->query($sql);
        return $query->result_array();
   }

     function search_sites($searchData) {
          $sql = "SELECT * FROM tbl_sites WHERE WHERE hide = 'N' AND name LIKE '%".$searchData['name']."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function add_report_info($siteID) {
        $db = get_instance()->db->conn_id; 
        $sql = "SELECT * FROM tbl_report_common WHERE id = '0'";
        $query = $this->db->query($sql);
        $commonValue = $query->result_array();
        
        $sql2 = "INSERT INTO tbl_report_common (
            id,
            scope_of_works,
            recommendations,
            methologies,
            priority_rating_system,
            statement_of_limitations,
            areas_not_accessed
        ) VALUES (
            '".mysqli_real_escape_string($db, $siteID)."',
            '".mysqli_real_escape_string($db, $commonValue['scope_of_works'])."',
            '".mysqli_real_escape_string($db, $commonValue['recommendations'])."',
            '".mysqli_real_escape_string($db, $commonValue['methologies'])."',
            '".mysqli_real_escape_string($db, $commonValue['priority_rating_system'])."',
            '".mysqli_real_escape_string($db, $commonValue['statement_of_limitations'])."',
            '".mysqli_real_escape_string($db, $commonValue['areas_not_accessed'])."'
        );";
        $query2 = $this->db->query($sql2);
   }

   function updateReportPlan($siteID, $report_info) {
    $db = get_instance()->db->conn_id; 
    
    $sql = "UPDATE tbl_report_common SET 
          scope_of_works = '".mysqli_real_escape_string($db, $report_info[0]['scope_of_works'])."',
          recommendations = '".mysqli_real_escape_string($db, $report_info[0]['recommendations'])."',
          methologies = '".mysqli_real_escape_string($db, $report_info[0]['methologies'])."',
          priority_rating_system = '".mysqli_real_escape_string($db, $report_info[0]['priority_rating_system'])."',
          statement_of_limitations = '".mysqli_real_escape_string($db, $report_info[0]['statement_of_limitations'])."',
          areas_not_accessed = '".mysqli_real_escape_string($db, $report_info[0]['areas_not_accessed'])."'
          WHERE id = ".$siteID."";
    $query = $this->db->query($sql);
}

     //get the sitename & password from tbl_usrs
     function get_sites_list() {
          $sql = "SELECT * FROM tbl_sites WHERE hide = 'N' ORDER BY site ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_findings_summary() {
        $sql = "SELECT buildingName, hazard_type, friability, sample_status, COUNT(buildingName) AS total_items FROM tbl_items GROUP BY buildingName, hazard_type, friability, sample_status;";
        $query = $this->db->query($sql);
        return $query->result_array();
   }

     function filter_letter_sites($filterletter) {
          $sql = "SELECT * FROM tbl_sites WHERE name LIKE '".$filterletter."%' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     // get the site info
     function get_site_info($siteID) {
          $sql = "SELECT * FROM tbl_sites WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get site depending on client
     function get_client_site_list($clientID) {
          $sql = "SELECT * FROM tbl_sites WHERE clientID = '".$clientID."' AND hide = 'N' ORDER BY site ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get image info of website
     function get_image_info($siteID) {
          $sql = "SELECT full_path FROM tbl_site_images WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

          // get siteplan info of website
     function get_site_plan($siteID) {
          $sql = "SELECT full_path FROM tbl_site_plan WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

          // get siteplan info of website
     function get_site_document($siteID) {
        $sql2 = "DELETE FROM tbl_site_document WHERE full_path = ''";
        $query2 = $this->db->query($sql2);
        
        $sql = "SELECT full_path FROM tbl_site_document WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get siteplan info of website
     function get_site_documentlist($siteID) {
        $sql2 = "DELETE FROM tbl_site_document WHERE full_path = ''";
        $query2 = $this->db->query($sql2);

          $sql = "SELECT * FROM tbl_site_document WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get report info per individual site
     function get_report_info($siteID) {
          $sql = "SELECT * FROM tbl_report WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // get globa dat aof reports
     function get_reportcommon_info() {
          $sql = "SELECT * FROM tbl_report_common WHERE id = '0'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function new_site_info ($sitedata, $clientID) {
        $db = get_instance()->db->conn_id; 
        $numberBuildings = count($sitedata['building_name']);
        $i = 1;
        $buildingName = '';
        foreach ($sitedata['building_name'] as $building) {
            if ($i == $numberBuildings) {
                $buildingName .= $building;
            } else {
                $buildingName .= $building.'///';
            }
            $i++;
        }

          $sql = "INSERT INTO tbl_sites (
              site,
              property_number,
              full_address,
              city,
              state,
              post_code,
              survey_date,
              building_name,
              building_age,
              levels,
              inspected_by,
              site_contact,
              contact_title,
              construction_type,
              previous_report,
              company,
              area,
              roof_type,
              clientID, 
              report_name, 
              hide, 
              site_description, 
              site_use, 
              date_updated) 
          VALUES 
          ('".mysqli_real_escape_string($db, $sitedata['site'])."',
          '".mysqli_real_escape_string($db, $sitedata['property_number'])."',
          '".mysqli_real_escape_string($db, $sitedata['full_address'])."',
          '".mysqli_real_escape_string($db, $sitedata['city'])."',
          '".mysqli_real_escape_string($db, $sitedata['state'])."',
          '".mysqli_real_escape_string($db, $sitedata['post_code'])."',
          '".mysqli_real_escape_string($db, $sitedata['survey_date'])."',
          '".mysqli_real_escape_string($db, $buildingName)."',
          '".mysqli_real_escape_string($db, $sitedata['building_age'])."',
          '".mysqli_real_escape_string($db, $sitedata['levels'])."',
          '".mysqli_real_escape_string($db, $sitedata['inspected_by'])."',
          '".mysqli_real_escape_string($db, $sitedata['site_contact'])."',
          '".mysqli_real_escape_string($db, $sitedata['contact_title'])."',
          '".mysqli_real_escape_string($db, $sitedata['construction_type'])."',
          '".mysqli_real_escape_string($db, $sitedata['previous_report'])."',
          '".mysqli_real_escape_string($db, $sitedata['company'])."',
          '".mysqli_real_escape_string($db, $sitedata['area'])."',
          '".mysqli_real_escape_string($db, $sitedata['roof_type'])."',
          '".mysqli_real_escape_string($db, $clientID)."',
          '".mysqli_real_escape_string($db, $sitedata['report_name'])."',
          '".mysqli_real_escape_string($db, $sitedata['hide'])."',
          '".mysqli_real_escape_string($db, $sitedata['site_description'])."', 
          '".mysqli_real_escape_string($db, $sitedata['site_use'])."', 
          NOW())";
          $query = $this->db->query($sql);
          return $this->db->insert_id();
     }

     // Update site info
     function update_site_info ($sitedata) {
        $db = get_instance()->db->conn_id; 
        $numberBuildings = count($sitedata['building_name']);
        $i = 1;
        $buildingName = '';
        foreach ($sitedata['building_name'] as $building) {
            if ($i == $numberBuildings) {
                $buildingName .= $building;
            } else {
                $buildingName .= $building.'///';
            }
            $i++;
        }


          $data_upload_files = $this->upload->data();          
          $image = $data_upload_files['full_path']; 
          $sql = "UPDATE tbl_sites SET 
          site = '".mysqli_real_escape_string($db, $sitedata['site'])."',
          property_number = '".mysqli_real_escape_string($db, $sitedata['property_number'])."',
          full_address = '".mysqli_real_escape_string($db, $sitedata['full_address'])."',
          city = '".mysqli_real_escape_string($db, $sitedata['city'])."',
          state = '".mysqli_real_escape_string($db, $sitedata['state'])."',
          post_code = '".mysqli_real_escape_string($db, $sitedata['post_code'])."',
          survey_date = '".mysqli_real_escape_string($db, $sitedata['survey_date'])."',
          building_name = '".mysqli_real_escape_string($db, $buildingName)."',
          levels = '".mysqli_real_escape_string($db, $sitedata['levels'])."',
          inspected_by = '".mysqli_real_escape_string($db, $sitedata['inspected_by'])."',
          site_contact = '".mysqli_real_escape_string($db, $sitedata['site_contact'])."',
          contact_title = '".mysqli_real_escape_string($db, $sitedata['contact_title'])."',
          construction_type = '".mysqli_real_escape_string($db, $sitedata['construction_type'])."',
          previous_report = '".mysqli_real_escape_string($db, $sitedata['previous_report'])."',
          company = '".mysqli_real_escape_string($db, $sitedata['company'])."',
          clientID = '".mysqli_real_escape_string($db, $sitedata['clientID'])."',
          area = '".mysqli_real_escape_string($db, $sitedata['area'])."',
          date_updated = NOW(),
          report_name = '".mysqli_real_escape_string($db, $sitedata['report_name'])."',
          roof_type = '".mysqli_real_escape_string($db, $sitedata['roof_type'])."',
          building_age = '".mysqli_real_escape_string($db, $sitedata['building_age'])."',
          site_use = '".mysqli_real_escape_string($db, $sitedata['site_use'])."',
          site_description = '".mysqli_real_escape_string($db, $sitedata['site_description'])."'
          WHERE siteID = ".mysqli_real_escape_string($db, $sitedata['siteID'])."";
          $query = $this->db->query($sql);
     }

     // Update report per individual sites
     function update_report_info ($sitedata, $client_name, $full_address, $inspected_by, $siteID) {
          $introduction = 'This report presents the findings of an Hazardous Materials Risk Assessment conducted for '.$client_name.' of the site located at '.$full_address.'. The risk assessment was performed by '.$inspected_by.'. ';

          $sql = "UPDATE tbl_report SET 
          introduction = '".$introduction."',
          scope_of_works = 'Y',
          recommendations = 'Y',
          methologies = 'Y',
          risk_factors = 'Y',
          priority_rating_system = 'Y',
          asbestos_mng_req = 'Y',
          statement_of_limitations = 'Y'
          WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
     }
     // Update report per individual sites
     function update_reportwiz_info ($sitedata) {

          $sql = "UPDATE tbl_report SET 
          introduction = '".$sitedata['introduction']."',
          scope_of_works = '".$sitedata['scope_of_works']."',
          recommendations = '".$sitedata['recommendations']."',
          methologies = '".$sitedata['methologies']."',
          risk_factors = '".$sitedata['risk_factors']."',
          priority_rating_system = '".$sitedata['priority_rating_system']."',
          asbestos_mng_req = '".$sitedata['asbestos_mng_req']."',
          statement_of_limitations = '".$sitedata['statement_of_limitations']."',
          haz_material_mr = '".$sitedata['haz_material_mr']."'
          WHERE siteID = ".$sitedata['siteID']."";
          $query = $this->db->query($sql);
     }

          // Update scope of works on prefilled report
     function update_prefilled_info ($reportdata) {
          $db = get_instance()->db->conn_id;          
          $recommendations = mysqli_real_escape_string($db, $reportdata['recommendations']);
          $methologies = mysqli_real_escape_string($db, $reportdata['methologies']);
          $scope_of_works = mysqli_real_escape_string($db, $reportdata['scope_of_works']);
          $priority_rating_system = mysqli_real_escape_string($db, $reportdata['priority_rating_system']);
          $areas_not_accessed = mysqli_real_escape_string($db, $reportdata['areas_not_accessed']);
          $statement_of_limitations = mysqli_real_escape_string($db, $reportdata['statement_of_limitations']);

          $sql = "UPDATE tbl_report_common SET 
          scope_of_works = '".$scope_of_works."',
          recommendations = '".$recommendations."',
          methologies = '".$methologies."',
          priority_rating_system = '".$priority_rating_system."',
          statement_of_limitations = '".$statement_of_limitations."',
          areas_not_accessed = '".$areas_not_accessed."'      
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }

     // Update scope of works on prefilled report
     function update_scopeofworks_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          // $recommendations = mysqli_real_escape_string($db, $reportdata['recommendations']);
          // $methologies = mysqli_real_escape_string($db, $reportdata['methologies']);
          $scope_of_works = mysqli_real_escape_string($db, $reportdata['scope_of_works']);
          // $risk_factors = mysqli_real_escape_string($db, $reportdata['risk_factors']);
          // $priority_rating_system = mysqli_real_escape_string($db, $reportdata['priority_rating_system']);
          // $asbestos_mng_req = mysqli_real_escape_string($db, $reportdata['asbestos_mng_req']);
          // $haz_material_mr = mysqli_real_escape_string($db, $reportdata['haz_material_mr']);
          // $statement_of_limitations = mysqli_real_escape_string($db, $reportdata['statement_of_limitations']);

          // recommendations = '".$recommendations."',
          // scope_of_works = '".$scope_of_works."',
          // risk_factors = '".$risk_factors."',
          // priority_rating_system = '".$priority_rating_system."',
          // asbestos_mng_req = '".$asbestos_mng_req."',
          // haz_material_mr = '".$haz_material_mr."',
          // statement_of_limitations = '".$statement_of_limitations."'
           // methologies = '".$methologies."',

          $sql = "UPDATE tbl_report_common SET 
          scope_of_works = '".$scope_of_works."'          
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }

          // Update recommendations on prefilled report
     function update_recommendations_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $recommendations = mysqli_real_escape_string($db, $reportdata['recommendations']);

          $sql = "UPDATE tbl_report_common SET 
          recommendations = '".$recommendations."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }
               // Update methologies on prefilled report
     function update_methologies_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $methologies = mysqli_real_escape_string($db, $reportdata['methologies']);
          $sql = "UPDATE tbl_report_common SET 
          methologies = '".$methologies."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }
                    // Update riskfactors on prefilled report
     function update_risk_factors_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $risk_factors = mysqli_real_escape_string($db, $reportdata['risk_factors']);
          $sql = "UPDATE tbl_report_common SET 
          risk_factors = '".$risk_factors."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }
                         // Update riskfactors on prefilled report
     function update_psystem_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $priority_rating_system = mysqli_real_escape_string($db, $reportdata['priority_rating_system']);
          $sql = "UPDATE tbl_report_common SET 
          priority_rating_system = '".$priority_rating_system."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }
                              // Update riskfactors on prefilled report
     function update_amr_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $asbestos_mng_req = mysqli_real_escape_string($db, $reportdata['asbestos_mng_req']);
          $sql = "UPDATE tbl_report_common SET 
          asbestos_mng_req = '".$asbestos_mng_req."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }

     function update_hmr_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $asbestos_mng_req = mysqli_real_escape_string($db, $reportdata['asbestos_mng_req']);
          $sql = "UPDATE tbl_report_common SET 
          asbestos_mng_req = '".$asbestos_mng_req."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }

     function update_stateoflimitations_info ($reportdata) {
          $db = get_instance()->db->conn_id;
          $statement_of_limitations = mysqli_real_escape_string($db, $reportdata['statement_of_limitations']);
          $sql = "UPDATE tbl_report_common SET 
          statement_of_limitations = '".$statement_of_limitations."'
          WHERE id = '0'";
          $query = $this->db->query($sql);
     }

     function update_areasnotaccessed_info ($reportdata) {
        $db = get_instance()->db->conn_id;
        $areas_not_accessed = mysqli_real_escape_string($db, $reportdata['areas_not_accessed']);
        $sql = "UPDATE tbl_report_common SET 
        areas_not_accessed = '".$areas_not_accessed."'
        WHERE id = '0'";
        $query = $this->db->query($sql);
   }

     function upload_image($siteID, $fullPath) {
          $sql = "INSERT INTO tbl_site_images (siteID, full_path) VALUES ('".$siteID."', '".$fullPath."')";
          $query = $this->db->query($sql);
     }

     function upload_siteplan($siteID, $fullPath) {
          $sql = "INSERT INTO tbl_site_plan (siteID, full_path) VALUES ('".$siteID."', '".$fullPath."')";
          $query = $this->db->query($sql);
     }

     function upload_siteDocument($siteID, $file_name) {
         $file_name = str_replace(' ', '_', $file_name);
          $sql = "INSERT INTO tbl_site_document (siteID, full_path) VALUES ('".$siteID."', '".$file_name."')";
          $query = $this->db->query($sql);
     }

     function update_image($siteID, $fullPath) {
          $sql = "UPDATE tbl_site_images SET siteID = ".$siteID.", full_path = '".$fullPath."'
          WHERE siteID = ".$siteID."";
          $query = $this->db->query($sql);
     }

     function update_siteplan($siteID, $fullPath) {
          $sql = "UPDATE tbl_site_plan SET siteID = ".$siteID.", full_path = '".$fullPath."'
          WHERE siteID = ".$siteID."";
          $query = $this->db->query($sql);
     }
     

     function delete_site_info($siteID) {
          $sql = "UPDATE tbl_sites SET hide = 'Y' WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
     }

     function remove_site_file($docID) {
        $sql = " DELETE FROM tbl_site_document WHERE docID = '".$docID."'";
        $query = $this->db->query($sql);
   }

     function save_report_location($siteID,$pdfName) {
        $sql = "INSERT INTO tbl_report_saved (siteID, reportName, dateGenerated) VALUES ('".$siteID."', '".$pdfName."', '".time()."')";
        $query = $this->db->query($sql);
     }

     function get_sitedoc_removeblank($siteID) {
        $this->db->query("DELETE FROM tbl_site_document WHERE full_path = ''");

        $sql = "SELECT * FROM tbl_site_document WHERE siteID = '".$siteID."'";
        $query = $this->db->query($sql);
        return $query->result_array();
   }

   function test_ifprevious_reports($siteID) {

    $sql = "SELECT count(*) as total FROM tbl_report_saved WHERE siteID = '".$siteID."'";
    $query = $this->db->query($sql);
    $result = $query->result_array();

    if($result[0]['total'] == 0){
        return 'N';
    } else {
        return 'Y';
    }


}
}?>